﻿// <copyright file="BulkOperationAsyncTests.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.DotNet.Extensions;
using CleoOne.Common.Core.DotNet.Helpers.EFCore;
using CleoOne.Common.TestUtils.Traits;
using CleoOne.Common.TestUtils.Traits.Attributes;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace CleoOne.Common.Core.Tests.SqlBulkTests
{
    [Category("SqlBulkTests")]
    public class BulkOperationAsyncTests : IClassFixture<BulkOperationAsyncTests.DatabaseFixture>
    {
        #pragma warning disable SA1129 // Do not use default value type constructor
        private List<Item> GenerateItemsBulk(Guid bulkId = new Guid())
        {
            if (bulkId == Guid.Empty)
            {
                bulkId = Guid.NewGuid();
            }

            var r = new List<Item>();

            for (int i = 0; i < 10_000; i++)
            {
                r.Add(new Item()
                {
                    Id = 0,
                    Guid = Guid.NewGuid(),
                    Name = "Name" + i,
                    Price = 0,
                    ListOrder = i,
                    BulkIdentifier = bulkId,
                    DateTime = DateTime.UtcNow,
                    GuidProperty = Guid.NewGuid(),
                });
            }

            return r;
        }

        private List<AnotherItem> GenerateAnotherItemsBulk(Guid bulkId = new Guid())
        {
            if (bulkId == Guid.Empty)
            {
                bulkId = Guid.NewGuid();
            }

            var r = new List<AnotherItem>();

            for (int i = 0; i < 10_000; i++)
            {
                r.Add(new AnotherItem()
                {
                    Guid = Guid.NewGuid(),
                    BulkIdentifier = bulkId,
                    Description = "Default",
                    FirstName = "Name" + 1,
                    IdColumn = 0,
                    PrimaryKey = Guid.NewGuid().ToString(),
                });
            }

            return r;
        }

        private List<NoIdItem> GenerateNoIdsItems(Guid bulkId = new Guid())
            #pragma warning restore SA1129 // Do not use default value type constructor
        {
            if (bulkId == Guid.Empty)
            {
                bulkId = Guid.NewGuid();
            }

            var r = new List<NoIdItem>();

            for (int i = 0; i < 10_000; i++)
            {
                r.Add(new NoIdItem()
                {
                    Guid = Guid.NewGuid(),
                    BulkIdentifier = bulkId,
                    Description = "NoId",
                    FirstName = "Name" + 1,
                    PrimaryKey = Guid.NewGuid().ToString(),
                });
            }

            return r;
        }

        private int GetItemsCount()
        {
            using (var db = GetTestContext())
            {
                return db.Items.Count();
            }
        }

        private static TestContext GetTestContext()
        {
            return new TestContext("BulkTestsAsync");
        }

        private Task TimeoutForItemsTable(TimeSpan blockForTime)
        {
            return Task.Run(() =>
            {
                using (var db = GetTestContext())
                {
                    db.Database.SetCommandTimeout((int?)(blockForTime.TotalSeconds + 10));
                    var blockFor = blockForTime.ToString("hh\\:mm\\:ss");
                    #pragma warning disable EF1000 // Possible SQL injection vulnerability.
                    db.Database.ExecuteSqlRaw("BEGIN TRANSACTION "
                                              + "SELECT * FROM Items WITH (TABLOCKX, HOLDLOCK) WHERE 0 = 1 "
                                              + $"WAITFOR DELAY '{blockFor}' "
                                              + "ROLLBACK TRANSACTION ");
                    #pragma warning restore EF1000 // Possible SQL injection vulnerability.
                }
            });
        }

        [IgnoreInCi]
        [Fact]
        internal async Task BulkInsert_DoesNotSetIds()
        {
            var items = GenerateItemsBulk();
            int countBefore = GetItemsCount();

            using (var db = GetTestContext())
            {
                await db.Items.BulkInsertAsync(items);
            }

            var countAfter = GetItemsCount();
            Assert.Equal(countBefore + items.Count, countAfter);

            foreach (var item in items)
            {
                Assert.True(item.Id == 0);
            }
        }

        [IgnoreInCi]
        [Fact]
        internal async Task BulkInsert_SetOutputIdentity_SetsIds()
        {
            var items = GenerateItemsBulk();
            var copyItems = items.ToList();
            var buklId = items.First().BulkIdentifier;
            int countBefore;

            using (var db = GetTestContext())
            {
                countBefore = db.Items.Count();
                await db.Items.BulkInsertAsync(items, setOutputIdentity: true);
            }

            using (var db = GetTestContext())
            {
                var countAfter = db.Items.Count();
                Assert.Equal(countBefore + items.Count, countAfter);
            }

            for (var i = 0; i < items.Count; i++)
            {
                var item = items[i];
                Assert.Same(items[i], copyItems[i]); // this verifies that the things in list are not overwritten
                Assert.True(item.Id != 0);
            }

            using (var db = GetTestContext())
            {
                var thisBulk = db.Items.Where(x => x.BulkIdentifier == buklId).OrderBy(x => x.Id).ToList();
                Assert.True(thisBulk.SequencePredicateEqual(items.OrderBy(x => x.Id), (x, y) => x.Guid == y.Guid));
            }
        }

        [IgnoreInCi]
        [Fact]
        internal async Task BulkInsert_DifferentIdAndPk_SetOutputIdentity_Throws()
        {
            var items = GenerateAnotherItemsBulk();

            int countBefore = -1;

            await Assert.ThrowsAsync<NotImplementedException>(async () =>
            {
                using (var db = GetTestContext())
                {
                    countBefore = db.AnotherItems.Count();
                    await db.AnotherItems.BulkInsertAsync(items, setOutputIdentity: true);
                }
            });

            using (var db = GetTestContext())
            {
                var countAfter = db.AnotherItems.Count();
                Assert.Equal(countBefore, countAfter);
            }
        }

        [IgnoreInCi]
        [Fact]
        internal async Task BulkInsert_NoIdTable_SetOutputIdentity_Throws()
        {
            var items = GenerateNoIdsItems();
            int countBefore = -1;

            await Assert.ThrowsAsync<InvalidOperationException>(async () =>
            {
                using (var db = GetTestContext())
                {
                    countBefore = db.NoIdItems.Count();
                    await db.NoIdItems.BulkInsertAsync(items, setOutputIdentity: true);
                }
            });

            using (var db = GetTestContext())
            {
                var countAfter = db.NoIdItems.Count();
                Assert.Equal(countBefore, countAfter);
            }
        }

        [IgnoreInCi]
        [Fact]
        internal async Task BulkInsert_ConstraintBroken_Throws()
        {
            var items = GenerateNoIdsItems();
            int countBefore = -1;
            items.ForEach(x => x.Guid = items.First().Guid);

            await Assert.ThrowsAnyAsync<SqlException>(async () =>
            {
                using (var db = GetTestContext())
                {
                    countBefore = db.NoIdItems.Count();
                    await db.NoIdItems.BulkInsertAsync(items);
                }
            });

            using (var db = GetTestContext())
            {
                var countAfter = db.NoIdItems.Count();
                Assert.Equal(countBefore, countAfter);
            }
        }

        [IgnoreInCi]
        [Fact]
        internal async Task BulkInsert_NoIdTable_Inserts()
        {
            var items = GenerateNoIdsItems();
            var copyItems = items.ToList();
            var bulkId = items.First().BulkIdentifier;

            int countBefore = -1;

            using (var db = GetTestContext())
            {
                countBefore = db.NoIdItems.Count();
                await db.NoIdItems.BulkInsertAsync(items);
            }

            using (var db = GetTestContext())
            {
                var countAfter = db.NoIdItems.Count();
                Assert.Equal(countBefore + items.Count, countAfter);
            }

            for (var i = 0; i < items.Count; i++)
            {
                Assert.Same(items[i], copyItems[i]); // this verifies that the things in list are not overwritten
            }

            using (var db = GetTestContext())
            {
                var thisBulk = db.NoIdItems.Where(x => x.BulkIdentifier == bulkId).ToList();
                var thisBulkOrdered = thisBulk.OrderBy(x => x.Guid).ToList();
                var itemsOrdered = items.OrderBy(x => x.Guid).ToList();
                Assert.True(thisBulkOrdered.SequencePredicateEqual(itemsOrdered, (x, y) => x.Guid == y.Guid));
            }
        }

        [IgnoreInCi]
        [Fact]
        internal async Task BulkInsert_SetOutputIdentityNonZeroIds_Throws()
        {
            var items = GenerateItemsBulk();
            int countBefore = GetItemsCount();

            var id = 1;

            foreach (var item in items)
            {
                item.Id = id++;
            }

            await Assert.ThrowsAsync<InvalidOperationException>(async () =>
            {
                using (var db = GetTestContext())
                {
                    await db.Items.BulkInsertAsync(items, setOutputIdentity: true);
                }
            });

            var countAfter = GetItemsCount();
            Assert.Equal(countBefore, countAfter);
        }

        [IgnoreInCi]
        [Fact]
        internal async Task BulkInsert_SetOutputIdentity_Timeout_RollbacksIds()
        {
            var items = GenerateItemsBulk();
            int countBefore = GetItemsCount();

            var sqlTimeOutTask =
                TimeoutForItemsTable(TimeSpan.FromSeconds(SqlHelper.DefaultMaxSqlRetryCount * SqlHelper.DefaultSqlRetryDelay.TotalSeconds));

            var sqlException = await Assert.ThrowsAnyAsync<RetryLimitExceededException>(async () =>
            {
                using (var db = GetTestContext())
                {
                    db.Database.SetCommandTimeout(1);
                    await db.Items.BulkInsertAsync(items, setOutputIdentity: true);
                }
            });

            Assert.Contains("Execution Timeout Expired", sqlException.InnerException.Message);

            sqlTimeOutTask.Wait();

            var countAfter = GetItemsCount();

            Assert.Equal(countBefore, countAfter);

            foreach (var item in items)
            {
                Assert.True(item.Id == 0);
            }
        }

        [IgnoreInCi]
        [Fact]
        internal async Task BulkInsert_UserTransactionNotCommitted_NothingInserted()
        {
            var countBefore = GetItemsCount();

            using (var db = GetTestContext())
            {
                using (var t = db.Database.BeginTransaction())
                {
                    var items = GenerateItemsBulk();
                    await db.Items.BulkInsertAsync(items);
                }
            }

            var countAfter = GetItemsCount();
            Assert.Equal(countBefore, countAfter);
        }

        [IgnoreInCi]
        [Fact]
        internal async Task BulkInsert_SetOutputIdentity_UserTransactionIdsNotSet_ThrowsAndDoesNotModifyIds()
        {
            var countBefore = GetItemsCount();
            var items = GenerateItemsBulk();

            var exception = await Assert.ThrowsAsync<InvalidOperationException>(async () =>
            {
                using (var db = GetTestContext())
                {
                    var executionStrategy = db.Database.CreateExecutionStrategy();

                    await executionStrategy.ExecuteAsync(async () =>
                    {
                        using (var t = db.Database.BeginTransaction())
                        {
                            await db.Items.BulkInsertAsync(items, setOutputIdentity: true);
                            t.Commit();
                        }
                    });
                }
            });

            Assert.Contains(
                "For user initiated transaction for Insert-SetIdentity: All Ids expected negative && distinct. User is responsible for retries and rollback of the ids.",
                exception.Message);

            var countAfter = GetItemsCount();
            Assert.Equal(countBefore, countAfter);

            items.ForEach(x => Assert.True(x.Id == 0));
        }

        [IgnoreInCi]
        [Fact]
        internal async Task BulkInsert_SetOutputIdentity_UserTransactionNotCommitted_IdsSetButItemsNotInserted()
        {
            var countBefore = GetItemsCount();
            var items = GenerateItemsBulk();
            var id = int.MinValue;

            foreach (var item in items)
            {
                item.Id = id++;
            }

            using (var db = GetTestContext())
            {
                var executionStrategy = db.Database.CreateExecutionStrategy();

                await executionStrategy.ExecuteAsync(async () =>
                {
                    using (var t = db.Database.BeginTransaction())
                    {
                        await db.Items.BulkInsertAsync(items, setOutputIdentity: true);
                    }
                });
            }

            var countAfter = GetItemsCount();
            Assert.Equal(countBefore, countAfter);
            Assert.True(items.All(x => x.Id > 0));
            Assert.Equal(items.DistinctBy(x => x.Id).Count(), items.Count);
        }

        [IgnoreInCi]
        [Fact]
        internal async Task BulkInsert_SetOutputIdentity_UserTransaction_ModifiesIds()
        {
            var countBefore = GetItemsCount();
            var itemsBulkGuid = Guid.NewGuid();
            var items = GenerateItemsBulk(itemsBulkGuid);

            var id = int.MinValue;

            foreach (var item in items)
            {
                item.Id = id++;
            }

            using (var db = GetTestContext())
            {
                var executionStrategy = db.Database.CreateExecutionStrategy();

                await executionStrategy.ExecuteAsync(async () =>
                {
                    using (var t = db.Database.BeginTransaction())
                    {
                        await db.Items.BulkInsertAsync(items, setOutputIdentity: true);
                        t.Commit();
                    }
                });
            }

            var countAfter = GetItemsCount();

            Assert.Equal(countBefore + items.Count, countAfter);

            using (var db = GetTestContext())
            {
                var inserted = db.Items.Where(x => x.BulkIdentifier == itemsBulkGuid).OrderBy(x => x.Id).ToList();
                Assert.Equal(items.Count, inserted.Count);

                for (int i = 0; i < inserted.Count; i++)
                {
                    Assert.True(items[i].Guid == inserted[i].Guid);
                }
            }
        }

        [IgnoreInCi]
        [Fact]
        internal async Task BulkInsert_UserTransaction_InsertsAndDoesNotModifyIds()
        {
            var countBefore = GetItemsCount();
            var items = GenerateItemsBulk();

            using (var db = GetTestContext())
            {
                using (var t = db.Database.BeginTransaction())
                {
                    await db.Items.BulkInsertAsync(items);
                    t.Commit();
                }
            }

            var countAfter = GetItemsCount();
            Assert.Equal(countBefore + items.Count, countAfter);

            items.ForEach(x => Assert.True(x.Id == 0));
        }

        [IgnoreInCi]
        [Fact]
        internal async Task BulkInsert_UserTransaction_FailedAfterInsert_DoesNotRollbackIds()
        {
            var countBefore = GetItemsCount();
            var items = GenerateItemsBulk();
            var id = int.MinValue;

            foreach (var item in items)
            {
                item.Id = id++;
            }

            var thr = true;

            var exception = await Assert.ThrowsAsync<Exception>(async () =>
            {
                using (var db = GetTestContext())
                {
                    var executionStrategy = db.Database.CreateExecutionStrategy();

                    await executionStrategy.ExecuteAsync(async () =>
                    {
                        using (var t = db.Database.BeginTransaction())
                        {
                            await db.Items.BulkInsertAsync(items, setOutputIdentity: true);

                            // This throw is here to simulate fail of user transaction after bulk insert (we could have timeout here, but it would make )
                            // The if is here because without if, there would be unreachable code warning
                            if (thr)
                            {
                                throw new Exception("Rest of transaction failed.");
                            }

                            t.Commit();
                        }
                    });
                }
            });

            Assert.Contains("Rest of transaction failed.", exception.Message);

            var countAfter = GetItemsCount();
            Assert.Equal(countBefore, countAfter);                          // nothing inserted
            Assert.True(items.All(x => x.Id > 0));                          // ids were set
            Assert.Equal(items.DistinctBy(x => x.Id).Count(), items.Count); // ids are distinct
        }

        [IgnoreInCi]
        [Fact]
        internal async Task BulkInsert_UserTransaction_NoRetries_Throws()
        {
            var itemsBulkGuid = Guid.NewGuid();
            var insertedItems = GenerateItemsBulk(itemsBulkGuid);

            var timeout = TimeoutForItemsTable(TimeSpan.FromSeconds(3));

            var sqlException = await Assert.ThrowsAnyAsync<SqlException>(async () =>
            {
                using (var db = GetTestContext())
                using (var tr = db.Database.BeginTransaction())
                {
                    db.Database.SetCommandTimeout(1);
                    await db.Items.BulkInsertAsync(insertedItems);
                    tr.Commit();
                }
            });

            Assert.Contains("Execution Timeout Expired", sqlException.Message);

            timeout.Wait();

            using (var db = GetTestContext())
            {
                var dbItems = db.Items.Where(x => x.BulkIdentifier == itemsBulkGuid).ToList();
                Assert.Empty(dbItems);
            }
        }

        [IgnoreInCi]
        [Fact]
        internal async Task BulkInsert_Timeout_Retries()
        {
            var itemsBulkGuid = Guid.NewGuid();
            var insertedItems = GenerateItemsBulk(itemsBulkGuid);

            var timeout = TimeoutForItemsTable(TimeSpan.FromSeconds(3));

            using (var db = GetTestContext())
            {
                db.Database.SetCommandTimeout(1);
                await db.Items.BulkInsertAsync(insertedItems, setOutputIdentity: true);
            }

            timeout.Wait();

            using (var db = GetTestContext())
            {
                var dbItems = db.Items.Where(x => x.BulkIdentifier == itemsBulkGuid).ToList();

                Assert.True(dbItems.OrderBy(x => x.Id)
                                   .SequencePredicateEqual(insertedItems.OrderBy(x => x.Id), (x, y) => x.Guid == y.Guid));
            }
        }

        [IgnoreInCi]
        [Fact]
        internal async Task BulkUpdate_ByPKs()
        {
            var itemsBulkGuid = Guid.NewGuid();
            var insertedItems = GenerateItemsBulk(itemsBulkGuid);

            using (var db = GetTestContext())
            {
                await db.Items.BulkInsertAsync(insertedItems, setOutputIdentity: true);
            }

            var (toUpdate, notToUpdate) = insertedItems.SplitOn(x => x.ListOrder % 3 != 0);

            foreach (var upItem in toUpdate)
            {
                upItem.Price = 999;
            }

            using (var db = GetTestContext())
            {
                await db.Items.BulkInsertOrUpdateByPrimaryKeysAsync(toUpdate);
            }

            using (var db = GetTestContext())
            {
                var bulk = db.Items.Where(x => x.BulkIdentifier == itemsBulkGuid).ToList();

                var (updated, notUpdated) = bulk.SplitOn(x => x.Price == 999);

                Assert.True(toUpdate.OrderBy(x => x.Id)
                                    .SequencePredicateEqual(updated.OrderBy(x => x.Id), (x, y) => x.Guid == y.Guid));

                Assert.True(notToUpdate.OrderBy(x => x.Id)
                                       .SequencePredicateEqual(notUpdated.OrderBy(x => x.Id), (x, y) => x.Guid == y.Guid));
            }
        }

        [IgnoreInCi]
        [Fact]
        internal async Task BulkUpdate_ByColumns_UpdateByNotDistinct_Throws()
        {
            var itemsBulkGuid = Guid.NewGuid();
            var insertedItems = GenerateItemsBulk(itemsBulkGuid);

            using (var db = GetTestContext())
            {
                await db.Items.BulkInsertAsync(insertedItems);
            }

            foreach (var upItem in insertedItems)
            {
                upItem.Name = itemsBulkGuid.ToString();
                upItem.Price = -663;
            }

            var columnsBy = new[] { nameof(Item.BulkIdentifier) };

            await Assert.ThrowsAsync<InvalidOperationException>(async () =>
            {
                using (var db = GetTestContext())
                {
                    await db.Items.BulkUpdateByAsync(insertedItems, columnsBy);
                }
            });
        }

        [IgnoreInCi]
        [Fact]
        internal async Task BulkUpdate_ByColumns_DoesNotUpdateIdentity()
        {
            var itemsBulkGuid = Guid.NewGuid();
            var insertedItems = GenerateItemsBulk(itemsBulkGuid);
            var maxId = -1;

            using (var db = GetTestContext())
            {
                await db.Items.BulkInsertAsync(insertedItems);
                maxId = db.Items.Max(x => x.Id);
            }

            var tempId = maxId;

            foreach (var upItem in insertedItems)
            {
                upItem.Name = itemsBulkGuid.ToString();
                upItem.Id = tempId++;
                upItem.Price = 771;
            }

            var columnsBy = new[] { nameof(Item.BulkIdentifier), nameof(Item.Guid) };

            using (var db = GetTestContext())
            {
                await db.Items.BulkUpdateByAsync(insertedItems, columnsBy);
            }

            using (var db = GetTestContext())
            {
                var bulk = db.Items.Where(x => x.BulkIdentifier == itemsBulkGuid).ToList();
                var currentMaxId = db.Items.Max(x => x.Id);
                Assert.Equal(maxId, currentMaxId);
                Assert.True(bulk.All(x => x.Price == 771)); // things got updated
                Assert.True(bulk.All(x => x.Id <= maxId));  // ids was not changed
            }
        }

        [IgnoreInCi]
        [Fact]
        internal async Task BulkUpdate_ByColumns_UpdatingPKs_Throws()
        {
            var itemsBulkGuid = Guid.NewGuid();
            var insertedItems = GenerateAnotherItemsBulk(itemsBulkGuid);

            using (var db = GetTestContext())
            {
                await db.AnotherItems.BulkInsertAsync(insertedItems);
            }

            foreach (var upItem in insertedItems)
            {
                upItem.FirstName = "Haha";
                upItem.PrimaryKey += "x";
            }

            var columnsBy = new[] { nameof(AnotherItem.BulkIdentifier), nameof(AnotherItem.Guid) };

            await Assert.ThrowsAsync<InvalidOperationException>(async () =>
            {
                using (var db = GetTestContext())
                {
                    await db.AnotherItems.BulkUpdateByAsync(insertedItems, columnsBy);
                }
            });

            using (var db = GetTestContext())
            {
                var bulk = db.AnotherItems.Where(x => x.BulkIdentifier == itemsBulkGuid).ToList();
                Assert.True(bulk.All(x => x.FirstName != "Haha" && !x.PrimaryKey.StartsWith("x")));
            }
        }

        [IgnoreInCi]
        [Fact]
        internal async Task BulkUpdate_ByColumns_IdDifferentFromPK_CanUpdate()
        {
            var itemsBulkGuid = Guid.NewGuid();
            var insertedItems = GenerateAnotherItemsBulk(itemsBulkGuid);

            using (var db = GetTestContext())
            {
                await db.AnotherItems.BulkInsertAsync(insertedItems);
            }

            foreach (var upItem in insertedItems)
            {
                upItem.FirstName = "Updated!";
                upItem.PrimaryKey += "zzz";
            }

            var columnsBy = new[] { nameof(AnotherItem.BulkIdentifier), nameof(AnotherItem.Guid) };

            using (var db = GetTestContext())
            {
                await db.AnotherItems.BulkUpdateByAsync(insertedItems,
                                                        columnsBy,
                                                        columnsToExclude: new[] { nameof(AnotherItem.PrimaryKey) });
            }

            using (var db = GetTestContext())
            {
                var bulk = db.AnotherItems.Where(x => x.BulkIdentifier == itemsBulkGuid).ToList();
                Assert.True(bulk.All(x => x.FirstName == "Updated!" && !x.PrimaryKey.StartsWith("zzz")));
            }
        }

        [IgnoreInCi]
        [Fact]
        internal async Task BulkUpdate_ByColumns_ColumnsCanBeExcluded()
        {
            var itemsBulkGuid = Guid.NewGuid();
            var insertedItems = GenerateAnotherItemsBulk(itemsBulkGuid);

            using (var db = GetTestContext())
            {
                await db.AnotherItems.BulkInsertAsync(insertedItems);
            }

            var fn = "WillNotBeUpdated";
            var desc = "WillBE";

            foreach (var upItem in insertedItems)
            {
                upItem.FirstName = fn;
                upItem.Description = desc;
            }

            var columnsBy = new[] { nameof(AnotherItem.BulkIdentifier), nameof(AnotherItem.Guid) };

            using (var db = GetTestContext())
            {
                await db.AnotherItems.BulkUpdateByAsync(insertedItems,
                                                        columnsBy,
                                                        columnsToExclude: new[]
                                                            { nameof(AnotherItem.PrimaryKey), nameof(AnotherItem.FirstName) });
            }

            using (var db = GetTestContext())
            {
                var bulk = db.AnotherItems.Where(x => x.BulkIdentifier == itemsBulkGuid).ToList();
                Assert.True(bulk.All(x => x.FirstName != fn && x.Description == desc));
            }
        }

        [IgnoreInCi]
        [Fact]
        internal async Task BulkUpdate_UpdateBy_CannotUpdateByIdOrPrimaryKey()
        {
            var itemsBulkGuid = Guid.NewGuid();
            var insertedItems = GenerateAnotherItemsBulk(itemsBulkGuid);

            using (var db = GetTestContext())
            {
                await db.AnotherItems.BulkInsertAsync(insertedItems);
            }

            var fn = "WillNotBeUpdated";
            var desc = "WillBE";

            foreach (var upItem in insertedItems)
            {
                upItem.FirstName = fn;
                upItem.Description = desc;
            }

            await Assert.ThrowsAsync<InvalidOperationException>(async () =>
            {
                using (var db = GetTestContext())
                {
                    await db.AnotherItems.BulkUpdateByAsync(insertedItems, new[] { nameof(AnotherItem.PrimaryKey) });
                }
            });

            await Assert.ThrowsAsync<InvalidOperationException>(async () =>
            {
                using (var db = GetTestContext())
                {
                    await db.AnotherItems.BulkUpdateByAsync(insertedItems, new[] { nameof(AnotherItem.Guid) });
                }
            });

            // check that the update did not go trough
            using (var db = GetTestContext())
            {
                var bulk = db.AnotherItems.Where(x => x.BulkIdentifier == itemsBulkGuid).ToList();
                Assert.True(bulk.All(x => x.FirstName != fn && x.Description != desc));
            }
        }

        [IgnoreInCi]
        [Fact]
        internal async Task BulkUpdate_ByPKsAndPKsNotSet_Throws()
        {
            var itemsBulkGuid = Guid.NewGuid();
            var toUpdate = GenerateItemsBulk(itemsBulkGuid);

            foreach (var upItem in toUpdate)
            {
                upItem.Name = itemsBulkGuid.ToString();
            }

            await Assert.ThrowsAsync<InvalidOperationException>(async () =>
            {
                using (var db = GetTestContext())
                {
                    await db.Items.BulkUpdateByPrimaryKeysAsync(toUpdate);
                }
            });

            using (var db = GetTestContext())
            {
                var countAfter = db.Items.Count(x => x.Name == itemsBulkGuid.ToString());
                Assert.Equal(0, countAfter);
            }
        }

        [IgnoreInCi]
        [Fact]
        internal async Task BulkUpdate_ColumnsCanBeIncluded()
        {
            var itemsBulkGuid = Guid.NewGuid();
            var insertedItems = GenerateItemsBulk(itemsBulkGuid);

            using (var db = GetTestContext())
            {
                await db.Items.BulkInsertAsync(insertedItems);
            }

            var willBeUpdated = "sfsdfgsdaf";
            var willNotBeUpdate = "asddsdsf";

            foreach (var upItem in insertedItems)
            {
                upItem.Name = willBeUpdated;
                upItem.Description = willNotBeUpdate;
            }

            var columnsBy = new[] { nameof(Item.BulkIdentifier), nameof(Item.Guid) };

            using (var db = GetTestContext())
            {
                await db.Items.BulkUpdateByAsync(insertedItems, columnsBy, columnsToInclude: new[] { nameof(Item.Name) });
            }

            using (var db = GetTestContext())
            {
                var bulk = db.Items.Where(x => x.BulkIdentifier == itemsBulkGuid).ToList();
                Assert.True(bulk.All(x => x.Name == willBeUpdated && x.Description != willNotBeUpdate)); // things got updated correctly
            }
        }

        [IgnoreInCi]
        [Fact]
        internal async Task BatchUpdate_UpdateCorrectColumns()
        {
            var itemsBulkGuid = Guid.NewGuid();
            var insertedItems = GenerateItemsBulk(itemsBulkGuid);

            using (var db = GetTestContext())
            {
                await db.Items.BulkInsertAsync(insertedItems);
            }

            using (var db = GetTestContext())
            {
                var updated = await db.Items.Where(x => x.BulkIdentifier == itemsBulkGuid)
                                      .BatchUpdateAsync(x => new Item() { Description = x.Name + "z" });

                Assert.Equal(insertedItems.Count, updated);
            }

            using (var db = GetTestContext())
            {
                var bulk = db.Items.Where(x => x.BulkIdentifier == itemsBulkGuid).ToList();
                Assert.True(bulk.All(x => x.Description == x.Name + "z")); // things got updated correctly
            }
        }

        [IgnoreInCi]
        [Fact]
        internal async Task BatchDelete()
        {
            var itemsBulkGuid = Guid.NewGuid();
            var insertedItems = GenerateItemsBulk(itemsBulkGuid);

            using (var db = GetTestContext())
            {
                await db.Items.BulkInsertAsync(insertedItems);
            }

            using (var db = GetTestContext())
            {
                var deleted = await db.Items.Where(x => x.BulkIdentifier == itemsBulkGuid && x.ListOrder % 3 == 0).BatchDeleteAsync();
                Assert.Equal(insertedItems.Count(x => x.ListOrder % 3 == 0), deleted);
            }

            using (var db = GetTestContext())
            {
                var bulk = db.Items.Where(x => x.BulkIdentifier == itemsBulkGuid).ToList();
                Assert.True(bulk.All(x => x.ListOrder % 3 != 0)); // things got deleted correctly
            }
        }

        [IgnoreInCi]
        [Fact]
        internal async Task BulkInsert_IdentitySet_Throws()
        {
            var cntBefore = GetItemsCount();

            var items = GenerateItemsBulk();
            var itemsAnother = GenerateAnotherItemsBulk();

            var idTemp = 0;

            items.ForEach(x => x.Id = ++idTemp);
            itemsAnother.ForEach(x => x.IdColumn = ++idTemp);

            await Assert.ThrowsAsync<InvalidOperationException>(async () =>
            {
                using (var db = GetTestContext())
                {
                    await db.Items.BulkInsertAsync(items);
                }
            });

            await Assert.ThrowsAsync<InvalidOperationException>(async () =>
            {
                using (var db = GetTestContext())
                {
                    await db.Items.BulkInsertAsync(items, setOutputIdentity: true);
                }
            });

            await Assert.ThrowsAsync<InvalidOperationException>(async () =>
            {
                using (var db = GetTestContext())
                {
                    await db.AnotherItems.BulkInsertAsync(itemsAnother);
                }
            });

            await Assert.ThrowsAsync<NotImplementedException>(async () =>
            {
                using (var db = GetTestContext())
                {
                    await db.AnotherItems.BulkInsertAsync(itemsAnother, setOutputIdentity: true);
                }
            });

            await Assert.ThrowsAsync<InvalidOperationException>(async () =>
            {
                var items2 = GenerateNoIdsItems();

                using (var db = GetTestContext())
                {
                    await db.NoIdItems.BulkInsertAsync(items2, setOutputIdentity: true);
                }
            });

            var cntAfter = GetItemsCount();
            Assert.Equal(cntBefore, cntAfter);
        }

        [IgnoreInCi]
        [Fact]
        internal async Task BulkInsertOrUpdateByPrimaryKey_IdDifferentFromPK_IdsSet_Throws()
        {
            var cntBefore = GetItemsCount();
            var itemsAnother = GenerateAnotherItemsBulk();
            var idTemp = 0;
            itemsAnother.ForEach(x => x.IdColumn = ++idTemp);

            // this for table with Id != PK, as Ids are set by db, you should not try to InsertOrUpdate with Ids set (there is check as prevention against error - do no insert already tracked entities)
            await Assert.ThrowsAsync<InvalidOperationException>(async () =>
            {
                using (var db = GetTestContext())
                {
                    await db.AnotherItems.BulkInsertOrUpdateByPrimaryKeysAsync(itemsAnother);
                }
            });

            var cntAfter = GetItemsCount();
            Assert.Equal(cntBefore, cntAfter);
        }

        [IgnoreInCi]
        [Fact]
        internal async Task BulkInsertOrUpdateByPrimaryKeys()
        {
            // Arrange
            var insertItemsBulk = Guid.NewGuid();
            var insertedItems = GenerateItemsBulk(insertItemsBulk);
            insertedItems.ForEach(x => x.Price = -511);

            var updateBulkId = Guid.NewGuid();
            var itemsToUpdate = GenerateItemsBulk(updateBulkId);

            using (var db = GetTestContext())
            {
                await db.Items.BulkInsertAsync(itemsToUpdate, setOutputIdentity: true);
            }

            var updatedDes = Guid.NewGuid().ToString();
            itemsToUpdate.ForEach(x => x.Description = updatedDes);
            var cntBefore = GetItemsCount();

            // Act
            using (var db = GetTestContext())
            {
                await db.Items.BulkInsertOrUpdateByPrimaryKeysAsync(insertedItems.Concat(itemsToUpdate).OrderBy(x => x.Guid).ToList());
            }

            // Assert
            using (var db = GetTestContext())
            {
                var dbItems = db.Items.Where(x => x.BulkIdentifier == insertItemsBulk || x.BulkIdentifier == updateBulkId).ToList();
                Assert.Equal(cntBefore + insertedItems.Count, GetItemsCount());
                Assert.Equal(dbItems.Count, insertedItems.Count + itemsToUpdate.Count);
                Assert.True(dbItems.Where(x => x.BulkIdentifier == updateBulkId).All(x => x.Description == updatedDes));

                Assert.True(
                    dbItems.Where(x => x.BulkIdentifier == insertItemsBulk).All(x => x.Description != updatedDes && x.Price == -511));
            }
        }

        [IgnoreInCi]
        [Fact]
        internal async Task BulkInsertOrUpdateBy()
        {
            // Arrange
            var insertItemsBulk = Guid.NewGuid();
            var insertedItems = GenerateItemsBulk(insertItemsBulk);
            insertedItems.ForEach(x => x.Price = -511);

            var updateBulkId = Guid.NewGuid();
            var itemsToUpdate = GenerateItemsBulk(updateBulkId);

            using (var db = GetTestContext())
            {
                await db.Items.BulkInsertAsync(itemsToUpdate, setOutputIdentity: true);
            }

            var updatedDes = Guid.NewGuid().ToString();
            itemsToUpdate.ForEach(x => x.Description = updatedDes);
            var cntBefore = GetItemsCount();

            // Act
            using (var db = GetTestContext())
            {
                await db.Items.BulkInsertOrUpdateByAsync(insertedItems.Concat(itemsToUpdate).OrderBy(x => x.Guid).ToList(),
                                                         new[] { nameof(Item.Guid) });
            }

            // Assert
            using (var db = GetTestContext())
            {
                var dbItems = db.Items.Where(x => x.BulkIdentifier == insertItemsBulk || x.BulkIdentifier == updateBulkId).ToList();
                Assert.Equal(cntBefore + insertedItems.Count, GetItemsCount());
                Assert.Equal(dbItems.Count, insertedItems.Count + itemsToUpdate.Count);
                Assert.True(dbItems.Where(x => x.BulkIdentifier == updateBulkId).All(x => x.Description == updatedDes));

                Assert.True(
                    dbItems.Where(x => x.BulkIdentifier == insertItemsBulk).All(x => x.Description != updatedDes && x.Price == -511));
            }
        }

        [IgnoreInCi]
        [Fact]
        internal async Task BulkInsert_IdsSet_Throws()
        {
            var items = GenerateItemsBulk();
            var countBefore = GetItemsCount();
            var tempId = countBefore + 1;
            items.ForEach(x => x.Id = ++tempId);

            await Assert.ThrowsAsync<InvalidOperationException>(async () =>
            {
                using (var db = GetTestContext())
                {
                    await db.Items.BulkInsertAsync(items);
                }
            });

            await Assert.ThrowsAsync<InvalidOperationException>(async () =>
            {
                using (var db = GetTestContext())
                {
                    await db.Items.BulkInsertAsync(items, setOutputIdentity: true);
                }
            });
        }

        [IgnoreInCi]
        [Fact]
        internal async Task BulkUpdate_NoIdColumn()
        {
            var bulkId = Guid.NewGuid();
            var items = GenerateNoIdsItems(bulkId);

            using (var db = GetTestContext())
            {
                await db.NoIdItems.BulkInsertAsync(items);
            }

            var updated = Guid.NewGuid().ToString();
            items.ForEach(x => x.Description = updated);

            using (var db = GetTestContext())
            {
                await db.NoIdItems.BulkUpdateByPrimaryKeysAsync(items);
            }

            using (var db = GetTestContext())
            {
                var dbItems = db.NoIdItems.Where(x => x.BulkIdentifier == bulkId).ToList();
                Assert.True(items.OrderBy(x => x.Guid).SequencePredicateEqual(dbItems.OrderBy(x => x.Guid), (x, y) => x.Guid == y.Guid));
                Assert.True(dbItems.All(x => x.Description == updated));
            }
        }

        [IgnoreInCi]
        [Fact]
        internal async Task BulkInsertKeepIdentity()
        {
            var bulkId = Guid.NewGuid();
            var items = GenerateItemsBulk(bulkId);

            int id = 123_123_123;

            items.ForEach(x => x.Id = id++);

            using (var db = GetTestContext())
            {
                await db.Items.BulkInsertKeepSourceIdentityAsync(items);
            }

            using (var db = GetTestContext())
            {
                var dbItems = db.Items.Where(x => x.BulkIdentifier == bulkId).ToList();

                Assert.True(items.OrderBy(x => x.Guid)
                                 .SequencePredicateEqual(dbItems.OrderBy(x => x.Guid), (x, y) => x.Guid == y.Guid && x.Id == y.Id));
            }
        }

        [IgnoreInCi]
        [Fact]
        internal async Task BulkInsertKeepIdentity_DoesNotInsertDuplicates()
        {
            var bulkId = Guid.NewGuid();
            var items = GenerateItemsBulk(bulkId);

            int id = 999_999_999;

            items.ForEach(x => x.Id = id++);

            using (var db = GetTestContext())
            {
                await db.Items.BulkInsertKeepSourceIdentityAsync(items);
            }

            // insert succeeded
            using (var db = GetTestContext())
            {
                var dbItems = db.Items.Where(x => x.BulkIdentifier == bulkId).ToList();

                Assert.True(items.OrderBy(x => x.Guid)
                                 .SequencePredicateEqual(dbItems.OrderBy(x => x.Guid), (x, y) => x.Guid == y.Guid && x.Id == y.Id));
            }

            var sqlException = await Assert.ThrowsAnyAsync<SqlException>(async () =>
            {
                using (var db = GetTestContext())
                {
                    await db.Items.BulkInsertKeepSourceIdentityAsync(items);
                }
            });

            Assert.Contains("Cannot insert duplicate key", sqlException.Message);
        }

        private class DatabaseFixture : IDisposable
        {
            public DatabaseFixture()
            {
                using (var db = GetTestContext())
                {
                    db.Database.EnsureDeleted();
                }

                using (var db = GetTestContext())
                {
                    db.Database.EnsureCreated();
                }
            }

            public void Dispose()
            {
                using (var db = GetTestContext())
                {
                    db.Database.EnsureDeleted();
                }
            }
        }
    }
}
