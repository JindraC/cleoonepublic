﻿// <copyright file="TestContext.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.DotNet.Helpers;
using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CleoOne.Common.Core.Tests.SqlBulkTests
{
    public class TestContext : DbContext
    {
        public DbSet<Item> Items { get; set; }

        public DbSet<AnotherItem> AnotherItems { get; set; }

        public DbSet<NoIdItem> NoIdItems { get; set; }

        public TestContext(string db)
            : base(ContextUtil.GetOptions(db))
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Item>().HasIndex(x => x.Guid).IsUnique();
            modelBuilder.Entity<AnotherItem>().HasIndex(x => x.Guid).IsUnique();
            modelBuilder.Entity<NoIdItem>().HasIndex(x => x.Guid).IsUnique();
        }
    }

    public static class ContextUtil
    {
        // if you are getting this error: https://dba.stackexchange.com/questions/191393/localdb-v14-creates-wrong-path-for-mdf-files
        // then install latest SQL Server 2017 Cumulative Update from here to fix this LocalDB bug: https://support.microsoft.com/en-us/help/4047329/sql-server-2017-build-versions

        public static DbContextOptions GetOptions(string db)
        {
            var builder = new DbContextOptionsBuilder<TestContext>();
            var databaseName = db;

            var connectionString =
                $"Server=(localdb)\\MSSQLLocalDB;Database={databaseName};Trusted_Connection=True;MultipleActiveResultSets=true";

            builder.UseSqlServer(connectionString,
                                 options => options.ExecutionStrategy(dependencies =>
                                                                          new MarsHeaderRepairExecutionStrategy(
                                                                              dependencies))); // Can NOT Test with UseInMemoryDb (Exception: Relational-specific methods can only be used when the context is using a relational)

            // builder.UseSqlServer(connectionString);

            return builder.Options;
        }
    }

    public class Item
    {
        public int Id { get; set; }

        public int Price { get; set; }

        public string Name { get; set; }

        public Guid Guid { get; set; }

        public int ListOrder { get; set; }

        public Guid BulkIdentifier { get; set; }

        public DateTime DateTime { get; set; }

        public Guid GuidProperty { get; set; }

        public string Description { get; set; }
    }

    public class AnotherItem
    {
        [Key]
        [MaxLength(255)]
        public string PrimaryKey { get; set; }

        public string FirstName { get; set; }

        public string Description { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdColumn { get; set; }

        public Guid Guid { get; set; }

        public Guid BulkIdentifier { get; set; }
    }

    public class NoIdItem
    {
        [Key]
        [MaxLength(255)]
        public string PrimaryKey { get; set; }

        public string FirstName { get; set; }

        public string Description { get; set; }

        public Guid Guid { get; set; }

        public Guid BulkIdentifier { get; set; }
    }
}
