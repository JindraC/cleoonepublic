﻿// <copyright file="EnumerableExtensionsTests.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.DotNet.Extensions;
using System;
using System.Linq;
using Xunit;

namespace CleoOne.Common.Core.Tests.DotNet.Extensions
{
    public class EnumerableExtensionsTests
    {
        [Fact]
        public void JoinWithSeparatorWorks()
        {
            var empty = Array.Empty<string>();
            var tested = empty.JoinWithSeparator("-").ToList();
            Assert.Empty(tested);

            // single
            var single = new[] { "single" };
            var testedSingle = single.JoinWithSeparator("-").ToList();
            Assert.Single(testedSingle);

            // two
            var two = new[] { "1", "2" };
            var twoJoined = two.JoinWithSeparator("-").ToList();
            Assert.Equal(3, twoJoined.Count);
            Assert.Equal("-", twoJoined[1]);

            // three
            var three = new[] { "1", "2", "3" };
            var threeJoined = three.JoinWithSeparator("-").ToList();
            Assert.Equal(5, threeJoined.Count);

            // ten
            var many = Enumerable.Range(0, 10).Select(x => x.ToString()).ToArray();
            var manyJoined = many.JoinWithSeparator("-").ToList();
            Assert.Equal(many.Length + many.Length - 1, manyJoined.Count);

            for (int i = 0; i < manyJoined.Count; i++)
            {
                if (i % 2 == 0)
                {
                    Assert.Equal((i / 2).ToString(), manyJoined[i]);
                }
                else
                {
                    Assert.Equal("-", manyJoined[i]);
                }
            }
        }
    }
}
