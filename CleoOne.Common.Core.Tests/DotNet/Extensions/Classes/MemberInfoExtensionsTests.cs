﻿// <copyright file="MemberInfoExtensionsTests.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.DotNet.Extensions.Classes;
using System;
using System.Linq;
using System.Reflection;
using Xunit;

#pragma warning disable 169
#pragma warning disable 414

namespace CleoOne.Common.Core.Tests.DotNet.Extensions.Classes
{
    public class MemberInfoExtensionsTests
    {
        private class Dummy
        {
            private string _string;

            private static string _staticString;

            public string PublicString;

            public string StringPropPublic { get; set; }

            private string StringPropPrivate { get; set; }

            private int _int;

            private static int _staticInt;

            public int PublicInt;

            public int IntPropPublic { get; set; }

            private int IntPropPrivate { get; set; }

            public static Dummy GetInstance()
            {
                var dummy = new Dummy();

                dummy.IntPropPrivate = 42;
                dummy.IntPropPublic = 125;
                dummy.StringPropPrivate = "xyz";
                dummy.StringPropPublic = "xyzPublic";

                dummy._int = 456;
                dummy._string = "fld";
                dummy.PublicInt = 789;
                dummy.PublicString = "publicFields";

                // These two are to avoid warnings.
                _staticString = "sdasdf";
                _staticInt = 42;

                return dummy;
            }
        }

        [Fact]
        public void FastGetValue()
        {
            var instance = Dummy.GetInstance();

            var member = instance.GetType()
                                 .GetMembers(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                                 .Where(x => x.MemberType == MemberTypes.Field || x.MemberType == MemberTypes.Property);

            foreach (var memberInfo in member)
            {
                Check(instance, memberInfo);
            }
        }

        private void Check(object instance, MemberInfo member)
        {
            var byFast = instance.GetPropertyOrFieldValueByExpression(member.Name);

            var byReflection = member.GetPropertyOrFieldValue(instance);

            Assert.True(Equals(byReflection, byFast));
        }

        private interface IInterface
        {
            string InterfaceProp { get; }
        }

        private class Child : IInterface
        {
            string IInterface.InterfaceProp { get; } = "123";
        }

        [Fact]
        public void ExplicitImplementation_IsNotSupported()
        {
            var child = new Child();

            var member = typeof(IInterface).GetProperties().Single();

            Assert.Throws<InvalidOperationException>(() => Check(child, member));
        }

        private abstract class Base
        {
            public string PublicBase { get; } = "PublicBase";

            private string _baseField = "baseField";

            private string BaseProp { get; } = "BaseProp";

            internal virtual string VirtualProp => "baseValue";

            public abstract string AbstractProp { get; }

            public static string GetBaseFieldName => nameof(_baseField);
        }

        private class Child2 : Base
        {
            public override string AbstractProp { get; } = "childValue";

            internal override string VirtualProp { get; } = "childVirtual";
        }

        [Fact]
        public void InheritanceTest()
        {
            var child = new Child2();

            Assert.Equal(child.PublicBase, child.GetPropertyOrFieldValueByExpression(nameof(Base.PublicBase)));
            Assert.Equal(child.AbstractProp, child.GetPropertyOrFieldValueByExpression(nameof(Base.AbstractProp)));
            Assert.Equal(child.VirtualProp, child.GetPropertyOrFieldValueByExpression(nameof(Base.VirtualProp)));
            Assert.Equal("baseField", child.GetPropertyOrFieldValueByExpression(Base.GetBaseFieldName));

            // Private properties are not supported
            Assert.Throws<InvalidOperationException>(() => child.GetPropertyOrFieldValueByExpression("BaseProp"));
        }
    }
}
