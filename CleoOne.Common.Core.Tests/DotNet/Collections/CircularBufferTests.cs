﻿// <copyright file="CircularBufferTests.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.DotNet.Collections;
using System;
using System.Linq;
using Xunit;

namespace CleoOne.Common.Core.Tests.DotNet.Collections
{
    public class CircularBufferTests
    {
        [Fact]
        public void CircularBuffer_GetEnumeratorConstructorCapacity_ReturnsEmptyCollection()
        {
            var buffer = new CircularBuffer<string>(5);
            Assert.Empty(buffer.ToArray());
        }

        [Fact]
        public void CircularBuffer_ConstructorSizeIndexAccess_CorrectContent()
        {
            var buffer = new CircularBuffer<int>(5, new[] { 0, 1, 2, 3 });

            Assert.Equal(5, buffer.Capacity);
            Assert.Equal(4, buffer.Size);

            for (int i = 0; i < 4; i++)
            {
                Assert.Equal(buffer[i], i);
            }
        }

        [Fact]
        public void CircularBuffer_Constructor_ExceptionWhenSourceIsLargerThanCapacity()
        {
            Assert.Throws<ArgumentException>(() => new CircularBuffer<int>(3, new[] { 0, 1, 2, 3 }));
        }

        [Fact]
        public void CircularBuffer_GetEnumeratorConstructorDefinedArray_CorrectContent()
        {
            var buffer = new CircularBuffer<int>(5, new[] { 0, 1, 2, 3 });

            int x = 0;

            foreach (var item in buffer)
            {
                Assert.Equal(item, x);
                x++;
            }
        }

        [Fact]
        public void CircularBuffer_PushBack_CorrectContent()
        {
            var buffer = new CircularBuffer<int>(5);

            for (int i = 0; i < 5; i++)
            {
                buffer.PushBack(i);
            }

            Assert.Equal(0, buffer.Front());

            for (int i = 0; i < 5; i++)
            {
                Assert.Equal(buffer[i], i);
            }
        }

        [Fact]
        public void CircularBuffer_PushBackOverflowingBuffer_CorrectContent()
        {
            var buffer = new CircularBuffer<int>(5);

            for (int i = 0; i < 10; i++)
            {
                buffer.PushBack(i);
            }

            Assert.Equal(buffer.ToArray(), new[] { 5, 6, 7, 8, 9 });
        }

        [Fact]
        public void CircularBuffer_GetEnumeratorOverflowedArray_CorrectContent()
        {
            var buffer = new CircularBuffer<int>(5);

            for (int i = 0; i < 10; i++)
            {
                buffer.PushBack(i);
            }

            // buffer should have [5,6,7,8,9]
            int x = 5;

            foreach (var item in buffer)
            {
                Assert.Equal(item, x);
                x++;
            }
        }

        [Fact]
        public void CircularBuffer_ToArrayConstructorDefinedArray_CorrectContent()
        {
            var buffer = new CircularBuffer<int>(5, new[] { 0, 1, 2, 3 });

            Assert.Equal(buffer.ToArray(), new[] { 0, 1, 2, 3 });
        }

        [Fact]
        public void CircularBuffer_ToArrayOverflowedBuffer_CorrectContent()
        {
            var buffer = new CircularBuffer<int>(5);

            for (int i = 0; i < 10; i++)
            {
                buffer.PushBack(i);
            }

            Assert.Equal(buffer.ToArray(), new[] { 5, 6, 7, 8, 9 });
        }

        [Fact]
        public void CircularBuffer_PushFront_CorrectContent()
        {
            var buffer = new CircularBuffer<int>(5);

            for (int i = 0; i < 5; i++)
            {
                buffer.PushFront(i);
            }

            Assert.Equal(buffer.ToArray(), new[] { 4, 3, 2, 1, 0 });
        }

        [Fact]
        public void CircularBuffer_PushFrontAndOverflow_CorrectContent()
        {
            var buffer = new CircularBuffer<int>(5);

            for (int i = 0; i < 10; i++)
            {
                buffer.PushFront(i);
            }

            Assert.Equal(buffer.ToArray(), new[] { 9, 8, 7, 6, 5 });
        }

        [Fact]
        public void CircularBuffer_Front_CorrectItem()
        {
            var buffer = new CircularBuffer<int>(5, new[] { 0, 1, 2, 3, 4 });

            Assert.Equal(0, buffer.Front());
        }

        [Fact]
        public void CircularBuffer_Back_CorrectItem()
        {
            var buffer = new CircularBuffer<int>(5, new[] { 0, 1, 2, 3, 4 });
            Assert.Equal(4, buffer.Back());
        }

        [Fact]
        public void CircularBuffer_BackOfBufferOverflowByOne_CorrectItem()
        {
            var buffer = new CircularBuffer<int>(5, new[] { 0, 1, 2, 3, 4 });
            buffer.PushBack(42);
            Assert.Equal(buffer.ToArray(), new[] { 1, 2, 3, 4, 42 });
            Assert.Equal(42, buffer.Back());
        }

        [Fact]
        public void CircularBuffer_Front_EmptyBufferThrowsException()
        {
            var buffer = new CircularBuffer<int>(5);

            var ex = Assert.Throws<InvalidOperationException>(() => buffer.Front());
            Assert.Contains("empty buffer", ex.Message);
        }

        [Fact]
        public void CircularBuffer_Back_EmptyBufferThrowsException()
        {
            var buffer = new CircularBuffer<int>(5);

            var ex = Assert.Throws<InvalidOperationException>(() => buffer.Back());
            Assert.Contains("empty buffer", ex.Message);
        }

        [Fact]
        public void CircularBuffer_PopBack_RemovesBackElement()
        {
            var buffer = new CircularBuffer<int>(5, new[] { 0, 1, 2, 3, 4 });

            Assert.Equal(5, buffer.Size);

            buffer.PopBack();

            Assert.Equal(4, buffer.Size);
            Assert.Equal(buffer.ToArray(), new[] { 0, 1, 2, 3 });
        }

        [Fact]
        public void CircularBuffer_PopBackInOverflowBuffer_RemovesBackElement()
        {
            var buffer = new CircularBuffer<int>(5, new[] { 0, 1, 2, 3, 4 });
            buffer.PushBack(5);

            Assert.Equal(5, buffer.Size);
            Assert.Equal(buffer.ToArray(), new[] { 1, 2, 3, 4, 5 });

            buffer.PopBack();

            Assert.Equal(4, buffer.Size);
            Assert.Equal(buffer.ToArray(), new[] { 1, 2, 3, 4 });
        }

        [Fact]
        public void CircularBuffer_PopFront_RemovesBackElement()
        {
            var buffer = new CircularBuffer<int>(5, new[] { 0, 1, 2, 3, 4 });

            Assert.Equal(5, buffer.Size);

            buffer.PopFront();

            Assert.Equal(4, buffer.Size);
            Assert.Equal(buffer.ToArray(), new[] { 1, 2, 3, 4 });
        }

        [Fact]
        public void CircularBuffer_PopFrontInOverflowBuffer_RemovesBackElement()
        {
            var buffer = new CircularBuffer<int>(5, new[] { 0, 1, 2, 3, 4 });
            buffer.PushFront(5);

            Assert.Equal(5, buffer.Size);
            Assert.Equal(buffer.ToArray(), new[] { 5, 0, 1, 2, 3 });

            buffer.PopFront();

            Assert.Equal(4, buffer.Size);
            Assert.Equal(buffer.ToArray(), new[] { 0, 1, 2, 3 });
        }

        [Fact]
        public void CircularBuffer_SetIndex_ReplacesElement()
        {
            var buffer = new CircularBuffer<int>(5, new[] { 0, 1, 2, 3, 4 });

            buffer[1] = 10;
            buffer[3] = 30;

            Assert.True(buffer.ToArray().SequenceEqual(new[] { 0, 10, 2, 30, 4 }));
        }

        [Fact]
        public void CircularBuffer_WithDifferentSizeAndCapacity_BackReturnsLastArrayPosition()
        {
            // test to confirm this issue does not happen anymore:
            // https://github.com/joaoportela/CircullarBuffer-CSharp/issues/2

            var buffer = new CircularBuffer<int>(5, new[] { 0, 1, 2, 3, 4 });

            buffer.PopFront(); // (make size and capacity different)

            Assert.Equal(4, buffer.Back());
        }
    }
}
