﻿// <copyright file="DeepObjectComparerTests.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.DotNet.Comparers;
using CleoOne.Common.Core.DotNet.Extensions.ValueTypes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Numerics;
using Xunit;
using static CleoOne.Common.Core.DotNet.Comparers.DeepObjectComparer;

namespace CleoOne.Common.Core.Tests.DotNet.Comparers
{
    [SuppressMessage("ReSharper", "UselessBinaryOperation", Justification = "Approved")]
    public class DeepObjectComparerTests
    {
        [Fact]
        public void Test_BasicTypes()
        {
            AssertObjectsAreSame(new object(), new object());

            AssertObjectsAreSame(1, 1);

            AssertObjectsAreSame(decimal.Parse("1.00000000"), decimal.Parse("1"));
            AssertObjectsAreDifferent(decimal.Parse("1.000000001"), decimal.Parse("1"));

            AssertObjectsAreSame(1.23f, 1.23f);
            AssertObjectsAreDifferent(1.231f, 1.23f);

            AssertObjectsAreSame(1.23d, 1.23d);
            AssertObjectsAreDifferent(1.231d, 1.23d);

            AssertObjectsAreSame("str", "s" + "tr");

            AssertObjectsAreSame(new DateTime(1993, 9, 29, 12, 34, 56),
                                 DateTimeOffset.FromUnixTimeMilliseconds(new DateTime(1993, 9, 29, 12, 34, 56).ToUnixMilliseconds())
                                               .DateTime);

            AssertObjectsAreSame(TimeSpan.FromDays(1), TimeSpan.FromHours(24));

            AssertObjectsAreSame(true, true);
            AssertObjectsAreDifferent(true, false);

            var bigInteger1 = new BigInteger(long.MaxValue);
            bigInteger1 += 2;

            var bigInteger2 = new BigInteger(long.MaxValue);
            bigInteger2++;
            bigInteger2++;

            AssertObjectsAreSame(bigInteger1, bigInteger2);
        }

        private static void AssertObjectsAreSame(object x, object y)
        {
            var deepObjectComparer = new DeepObjectComparer();

            Assert.True(deepObjectComparer.HaveSameAllMembers(x, y));
            Assert.True(deepObjectComparer.HaveSameFields(x, y));
            Assert.True(deepObjectComparer.HaveSamePublicProperties(x, y));
        }

        private static void AssertObjectsAreDifferent(object x, object y)
        {
            var deepObjectComparer = new DeepObjectComparer();

            Assert.False(deepObjectComparer.HaveSameAllMembers(x, y));
            Assert.False(deepObjectComparer.HaveSameFields(x, y));
            Assert.False(deepObjectComparer.HaveSamePublicProperties(x, y));
        }

        private struct StructWithReferenceFields
        {
            public object Object { get; set; }

            public string String { get; set; }

            public SelfReferencingClass Class { get; set; }
        }

        [Fact]
        public void ReferencePropertiesStructTest()
        {
            StructWithReferenceFields x = default;
            StructWithReferenceFields y = default;

            x.String = "x";
            y.String = "x";

            AssertObjectsAreSame(x, y);

            x.String = "x";
            y.String = "y";

            AssertObjectsAreDifferent(x, y);
        }

        private struct ValueFieldsStruct
        {
            internal bool _bool;
            internal int _int;
        }

        [Fact]
        public void ValueFieldsStructTest()
        {
            ValueFieldsStruct x = default;
            ValueFieldsStruct y = default;

            x._int = 2;
            y._int = 2;

            AssertObjectsAreSame(x, y);
        }

        [Fact]
        public void ValueFieldsStructTest_Different()
        {
            ValueFieldsStruct x = default;
            ValueFieldsStruct y = default;

            x._bool = true;
            y._bool = false;

            var deepObjectComparer = new DeepObjectComparer();

            Assert.False(deepObjectComparer.HaveSameAllMembers(x, y));
            Assert.False(deepObjectComparer.HaveSameFields(x, y));
            Assert.True(deepObjectComparer.HaveSamePublicProperties(x, y));
        }

        private class SelfReferencingClass
        {
            public SelfReferencingClass Me { get; set; }
        }

        [Fact]
        public void SelfReferencingTest()
        {
            var x = new SelfReferencingClass { Me = new SelfReferencingClass() };

            var y = new SelfReferencingClass { Me = new SelfReferencingClass() };

            AssertObjectsAreSame(x, y);
        }

        [Fact]
        public void DictionaryAsEnumerableTest()
        {
            var x = new Dictionary<string, int>()
            {
                { "1", 1 },
                { "2", 2 },
                { "3", 3 },
            };

            var dictionaryInDifferentOrder = x.OrderByDescending(kvp => kvp.Value).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);

            var dictionarySameOrder = x.ToDictionary(kvp => kvp.Key, kvp => kvp.Value);

            AssertObjectsAreSame(x, dictionarySameOrder);

            AssertObjectsAreDifferent(x, dictionaryInDifferentOrder);
        }

        private class TestClass1
        {
            public string String { get; set; }
        }

        private class TestClass2
        {
            public string String { get; set; }
        }

        [Fact]
        public void DifferentTypesTest()
        {
            var x = new TestClass1()
            {
                String = "123",
            };

            var y = new TestClass2()
            {
                String = "123",
            };

            var deepObjectComparer = new DeepObjectComparer();

            Assert.False(deepObjectComparer.HaveSameAllMembers(x, y));
            Assert.False(deepObjectComparer.HaveSamePublicProperties(x, y));
            Assert.False(deepObjectComparer.HaveSameFields(x, y));
        }

        private class ObjectPropertyClass
        {
            public object Object { get; set; }
        }

        internal enum NoNoneEnum
        {
            One = 1,
            Two = 2,
        }

        [Fact]
        public void TestPrivateEnum()
        {
            NoNoneEnum enumType = (NoNoneEnum)0;
            var str = enumType.ToString();
            Assert.True(str == "0");
        }

        [Fact]
        public void DifferentListInClasses()
        {
            // this is intentionally in this way
            var x = new ClassWithList { List = new List<int>(10) { 1, 2, 3 } };
            var y = new ClassWithList { List = new List<int>(10) { 1, 2, 4 } };

            var deepObjectComparer = new DeepObjectComparer();
            // deepObjectComparer.ThrowExceptionOnFalse = true;

            Assert.False(deepObjectComparer.HaveSameAllMembers(x, y));
            Assert.False(deepObjectComparer.HaveSamePublicProperties(x, y));
            Assert.False(deepObjectComparer.HaveSameFields(x, y));
        }

        private class ClassWithList
        {
            public List<int> List { get; set; }
        }

        [Fact]
        public void ListNotDependentOnSizeOfUnderlyingArray()
        {
            // this is intentionally in this way
            var x = new List<int>(10) { 1, 2, 3 };

            var y = new List<int>(4) { 1, 2, 3 };

            var deepObjectComparer = new DeepObjectComparer();

            Assert.True(deepObjectComparer.HaveSameAllMembers(x, y));
            Assert.True(deepObjectComparer.HaveSamePublicProperties(x, y));
            Assert.True(deepObjectComparer.HaveSameFields(x, y));
        }

        [Fact]
        public void UnsupportedBaseTypesForListThrowsException()
        {
            var x = new ListSuccessor { Number = 5 };
            var y = new ListSuccessor { Number = 5 };

            var deepObjectComparer = new DeepObjectComparer();
            var ex1 = Assert.Throws<NotImplementedException>(() => deepObjectComparer.HaveSameAllMembers(x, y));
            var ex2 = Assert.Throws<NotImplementedException>(() => deepObjectComparer.HaveSamePublicProperties(x, y));
            var ex3 = Assert.Throws<NotImplementedException>(() => deepObjectComparer.HaveSameFields(x, y));
            var result = new[] { ex1, ex2, ex3 }.All(ex => ex.Message.Contains("is not supported as base type"));
            Assert.True(result);
        }

        private class ListSuccessor : List<int>
        {
            public int Number { get; set; }
        }

        [Fact]
        public void UnsupportedBaseTypesForHashSetThrowsException()
        {
            var x = new HashSetSuccessor { Number = 5 };
            var y = new HashSetSuccessor { Number = 5 };

            var deepObjectComparer = new DeepObjectComparer();
            var ex1 = Assert.Throws<NotImplementedException>(() => deepObjectComparer.HaveSameAllMembers(x, y));
            var ex2 = Assert.Throws<NotImplementedException>(() => deepObjectComparer.HaveSamePublicProperties(x, y));
            var ex3 = Assert.Throws<NotImplementedException>(() => deepObjectComparer.HaveSameFields(x, y));
            var result = new[] { ex1, ex2, ex3 }.All(ex => ex.Message.Contains("is not supported as base type"));
            Assert.True(result);
        }

        private class HashSetSuccessor : HashSet<int>
        {
            public int Number { get; set; }
        }

        private class DerivedEnumerable : IEnumerable<int>
        {
            private static readonly int[] _integers = new[] { 1, 2, 3 };

            public int FavoriteNumber { get; set; }

            public IEnumerator<int> GetEnumerator()
            {
                foreach (var i in _integers)
                {
                    yield return i;
                }
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }
        }

        [Fact]
        public void ImplementedEnumerableDiffersOnProperty()
        {
            // this is intentionally in this way
            var x = new DerivedEnumerable { FavoriteNumber = 5 };
            var y = new DerivedEnumerable { FavoriteNumber = 7 };

            var deepObjectComparer = new DeepObjectComparer();

            Assert.False(deepObjectComparer.HaveSameAllMembers(x, y));
            Assert.False(deepObjectComparer.HaveSamePublicProperties(x, y));
            Assert.False(deepObjectComparer.HaveSameFields(x, y));
        }

        private class SimpleEqualsClass
        {
            public int NumberOnProperty { get; set; }

            public SimpleEqualsClass Parent { get; set; }

            public override bool Equals(object obj)
            {
                return true;
            }

            public override int GetHashCode()
            {
                return 0;
            }
        }

        [Fact]
        public void ClassWithSimpleEquals()
        {
            var x = new SimpleEqualsClass { NumberOnProperty = 5, Parent = new SimpleEqualsClass { NumberOnProperty = 5 } };
            var y = new SimpleEqualsClass { NumberOnProperty = 5, Parent = new SimpleEqualsClass { NumberOnProperty = 6 } };

            var deepObjectComparer = new DeepObjectComparer();

            Assert.False(deepObjectComparer.HaveSameAllMembers(x, y));
            Assert.False(deepObjectComparer.HaveSamePublicProperties(x, y));
            Assert.False(deepObjectComparer.HaveSameFields(x, y));
        }

        private class SimplePredecessorClass
        {
            private int _someField = 1;

            public SimplePredecessorClass(int someField)
            {
                _someField = someField;
            }
        }

        private class SimpleSuccessorClass : SimplePredecessorClass
        {
            private int _someField = 1;
            private int _anotherField = 2;

            public SimpleSuccessorClass(int actualSomeField, int actualAnotherField, int parentSomeField)
                : base(parentSomeField)
            {
                _someField = actualSomeField;
                _anotherField = actualAnotherField;
            }
        }

        [Fact]
        public void ParentPrivateFieldTest()
        {
            var x = new SimpleSuccessorClass(1, 2, 3);
            var y = new SimpleSuccessorClass(1, 2, 4);

            var deepObjectComparer = new DeepObjectComparer();

            Assert.False(deepObjectComparer.HaveSameAllMembers(x, y));
            Assert.True(deepObjectComparer.HaveSamePublicProperties(x, y));
            Assert.False(deepObjectComparer.HaveSameFields(x, y));
        }

        private class SimpleIntComparer : IEqualityComparer<int>
        {
            public int Number { get; set; }

            public bool Equals(int x, int y)
            {
                return x == y;
            }

            public int GetHashCode(int o)
            {
                return o;
            }
        }

        private class ClassWithCorruptedGetHashCode
        {
            public int Number { get; set; }

            public override int GetHashCode()
            {
                throw new Exception("This GetHashCode is broken");
            }
        }

        [Fact]
        public void ClassWithCorruptedGetHashCodeTest()
        {
            var x = new ClassWithCorruptedGetHashCode();
            var y = new ClassWithCorruptedGetHashCode();
            var deepObjectComparer = new DeepObjectComparer();

            Assert.True(deepObjectComparer.HaveSameAllMembers(x, y));
            Assert.True(deepObjectComparer.HaveSamePublicProperties(x, y));
            Assert.True(deepObjectComparer.HaveSameFields(x, y));
        }

        [Fact]
        public void HashSetWithDifferentComparers()
        {
            var x = new HashSet<int>(new SimpleIntComparer { Number = 1 });
            var y = new HashSet<int>(new SimpleIntComparer { Number = 2 });
            var deepObjectComparer = new DeepObjectComparer();

            Assert.False(deepObjectComparer.HaveSameAllMembers(x, y));
            Assert.False(deepObjectComparer.HaveSamePublicProperties(x, y));
            Assert.False(deepObjectComparer.HaveSameFields(x, y));
        }

        [Fact]
        public void BoxedPropertyClass()
        {
            object obj1 = 1;
            object obj2 = 1;

            Assert.False(ReferenceEquals(obj2, obj1));

            var x = new ObjectPropertyClass()
            {
                Object = obj1,
            };

            var y = new ObjectPropertyClass()
            {
                Object = obj2,
            };

            var deepObjectComparer = new DeepObjectComparer();
            Assert.True(deepObjectComparer.HaveSameAllMembers(x, y));
            Assert.True(deepObjectComparer.HaveSameFields(x, y));
            Assert.True(deepObjectComparer.HaveSamePublicProperties(x, y));
        }

        [Fact]
        public void OmittingSpecifiedProperty()
        {
            var x = new ClassWithStringPropertySon { VirtualText = "A1" };
            var y = new ClassWithStringPropertySon { VirtualText = "A2" };

            var deepObjectComparer = new DeepObjectComparer();
            Assert.False(deepObjectComparer.HaveSameAllMembers(x, y));
            Assert.False(deepObjectComparer.HaveSameFields(x, y));
            Assert.False(deepObjectComparer.HaveSamePublicProperties(x, y));

            deepObjectComparer = new DeepObjectComparer(false,
                                                        c => c.AddOmittedProperty<ClassWithStringPropertyFather>(
                                                            tc => tc.VirtualText,
                                                            MemberOmittingFlags.OnSons));

            Assert.True(deepObjectComparer.HaveSamePublicProperties(x, y));
            Assert.True(deepObjectComparer.HaveSameFields(x, y));

            deepObjectComparer = new DeepObjectComparer(false,
                                                        c => c.AddOmittedProperty<ClassWithStringPropertySon>(
                                                            tc => tc.VirtualText,
                                                            MemberOmittingFlags.OnSameType));

            Assert.True(deepObjectComparer.HaveSameFields(x, y));
            Assert.True(deepObjectComparer.HaveSamePublicProperties(x, y));

            deepObjectComparer = new DeepObjectComparer(false,
                                                        c => c.AddOmittedProperty<ClassWithStringPropertyFather>(
                                                            tc => tc.VirtualText,
                                                            MemberOmittingFlags.OnSameType));

            Assert.False(deepObjectComparer.HaveSamePublicProperties(x, y));
            Assert.False(deepObjectComparer.HaveSameFields(x, y));
        }

        [Fact]
        public void OmittingSpecifiedProperty_Inheritance()
        {
            var x = new ClassWithStringPropertySon { VirtualText = "A1", SetFathersInt = 1 };
            var y = new ClassWithStringPropertySon { VirtualText = "A1", SetFathersInt = 2 };

            var deepObjectComparer = new DeepObjectComparer();

            Assert.False(deepObjectComparer.HaveSameFields(x, y));
            Assert.False(deepObjectComparer.HaveSamePublicProperties(x, y));

            deepObjectComparer = new DeepObjectComparer(false,
                                                        c => c.AddOmittedProperty<ClassWithStringPropertySon>(
                                                            tc => tc.FathersInt,
                                                            MemberOmittingFlags.OnSameType));

            Assert.True(deepObjectComparer.HaveSamePublicProperties(x, y));
            Assert.False(deepObjectComparer.HaveSameFields(x, y));

            deepObjectComparer = new DeepObjectComparer(false,
                                                        c => c.AddOmittedProperty<ClassWithStringPropertyFather>(tc => tc.FathersInt,
                                                                                                                 MemberOmittingFlags
                                                                                                                    .OnSameTypeOrOnParents));

            Assert.False(deepObjectComparer.HaveSamePublicProperties(x, y));
            Assert.False(deepObjectComparer.HaveSameFields(x, y));

            deepObjectComparer = new DeepObjectComparer(false,
                                                        c => c.AddOmittedProperty<ClassWithStringPropertyFather>(tc => tc.FathersInt,
                                                                                                                 MemberOmittingFlags
                                                                                                                    .OnSameTypeOrOnSons));

            Assert.True(deepObjectComparer.HaveSamePublicProperties(x, y));
            Assert.False(deepObjectComparer.HaveSameFields(x, y));

            deepObjectComparer = new DeepObjectComparer(false,
                                                        c => c.AddOmittedProperty<ClassWithStringPropertySon>(
                                                            tc => tc.FathersInt,
                                                            MemberOmittingFlags.OnAnyTypeInFamily));

            Assert.True(deepObjectComparer.HaveSamePublicProperties(x, y));
            Assert.False(deepObjectComparer.HaveSameFields(x, y));

            deepObjectComparer = new DeepObjectComparer(false,
                                                        c => c.AddOmittedField<ClassWithStringPropertySon>(
                                                            "_privateInt",
                                                            MemberOmittingFlags.OnSameType));

            Assert.False(deepObjectComparer.HaveSameFields(x, y));
            Assert.False(deepObjectComparer.HaveSamePublicProperties(x, y));

            deepObjectComparer = new DeepObjectComparer(false,
                                                        c => c.AddOmittedField<ClassWithStringPropertySon>(
                                                            "_privateInt",
                                                            MemberOmittingFlags.OnSameTypeOrOnParents));

            Assert.True(deepObjectComparer.HaveSameFields(x, y));
            Assert.False(deepObjectComparer.HaveSamePublicProperties(x, y));

            deepObjectComparer = new DeepObjectComparer(false,
                                                        c => c.AddOmittedField<ClassWithStringPropertyFather>(
                                                            "_privateInt",
                                                            MemberOmittingFlags.OnSameTypeOrOnSons));

            Assert.True(deepObjectComparer.HaveSameFields(x, y));
            Assert.False(deepObjectComparer.HaveSamePublicProperties(x, y));

            deepObjectComparer = new DeepObjectComparer(false,
                                                        c => c.AddOmittedField<ClassWithStringPropertySon>(
                                                            "_privateInt",
                                                            MemberOmittingFlags.OnAnyTypeInFamily));

            Assert.True(deepObjectComparer.HaveSameFields(x, y));
            Assert.False(deepObjectComparer.HaveSamePublicProperties(x, y));

            deepObjectComparer = new DeepObjectComparer(true,
                                                        c =>
                                                        {
                                                            c.AddOmittedProperty<ClassWithStringPropertySon>(
                                                                tc => tc.FathersInt,
                                                                MemberOmittingFlags.OnAnyTypeInFamily);

                                                            c.AddOmittedField<ClassWithStringPropertySon>(
                                                                "_privateInt",
                                                                MemberOmittingFlags.OnAnyTypeInFamily);
                                                        });

            Assert.True(deepObjectComparer.HaveSameFields(x, y));
            Assert.True(deepObjectComparer.HaveSamePublicProperties(x, y));
        }

        [Fact]
        public void OmittingFieldOnParentByOmittingPropertyOnSon()
        {
            var x = new SonWithParentalProperty { Text = "text X" };
            var y = new SonWithParentalProperty { Text = "text Y" };
            var deepObjectComparer = new DeepObjectComparer(false);
            Assert.False(deepObjectComparer.HaveSameAllMembers(x, y));

            deepObjectComparer = new DeepObjectComparer(true, c => c.AddOmittedProperty<SonWithParentalProperty>(s => s.Text));
            Assert.True(deepObjectComparer.HaveSameAllMembers(x, y));
        }

        private interface IClassWithStringProperty
        {
            string VirtualText { get; set; }
        }

        private class ClassWithProperty
        {
            public string Text { get; set; }
        }

        private class SonWithParentalProperty : ClassWithProperty
        {
        }

        private class ClassWithStringPropertyFather : IClassWithStringProperty
        {
            private int _privateInt = 1;

            public virtual int PrivateInt { get { return _privateInt; } set { _privateInt = value; } }

            public virtual string VirtualText { get; set; }

            public int FathersInt { get { return _privateInt; } }
        }

        private class ClassWithStringPropertySon : ClassWithStringPropertyFather
        {
            private int _privateInt = 1;

            public override int PrivateInt { get { return _privateInt; } set { _privateInt = value; } }

            public override string VirtualText { get; set; }

            public int SetFathersInt { set { base.PrivateInt = value; } }
        }

        [Fact]
        public void DifferentStringInstanceTest()
        {
            string string1 = "test";
            string string2 = "tes";
            string2 += "t";

            Assert.False(ReferenceEquals(string1, string2));

            var x = new TestClass1()
            {
                String = string1,
            };

            var y = new TestClass1()
            {
                String = string2,
            };

            var deepObjectComparer = new DeepObjectComparer();
            Assert.True(deepObjectComparer.HaveSameAllMembers(x, y));
            Assert.True(deepObjectComparer.HaveSameFields(x, y));
            Assert.True(deepObjectComparer.HaveSamePublicProperties(x, y));
        }

        private class PrivatePropertyClass
        {
            public void SetProperty(string s) => PrivateProperty = s;

            private string PrivateProperty { get; set; }
        }

        [Fact]
        public void PrivatePropertyTest()
        {
            var x = new PrivatePropertyClass();
            x.SetProperty("a");

            var y = new PrivatePropertyClass();
            y.SetProperty("b");

            var deepObjectComparer = new DeepObjectComparer();
            Assert.False(deepObjectComparer.HaveSameAllMembers(x, y));
            Assert.False(deepObjectComparer.HaveSameFields(x, y));
            Assert.True(deepObjectComparer.HaveSamePublicProperties(x, y));
        }

        private class OnlyPropertyClass
        {
            private string _value;

            public string OnlySetterProperty { set { _value = value; } }

            public string OnlyGetterProperty { get { return "hardcoded"; } }
        }

        [Fact]
        public void OnlyGetterAndSetterProperty()
        {
            var x = new OnlyPropertyClass() { OnlySetterProperty = "a" };

            var y = new OnlyPropertyClass() { OnlySetterProperty = "a" };

            var deepObjectComparer = new DeepObjectComparer();
            Assert.True(deepObjectComparer.HaveSameAllMembers(x, y));
            Assert.True(deepObjectComparer.HaveSameFields(x, y));
            Assert.True(deepObjectComparer.HaveSamePublicProperties(x, y));
        }

        [Fact]
        public void OnlyGetterAndSetterProperty_Differs()
        {
            var x = new OnlyPropertyClass() { OnlySetterProperty = "a" };

            var y = new OnlyPropertyClass() { OnlySetterProperty = "b" };

            var deepObjectComparer = new DeepObjectComparer();
            Assert.False(deepObjectComparer.HaveSameAllMembers(x, y));
            Assert.False(deepObjectComparer.HaveSameFields(x, y));
            Assert.True(deepObjectComparer.HaveSamePublicProperties(x, y));
        }

        private struct TestStruct : IEquatable<TestStruct>
        {
            public int Integer { get; set; }

            public override bool Equals(object obj)
            {
                throw new Exception("FromStruct");
            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }

            public bool Equals(TestStruct other)
            {
                throw new NotImplementedException();
            }
        }

        private class WithStruct
        {
            public TestStruct TestStruct { get; set; }
        }

        [Fact]
        public void StructComparisonShouldNotCallEquals()
        {
            WithStruct x = new WithStruct();
            WithStruct y = new WithStruct();

            var deepObjectComparer = new DeepObjectComparer();

            Assert.True(deepObjectComparer.HaveSameAllMembers(x, y));
            Assert.True(deepObjectComparer.HaveSameFields(x, y));
            Assert.True(deepObjectComparer.HaveSamePublicProperties(x, y));
        }

        private class EmptyClass
        {
        }

        [Fact]
        public void EmptyClassIsSame()
        {
            var x = new EmptyClass();

            var y = new EmptyClass();

            var deepObjectComparer = new DeepObjectComparer();
            Assert.True(deepObjectComparer.HaveSameAllMembers(x, y));
            Assert.True(deepObjectComparer.HaveSameFields(x, y));
            Assert.True(deepObjectComparer.HaveSamePublicProperties(x, y));
        }

        [Fact]
        public void ClassWithDelegatesThrowsException()
        {
            var x = new ClassWithDelegates { FuncInt = z => 1 };
            var y = new ClassWithDelegates { FuncInt = z => 1 };

            var deepObjectComparer = new DeepObjectComparer();
            var ex1 = Assert.Throws<NotImplementedException>(() => deepObjectComparer.HaveSameAllMembers(x, y));
            var ex2 = Assert.Throws<NotImplementedException>(() => deepObjectComparer.HaveSamePublicProperties(x, y));
            var ex3 = Assert.Throws<NotImplementedException>(() => deepObjectComparer.HaveSameFields(x, y));
            var result = new[] { ex1, ex2, ex3 }.All(ex => ex.Message.Contains("Delegates are not supported type."));
            Assert.True(result);
        }

        private class ClassWithDelegates
        {
            public Func<int, int> FuncInt { get; set; }
        }

        [Fact]
        public void GlobalTest_True()
        {
            var sampleA1 = TestClass.GetSample();
            var sampleA2 = TestClass.GetSample();
            var sampleB = TestClass.GetSampleB();

            var deepObjectComparer1 = new DeepObjectComparer();
            var haveSameFieldsA = deepObjectComparer1.HaveSameFields(sampleA1, sampleA2);
            var haveSameFieldsB = deepObjectComparer1.HaveSameFields(sampleA1, sampleB);

            var deepObjectComparer2 = new DeepObjectComparer();
            var haveSameAllMembersA = deepObjectComparer2.HaveSameAllMembers(sampleA1, sampleA2);
            var haveSameAllMembersB = deepObjectComparer2.HaveSameAllMembers(sampleA1, sampleB);

            var deepObjectComparer3 = new DeepObjectComparer();
            var haveSamePublicPropertiesA = deepObjectComparer3.HaveSamePublicProperties(sampleA1, sampleA2);
            var haveSamePublicPropertiesB = deepObjectComparer3.HaveSamePublicProperties(sampleA1, sampleB);

            var allTrues = new bool[] { haveSameFieldsA, haveSameAllMembersA, haveSamePublicPropertiesA };
            var allFalses = new bool[] { haveSameFieldsB, haveSameAllMembersB, haveSamePublicPropertiesB };

            var areConditionsValid = allTrues.All(x => true) && allFalses.All(x => x == false);
            Assert.True(areConditionsValid);
        }

        [Fact]
        public void HaveSameAllMembers_True()
        {
            var doc = new DeepObjectComparer();
            var x = TestClass.GetSample();
            var y = TestClass.GetSample();

            Assert.True(doc.HaveSameAllMembers(x, y));
        }

        [Fact]
        public void HaveSameAllMembers_False()
        {
            var deepObjectComparer = new DeepObjectComparer(false);
            var x = TestClass.GetSample();
            var y = TestClass.GetSample_WithDifferentPublicProperty();

            Assert.False(deepObjectComparer.HaveSameAllMembers(x, y));
        }

        [Fact]
        public void HaveSameAllMembers_WithDifferentListItems_False()
        {
            var deepObjectComparer = new DeepObjectComparer();
            var x = TestClass.GetSample();
            var y = TestClass.GetSample_WithDifferentListItems();

            Assert.False(deepObjectComparer.HaveSameAllMembers(x, y));
        }

        [Fact]
        public void HaveSameAllMembers_WithLargerList_False()
        {
            var deepObjectComparer = new DeepObjectComparer();
            var x = TestClass.GetSample();
            var y = TestClass.GetSample_WithLargerList();

            Assert.False(deepObjectComparer.HaveSameAllMembers(x, y));
        }

        [Fact]
        public void HaveSameAllMembers_WithDifferentArrayItems_False()
        {
            var deepObjectComparer = new DeepObjectComparer();
            var x = TestClass.GetSample();
            var y = TestClass.GetSample_WithDifferentArrayItems();

            Assert.False(deepObjectComparer.HaveSameAllMembers(x, y));
        }

        [Fact]
        public void HaveSameAllMembers_WithLargerArray_False()
        {
            var deepObjectComparer = new DeepObjectComparer();
            var x = TestClass.GetSample();
            var y = TestClass.GetSample_WithLargerArray();

            Assert.False(deepObjectComparer.HaveSameAllMembers(x, y));
        }

        [Fact]
        public void HashSetAsEnumerableTest()
        {
            var deepObjectComparer = new DeepObjectComparer();
            var x = TestClass.GetSample();
            var y = TestClass.GetSample();

            x.LoadedPublicField = new HashSet<int> { 1, 2, 3 };
            y.LoadedPublicField = new HashSet<int> { 1, 3, 2 };

            x.LoadedPublicProperty = new HashSet<int> { 1, 2, 3 };
            y.LoadedPublicProperty = new HashSet<int> { 1, 3, 2 };

            Assert.False(deepObjectComparer.HaveSameAllMembers(x, y));
            Assert.False(deepObjectComparer.HaveSameFields(x, y));
            Assert.False(deepObjectComparer.HaveSamePublicProperties(x, y));
        }

        [Fact]
        public void OmittedTypes_ByObjectType_Assignable_True()
        {
            var deepObjectComparer =
                new DeepObjectComparer(true,
                                       c => c.AddOmittedType(typeof(OmittedClass),
                                                             MemberComparisonFlags.ByObjectType,
                                                             compareAssignableTo: true));

            var x = new AllowedClass();
            var y = new AllowedClass();
            x.InstanceOnClass.NumberPublicProperty = 4;
            y.InstanceOnClass.NumberPublicProperty = 5;

            Assert.True(deepObjectComparer.HaveSameAllMembers(x, y));
            Assert.True(deepObjectComparer.HaveSameFields(x, y));
            Assert.True(deepObjectComparer.HaveSamePublicProperties(x, y));
        }

        [Fact]
        public void OmittedTypes_ByObjectType_Assignable_False()
        {
            var deepObjectComparer = new DeepObjectComparer(false,
                                                            c => c.AddOmittedTypes(new[] { typeof(IOmittedClass) },
                                                                                   MemberComparisonFlags.ByObjectType,
                                                                                   compareAssignableTo: false));

            var x = new AllowedClass();
            var y = new AllowedClass();
            x.InstanceOnClass.NumberPublicProperty = 4;
            y.InstanceOnClass.NumberPublicProperty = 5;

            Assert.False(deepObjectComparer.HaveSameAllMembers(x, y));
            Assert.False(deepObjectComparer.HaveSameFields(x, y));
            Assert.False(deepObjectComparer.HaveSamePublicProperties(x, y));
        }

        [Fact]
        public void OmittedTypes_ByMemberType_False()
        {
            var deepObjectComparer = new DeepObjectComparer(true,
                                                            c => c.AddOmittedType(typeof(IOmittedClass),
                                                                                  MemberComparisonFlags.ByMemberType,
                                                                                  compareAssignableTo: true));

            var x = new AllowedClass();
            var y = new AllowedClass();

            x.InstanceOnInterface.NumberPublicProperty = 4;
            y.InstanceOnInterface.NumberPublicProperty = 5;

            Assert.True(deepObjectComparer.HaveSameAllMembers(x, y));
            Assert.True(deepObjectComparer.HaveSameFields(x, y));
            Assert.True(deepObjectComparer.HaveSamePublicProperties(x, y));
        }

        private class AllowedClass
        {
            public IOmittedClass InstanceOnInterface { get; set; } = new OmittedClass();

            public OmittedClass InstanceOnClass { get; set; } = new OmittedClass();
        }

        private class OmittedClass : IOmittedClass
        {
            public int NumberPublicProperty { get; set; }
        }

        private interface IOmittedClass
        {
            public int NumberPublicProperty { get; set; }
        }

        [Fact]
        public void HashSetAsEnumerableTest_3()
        {
            var deepObjectComparer = new DeepObjectComparer();
            var x = new ClassWithObjectProperty();
            var y = new ClassWithObjectProperty();

            x.MyHashSet = new HashSet<int> { 1, 2, 3 };
            y.MyHashSet = new HashSet<int> { 1, 3, 2 };

            Assert.False(deepObjectComparer.HaveSameAllMembers(x, y));
            Assert.False(deepObjectComparer.HaveSameFields(x, y));
            Assert.False(deepObjectComparer.HaveSamePublicProperties(x, y));
        }

        private class ClassWithObjectProperty
        {
            public object MyHashSet { get; set; }
        }

        [Fact]
        public void HashSetAsEnumerableTest_2()
        {
            var deepObjectComparer = new DeepObjectComparer();

            var y = new HashSet<int> { 1, 3, 2 };
            var x = new HashSet<int> { 1, 2, 3 };

            deepObjectComparer.HaveSameFields(x, y);
            deepObjectComparer.HaveSamePublicProperties(x, y);

            Assert.False(deepObjectComparer.HaveSameAllMembers(x, y));
            Assert.False(deepObjectComparer.HaveSameFields(x, y));
            Assert.False(deepObjectComparer.HaveSamePublicProperties(x, y));
        }

        [Fact]
        public void HaveSamePublicProperties_WithDifferentPublicProperty_False()
        {
            var deepObjectComparer = new DeepObjectComparer();
            var x = TestClass.GetSample();
            var y = TestClass.GetSample_WithDifferentPublicProperty();

            Assert.False(deepObjectComparer.HaveSamePublicProperties(x, y));
        }

        [Fact]
        public void HaveSamePublicProperties_WithDifferentFields_True()
        {
            var deepObjectComparer = new DeepObjectComparer();
            var x = TestClass.GetSample();
            var y = TestClass.GetSample_WithDifferentPrivateField();
            x.NumberPublicField = 3;
            y.NumberPublicField = 4;

            Assert.True(deepObjectComparer.HaveSamePublicProperties(x, y));
        }

        [Fact]
        public void HaveSamePublicProperties_WithDifferentPrivateProperty_True()
        {
            var deepObjectComparer = new DeepObjectComparer();
            var x = TestClass.GetSample();
            var y = TestClass.GetSample_WithDifferentPrivateProperty();

            Assert.True(deepObjectComparer.HaveSamePublicProperties(x, y));
        }

        [Fact]
        public void HaveSameFields_WithDifferentPrivateProperty_False()
        {
            var deepObjectComparer = new DeepObjectComparer();
            var x = TestClass.GetSample();
            var y = TestClass.GetSample_WithDifferentPrivateProperty();

            Assert.False(deepObjectComparer.HaveSameFields(x, y));
        }

        [Fact]
        public void HaveSameFields_WithDifferentPublicProperty_False()
        {
            var deepObjectComparer = new DeepObjectComparer();
            var x = TestClass.GetSample();
            var y = TestClass.GetSample_WithDifferentPublicProperty();

            Assert.False(deepObjectComparer.HaveSameFields(x, y));
        }

        [Fact]
        public void HaveSameFields_False()
        {
            var deepObjectComparer = new DeepObjectComparer();
            var x = TestClass.GetSample();
            var y = TestClass.GetSample_WithDifferentPrivateProperty();
            x.NumberPublicField = 7;
            y.NumberPublicField = 3;

            Assert.False(deepObjectComparer.HaveSameFields(x, y));
        }

        #pragma warning disable CS0414 // Avoid unused private fields
        #pragma warning disable CA1823 // Avoid unused private fields
        private class TestClass : ITestClass
        {
            public TestClass ParentPublicProp { get; set; }

            private TestClass ParentPrivateProp { get; set; }

            public TestClass ParentPublicField;
            private TestClass ParentPrivateField;

            public string NamePublicProp { get; set; }

            private string NamePrivateProp { get; set; }

            public string NamePublicField = "PS1";

            private string NamePrivateField = "PS2";

            public Exception ExPublicProp { get; set; }

            private Exception ExPrivateProp { get; set; }

            public int NumberPublicProperty { get; set; } = 8;

            private int NumberPrivateProperty { get; set; } = 9;

            public int NumberPublicField = 10;

            private int NumberPrivateField = 11;

            public List<int> ListValsPublicProp { get; set; } = new List<int> { 1, 2, 3 };

            public List<int> ListValsPublicField = new List<int> { 1, 2, 3 };

            public int[] ArrayValsPublicProp { get; set; } = new int[] { 1, 2, 3 };

            public int[] ArrayValsPublicField = new int[] { 1, 2, 3 };

            public object LoadedPublicProperty { get; set; } = new object();

            public object LoadedPublicField = new object();

            public static TestClass GetSample()
            {
                var sampleP = new TestClass
                    { ExPublicProp = new Exception("This is exception2"), NamePublicProp = "ParentOne", NumberPublicProperty = 1 };

                var sample = new TestClass
                    { ExPublicProp = new Exception("This is exception1"), NamePublicProp = "MainSon", NumberPublicProperty = 2 };

                sample.ParentPrivateField = sampleP;
                sample.ParentPublicField = sampleP;
                sample.ParentPrivateProp = sampleP;
                sample.ParentPublicProp = sampleP;

                sampleP.ParentPrivateProp = sample;
                sampleP.ParentPublicProp = sample;
                sampleP.ParentPublicField = sample;
                sampleP.ParentPrivateField = sample;

                return sample;
            }

            public static TestClass GetSample_WithDifferentListItems()
            {
                var sample = GetSample();
                sample.ListValsPublicProp = new List<int> { 1, 3, 2 };
                sample.ListValsPublicField = new List<int> { 1, 3, 2 };

                return sample;
            }

            public static TestClass GetSample_WithLargerList()
            {
                var sample = GetSample();
                sample.ListValsPublicProp = new List<int> { 1, 2, 3, 4 };
                sample.ListValsPublicField = new List<int> { 1, 2, 3, 4 };

                return sample;
            }

            public static TestClass GetSample_WithDifferentArrayItems()
            {
                var sample = GetSample();
                sample.ArrayValsPublicProp = new int[] { 1, 0, 3 };
                sample.ArrayValsPublicField = new int[] { 1, 0, 3 };

                return sample;
            }

            public static TestClass GetSample_WithLargerArray()
            {
                var sample = GetSample();
                sample.ArrayValsPublicProp = new int[] { 1, 2, 3, 4 };
                sample.ArrayValsPublicField = new int[] { 1, 2, 3, 4 };

                return sample;
            }

            public static TestClass GetSample_WithHashSet()
            {
                var sample = GetSample();
                sample.LoadedPublicProperty = new HashSet<int>();
                sample.LoadedPublicField = new HashSet<int>();

                return sample;
            }

            public static TestClass GetSample_WithDifferentPublicProperty()
            {
                var sample = GetSample();
                sample.NumberPublicProperty = 88;

                return sample;
            }

            public static TestClass GetSample_WithDifferentPrivateProperty()
            {
                var sample = GetSample();
                sample.NumberPrivateProperty = 7;

                return sample;
            }

            public static TestClass GetSample_WithDifferentPublicField()
            {
                var sample = GetSample();
                sample.NumberPublicField = 189;

                return sample;
            }

            public static TestClass GetSample_WithDifferentPrivateField()
            {
                var sample = GetSample();
                sample.NamePrivateField = "Different text";

                return sample;
            }

            public static TestClass GetSampleA_WithDifferentPublicProperty_InParent()
            {
                var sample = GetSample();
                sample.ParentPublicProp.NamePublicProp = "Different text in parent property";

                return sample;
            }

            public static TestClass GetSampleA_WithDifferentPrivateField_InParent()
            {
                var sample = GetSample();
                sample.ParentPublicProp.NamePrivateField = "Different text in parent";

                return sample;
            }

            public static TestClass GetSampleB()
            {
                var parent = new TestClass
                    { ExPublicProp = new Exception("This is exception"), NamePublicProp = "MainOne1", NumberPublicProperty = 1 };

                var sample = new TestClass
                {
                    ExPublicProp = new Exception("This is exception"), NamePublicProp = "Ancestor", ParentPublicProp = parent,
                    NumberPublicProperty = 2, ParentPublicField = parent,
                };

                sample.NumberPublicProperty = 45;
                sample.NumberPublicField = 78;

                return sample;
            }

            //private enum NoNoneEnum
            //{
            //    One = 1,
            //    Two = 2,
            //}
        }
        #pragma warning restore CA1823 // Avoid unused private fields
        #pragma warning restore CS0414 // Avoid unused private fields

        public interface ITestClass
        {
        }
    }
}
