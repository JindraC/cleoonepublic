﻿// <copyright file="JsonExtensionsTests.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.DotNet.JsonText;
using CleoOne.Common.Core.DotNet.NewtonsoftSerialization;
using System;
using System.Collections.Generic;
using Xunit;

namespace CleoOne.Common.Core.Tests.DotNet
{
    public class JsonExtensionsTests
    {
        private class WithNonDefaultCtorBase
        {
            public WithNonDefaultCtorBase(double a)
            {
                ReadonlyProp = (a + 1).ToString();
            }

            public string ReadonlyProp { get; }

            public string Prop { get; set; }
        }

        private class WithNonDefaultCtor : WithNonDefaultCtorBase
        {
            public WithNonDefaultCtor(double a)
                : base(a)
            {
            }
        }

        [Fact]
        public void ConversionOf_NonDefault_Constructor_And_ReadOnlyProperties()
        {
            var obj = new WithNonDefaultCtor(1) { Prop = "Haha" };
            var a = NewtonsoftJsonSerializer.Serialize(obj);
            var b = JsonDotNetSerializer.Serialize(obj, serializeNotLogged: true);

            Assert.Equal(a, b);
        }

        private class WithDecimal
        {
            public decimal Decimal { get; set; }
        }

        [Fact]
        public void HandlingOf_Decimals_AsStrings()
        {
            var json = "{\"Decimal\" : \"12.3\"}";
            var a = NewtonsoftJsonSerializer.DeserializeJson<WithDecimal>(json);
            var b = JsonDotNetSerializer.DeserializeJson<WithDecimal>(json);

            Assert.Equal(a.Decimal, b.Decimal);
            Assert.Equal(12.3m, a.Decimal);
        }

        [Fact(Skip = "We have not yet implemented not logged for the JsonText. And we probably do not needed.")]
        public void NotLoggedPropertiesAreNotLogged()
        {
            throw new NotImplementedException();
        }

        private enum TestEnum
        {
            None,
            USD,
            UsDollar,
            euroDollar,
        }

        [Fact]
        public void EnumDictionary_IsSerialized_AsExpected()
        {
            var dict = new Dictionary<TestEnum, int>()
            {
                { TestEnum.USD, 1 },
                { TestEnum.UsDollar, 2 },
                { TestEnum.euroDollar, 3 },
            };

            var obj = new { dict = dict };

            var newton = NewtonsoftJsonSerializer.Serialize(obj, serializeNotLogged: true);
            var jsonText = JsonDotNetSerializer.Serialize(obj, serializeNotLogged: true);

            Assert.Equal(newton, jsonText);

            // camel cased
            var newtonCamel = NewtonsoftJsonSerializer.SerializeCamelCase(obj, serializeNotLogged: true);
            var jsonTextCamel = JsonDotNetSerializer.SerializeCamelCase(obj, serializeNotLogged: true);

            Assert.Equal(newtonCamel, jsonTextCamel);

            var expectedJson =
                @"{
  ""dict"": {
    ""usd"": 1,
    ""usDollar"": 2,
    ""euroDollar"": 3
  }
}";

            Assert.Equal(expectedJson, jsonTextCamel);
        }

        [Fact]
        public void StringDictionary_IsSerialized_AsExpected()
        {
            var dict = new Dictionary<string, int>()
            {
                { "USD", 1 },
                { "UsDollar", 2 },
                { "euroDollar", 3 },
            };

            var obj = new { dict = dict };

            var newton = NewtonsoftJsonSerializer.Serialize(obj, serializeNotLogged: true);
            var jsonText = JsonDotNetSerializer.Serialize(obj, serializeNotLogged: true);

            Assert.Equal(newton, jsonText);

            // camel cased
            var newtonCamel = NewtonsoftJsonSerializer.SerializeCamelCase(obj, serializeNotLogged: true);
            var jsonTextCamel = JsonDotNetSerializer.SerializeCamelCase(obj, serializeNotLogged: true);

            Assert.Equal(newtonCamel, jsonTextCamel);

            var expectedJson =
                @"{
  ""dict"": {
    ""USD"": 1,
    ""UsDollar"": 2,
    ""euroDollar"": 3
  }
}";

            Assert.Equal(expectedJson, jsonTextCamel);
        }
    }
}
