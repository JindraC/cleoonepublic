﻿// <copyright file="SelfRefreshingCacheTests.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.Common.Components.Times;
using CleoOne.Common.Core.DotNet.Caches;
using CleoOne.Common.TestUtils.Helpers;
using CleoOne.Common.TestUtils.Traits.Attributes;
using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace CleoOne.Common.Core.Tests.DotNet.Caches
{
    [SuppressMessage("ReSharper", "UselessBinaryOperation", Justification = "Approved")]
    public class SelfRefreshingCacheTests
    {
        [IgnoreInCi]
        [Fact]
        public void SelfRefreshingCacheDisposableByWeakReferenceTest()
        {
            const int CacheTimeout = 10;
            int callCounter = 0;

            void CreateCacheWaitAndForgetIt()
            {
                #pragma warning disable CA2000 // Dispose objects before losing scope
                var cache = new SelfRefreshingCache<DateTime>(TimeSpan.FromMilliseconds(CacheTimeout),
                                                              () =>
                                                              {
                                                                  Interlocked.Increment(ref callCounter);

                                                                  return DateTime.UtcNow;
                                                              });
                #pragma warning restore CA2000 // Dispose objects before losing scope

                var firstTime = cache.Value;

                while (true)
                {
                    var secondTime = cache.Value;

                    // We wait till self-refreshing loop is activated (it may take some time e.g. in BitBucket pipeline)
                    if (secondTime != firstTime)
                    {
                        break;
                    }

                    Thread.Sleep(CacheTimeout);
                }

                cache = null;
            }

            CreateCacheWaitAndForgetIt();

            GC.Collect(2, GCCollectionMode.Forced, true, true);
            GC.WaitForPendingFinalizers();

            var stoppedCallCounter = callCounter;

            for (int i = 0; i < 10; i++)
            {
                Thread.Sleep(100);

                // The call counter should not change .. if it changes, the cache was not properly collected and disposed
                Assert.Equal(stoppedCallCounter, callCounter);
            }
        }

        [IgnoreInCi] // Manual because in CI behaved inconsistently (sometimes passing, sometimes failing)
        [Fact]
        public void TestSelfRefreshingCache()
        {
            // Initiation of 3 ThreadPool threads:
            var startedThreadsCount = 0;
            using var initializedResetEventSlim = new ManualResetEventSlim();

            for (int i = 0; i < 3; i++)
            {
                Task.Run(() =>
                {
                    Interlocked.Increment(ref startedThreadsCount);
                    initializedResetEventSlim.Wait();
                });
            }

            while (startedThreadsCount < 3)
            {
            }

            initializedResetEventSlim.Set();

            var multiplier = TestLoggingHelper.IsInCi() ? 30 : 1; // Increase value when debugging
            var time = RealTime.Instance;

            void WaitExact(int milliseconds)
            {
                var stopwatch = Stopwatch.StartNew();

                while (stopwatch.ElapsedMilliseconds < milliseconds)
                {
                }
            }

            var refreshCounter = 0;

            using var refreshingCache = new SelfRefreshingCache<DateTime>(TimeSpan.FromMilliseconds(100) * multiplier,
                                                                          TimeSpan.FromMilliseconds(1000) * multiplier,
                                                                          time,
                                                                          () =>
                                                                          {
                                                                              Interlocked.Increment(ref refreshCounter);

                                                                              return time.UtcNow;
                                                                          });

            WaitExact(150);

            // cache should be lazy, therefore refresh counter should be still 0
            Assert.Equal(0, refreshCounter);

            var firstCached = refreshingCache.Value;
            var firstNow = time.UtcNow;
            var firstDifference = (firstNow - firstCached).TotalMilliseconds;

            // we got value for the first time: there should be 1 call
            Assert.Equal(1, refreshCounter);
            // the current time value and cached value should be almost same (the calls should have happened at the same time)
            Assert.InRange(firstDifference, -10 * multiplier, 10 * multiplier);

            WaitExact(50 * multiplier);

            var secondCached = refreshingCache.Value;
            var secondNow = time.UtcNow;
            var secondDifference = (secondNow - secondCached).TotalMilliseconds;

            // we should still get the cached value, since the minimal lifetime is 100 ms and now it is 50 ms after the first call:
            Assert.Equal(1, refreshCounter);
            // we should get the cached value, which should be approx. 50 ms old
            Assert.InRange(secondDifference, 40 * multiplier, 80 * multiplier);

            WaitExact(100 * multiplier);

            var thirdCached = refreshingCache.Value;
            var thirdNow = time.UtcNow;
            var thirdDifference = (thirdNow - thirdCached).TotalMilliseconds;

            // after another 50 ms the self refresh should refresh the value internally in the cache
            Assert.Equal(2, refreshCounter);
            // since we waited 100 ms (50 ms more than when the cache should have been refreshed), then
            // the cached value should be approx. 50 ms old
            Assert.InRange(thirdDifference, 20 * multiplier, 80 * multiplier);

            WaitExact(100 * multiplier);

            // after waiting another 100 ms, there should be one more refresh:
            Assert.Equal(3, refreshCounter);

            var fourthCached = refreshingCache.Value;

            // and since we should be getting the already refreshed value (and we did not wait meanwhile), there should be no more calls
            Assert.Equal(3, refreshCounter);

            var fifthCached = refreshingCache.Value;

            // and since we should be getting the already refreshed value (and we did not wait meanwhile), there should be no more calls
            Assert.Equal(3, refreshCounter);
        }
    }
}
