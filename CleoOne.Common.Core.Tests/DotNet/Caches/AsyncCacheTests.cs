﻿// <copyright file="AsyncCacheTests.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace CleoOne.Common.Core.Tests.DotNet.Caches
{
    public class AsyncCacheTests
    {
        /*
                private readonly ITestOutputHelper _testOutputHelper;
                private DateTime _startDateTime;
                private int _loadFunctionCycle;
        
                public AsyncCacheTests(ITestOutputHelper testOutputHelper)
                {
                    _testOutputHelper = testOutputHelper;
                }
        
                [Fact]
                public void ParallelGet()
                {
                    var settings = new DictionaryCacheAsyncSettings()
                    {
                        ItemRefreshPeriodInSeconds = 10,
                        ItemValidityAfterRefreshPeriodInSeconds = 10,
                        MaxItems = 100,
                        RetrialCount = 1,
                        RetrialDelayMilliseconds = 1000
                    };
        
                    //var cache = new DictionaryCacheAsync<int, int>(settings, MockLoadFunction);
                    
                    Console.WriteLine($"Test Started.");
        
                    Parallel.Invoke(CycleUpdater);
        
                    for(var count = 1; count <= 100; count++)
                    {
                        Parallel.Invoke(async () => await GetCacheValueWrapped(),
                                        async () => await GetCacheValueWrapped(),
                                        async () => await GetCacheValueWrapped(),
                                        async () => await GetCacheValueWrapped(),
                                        async () => await GetCacheValueWrapped());
        
                        Thread.Sleep(1000);
                    }
        
                    async Task GetCacheValueWrapped()
                    {
                        var testData = new TestData();
        
                        Thread.Sleep(new Random().Next(0, 5000));
        
                        //testData.Result = await cache.GetAsync(1);
        
                        _testOutputHelper.WriteLine($"{TimeToSeconds(testData.RequestCreated)} - {TimeToSeconds(DateTime.Now)} - {testData.Result}");
                    }
        
                    decimal TimeToSeconds(DateTime time)
                    {
                        return (_startDateTime.ToUnixMilliseconds() - time.ToUnixMilliseconds()) / 1000;
                    }
                }
        
                private async Task<int> MockLoadFunction(int input)
                {
                    Thread.Sleep(new Random().Next(100, 5000));
        
                    return input * _loadFunctionCycle;
                }
        
                private void CycleUpdater()
                {
                    while (true)
                    {
                        if (_startDateTime.ToUnixMilliseconds() % (10 * 1000) == 0)
                        {
                            _loadFunctionCycle++;
                        }
                    }
                }
        
                private class TestData
                {
                    public DateTime RequestCreated { get; set; }
        
                    public DateTime RequestDelivered { get; set; }
        
                    public int Result { get; set; }
        
                    internal TestData()
                    {
                        RequestCreated= DateTime.Now;
                    }
                }*/
    }
}
