﻿// <copyright file="LoggingTests.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.DotNet.Extensions.Classes;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Extensions.Logging;
using Serilog.Sinks.TestCorrelator;
using System;
using System.Linq;
using System.Threading;
using Xunit;
using ILogger = Microsoft.Extensions.Logging.ILogger;

namespace CleoOne.Common.Core.Tests.Common.Logging
{
    public class LoggingTests : IDisposable
    {
        private readonly ILogger _logger;

        private readonly SerilogLoggerFactory _serilogFactory;

        public LoggingTests()
        {
            var serilogLogger = new LoggerConfiguration().Enrich.FromLogContext().WriteTo.TestCorrelator().CreateLogger();

            _serilogFactory = new SerilogLoggerFactory(serilogLogger);

            _logger = _serilogFactory.CreateLogger("LoggingTesting");
        }

        [Fact]
        public void MultiplePropertyAssigned_ByProperty_LastOneWins()
        {
            using (TestCorrelator.CreateContext())
            {
                _logger.LogInformation("outside0");

                using (_logger.BeginScopeWith("A", 1))
                {
                    _logger.LogInformation("outer1");

                    using (_logger.BeginScopeWith("A", 2))
                    {
                        _logger.LogInformation("inner2");
                    }

                    _logger.LogInformation("outer3");
                }

                _logger.LogInformation("outside4");

                var eventLogs = TestCorrelator.GetLogEventsFromCurrentContext().ToList();

                Assert.True(!eventLogs[0].Properties.ContainsKey("A"));
                Assert.True(eventLogs[1].Properties["A"].ToString() == "1");
                Assert.True(eventLogs[2].Properties["A"].ToString() == "2");
                Assert.True(eventLogs[3].Properties["A"].ToString() == "1");
                Assert.True(!eventLogs[4].Properties.ContainsKey("A"));
            }
        }

        [Fact]
        public void LoggingScope_Flows_ToAnotherThread()
        {
            using (TestCorrelator.CreateContext())
            {
                using (_logger.BeginScopeWith("A", 1))
                {
                    _logger.LogInformation("outer1");

                    // on different thread
                    var thread = new Thread(_ =>
                    {
                        _logger.LogInformation("fromThread");
                    });

                    thread.Start();
                    thread.Join();

                    var eventLogs = TestCorrelator.GetLogEventsFromCurrentContext().ToList();

                    Assert.True(eventLogs.Single(x => x.MessageTemplate.Text == "fromThread").Properties["A"].ToString() == "1");
                }
            }
        }

        [Fact]
        public void LoggingScope_Does_Not_FlowUp()
        {
            using var manualResetEventInner = new ManualResetEventSlim();
            using var manualResetEventOuter = new ManualResetEventSlim();

            using (TestCorrelator.CreateContext())
            {
                using (_logger.BeginScopeWith("A", 1))
                {
                    _logger.LogInformation("outer1");

                    // on different thread
                    var thread = new Thread(_ =>
                    {
                        using (_logger.BeginScopeWith("A", 2))
                        {
                            _logger.LogInformation("fromThread");
                            // ReSharper disable once AccessToDisposedClosure
                            manualResetEventInner.Set();
                            // ReSharper disable once AccessToDisposedClosure
                            manualResetEventOuter.Wait();
                        }
                    })
                    {
                        Name = "OtherThread",
                    };

                    thread.Start();

                    // Wait until the other thread is inside inner scope
                    manualResetEventInner.Wait();

                    // now the context on other thread should be opened
                    _logger.LogInformation("originalThread");

                    // let the other thread to run (and close its scope)
                    manualResetEventOuter.Set();
                    thread.Join();

                    _logger.LogInformation("originalThread2");

                    var eventLogs = TestCorrelator.GetLogEventsFromCurrentContext().ToList();

                    Assert.True(eventLogs.Single(x => x.MessageTemplate.Text == "outer1").Properties["A"].ToString() == "1");
                    Assert.Equal("2", eventLogs.Single(x => x.MessageTemplate.Text == "fromThread").Properties["A"].ToString());

                    // other thread does not affect our scope
                    Assert.Equal("1", eventLogs.Single(x => x.MessageTemplate.Text == "originalThread").Properties["A"].ToString());
                    Assert.True(eventLogs.Single(x => x.MessageTemplate.Text == "originalThread2").Properties["A"].ToString() == "1");
                }
            }
        }

        [Fact]
        public void LoggerWithProperty_WorksAsExpected()
        {
            using (TestCorrelator.CreateContext())
            {
                var loggerWithProperty = _logger.WithProperty("A", 1);

                _logger.LogInformation("without");

                loggerWithProperty.LogInformation("with");

                var withMultiple = _logger.WithProperties(("B", 1), ("C", 1));

                var withMultipleInherited = loggerWithProperty.WithProperties(("B", 1), ("C", 1));

                withMultiple.LogInformation("multiple");

                withMultipleInherited.LogInformation("multipleInherited");

                var eventLogs = TestCorrelator.GetLogEventsFromCurrentContext().ToList();

                Assert.True(!eventLogs.Single(x => x.MessageTemplate.Text == "without").Properties.ContainsKey("A"));
                Assert.Equal("1", eventLogs.Single(x => x.MessageTemplate.Text == "with").Properties["A"].ToString());

                Assert.Equal("1", eventLogs.Single(x => x.MessageTemplate.Text == "multiple").Properties["B"].ToString());
                Assert.Equal("1", eventLogs.Single(x => x.MessageTemplate.Text == "multiple").Properties["C"].ToString());
                Assert.False(eventLogs.Single(x => x.MessageTemplate.Text == "multiple").Properties.ContainsKey("A"));

                Assert.Equal("1", eventLogs.Single(x => x.MessageTemplate.Text == "multipleInherited").Properties["A"].ToString());
                Assert.Equal("1", eventLogs.Single(x => x.MessageTemplate.Text == "multipleInherited").Properties["B"].ToString());
                Assert.Equal("1", eventLogs.Single(x => x.MessageTemplate.Text == "multipleInherited").Properties["C"].ToString());
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            _serilogFactory.Dispose();
        }
    }
}
