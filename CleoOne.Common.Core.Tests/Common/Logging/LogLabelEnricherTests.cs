﻿// <copyright file="LogLabelEnricherTests.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.Common.Logging;
using CleoOne.Common.Core.DotNet.Extensions.Classes;
using Serilog;
using Serilog.Extensions.Logging;
using Serilog.Sinks.TestCorrelator;
using System;
using System.Linq;
using Xunit;
using ILogger = Microsoft.Extensions.Logging.ILogger;

namespace CleoOne.Common.Core.Tests.Common.Logging
{
    public class LogLabelEnricherTests : IDisposable
    {
        private readonly ILogger _logger;

        private readonly SerilogLoggerFactory _serilogFactory;

        public LogLabelEnricherTests()
        {
            var serilogLogger = new LoggerConfiguration()
                               .Enrich.FromLogContext()
                               .Enrich.With<LogLabelEnricher>()
                               .WriteTo.TestCorrelator()
                               .CreateLogger();

            _serilogFactory = new SerilogLoggerFactory(serilogLogger);

            _logger = _serilogFactory.CreateLogger("LoggingTesting");
        }

        [Fact]
        public void LogLabelEnrichmentWorks()
        {
            using (TestCorrelator.CreateContext())
            {
                _logger.LogInformation(LogLabel.MethodCallMetrics, "Blah {MessageProp}.", 123);

                var logEvent = TestCorrelator.GetLogEventsFromCurrentContext().Single();
                Assert.Equal("123", logEvent.Properties["MessageProp"].ToString());

                var expectedLabel = '"' + LogLabel.MethodCallMetrics.ToString() + '"';
                var actualLabel = logEvent.Properties["LogLabel"].ToString();
                Assert.Equal(expectedLabel, actualLabel);
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            _serilogFactory.Dispose();
        }
    }
}
