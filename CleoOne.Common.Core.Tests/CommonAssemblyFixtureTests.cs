﻿// <copyright file="CommonAssemblyFixtureTests.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.TestUtils.Fixtures;
using System.Globalization;
using System.Threading;
using Xunit;

namespace CleoOne.Common.Core.Tests
{
    public class CommonAssemblyFixtureTests
    {
        private readonly SetCultureAssemblyFixture _fixture;

        // To ensure that the Fixture is injected
        public CommonAssemblyFixtureTests(SetCultureAssemblyFixture fixture)
        {
            _fixture = fixture;
        }

        [Fact]
        public void AssemblyFixtureTest()
        {
            Assert.Equal(1, _fixture.InstantiationCount);
        }

        [Fact]
        public void AssemblyFixtureTest2()
        {
            Assert.Equal(1, _fixture.InstantiationCount);
        }

        [Fact]
        public void CurrentCultureTest()
        {
            Assert.Equal(CultureInfo.InvariantCulture, CultureInfo.DefaultThreadCurrentCulture);
            Assert.Equal(CultureInfo.InvariantCulture, CultureInfo.DefaultThreadCurrentUICulture);
            Assert.Equal(CultureInfo.InvariantCulture, Thread.CurrentThread.CurrentCulture);
            Assert.Equal(CultureInfo.InvariantCulture, Thread.CurrentThread.CurrentUICulture);
        }
    }

    // public class AssemblyFixtureIsCreatedFro

    public class CommonAssemblyFixtureTests2
    {
        private readonly SetCultureAssemblyFixture _fixture;

        public CommonAssemblyFixtureTests2(SetCultureAssemblyFixture fixture)
        {
            _fixture = fixture;
        }

        [Fact]
        public void AssemblyFixtureTest()
        {
            Assert.Equal(1, _fixture.InstantiationCount);
        }
    }
}
