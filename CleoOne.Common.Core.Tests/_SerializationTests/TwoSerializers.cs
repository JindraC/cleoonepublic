﻿// <copyright file="TwoSerializers.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.DotNet.JsonText;
using CleoOne.Common.Core.DotNet.JsonText.CustomSerialization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Xunit;

namespace CleoOne.Common.Core.Tests._SerializationTests
{
    public class TwoSerializers
    {
        private class ResponseDto
        {
            public int Value { get; set; }

            public string String { get; set; }

            public int? Integer { get; set; }
        }

        [Route(nameof(ControllerA))]
        private class ControllerA : ControllerBase
        {
            [HttpGet]
            [Route("action")]
            public IActionResult Get()
            {
                return Ok(new ResponseDto()
                {
                    Value = 1,
                });
            }
        }

        [Route(nameof(ControllerB))]
        private class ControllerB : ControllerBase
        {
            [HttpGet]
            [Route("action")]
            [IgnoreNullJsonSerialization]
            public IActionResult Get()
            {
                return Ok(new ResponseDto()
                {
                    Value = 1,
                });
            }
        }

        [Fact]
        public async Task TestTwoSerializers()
        {
            var webHostBuilder = new WebHostBuilder().ConfigureServices(services =>
                                                      {
                                                          services.AddMvcCore()
                                                                  .ConfigureApplicationPartManager(manager =>
                                                                   {
                                                                       manager.FeatureProviders.Add(new MyControllerFeatureProvider());
                                                                   })
                                                                  .AddJsonOptions(jsonOptions =>
                                                                   {
                                                                       // FE needs explicit nulls, so it does not need to do checks for property present.
                                                                       jsonOptions.JsonSerializerOptions.IgnoreNullValues = false;

                                                                       jsonOptions.JsonSerializerOptions.AddDefaultCleoOptions();
                                                                   });

                                                          services.AllowIgnoreNullJsonSerialization();
                                                      })
                                                     .Configure(app =>
                                                      {
                                                          app.UseRouting();

                                                          app.UseEndpoints(r => r.MapControllers());
                                                      });

            using (var testServer = new TestServer(webHostBuilder))
            {
                var client = testServer.CreateClient();

                var aJson = await client.GetStringAsync(nameof(ControllerA) + "/action");
                var bJson = await client.GetStringAsync(nameof(ControllerB) + "/action");

                Assert.Contains("string", aJson);
                Assert.Contains("integer", aJson);

                Assert.DoesNotContain("string", bJson);
                Assert.DoesNotContain("integer", bJson);
            }
        }

        private class MyControllerFeatureProvider : IApplicationFeatureProvider<ControllerFeature>
        {
            public void PopulateFeature(IEnumerable<ApplicationPart> parts, ControllerFeature feature)
            {
                foreach (var candidate in new[] { typeof(ControllerA), typeof(ControllerB) })
                {
                    feature.Controllers.Add(candidate.GetTypeInfo());
                }
            }
        }
    }
}
