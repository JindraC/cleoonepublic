﻿// <copyright file="DependencyInjectionFixtureBase.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using Microsoft.Extensions.DependencyInjection;
using System;

namespace CleoOne.Common.TestUtils.Fixtures
{
    public abstract class DependencyInjectionFixtureBase : IDisposable
    {
        private ServiceProvider _serviceProvider;

        private readonly ServiceCollection _serviceCollection;

        public ServiceProvider ServiceProvider
        {
            get
            {
                // This is here because, we should lazily create the services only when they are used.
                lock (_serviceCollection)
                {
                    return _serviceProvider ??= _serviceCollection.BuildServiceProvider();
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Usage",
                                                         "CA2214:Do not call overridable methods in constructors",
                                                         Justification = "Allowed in test utils.")]
        protected DependencyInjectionFixtureBase()
        {
            _serviceCollection = new ServiceCollection();

            ConfigureServices(_serviceCollection);
        }

        protected abstract void ConfigureServices(ServiceCollection services);

        public void Dispose()
        {
            _serviceProvider?.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
