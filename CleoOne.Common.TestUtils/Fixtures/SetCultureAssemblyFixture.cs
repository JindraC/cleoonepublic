﻿// <copyright file="SetCultureAssemblyFixture.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.DotNet.Helpers;
using System.Threading;

namespace CleoOne.Common.TestUtils.Fixtures
{
    public class SetCultureAssemblyFixture
    {
        private static int _instanceCount = 0;

        public int InstantiationCount => _instanceCount;

        public SetCultureAssemblyFixture()
        {
            // Only for test that the Fixture was actually called.
            // InstantiationCount++;

            Interlocked.Increment(ref _instanceCount);

            CultureHelper.SetInvariantCulture();
        }
    }
}
