﻿// <copyright file="AutoFixturePostprocessors.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using AutoFixture;
using AutoFixture.Kernel;
using CleoOne.Common.Core.DotNet.Helpers;
using System;

namespace CleoOne.Common.TestUtils.Fixtures
{
    public static class AutoFixturePostProcessors
    {
        public static void AddPostProcessor(this IFixture fixture, ISpecimenCommand specimenCommand)
        {
            fixture.Behaviors.Add(new PostprocessorBehavior(specimenCommand, new AnyTypeSpecification()));
        }

        public static void AddPostProcessor<T>(this IFixture fixture, ISpecimenCommand specimenCommand)
        {
            fixture.Behaviors.Add(new PostprocessorBehavior(specimenCommand, new ExactTypeSpecification(typeof(T))));
        }

        public static void AddPostProcessor<T>(this IFixture fixture, Action<T> postProcessor)
        {
            fixture.Behaviors.Add(
                new PostprocessorBehavior(
                    new ActionSpecimenCommand<T>(postProcessor),
                    new ExactTypeSpecification(typeof(T))));
        }

        private class PostprocessorBehavior : ISpecimenBuilderTransformation
        {
            private readonly ISpecimenCommand _command;
            private readonly IRequestSpecification _specification;

            public PostprocessorBehavior(ISpecimenCommand command, IRequestSpecification specification)
            {
                Assertion.ArgumentNotNull(command, nameof(command));

                _command = command;
                _specification = specification;
            }

            public ISpecimenBuilderNode Transform(ISpecimenBuilder builder)
            {
                return new Postprocessor(builder, _command, _specification);
            }
        }
    }
}
