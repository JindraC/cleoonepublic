﻿// <copyright file="SetupLoggingForTestsAssemblyFixture.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.TestUtils.Helpers;
using Serilog;
using System;

namespace CleoOne.Common.TestUtils.Fixtures
{
    public class SetupLoggingForTestsAssemblyFixture : IDisposable
    {
        public SetupLoggingForTestsAssemblyFixture()
        {
            TestLoggingHelper.InitializeLogging();
        }

        public void Dispose()
        {
            // This is here to ensure that all of the test output is written
            GC.SuppressFinalize(this);
            Log.CloseAndFlush();
        }
    }
}
