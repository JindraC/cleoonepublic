﻿// <copyright file="TestHttpClientFactory.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using Microsoft.Extensions.DependencyInjection;
using System;
using System.Net.Http;

namespace CleoOne.Common.TestUtils.Mocks
{
    public static class TestHttpClientFactory
    {
        private static readonly Lazy<IHttpClientFactory> _httpClientFactory = new Lazy<IHttpClientFactory>(() =>
        {
            var serviceProvider = new ServiceCollection().AddHttpClient().BuildServiceProvider();
            var httpClientFactory = serviceProvider.GetService<IHttpClientFactory>();

            return httpClientFactory;
        });

        public static IHttpClientFactory GetHttpClientFactory() => _httpClientFactory.Value;
    }
}
