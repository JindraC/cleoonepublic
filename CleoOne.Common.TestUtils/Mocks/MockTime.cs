﻿// <copyright file="MockTime.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.Common.Components.Times;
using System;

namespace CleoOne.Common.TestUtils.Mocks
{
    public class MockTime : ITime
    {
        public DateTime UtcNow { get; private set; }

        public void SetTime(DateTime time)
        {
            UtcNow = time;
        }
    }
}
