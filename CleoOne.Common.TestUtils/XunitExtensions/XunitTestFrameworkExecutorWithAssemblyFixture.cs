// <copyright file="XunitTestFrameworkExecutorWithAssemblyFixture.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Collections.Generic;
using System.Reflection;
using Xunit.Abstractions;
using Xunit.Sdk;

namespace CleoOne.Common.TestUtils.XunitExtensions
{
    internal class XunitTestFrameworkExecutorWithAssemblyFixture : XunitTestFrameworkExecutor
    {
        public XunitTestFrameworkExecutorWithAssemblyFixture(AssemblyName assemblyName,
                                                             ISourceInformationProvider sourceInformationProvider,
                                                             IMessageSink diagnosticMessageSink)
            : base(assemblyName, sourceInformationProvider, diagnosticMessageSink)
        {
        }

        protected override async void RunTestCases(IEnumerable<IXunitTestCase> testCases,
                                                   IMessageSink executionMessageSink,
                                                   ITestFrameworkExecutionOptions executionOptions)
        {
            using (var assemblyRunner =
                new XunitTestAssemblyRunnerWithAssemblyFixture(TestAssembly,
                                                               testCases,
                                                               DiagnosticMessageSink,
                                                               executionMessageSink,
                                                               executionOptions))
            {
                await assemblyRunner.RunAsync();
            }
        }
    }
}
