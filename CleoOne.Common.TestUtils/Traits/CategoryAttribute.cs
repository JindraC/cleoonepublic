﻿// <copyright file="CategoryAttribute.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using Xunit.Sdk;

namespace CleoOne.Common.TestUtils.Traits
{
    [TraitDiscoverer(typeName: "CleoOne.Common.TestUtils.Traits." + nameof(CategoryDiscoverer), assemblyName: "CleoOne.Common.TestUtils")]
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
    #pragma warning disable CA1813 // Avoid unsealed attributes
    public class CategoryAttribute : Attribute, ITraitAttribute
        #pragma warning restore CA1813 // Avoid unsealed attributes
    {
        public IEnumerable<string> Categories { get; }

        public CategoryAttribute(string category)
        {
            Categories = new[] { category, };
        }

        /// <summary>
        /// When you want to assign multiple categories at once
        /// </summary>
        /// <param name="categories"></param>
        public CategoryAttribute(params string[] categories)
        {
            Categories = categories.ToArray();
        }
    }
}
