﻿// <copyright file="CategoryDiscoverer.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.DotNet.Extensions.Strings;
using System.Collections.Generic;
using Xunit.Abstractions;
using Xunit.Sdk;

namespace CleoOne.Common.TestUtils.Traits
{
    public class CategoryDiscoverer : ITraitDiscoverer
    {
        private readonly IMessageSink _messageSink;

        public CategoryDiscoverer(IMessageSink messageSink)
        {
            _messageSink = messageSink;
        }

        /// <summary>
        /// Gets the trait values from the Category attribute.
        /// </summary>
        /// <param name="traitAttribute">The trait attribute containing the trait values.</param>
        /// <returns>The trait values.</returns>
        public IEnumerable<KeyValuePair<string, string>> GetTraits(IAttributeInfo traitAttribute)
        {
            var categories = traitAttribute.GetNamedArgument<IEnumerable<string>>(nameof(CategoryAttribute.Categories));

            if (categories != null)
            {
                foreach (var category in categories)
                {
                    if (!category.IsNullOrEmpty())
                    {
                        yield return new KeyValuePair<string, string>("Category", category);
                    }
                }
            }
        }
    }
}
