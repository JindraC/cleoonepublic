﻿// <copyright file="TestCategory.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace CleoOne.Common.TestUtils.Traits
{
    public static class TestCategory
    {
        public const string NeedsDB = "NeedsDB";
        public const string Slow = "Slow";
        public const string IgnoreInCI = "IgnoreInCI";
        public const string LocalMongoNeeded = "LocalMongoNeeded";
        public const string Manual = "Manual";
        public const string NotATest = "NotATest";
    }
}
