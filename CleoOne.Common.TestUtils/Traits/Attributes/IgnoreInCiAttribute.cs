﻿// <copyright file="IgnoreInCiAttribute.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;

namespace CleoOne.Common.TestUtils.Traits.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public sealed class IgnoreInCiAttribute : CategoryAttribute
    {
        public IgnoreInCiAttribute()
            : base(TestCategory.IgnoreInCI)
        {
        }
    }
}
