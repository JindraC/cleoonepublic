﻿// <copyright file="ContainerHelper.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.Common.Helpers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using System;
using System.IO;

namespace CleoOne.Common.TestUtils.DependencyInjection
{
    public static class ContainerHelper
    {
        private class TestWebHostEnvironment : IWebHostEnvironment
        {
            public string ApplicationName { get; set; }

            public IFileProvider ContentRootFileProvider { get; set; } = new NullFileProvider();

            public string ContentRootPath { get; set; } = Directory.GetCurrentDirectory();

            public string EnvironmentName { get; set; } = "Development";

            public IFileProvider WebRootFileProvider { get; set; } = new NullFileProvider();

            public string WebRootPath { get; set; } = Directory.GetCurrentDirectory();
        }

        public static IServiceCollection GetConfiguredServiceCollectionForStartUpClass<TStartUp>()
        {
            var configuration = ConfigurationHelper.GetConfigurationForService<TStartUp>();

            ServiceCollection services = new ServiceCollection();

            var instance = ActivatorUtilities.CreateInstance<TStartUp>(null, configuration, new TestWebHostEnvironment());

            var configureMethod = typeof(TStartUp).GetMethod("ConfigureServices");

            if (configureMethod != null)
            {
                configureMethod.Invoke(instance, new[] { services, });
            }
            else
            {
                throw new ArgumentException($"\"ConfigureServices\" method not found on type {typeof(TStartUp).Name}.");
            }

            return services;
        }
    }
}
