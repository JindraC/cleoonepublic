﻿// <copyright file="AssemblyFixtureDeclaration.cs" company="CLEO Finance Ltd.">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using CleoOne.Common.TestUtils.Attributes;
using CleoOne.Common.TestUtils.Fixtures;
using Xunit;

//The custom test framework enables the support
[assembly: TestFramework("CleoOne.Common.TestUtils.XunitExtensions.XunitTestFrameworkWithAssemblyFixture",
                         "CleoOne.Common.TestUtils")]

// Add one of these for every fixture classes for the assembly.
// Just like other fixtures, you can implement IDisposable and it'll
// get cleaned up at the end of the test run.
[assembly: AssemblyFixture(typeof(SetCultureAssemblyFixture))]
[assembly: AssemblyFixture(typeof(SetupLoggingForTestsAssemblyFixture))]
