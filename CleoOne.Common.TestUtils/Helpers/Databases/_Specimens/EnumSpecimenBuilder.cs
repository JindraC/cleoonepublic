﻿// <copyright file="EnumSpecimenBuilder.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using AutoFixture.Kernel;
using System;
using System.Linq;
using System.Reflection;
using static CleoOne.Common.Core.DotNet.Extensions.ValueTypes.DotNetEnumExtensions;

namespace CleoOne.Common.TestUtils.Helpers.Databases._Specimens
{
    public class EnumSpecimenBuilder : ISpecimenBuilder
    {
        public object Create(object request, ISpecimenContext context)
        {
            var type = request as Type ?? (request as SeededRequest)?.Request as Type;

            if (type != null)
            {
                TypeInfo typeInfo = type.GetTypeInfo();
                var isNullable = typeInfo.IsGenericType && typeInfo.GetGenericTypeDefinition() == typeof(Nullable<>);

                if (isNullable)
                {
                    type = type.GetGenericArguments().Single();
                }

                if (type.IsEnum
                    && ((type.FullName ?? string.Empty).StartsWith("CleoOne.") || (type.FullName ?? string.Empty).StartsWith("DSuite.")))
                {
                    return GetRandomEnumWithoutCleoOneNone(type);
                }
            }

            return new NoSpecimen();
        }
    }
}
