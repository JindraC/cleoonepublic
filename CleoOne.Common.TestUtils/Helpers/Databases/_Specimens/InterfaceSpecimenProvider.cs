﻿// <copyright file="InterfaceSpecimenProvider.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using AutoFixture.Kernel;
using System;
using System.Linq;

namespace CleoOne.Common.TestUtils.Helpers.Databases._Specimens
{
    public class InterfaceSpecimenProvider<TInterface> : ISpecimenBuilder
    {
        private readonly Random _random = new Random();
        private readonly Type[] _interfaceImplementors;

        public InterfaceSpecimenProvider()
        {
            if (!typeof(TInterface).IsInterface)
            {
                throw new InvalidOperationException(typeof(TInterface).Name + " must be interface.");
            }

            _interfaceImplementors = AppDomain.CurrentDomain
                                              .GetAssemblies()
                                              .SelectMany(assembly => assembly.GetTypes())
                                              .Where(t => !t.IsInterface && t.IsAssignableTo(typeof(TInterface)))
                                              .ToArray();

            if (_interfaceImplementors.Length == 0)
            {
                throw new InvalidOperationException($"No implementations for type {typeof(TInterface).Name} were found.");
            }
        }

        public object Create(object request, ISpecimenContext context)
        {
            if (request is Type type
                && type == typeof(TInterface))
            {
                var randomIndex = _random.Next(0, _interfaceImplementors.Length);

                return context.Resolve(_interfaceImplementors[randomIndex]);
            }

            return new NoSpecimen();
        }
    }
}
