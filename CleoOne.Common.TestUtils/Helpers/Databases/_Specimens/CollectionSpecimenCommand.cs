﻿// <copyright file="CollectionSpecimenCommand.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using AutoFixture.Kernel;
using CleoOne.Common.Core.DotNet.Collections;
using CleoOne.Common.Core.DotNet.Extensions.Classes;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CleoOne.Common.TestUtils.Helpers.Databases._Specimens
{
    public class CollectionSpecimenCommand : ISpecimenCommand
    {
        private readonly Random _random = new Random();

        public void Execute(object specimen, ISpecimenContext context)
        {
            ExecuteOnce(specimen, context, new HashSet<object>(ReferenceEqualityComparer.Instance));
        }

        public void ExecuteOnce(object specimen, ISpecimenContext context, HashSet<object> alreadyCalled)
        {
            if (specimen == null)
            {
                return;
            }

            var type = specimen.GetType();

            if (type.IsValueType)
            {
                return;
            }

            // This is check for if we have a collection, which implements IEnumerable but not ICollection (and we cannot add items to that by AutoFixture)
            if (type.ImplementsGenericInterface(typeof(IEnumerable<>))
                && !type.ImplementsGenericInterface(typeof(ICollection<>))
                && type != typeof(string)
                && type.Name != "ConvertedEnumerable`1")
            {
                throw new NotImplementedException(
                    $"Adding items to IEnumerable of type: {type.Name} is (most probably) not implemented. "
                    + $"ICollections are filled automatically, consider changing the type to ICollection<>.");
            }

            // prevention of StackOverflowException:
            var called = !alreadyCalled.Add(specimen);

            if (called)
            {
                return;
            }

            var genericType = type.IsGenericType ? type.GetGenericTypeDefinition() : null;

            if (genericType != typeof(List<>)
                && genericType != typeof(HashSet<>)
                && genericType != typeof(Dictionary<,>)
                && genericType != typeof(ConcurrentDictionary<,>))
            {
                // IMPORTANT: read-only properties do not come to any ISpecimenBuilder ...
                //            therefore we have to handle them already during handling the parent object!

                var readOnlyProperties = type.GetTypeInfo()
                                             .GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.GetProperty)
                                             .Where(propertyInfo => propertyInfo.GetSetMethod() == null
                                                                    && propertyInfo.GetIndexParameters().Length == 0)
                                             .ToList();

                if (type.InheritsFromGenericType(typeof(DictionaryList<,>)))
                {
                    readOnlyProperties = readOnlyProperties.Where(property => property.Name != "InnerDictionary"
                                                                              && property.Name != "InnerList"
                                                                              && property.Name != "Keys"
                                                                              && property.Name != "Values")
                                                           .ToList();
                }

                if (type.ImplementsGenericInterface(typeof(IDictionary<,>)))
                {
                    readOnlyProperties = readOnlyProperties.Where(property => property.Name != "Keys"
                                                                              && property.Name != "Values")
                                                           .ToList();
                }

                foreach (var readOnlyProperty in readOnlyProperties)
                {
                    var propertyValue = readOnlyProperty.GetValue(specimen);

                    ExecuteOnce(propertyValue, context, alreadyCalled);
                }
            }

            // we select not yet filled ICollections:
            if (!type.ImplementsGenericInterface(typeof(ICollection<>))
                || ((specimen as IEnumerable)?.Cast<object>().Any() ?? false))
            {
                return;
            }

            var iCollectionType = type.GetInterfaces()
                                      .Single(interfaceType => interfaceType.IsGenericType
                                                               && interfaceType.GetGenericTypeDefinition() == typeof(ICollection<>));

            var addMethod = iCollectionType.GetMethod("Add", BindingFlags.Instance | BindingFlags.Public);

            var isDictionary = type.ImplementsGenericInterface(typeof(IDictionary<,>));

            if (isDictionary)
            {
                // if the object implements also IDictionary - we give it preference before ICollection:

                var iDictionaryInterface = type.GetInterfaces()
                                               .Single(interfaceType => interfaceType.IsGenericType
                                                                        && interfaceType.GetGenericTypeDefinition()
                                                                        == typeof(IDictionary<,>));

                addMethod = iDictionaryInterface.GetMethod("Add", BindingFlags.Instance | BindingFlags.Public);
            }

            // for dictionary we insert just one element, because a collision may occur, for others anything from 1 to 3 items
            var count = _random.Next(1, 4);

            for (int i = 0; i < count; i++)
            {
                // here we add items to the ICollection/IDictionary

                var arguments = addMethod.GetParameters();

                if (arguments.Length == 1)
                {
                    var objectToInsert = context.Resolve(arguments[0].ParameterType);

                    // Equivalent to: ((ICollection<ArgumentType>)specimen).Add(objectToInsert);
                    CatchValueAlreadyAddedException(() => addMethod.Invoke(specimen, new object[] { objectToInsert, }));
                }
                else if (arguments.Length == 2)
                {
                    var objectToInsert = context.Resolve(arguments[0].ParameterType);
                    var objectToInsert2 = context.Resolve(arguments[1].ParameterType);

                    // Equivalent to: ((IDictionary<TKey,TValue>)specimen).Add(objectToInsert, objectToInsert2);
                    CatchValueAlreadyAddedException(() => addMethod.Invoke(specimen, new object[] { objectToInsert, objectToInsert2, }));
                }
                else
                {
                    throw new NotImplementedException();
                }
            }
        }

        private static void CatchValueAlreadyAddedException(Action action)
        {
            try
            {
                action();
            }
            catch (Exception e) when (e.ToMergedMessages().Contains("An item with the same key has already been added."))
            {
            }
        }
    }
}
