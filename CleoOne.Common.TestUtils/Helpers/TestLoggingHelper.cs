﻿// <copyright file="TestLoggingHelper.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using Serilog;
using Serilog.Events;
using System;
using System.IO;

namespace CleoOne.Common.TestUtils.Helpers
{
    public static class TestLoggingHelper
    {
        internal static void InitializeLogging()
        {
            var serilogConfiguration = new LoggerConfiguration().WriteTo.Console(LogEventLevel.Debug)
                                                                .Enrich.WithProperty("IsUnitTest", true)
                                                                .MinimumLevel.Debug();

            if (Directory.Exists(@"C:\Program Files\Seq")) // write to seq if installed
            {
                serilogConfiguration = serilogConfiguration.WriteTo.Seq("http://localhost:5341", LogEventLevel.Debug);
            }

            var serilogLogger = serilogConfiguration.CreateLogger();

            // assign also to the global logger in order to be sure that inner loggers use the same logger
            // This line is important, because it means that during unit test, when someone calls LoggerHelper.GetLoggerForThis(),
            // it will return this logger - because LoggerHelper use serilog factory which uses static logger saved in Serilog.Log.Logger property.

            Log.Logger = serilogLogger;
        }

        public static bool IsInCi()
        {
            return Environment.GetEnvironmentVariable("CI") == "true";
        }
    }
}
