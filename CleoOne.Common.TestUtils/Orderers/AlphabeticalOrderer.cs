﻿// <copyright file="AlphabeticalOrderer.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Collections.Generic;
using System.Linq;
using Xunit.Abstractions;
using Xunit.Sdk;

namespace CleoOne.Common.TestUtils.Orderers
{
    /// <summary>
    /// <example>[TestCaseOrderer("CleoOne.Common.TestUtils.Orderers.AlphabeticalOrderer", "CleoOne.Common.TestUtils")]</example>
    /// </summary>
    public class AlphabeticalOrderer : ITestCaseOrderer
    {
        public IEnumerable<TTestCase> OrderTestCases<TTestCase>(
            IEnumerable<TTestCase> testCases)
            where TTestCase : ITestCase => testCases.OrderBy(testCase => testCase.TestMethod.Method.Name);
    }
}
