﻿// <copyright file="DisplayNameOrderer.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Collections.Generic;
using System.Linq;
using Xunit;
using Xunit.Abstractions;

namespace CleoOne.Common.TestUtils.Orderers
{
    /// <summary>
    /// <example>[TestCaseOrderer("CleoOne.Common.TestUtils.Orderers.DisplayNameOrderer", "CleoOne.Common.TestUtils")]</example>
    /// </summary>
    public class DisplayNameOrderer : ITestCollectionOrderer
    {
        public IEnumerable<ITestCollection> OrderTestCollections(
            IEnumerable<ITestCollection> testCollections) => testCollections.OrderBy(collection => collection.DisplayName);
    }
}
