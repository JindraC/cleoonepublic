﻿// <copyright file="Helpers.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.Common.Helpers;
using CleoOne.Common.Core.Common.Web;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using System.Reflection;

namespace CleoOne.IntegrationTests.Common
{
    public static class Helpers
    {
        public static IWebHostBuilder CreateWebHostFromStartUp<TStartup>(string serviceName)
            where TStartup : class
        {
            var projectDir = ConfigurationHelper.GetReferencedProjectPath(typeof(TStartup).GetTypeInfo().Assembly);

            EnvironmentHelper.SetAsDevelopment();

            var configuration = StartUpHostBuilder.CreateHostConfiguration(typeof(TStartup));

            StartUpHostBuilder.SetUpSerilogLogging(configuration, serviceName + "_IntegrationTesting");

            var webHostBuilder =
                new WebHostBuilder()
                   .UseContentRoot(projectDir)
                   .UseConfiguration(configuration)
                   .UseStartup<TStartup>()
                   .ConfigureServices(services =>
                    {
                        services.AddMvc(options =>
                        {
                            options.Filters.Add(new AllowAnonymousFilter());
                            options.Filters.Add(new FakeUserFilter());
                        });

                        services.AddControllers().AddControllersAsServices();
                    })
                   .UseSerilog();

            return webHostBuilder;
        }
    }
}
