﻿// <copyright file="Extensions.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.DotNet.Extensions.Strings;
using CleoOne.Services.Api.Response;

namespace CleoOne.IntegrationTests.Common
{
    public static class Extensions
    {
        public static string GetErrorMessage(this IApiResponse response)
        {
            if (response.ErrorForDebug is { } errorForDebug)
            {
                var errorForDebugFriendly = new
                {
                    errorForDebug.ExceptionMessages,
                    errorForDebug.ExceptionStackTrace,
                    errorForDebug.DeeperDescription,
                    errorForDebug.LightValidationErrors,
                };

                return errorForDebugFriendly.ToJson();
            }

            return response.ErrorForDebug?.ToJson();
        }
    }
}
