﻿// <copyright file="FakeUserFilter.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CleoOne.IntegrationTests.Common
{
    internal class FakeUserFilter : IAsyncActionFilter
    {
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            string userIdentifier = null;

            if (context.HttpContext.Request.Headers.TryGetValue("fake-user-identifier", out var userIdentifierStringValues))
            {
                userIdentifier = userIdentifierStringValues.ToString();
            }
            else
            {
                if (context.HttpContext.Request.Headers.TryGetValue("fake-user-email", out var userEmailStringValues))
                {
                    throw new NotImplementedException();
                    //var userRepository = context.HttpContext.RequestServices.GetService<IUserRepository>();
                    //userIdentifier = (await userRepository.GetByEmail(userEmailStringValues.ToString())).UserIdentifier;
                }
            }

            if (string.IsNullOrEmpty(userIdentifier))
            {
                throw new Exception("Fake user not set in integration tests");
            }

            context.HttpContext.User = new ClaimsPrincipal(new ClaimsIdentity(new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, userIdentifier),
            }));

            await next();
        }
    }
}
