﻿// <copyright file="DependencyInjectionExtensions.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using Microsoft.Extensions.DependencyInjection;

namespace CleoOne.Common.Core.DependencyInjection
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddSingletonForBoth<TInterface, TImplementation>(this IServiceCollection services)
            where TInterface : class
            where TImplementation : class, TInterface
        {
            services.AddSingleton<TImplementation>();
            services.AddSingleton<TInterface>(sp => sp.GetRequiredService<TImplementation>());

            return services;
        }

        public static IServiceCollection AddSingletonForBoth<TInterface, TImplementation>(
            this IServiceCollection services,
            TImplementation singletonInstance)
            where TInterface : class
            where TImplementation : class, TInterface
        {
            services.AddSingleton<TImplementation>(singletonInstance);
            services.AddSingleton<TInterface>(singletonInstance);

            return services;
        }

        public static IServiceCollection AddSingletonForAll<TInterface1, TInterface2, TImplementation>(this IServiceCollection services)
            where TInterface1 : class
            where TInterface2 : class
            where TImplementation : class, TInterface1, TInterface2
        {
            services.AddSingleton<TImplementation>();
            services.AddSingleton<TInterface1>(sp => sp.GetRequiredService<TImplementation>());
            services.AddSingleton<TInterface2>(sp => sp.GetRequiredService<TImplementation>());

            return services;
        }
    }
}
