﻿// <copyright file="ICleoStartup.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace CleoOne.Common.Core.DependencyInjection
{
    public interface ICleoStartup
    {
    }
}
