﻿// <copyright file="ServiceCollectionExposer.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using Microsoft.Extensions.DependencyInjection;

namespace CleoOne.Common.Core.DependencyInjection
{
    public class ServiceCollectionExposer
    {
        public IServiceCollection ServiceCollection { get; }

        public ServiceCollectionExposer(IServiceCollection serviceCollection)
        {
            ServiceCollection = serviceCollection;
        }
    }
}
