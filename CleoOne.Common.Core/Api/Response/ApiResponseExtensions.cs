﻿// <copyright file="ApiResponseExtensions.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace CleoOne.Services.Api.Response
{
    public static class ApiResponseExtensions
    {
        //internal static void SetDebugExceptionInfo(this IApiResponse apiResponse, Exception exception)
        //{
        //    apiResponse.ErrorForDebug ??= new ApiErrorForDebug();

        //    apiResponse.ErrorForDebug.ExceptionMessages = exception.ToMergedMessages();
        //    apiResponse.ErrorForDebug.ExceptionStackTrace = exception.ToMergedStackTraces();
        //}

        public static void SetDebugLightValidationErrors(this IApiResponse apiResponse,
                                                           ModelValidationError[] modelValidationErrors)
        {
            apiResponse.ErrorForDebug ??= new ApiErrorForDebug();

            apiResponse.ErrorForDebug.LightValidationErrors = modelValidationErrors;
        }

        //public static void CopyIApiResponsePropertiesTo(this IApiResponse source, IApiResponse destination)
        //{
        //    destination.Successful = source.Successful;
        //    destination.ErrorForDebug = source.ErrorForDebug;
        //    destination.NotificationPopup = source.NotificationPopup;
        //    destination.ShowUserNotificationPopup = source.ShowUserNotificationPopup;
        //}
    }
}
