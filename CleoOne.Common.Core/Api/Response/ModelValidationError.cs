﻿// <copyright file="ModelValidationError.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace CleoOne.Services.Api.Response
{
    public class ModelValidationError
    {
        public string Path { get; set; }

        public string Property { get; set; }

        public string ErrorCode { get; set; }

        public string Error { get; set; }
    }
}
