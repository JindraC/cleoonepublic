﻿// <copyright file="ApiResponse.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using Newtonsoft.Json;
using System;

namespace CleoOne.Services.Api.Response
{
    public class ApiResponse<TResult> : ApiResponseEmpty
        // The class inheritance is here to prevent value type responses like ApiResponse<bool>,
        // because in those cases we cannot distinguish between default false value (forgotten to set) and properly set value to false
        where TResult : class
    {
        public TResult Result { get; }

        [JsonConstructor]
        protected ApiResponse()
            : base(false)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ApiResponse{TResult}"/> class
        /// as Successful with the Result.
        /// </summary>
        public ApiResponse(TResult result, IApiResponse apiResponse)
            : base(apiResponse)
        {
            if (!apiResponse.Successful)
            {
                throw new Exception("Cannot call this constructor with .Successful = false ApiResponse.");
            }

            Result = result;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ApiResponse{TResult}"/> class 
        /// with no Result.
        /// </summary>
        public ApiResponse(IApiResponse apiResponse)
            : base(apiResponse)
        {
        }
    }
}
