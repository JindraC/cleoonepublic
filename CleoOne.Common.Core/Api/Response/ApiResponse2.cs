﻿// <copyright file="ApiResponse2.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using Newtonsoft.Json;
using System;

#pragma warning disable SA1649 // File name should match first type name
namespace CleoOne.Services.Api.Response
{
    public class ApiResponse<TResult, TUnsuccessfulResult> : ApiResponse<TResult>
        where TResult : class
        where TUnsuccessfulResult : class
    {
        public TUnsuccessfulResult UnsuccessfulResult { get; }

        [JsonConstructor]
        protected ApiResponse()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ApiResponse{TResult, TUnsuccessfulResult}"/> class as 
        /// Successful with the Result.
        /// </summary>
        public ApiResponse(TResult result, IApiResponse apiResponse)
            : base(result, apiResponse)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ApiResponse{TResult, TUnsuccessfulResult}"/> class as
        /// Unsuccessful with UnsuccessfulResult.
        /// </summary>
        public ApiResponse(TUnsuccessfulResult unsuccessfulResult, IApiResponse apiResponse)
            : base(apiResponse)
        {
            UnsuccessfulResult = unsuccessfulResult;

            if (apiResponse.Successful)
            {
                throw new Exception("Cannot call this constructor with .Successful = true ApiResponse.");
            }
        }
    }
}
#pragma warning restore SA1649 // File name should match first type name
