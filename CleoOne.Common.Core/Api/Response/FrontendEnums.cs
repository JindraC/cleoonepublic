﻿// <copyright file="FrontendEnums.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace CleoOne.Services.Api.Models
{
    public enum PopupInformationEnum
    {
        None = 0,
    }

    public enum PopupSuccessEnum
    {
        None = 0,
    }

    public enum PopupWarningEnum
    {
        None = 0,
    }

    public enum PopupErrorEnum
    {
        None = 0,
        UnhandledException = 1,
    }
}
