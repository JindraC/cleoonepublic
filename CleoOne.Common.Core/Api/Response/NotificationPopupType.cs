﻿// <copyright file="NotificationPopupType.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace CleoOne.Services.Api.Response
{
    public enum NotificationPopupType
    {
        None = 0,
        Information = 1,
        Success = 2,
        Warning = 3,
        Error = 4,
    }
}
