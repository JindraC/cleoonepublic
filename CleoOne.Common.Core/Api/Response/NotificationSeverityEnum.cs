﻿// <copyright file="NotificationSeverityEnum.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace CleoOne.Services.Api.Response
{
    public enum NotificationSeverityEnum
    {
        None = 0,
        Info = 1,    // blue
        Success = 2, // green
        Warning = 3, // yellow
        Error = 4,   // red
    }
}
