﻿// <copyright file="FrontendNotificationPopup.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.DotNet.Extensions.Strings;
using CleoOne.Common.Core.DotNet.Extensions.ValueTypes;
using CleoOne.Services.Api.Models;

namespace CleoOne.Services.Api.Response
{
    // called often as Toastify after the framework we use on FE (but this can change, so not naming the class or properties based on it)
    public class FrontendNotificationPopup
    {
        public string PopupMessage { get; }

        public NotificationPopupType Type { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="FrontendNotificationPopup"/> class. <br/>
        /// Use static 'constructors' (<see cref="FrontendNotificationPopup.Error(FrontendNotificationPopup)"/>)
        /// instead of this one if possible.
        /// </summary>
        public FrontendNotificationPopup(string popupMessage, NotificationPopupType type)
        {
            PopupMessage = popupMessage;
            Type = type;
        }

        public static FrontendNotificationPopup Information(string popupMessage) =>
            new FrontendNotificationPopup(popupMessage, NotificationPopupType.Information);

        public static FrontendNotificationPopup Information(PopupInformationEnum popupInformationEnum) =>
            new FrontendNotificationPopup(popupInformationEnum.GetDescriptionOrToString(), NotificationPopupType.Information);

        public static FrontendNotificationPopup Success(string popupMessage) =>
            new FrontendNotificationPopup(popupMessage, NotificationPopupType.Success);

        public static FrontendNotificationPopup Success(PopupSuccessEnum popupSuccessEnum) =>
            new FrontendNotificationPopup(popupSuccessEnum.GetDescriptionOrToString(), NotificationPopupType.Success);

        public static FrontendNotificationPopup Warning(string popupMessage) =>
            new FrontendNotificationPopup(popupMessage, NotificationPopupType.Warning);

        public static FrontendNotificationPopup Warning(PopupWarningEnum popupWarningEnum) =>
            new FrontendNotificationPopup(popupWarningEnum.GetDescriptionOrToString(), NotificationPopupType.Warning);

        public static FrontendNotificationPopup Error(string popupMessage) =>
            new FrontendNotificationPopup(popupMessage, NotificationPopupType.Error);

        public static FrontendNotificationPopup Error(PopupErrorEnum popupErrorEnum) =>
            new FrontendNotificationPopup(popupErrorEnum.GetDescriptionOrToString(), NotificationPopupType.Error);

        public bool IsNotDefault()
        {
            return Type != default || !PopupMessage.IsNullOrEmpty();
        }
    }
}
