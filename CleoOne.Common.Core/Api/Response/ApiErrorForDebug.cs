﻿// <copyright file="ApiErrorForDebug.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.Common.Helpers;
using CleoOne.Common.Core.Common.Logging;
using CleoOne.Common.Core.DotNet.Extensions.Classes;
using CleoOne.Common.Core.DotNet.Helpers;
using System;
using System.Diagnostics;

namespace CleoOne.Services.Api.Response
{
    public class ApiErrorForDebug
    {
        public string DeeperDescription { get; set; }

        public ModelValidationError[] LightValidationErrors { get; set; }

        public string ExceptionMessages { get; set; }

        public string ExceptionStackTrace { get; set; }

        public string CorrelationId { get; }

        public string KibanaLink { get; }

        public ApiErrorForDebug()
        {
            CorrelationId = Activity.Current?.GetCorrelationIdOrNull();
            KibanaLink = CorrelationId != null ? GetKibanaLink(CorrelationId) : null;
        }

        public ApiErrorForDebug(string deeperDescription)
            : this()
        {
            DeeperDescription = deeperDescription;
        }

        public ApiErrorForDebug(Exception exception, string deeperDescription = null)
            : this(deeperDescription)
        {
            Assertion.ArgumentNotNull(exception, nameof(exception));

            ExceptionMessages = exception.ToMergedMessages();
            ExceptionStackTrace = exception.ToMergedStackTraces();
        }

        private string GetKibanaLink(string correlationId)
        {
            Assertion.ArgumentNotNull(correlationId, nameof(correlationId));

            return EnvironmentHelper.IsTesting()
                       ? KibanaLink("test")
                       : EnvironmentHelper.IsProduction()
                           ? KibanaLink("prod")
                           : null;

            string KibanaLink(string environment)
            {
                return $"http://kibana.{environment}.cleo.local/app/kibana#"
                       + "/discover?_g=(filters:!(),"
                       + "refreshInterval:(pause:!t,value:0),"
                       + "time:(from:now-1h,to:now))&_a=(columns:!('@m',ServiceName,'@l')," // from hour ago till now
                       + "filters:!(('$state':(store:appState),"
                       + $"meta:(alias:!n,disabled:!f,key:CorrelationId,negate:!f,params:(query:'{correlationId}'),type:phrase),"
                       + $"query:(match:(CorrelationId:(query:'{correlationId}',type:phrase))))),"
                       + "interval:auto,query:(language:kuery,query:''),sort:!(!('@t',desc)))";
            }
        }

        public string ToTestInformationDump()
        {
            return $"DeeperDescription: {DeeperDescription}\n"
                   + $"ExceptionMessages: {ExceptionMessages}\n"
                   + $"ExceptionStackTrace: {ExceptionStackTrace}";
        }
    }
}
