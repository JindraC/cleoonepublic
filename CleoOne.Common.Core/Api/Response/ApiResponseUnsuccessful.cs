﻿// <copyright file="ApiResponseUnsuccessful.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;

namespace CleoOne.Services.Api.Response
{
    public class ApiResponseUnsuccessful : ApiResponseEmpty
    {
        public object UnsuccessfulResult { get; set; }

        private ApiResponseUnsuccessful()
            : base(successful: false)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ApiResponseUnsuccessful"/> class
        /// without Notification Popup
        /// </summary>
        public ApiResponseUnsuccessful(object result)
            : this()
        {
            UnsuccessfulResult = result ?? throw new ArgumentNullException(nameof(result));
        }

        public ApiResponseUnsuccessful(object result, FrontendNotificationPopup notificationPopup)
            : this(result)
        {
            NotificationPopup = notificationPopup ?? throw new ArgumentNullException(nameof(notificationPopup));
            OmitShowingUserNotificationPopup = false;
        }

        public static ApiResponseUnsuccessful Empty() => new ApiResponseUnsuccessful();

        public IApiResponse ToDoubleGenericApiResponse(Type firstGenericType, Type secondGenericType)
        {
            var doubleGenericApiResponseType = typeof(ApiResponse<,>).MakeGenericType(firstGenericType, secondGenericType);

            var result = Activator.CreateInstance(doubleGenericApiResponseType, UnsuccessfulResult, this);

            return (IApiResponse)result;
        }
    }
}
