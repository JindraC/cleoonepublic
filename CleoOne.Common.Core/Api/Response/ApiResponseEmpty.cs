﻿// <copyright file="ApiResponseEmpty.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using Newtonsoft.Json;
using System;

namespace CleoOne.Services.Api.Response
{
    public class ApiResponseEmpty : IApiResponse
    {
        public bool Successful { get; set; }

        public ApiErrorForDebug ErrorForDebug { get; set; }

        public FrontendNotificationPopup NotificationPopup { get; set; }

        public bool OmitShowingUserNotificationPopup { get; set; }

        [JsonConstructor]
        private ApiResponseEmpty()
        {
        }

        public ApiResponseEmpty(bool successful)
        {
            Successful = successful;
            OmitShowingUserNotificationPopup = true;
        }

        public ApiResponseEmpty(bool successful, FrontendNotificationPopup notificationPopup)
            : this(successful)
        {
            NotificationPopup = notificationPopup ?? throw new ArgumentNullException(nameof(notificationPopup));
            OmitShowingUserNotificationPopup = false;
        }

        public ApiResponseEmpty(IApiResponse apiResponse)
        {
            Successful = apiResponse.Successful;
            ErrorForDebug = apiResponse.ErrorForDebug;
            NotificationPopup = apiResponse.NotificationPopup;
            OmitShowingUserNotificationPopup = apiResponse.OmitShowingUserNotificationPopup;
        }

        public ApiResponseEmpty(ApiErrorForDebug apiErrorForDebug, string errorNotification)
            : this(successful: false, FrontendNotificationPopup.Error(errorNotification))
        {
            ErrorForDebug = apiErrorForDebug;
            OmitShowingUserNotificationPopup = false;
        }

        public bool PopupIsFilledOrDisabledExplicitly()
        {
            return !OmitShowingUserNotificationPopup || (NotificationPopup != null && NotificationPopup.IsNotDefault());
        }

        public string ToTestInformationDump()
        {
            return ErrorForDebug?.ToTestInformationDump();
        }

        public IApiResponse ToSingleGenericApiResponse(Type genericType)
        {
            var singleGenericApiResponseType = typeof(ApiResponse<>).MakeGenericType(genericType);

            var result = Activator.CreateInstance(singleGenericApiResponseType, this);

            return (IApiResponse)result;
        }
    }
}
