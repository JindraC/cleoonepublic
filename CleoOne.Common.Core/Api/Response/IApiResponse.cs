﻿// <copyright file="IApiResponse.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace CleoOne.Services.Api.Response
{
    public interface IApiResponse
    {
        bool Successful { get; set; }

        ApiErrorForDebug ErrorForDebug { get; set; }

        FrontendNotificationPopup NotificationPopup { get; set; }

        /// <summary>
        /// If set to <see langword="true"/> the <see cref="NotificationPopup"/> will be not shown to the user
        /// and is expected to be <see langword="null"/>. <br/>
        /// If set to <see langword="false"/> the <see cref="NotificationPopup"/> will be shown of Front-End to the user.
        /// </summary>
        bool OmitShowingUserNotificationPopup { get; set; }
    }
}
