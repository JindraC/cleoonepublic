﻿// <copyright file="ApiResponseSuccessful.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;

namespace CleoOne.Services.Api.Response
{
    public class ApiResponseSuccessful : ApiResponseEmpty
    {
        public object Result { get; set; }

        private ApiResponseSuccessful()
            : base(successful: true)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ApiResponseSuccessful"/> class
        /// without Notification Popup
        /// </summary>
        public ApiResponseSuccessful(object successfulResult)
            : this()
        {
            Result = successfulResult ?? throw new ArgumentNullException(nameof(successfulResult));
        }

        public ApiResponseSuccessful(object successfulResult, FrontendNotificationPopup notificationPopup)
            : this(successfulResult)
        {
            NotificationPopup = notificationPopup ?? throw new ArgumentNullException(nameof(notificationPopup));
            OmitShowingUserNotificationPopup = false;
        }

        public static ApiResponseSuccessful Empty() => new ApiResponseSuccessful();

        public new IApiResponse ToSingleGenericApiResponse(Type genericType)
        {
            var singleGenericApiResponseType = typeof(ApiResponse<>).MakeGenericType(genericType);

            var result = Activator.CreateInstance(singleGenericApiResponseType, Result, this);

            return (IApiResponse)result;
        }

        public IApiResponse ToDoubleGenericApiResponse(Type firstGenericType, Type secondGenericType)
        {
            var doubleGenericApiResponseType = typeof(ApiResponse<,>).MakeGenericType(firstGenericType, secondGenericType);

            var result = Activator.CreateInstance(doubleGenericApiResponseType, Result, this);

            return (IApiResponse)result;
        }
    }
}
