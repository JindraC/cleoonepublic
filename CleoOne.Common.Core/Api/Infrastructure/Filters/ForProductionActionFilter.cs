﻿// <copyright file="ForProductionActionFilter.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.Common.Helpers;
using CleoOne.Services.Api.Infrastructure.Extensions;
using CleoOne.Services.Api.Response;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace CleoOne.Services.Api.Infrastructure.Filters
{
    public class ForProductionActionFilter : IActionFilter
    {
        private readonly IWebHostEnvironment _environment;

        public ForProductionActionFilter(IWebHostEnvironment environment)
        {
            _environment = environment;
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            if (context.OmitFilter(this))
            {
                return;
            }

            // nothing
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            if (context.OmitFilter(this))
            {
                return;
            }

            if (!_environment.IsProduction())
            {
                return;
            }

            if (context.Result is ObjectResult objectResult
                && objectResult.Value is IApiResponse apiResponse)
            {
                apiResponse.ErrorForDebug = null;
            }
        }
    }
}
