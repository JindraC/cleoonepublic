﻿// <copyright file="ApiResponseActionFilter.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.Common.Logging;
using CleoOne.Common.Core.DotNet.Extensions;
using CleoOne.Common.Core.DotNet.Extensions.Strings;
using CleoOne.Common.Core.PerformanceMetrics;
using CleoOne.Services.Api.Infrastructure.Extensions;
using CleoOne.Services.Api.Response;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Reflection;

namespace CleoOne.Services.Api.Infrastructure.Filters
{
    public class ApiResponseActionFilter : IActionFilter
    {
        private readonly ILogger _logger;
        private readonly ICallMetrics _metrics;

        private static readonly ConcurrentDictionary<(Type controllerType, MethodInfo methodInfo), Type> _responseTypesDictionary =
            new ConcurrentDictionary<(Type controllerType, MethodInfo methodInfo), Type>();

        private Type _responseType;

        private Type GetResponseType() => _responseType
                                          ?? throw new Exception(
                                              $"Response type was not determined yet (OnActionExecuting() was not called yet).");

        public ApiResponseActionFilter(CallMetricsFactory callMetrics)
        {
            _logger = this.GetLoggerForThis();
            _metrics = callMetrics.GetMetricsForThisWithCategory(this, "Filters");
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            if (context.OmitFilter(this))
            {
                return;
            }

            using var _ = _metrics.Measure();

            var controllerActionDescriptor = context.ActionDescriptor as ControllerActionDescriptor;

            if (controllerActionDescriptor == null)
            {
                throw new NotImplementedException(
                    $"This should not occur: ActionDescriptor should be always {typeof(ControllerActionDescriptor)}");
            }

            var keyTuple = (controllerActionDescriptor.ControllerTypeInfo.AsType(), controllerActionDescriptor.MethodInfo);

            _responseType = _responseTypesDictionary.GetOrAdd(keyTuple, _ => DetermineResponseType(controllerActionDescriptor));
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            try
            {
                OnActionExecutedPrivate(context);
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception,
                                    $"{nameof(ApiResponseActionFilter)} threw exception unexpectedly for {context.Result.ToJsonSafe(logger: _logger)}.");

                throw;
            }
        }

        private void OnActionExecutedPrivate(ActionExecutedContext context)
        {
            using var __ = _metrics.Measure();

            if (ShouldOmitThisFilter(context))
            {
                return;
            }

            var objectResult = context.Result as ObjectResult ?? throw new Exception(WrongResultTypeException());
            var generic = GetResultsGenericTypeDefinitionOrNull();

            objectResult.Value = objectResult.Value switch
            {
                ApiResponseSuccessful s when generic == typeof(ApiResponse<>) => ValidateSingleGenericApiResponse(s),
                ApiResponseSuccessful s when generic == typeof(ApiResponse<,>) => ValidateDoubleGenericApiResponse(s),

                ApiResponseUnsuccessful u when generic == typeof(ApiResponse<>) => ValidateSingleGenericApiResponse(u),
                ApiResponseUnsuccessful u when generic == typeof(ApiResponse<,>) => ValidateDoubleGenericApiResponse(u),

                ApiResponseEmpty e when generic == typeof(ApiResponse<>) && !e.Successful => ValidateSingleGenericApiResponse(e),
                ApiResponseEmpty e when GetResponseType() == typeof(ApiResponseEmpty) => e,

                ApiResponseEmpty e when generic == typeof(ApiResponse<>) && e.Successful => throw new Exception(EmptyResponseException()),
                ApiResponseEmpty e when generic == typeof(ApiResponse<,>) => throw new Exception(EmptyResponseDoubleGeneric()),

                IApiResponse _ => throw new Exception(IApiResponseText()),
                null => throw new NullReferenceException(NullReferenceException()),
                _ => throw new NotImplementedException(NotImplementedResultValueType()),
            };

            ThrowIfNotificationIsFilledButOmitted(objectResult.Value as IApiResponse ?? throw new Exception(ShouldBeIApiResponse()));

            return;

            string WrongResultTypeException() => $"Result conversion for result type: {context.Result?.GetType()} is not implemented.";

            string EmptyResponseException() => $"Successful {nameof(ApiResponseEmpty)} cant be assign to {typeof(ApiResponse<>)} response.";

            string EmptyResponseDoubleGeneric() => $"{nameof(ApiResponseEmpty)} cannot be applied to {typeof(ApiResponse<,>)} response.";

            string IApiResponseText() => $"ApiResponse shall be created automatically, not as input to Ok(apiResponse) etc. call.";

            string NullReferenceException() => $"Result.Value of {nameof(ObjectResult)} cannot be null";

            string NotImplementedResultValueType() => $"Not implemented response {objectResult.Value.GetType()} for {GetResponseType()}.";

            string ShouldBeIApiResponse() => $"the objectResult.Value should be of IApiResponse Type already here.";
        }

        private void ThrowIfNotificationIsFilledButOmitted(IApiResponse apiResponse)
        {
            if (apiResponse.OmitShowingUserNotificationPopup
                && apiResponse.NotificationPopup != null)
            {
                throw new Exception(
                    $"Data inconsistency - the notification should be null when omitted, for {apiResponse.ToJsonSafe(logger: _logger)}");
            }
        }

        private IApiResponse ValidateSingleGenericApiResponse(ApiResponseSuccessful apiResponseSuccessful)
        {
            var singleGenericType = GetResponseType().GetGenericArguments().Single();

            ThrowIfTypeIsNotAssignableToObject(singleGenericType, apiResponseSuccessful.Result);
            ThrowIfSuccessfulAndNullResult(apiResponseSuccessful);

            return apiResponseSuccessful.ToSingleGenericApiResponse(singleGenericType);
        }

        private IApiResponse ValidateDoubleGenericApiResponse(ApiResponseSuccessful apiResponseSuccessful)
        {
            var firstGenericType = GetResponseType().GetGenericArguments().First();
            var secondGenericType = GetResponseType().GetGenericArguments().Second();

            ThrowIfTypeIsNotAssignableToObject(firstGenericType, apiResponseSuccessful.Result);
            ThrowIfSuccessfulAndNullResult(apiResponseSuccessful);

            return apiResponseSuccessful.ToDoubleGenericApiResponse(firstGenericType, secondGenericType);
        }

        private IApiResponse ValidateSingleGenericApiResponse(ApiResponseUnsuccessful apiResponseUnsuccessful)
        {
            var singleGenericType = GetResponseType().GetGenericArguments().Single();

            ThrowIfResultIsNotNull(apiResponseUnsuccessful);
            ThrowIfNeitherResultOrPopupAreFilled(apiResponseUnsuccessful);

            return apiResponseUnsuccessful.ToSingleGenericApiResponse(singleGenericType);
        }

        private IApiResponse ValidateDoubleGenericApiResponse(ApiResponseUnsuccessful apiResponseUnsuccessful)
        {
            var firstGenericType = GetResponseType().GetGenericArguments().First();
            var secondGenericType = GetResponseType().GetGenericArguments().Second();

            ThrowIfTypeIsNotAssignableToObject(secondGenericType, apiResponseUnsuccessful.UnsuccessfulResult);
            ThrowIfNeitherResultOrPopupAreFilled(apiResponseUnsuccessful);

            return apiResponseUnsuccessful.ToDoubleGenericApiResponse(firstGenericType, secondGenericType);
        }

        private IApiResponse ValidateSingleGenericApiResponse(ApiResponseEmpty apiResponseEmpty)
        {
            return apiResponseEmpty.ToSingleGenericApiResponse(GetResponseType().GetGenericArguments().Single());
        }

        private bool ShouldOmitThisFilter(ActionExecutedContext context)
        {
            if (context.OmitFilter(this))
            {
                return true;
            }

            if (context.ExceptionHandled
                || context.Exception == null)
            {
                if (context.Result == null)
                {
                    throw new Exception($"At this stage the Result should be always filled.");
                }

                return true;
            }

            if (context.Exception is OperationCanceledException
                || (context.Result as IStatusCodeActionResult)?.StatusCode == 499 /* request cancelled */)
            {
                // this happens when cancelled by user, so nobody actually cares about the result
                return true;
            }

            if (context.Result is StatusCodeResult statusCodeResult
                && statusCodeResult.StatusCode != 200)
            {
                throw new Exception($"Only 200 (OK) status code is permitted by this filter.");
            }

            if (context.Result is ObjectResult objectResultWithStatusCode
                && objectResultWithStatusCode.StatusCode.HasValue
                && objectResultWithStatusCode.StatusCode != 200)
            {
                throw new Exception($"Only 200 (OK) status code is permitted by this filter.");
            }

            return false;
        }

        private Type GetResultsGenericTypeDefinitionOrNull()
        {
            var result = GetResponseType();

            return result.IsGenericType ? result.GetGenericTypeDefinition() : null;
        }

        private void ThrowIfNeitherResultOrPopupAreFilled(ApiResponseUnsuccessful apiResponseUnsuccessful)
        {
            if (apiResponseUnsuccessful.UnsuccessfulResult == null
                && !apiResponseUnsuccessful.PopupIsFilledOrDisabledExplicitly())
            {
                throw new Exception($"When request is unsuccessful notification popup or result have to be filled.");
            }
        }

        private void ThrowIfResultIsNotNull(ApiResponseUnsuccessful apiResponseUnsuccessful)
        {
            if (apiResponseUnsuccessful.UnsuccessfulResult != null)
            {
                throw new Exception($"Using Unsuccessful() for ApiResponse<> is allowed only without response object.");
            }
        }

        private void ThrowIfTypeIsNotAssignableToObject(Type type, object obj)
        {
            if (!obj.GetType().IsAssignableFrom(type))
            {
                throw new Exception($"The type {obj.GetType()} cannot be assigned to type {type}.");
            }
        }

        private void ThrowIfSuccessfulAndNullResult(ApiResponseSuccessful apiResponse)
        {
            if (apiResponse.Result == null)
            {
                throw new Exception($"Response was successful and result was NULL, which is forbidden.");
            }
        }

        private static Type DetermineResponseType(ControllerActionDescriptor controllerActionDescriptor)
        {
            var responseTypeAttributeObjects =
                controllerActionDescriptor.MethodInfo.GetCustomAttributes(typeof(ProducesResponseTypeAttribute), inherit: true);

            // ProducesResponseTypeAttribute is now required by all methods for two reasons:
            //     1. Because auto-generation of API consuming javascript code knows exact response type
            //     2. Because by this we ensure that all responses have the same type (again for frontend simplicity)
            //     3. For automatic response construction in case of method Controller.Ok() (this has empty result, but we want to fill it automatically)

            if (responseTypeAttributeObjects == null
                || responseTypeAttributeObjects.Length == 0)
            {
                throw new Exception(
                    $"Method {controllerActionDescriptor.DisplayName}() on controller {controllerActionDescriptor.ControllerName} "
                    + $"does not have 'ProducesResponseType' attribute, which is now required.");
            }

            var responseTypeAttributes = responseTypeAttributeObjects.Cast<ProducesResponseTypeAttribute>().ToList();
            var responseTypes = responseTypeAttributes.Select(attribute => attribute.Type).Distinct().ToList();

            if (responseTypes.Any(type => type == null))
            {
                throw new Exception(
                    $"Method {controllerActionDescriptor.DisplayName}() on controller {controllerActionDescriptor.ControllerName} "
                    + $"has at least one 'ProducesResponseType' attribute without response type.");
            }

            if (responseTypes.Count > 1)
            {
                throw new Exception(
                    $"Method {controllerActionDescriptor.DisplayName}() on controller {controllerActionDescriptor.ControllerName} "
                    + $"has more 'ProducesResponseType' attributes with different response types, but there should be just one response type.");
            }

            var responseType = responseTypes.Single();

            return responseType;
        }
    }
}
