﻿// <copyright file="ModelValidationActionFilter.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.Common.Logging;
using CleoOne.Common.Core.DotNet.Extensions.Strings;
using CleoOne.Common.Core.PerformanceMetrics;
using CleoOne.Services.Api.Infrastructure.Extensions;
using CleoOne.Services.Api.Infrastructure.Other;
using CleoOne.Services.Api.Response;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CleoOne.Services.Api.Infrastructure.Filters
{
    public class ModelValidationActionFilter : IAsyncActionFilter
    {
        private static readonly Regex _requiredPropertyRegex = new Regex(@"^Required property '(?<property>[^']+)' not found in JSON\.",
                                                                         RegexOptions.Singleline | RegexOptions.IgnoreCase);

        private static readonly Regex _settingErrorRegex = new Regex(
            @"^Error setting value to '(?<property>[^']+)' on '(?<classtype>[^']+)'\.",
            RegexOptions.Singleline | RegexOptions.IgnoreCase);

        private static readonly Regex _expectsValueRegex = new Regex(
            @"^Required property '(?<property>[^']+)' expects a value but got null\.",
            RegexOptions.Singleline | RegexOptions.IgnoreCase);

        private static readonly Regex _redundantPropertyRegex = new Regex(@"^Could not find member '(?<property>[^']+)' on object of type ",
                                                                          RegexOptions.Singleline | RegexOptions.IgnoreCase);

        /// <summary>
        /// When there are redundant properties in JSON, then just log them but do not send BadRequest
        /// </summary>
        public bool AreRedundantPropertiesLoggedOnly { get; } = true;

        private readonly ICallMetrics _metrics;

        public ModelValidationActionFilter(CallMetricsFactory callMetrics)
        {
            _metrics = callMetrics.GetMetricsForThisWithCategory(this, "Filters");
            _logger = this.GetLoggerForThis();
        }

        private static void After(ActionExecutedContext context, List<ModelValidationError> justToBeLogged)
        {
            var result = (IStatusCodeActionResult)context.Result;

            if (result.StatusCode == 400 /* bad request */
                || result.StatusCode == 499 /* request cancelled */)
            {
                // for bad request and operation cancelled we do not do anything
                return;
            }

            if (!(result is ObjectResult objectResult))
            {
                throw new Exception(
                    $"{nameof(context.Result)} must be already of type {nameof(ObjectResult)} here "
                    + $"(filter {nameof(ApiResponseActionFilter)} must be called before filter {nameof(ModelValidationActionFilter)}).");
            }

            if (!(objectResult.Value is IApiResponse apiResponse))
            {
                throw new Exception(
                    $"Value of the {nameof(ObjectResult)} must be already of type {nameof(IApiResponse)} here "
                    + $"(filter {nameof(ApiResponseActionFilter)} must be called before filter {nameof(ModelValidationActionFilter)}).");
            }

            if (justToBeLogged.Count > 0)
            {
                apiResponse.SetDebugLightValidationErrors(justToBeLogged.ToArray());
            }
        }

        private async Task<string> ReadBodyAsString(HttpContext context)
        {
            var initialPosition = context.Request.Body.Position;

            try
            {
                context.Request.Body.Position = 0L;
                using StreamReader reader = new StreamReader(context.Request.Body, leaveOpen: true);

                // We can safely use ReadToEndAsync as the Request is already read, so we will not hang here.
                string text = await reader.ReadToEndAsync();

                return text;
            }
            finally
            {
                context.Request.Body.Position = initialPosition;
            }
        }

        private static readonly RequiredPropertiesContractResolver _requiredPropertiesContractResolver =
            new RequiredPropertiesContractResolver();

        private readonly ILogger _logger;

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            if (context.OmitFilter(this))
            {
                await next();

                return;
            }

            var validationErrors = new List<ModelValidationError>();
            var justToBeLogged = new List<ModelValidationError>();

            await ValidateBody(context, validationErrors, justToBeLogged);

            if (context.ModelState.IsValid      // Validation from ASP NET Core
                && validationErrors.Count == 0) // Validation Error coming from our own validation
            {
                var after = await next();
                After(after, justToBeLogged);
            }
            else
            {
                // There are errors, either from ASP NET Core validation or from our custom json validation

                // Add possible ASP NET Core validation errors to our collection.
                foreach (var pair in context.ModelState)
                {
                    var key = pair.Key;
                    var modelStateNode = pair.Value;

                    foreach (var modelError in modelStateNode.Errors)
                    {
                        var error = new ModelValidationError
                        {
                            Path = key,
                            Property = key.Split('.').Last(),
                            ErrorCode = "ASPNETValidationError",
                            Error = !string.IsNullOrEmpty(modelError.ErrorMessage)
                                        ? modelError.ErrorMessage
                                        : modelError.ToJsonSafe(logger: _logger),
                        };

                        validationErrors.Add(error);
                    }
                }

                // Short circuit - we received invalid request, so we do not run rest of the filter pipeline.
                context.Result = new BadRequestObjectResult(validationErrors);
            }
        }

        private async Task ValidateBody(ActionExecutingContext context,
                                        List<ModelValidationError> validationErrors,
                                        List<ModelValidationError> justToBeLogged)
        {
            // We dont have body, so we dont not change anything
            if (context.HttpContext.Request.Body.Length == 0)
            {
                return;
            }

            using var _ = _metrics.Measure();

            var fromBodyParameter = context.ActionDescriptor.Parameters.SingleOrDefault(x => x.BindingInfo.BindingSource.Id == "Body");

            if (fromBodyParameter == null)
            {
                // We have body but no [FromBody] parameter.
                _logger.LogWarning("For {ControllerName}.{ActionMethod} we have body, but no [FromBody] parameter.",
                                   context.Controller.GetType().Name,
                                   (context.ActionDescriptor as ControllerActionDescriptor)?.ActionName);

                return;
            }

            var strBody = await ReadBodyAsString(context.HttpContext);

            var argType = fromBodyParameter.ParameterType;

            JsonConvert.DeserializeObject(strBody,
                                          argType,
                                          new JsonSerializerSettings()
                                          {
                                              ContractResolver = _requiredPropertiesContractResolver,
                                              MissingMemberHandling = MissingMemberHandling.Error,
                                              Error = (sender, args) =>
                                              {
                                                  var error = new ModelValidationError();
                                                  var key = args.ErrorContext.OriginalObject?.GetType().Name;

                                                  error.Path = key;

                                                  var jsonErrorContext = args.ErrorContext;

                                                  var requiredPropertyMatch =
                                                      _requiredPropertyRegex.Match(jsonErrorContext.Error.Message);

                                                  if (requiredPropertyMatch.Success)
                                                  {
                                                      error.Property = requiredPropertyMatch.Groups["property"].Value;
                                                      error.Path = $"{key}.{error.Property}".Trim('.');
                                                      error.ErrorCode = "PropertyNotFoundInJson";

                                                      error.Error = "Required property not found in JSON or has type mismatch.";

                                                      validationErrors.Add(error);
                                                      args.ErrorContext.Handled = true;

                                                      return;
                                                  }

                                                  var settingErrorMatch =
                                                      _settingErrorRegex.Match(jsonErrorContext.Error.Message);

                                                  if (settingErrorMatch.Success)
                                                  {
                                                      error.Property = settingErrorMatch.Groups["property"].Value;
                                                      error.ErrorCode = "ErrorSettingValue";
                                                      error.Error = "Error setting value to a property.";

                                                      validationErrors.Add(error);
                                                      args.ErrorContext.Handled = true;

                                                      return;
                                                  }

                                                  var expectsValueMatch =
                                                      _expectsValueRegex.Match(jsonErrorContext.Error.Message);

                                                  if (expectsValueMatch.Success)
                                                  {
                                                      error.Property = expectsValueMatch.Groups["property"].Value;
                                                      error.Path = $"{key}.{error.Property}".Trim('.');
                                                      error.ErrorCode = "NonNullValueExpected";
                                                      error.Error = "Required property expects a value but got null.";

                                                      validationErrors.Add(error);
                                                      args.ErrorContext.Handled = true;

                                                      return;
                                                  }

                                                  var redundantPropertyMatch =
                                                      _redundantPropertyRegex.Match(jsonErrorContext.Error.Message);

                                                  if (redundantPropertyMatch.Success)
                                                  {
                                                      error.Property = redundantPropertyMatch.Groups["property"].Value;

                                                      error.Path = key?.EndsWith(error.Property) == true
                                                                       ? key
                                                                       : $"{key}.{error.Property}".Trim('.');

                                                      error.ErrorCode = "RedundantPropertyInJson";

                                                      error.Error = "Property in json did not have equivalent property on the model class.";

                                                      if (AreRedundantPropertiesLoggedOnly)
                                                      {
                                                          justToBeLogged.Add(error);
                                                      }
                                                      else
                                                      {
                                                          validationErrors.Add(error);
                                                      }

                                                      args.ErrorContext.Handled = true;

                                                      return;
                                                  }

                                                  if (jsonErrorContext.Error.Message.StartsWith(
                                                      @"Error converting value {null} to type"))
                                                  {
                                                      error.Property = key?.Split('.').Last();
                                                      error.ErrorCode = "NonNullValueExpected";
                                                      error.Error = "Required property expects a value but got null.";

                                                      validationErrors.Add(error);
                                                      args.ErrorContext.Handled = true;

                                                      return;
                                                  }

                                                  if (jsonErrorContext.Error.Message.StartsWith(@"Could not convert "))
                                                  {
                                                      error.Property = key?.Split('.').Last();
                                                      error.ErrorCode = "WrongValueType";
                                                      error.Error = "Could not convert value into property type.";

                                                      validationErrors.Add(error);

                                                      args.ErrorContext.Handled = true;

                                                      return;
                                                  }

                                                  // Unrecognized error
                                                  error.Property = jsonErrorContext.Member?.ToString();
                                                  error.ErrorCode = "UnrecognizedJsonConvertError";
                                                  error.Error = jsonErrorContext.Error.ToString();

                                                  validationErrors.Add(error);
                                                  args.ErrorContext.Handled = true;
                                              },
                                          });
        }
    }
}
