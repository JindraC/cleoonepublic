﻿// <copyright file="DisableSwaggerAutoErrorGenerationAttribute.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;

namespace CleoOne.Services.Api.Infrastructure.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class DisableSwaggerAutoErrorGenerationAttribute : Attribute
    {
    }
}
