﻿// <copyright file="OmitCleoActionFilterAttribute.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;

namespace CleoOne.Services.Api.Infrastructure.Attributes
{
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
    public sealed class OmitCleoActionFilterAttribute : Attribute
    {
        private readonly HashSet<Type> _typesToOmitHashSet;

        public OmitCleoActionFilterAttribute(Type actionFilterType1, params Type[] nextActionFilterTypes)
        {
            _typesToOmitHashSet = new HashSet<Type>(new[] { actionFilterType1 }.Concat(nextActionFilterTypes ?? Array.Empty<Type>()));
        }

        public bool IsToOmit(Type actionFilterType)
        {
            return _typesToOmitHashSet.Contains(actionFilterType);
        }
    }
}
