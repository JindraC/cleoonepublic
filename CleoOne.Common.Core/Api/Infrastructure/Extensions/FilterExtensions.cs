﻿// <copyright file="FilterExtensions.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Services.Api.Infrastructure.Attributes;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Reflection;

namespace CleoOne.Services.Api.Infrastructure.Extensions
{
    public static class FilterExtensions
    {
        private static readonly ConcurrentDictionary<(Type controllerType, MethodInfo methodInfo, Type filterType), bool>
            _omitCacheDictionary = new ConcurrentDictionary<(Type controllerType, MethodInfo methodInfo, Type filterType), bool>();

        public static bool OmitFilter(this FilterContext context, IFilterMetadata filter)
        {
            var controllerActionDescriptor = (ControllerActionDescriptor)context.ActionDescriptor;

            var controllerType = controllerActionDescriptor.ControllerTypeInfo.AsType();
            var methodInfo = controllerActionDescriptor.MethodInfo;
            var filterType = filter.GetType();

            var keyTuple = (controllerType, methodInfo, filterType);

            return _omitCacheDictionary.GetOrAdd(keyTuple, OmitFilterFor);
        }

        private static bool OmitFilterFor((Type controllerType, MethodInfo methodInfo, Type filterType) keyTuple)
        {
            var controllerType = keyTuple.controllerType;
            var methodInfo = keyTuple.methodInfo;
            var filterType = keyTuple.filterType;

            if (controllerType.GetCustomAttribute<OmitAllCleoActionFiltersAttribute>(inherit: true) != null
                || methodInfo.GetCustomAttribute<OmitAllCleoActionFiltersAttribute>(inherit: true) != null)
            {
                return true;
            }

            var specificOmitAttributes1 = controllerType.GetCustomAttributes<OmitCleoActionFilterAttribute>(inherit: true);
            var specificOmitAttributes2 = methodInfo.GetCustomAttributes<OmitCleoActionFilterAttribute>(inherit: true);
            var specificOmitAttributes = specificOmitAttributes1.Concat(specificOmitAttributes2).ToList();

            return specificOmitAttributes.Any(omitAttribute => omitAttribute.IsToOmit(filterType));
        }
    }
}
