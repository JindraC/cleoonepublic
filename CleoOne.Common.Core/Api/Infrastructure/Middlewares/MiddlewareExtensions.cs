﻿// <copyright file="MiddlewareExtensions.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Services.Api.Infrastructure.Middlewares.MiddlewareMeasurement;
using Microsoft.AspNetCore.Builder;
using System;

namespace CleoOne.Services.Api.Infrastructure.Middlewares
{
    public static class MiddlewareExtensions
    {
        public static IApplicationBuilder UseUnhandledExceptionsLogging(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<LogUnhandledExceptionsMiddleware>();
        }

        public static IApplicationBuilder UseRequestDurationLogging(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<RequestDurationMiddleware>();
        }

        public static IApplicationBuilder UseMeasuredMiddleware<TMeasuredMiddleware>(this IApplicationBuilder builder)
        {
            return builder.UseAndMeasureIncludedMiddlewares(typeof(TMeasuredMiddleware).Name,
                                                            () => builder.UseMiddleware<TMeasuredMiddleware>());
        }

        public static IApplicationBuilder UseAndMeasureIncludedMiddlewares(this IApplicationBuilder builder,
                                                                           string name,
                                                                           Action addMeasuredMiddlewares)
        {
            builder = builder.UseMiddleware<NamedOuterMeasuringMiddlewareWrapper>(name);

            addMeasuredMiddlewares();

            return builder.UseMiddleware<NamedInnerMeasuringMiddlewareWrapper>(name);
        }
    }
}
