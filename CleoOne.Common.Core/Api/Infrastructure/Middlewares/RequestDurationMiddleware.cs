﻿// <copyright file="RequestDurationMiddleware.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.Common.Logging;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace CleoOne.Services.Api.Infrastructure.Middlewares
{
    public class RequestDurationMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<RequestDurationMiddleware> _logger;

        public RequestDurationMiddleware(RequestDelegate next, ILogger<RequestDurationMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            var watch = Stopwatch.StartNew();
            await _next.Invoke(context);
            watch.Stop();

            var path = context?.Request?.Path.ToString();

            if (path != null
                && !path.EndsWith("/alive", StringComparison.OrdinalIgnoreCase))
            {
                _logger.LogInformation("Api request: {Path} TOTAL duration: {RequestDurationSec_num} s",
                                       path,
                                       watch.ElapsedMilliseconds.MillisecondsToSeconds());
            }
        }
    }
}
