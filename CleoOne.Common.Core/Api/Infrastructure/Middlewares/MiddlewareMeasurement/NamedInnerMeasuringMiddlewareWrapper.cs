﻿// <copyright file="NamedInnerMeasuringMiddlewareWrapper.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.Common.Logging;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using System.Threading.Tasks;

namespace CleoOne.Services.Api.Infrastructure.Middlewares.MiddlewareMeasurement
{
    public class NamedInnerMeasuringMiddlewareWrapper
    {
        private readonly RequestDelegate _next;
        private readonly string _name;
        private readonly ILogger _logger;

        public NamedInnerMeasuringMiddlewareWrapper(RequestDelegate next, string name)
        {
            _next = next;
            _name = name;
            _logger = this.GetLoggerForThis();
        }

        public async Task InvokeAsync(HttpContext context, NamedMeasuredRequestScope measuredRequestScope)
        {
            var stopWatch = Stopwatch.StartNew();

            await _next(context);

            var elapsed = stopWatch.Elapsed;

            if (!measuredRequestScope.NestedWorkByMiddlewareType.TryAdd(_name, elapsed))
            {
                // this means that key for middleware is already here.
                _logger.LogCritical(
                    "Misconfigured middleware measurement, you are probably measuring multiple same middlewares.");
            }
        }
    }
}
