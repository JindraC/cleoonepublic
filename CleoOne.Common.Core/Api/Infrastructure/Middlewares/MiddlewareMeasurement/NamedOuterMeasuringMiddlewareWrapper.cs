﻿// <copyright file="NamedOuterMeasuringMiddlewareWrapper.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.Common.Logging;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace CleoOne.Services.Api.Infrastructure.Middlewares.MiddlewareMeasurement
{
    public class NamedOuterMeasuringMiddlewareWrapper
    {
        private readonly RequestDelegate _next;
        private readonly string _name;
        private readonly ILogger _logger;

        public NamedOuterMeasuringMiddlewareWrapper(RequestDelegate next, string name)
        {
            _next = next;
            _name = name;
            _logger = this.GetLoggerForThis();
        }

        public async Task InvokeAsync(HttpContext context, NamedMeasuredRequestScope measuredRequestScope)
        {
            var stopWatch = Stopwatch.StartNew();

            await _next(context);

            var elapsedTotal = stopWatch.Elapsed;

            TimeSpan durationOfMiddlewareLogic;

            if (measuredRequestScope.NestedWorkByMiddlewareType.TryGetValue(_name, out var durationOfNestedMiddlewares))
            {
                durationOfMiddlewareLogic = elapsedTotal - durationOfNestedMiddlewares;
            }
            else
            {
                // key not present -> this means that measured middleware was terminating (did not called next)
                durationOfMiddlewareLogic = elapsedTotal;
            }

            _logger.LogInformation("Middleware {MiddlewareName} execution duration: {MiddlewareDurationSec_num} s",
                                   _name,
                                   durationOfMiddlewareLogic.TotalMilliseconds.MillisecondsToSeconds());
        }
    }
}
