﻿// <copyright file="NamedMeasuredRequestScope.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Generic;

namespace CleoOne.Services.Api.Infrastructure.Middlewares.MiddlewareMeasurement
{
    public class NamedMeasuredRequestScope
    {
        public NamedMeasuredRequestScope()
        {
            NestedWorkByMiddlewareType = new Dictionary<string, TimeSpan>();
        }

        public Dictionary<string, TimeSpan> NestedWorkByMiddlewareType { get; }
    }
}
