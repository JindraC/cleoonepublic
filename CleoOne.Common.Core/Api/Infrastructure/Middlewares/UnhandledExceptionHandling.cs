﻿// <copyright file="UnhandledExceptionHandling.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.Common.Helpers;
using CleoOne.Common.Core.DotNet.Extensions.Classes;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Net;
using System.Threading.Tasks;

namespace CleoOne.Services.Api.Infrastructure.Middlewares
{
    public static class UnhandledExceptionHandling
    {
        public static async Task Handle(HttpContext context, IWebHostEnvironment env)
        {
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            context.Response.ContentType = "text/plain";

            var exceptionHandlerFeature = context.Features.Get<IExceptionHandlerFeature>();

            if (exceptionHandlerFeature != null)
            {
                if (env.IsProduction())
                {
                    await context.Response.WriteAsync(exceptionHandlerFeature.Error.GetType().Name);
                }
                else
                {
                    await context.Response.WriteAsync(exceptionHandlerFeature.Error.ToFullExceptionString());
                }
            }
            else
            {
                await context.Response.WriteAsync(
                    "IExceptionHandlerFeature was null, but the request is marked as failed by the AspNetCore pipeline.");
            }
        }
    }
}
