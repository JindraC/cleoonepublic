﻿// <copyright file="LogUnhandledExceptionsMiddleware.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.DotNet.Extensions.Classes;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CleoOne.Services.Api.Infrastructure.Middlewares
{
    public class LogUnhandledExceptionsMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        public LogUnhandledExceptionsMiddleware(RequestDelegate next, ILogger<LogUnhandledExceptionsMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception e)
            {
                _logger.LogCritical(e,
                                    "Unhandled exception in API request pipeline: {ExceptionType}: {ExceptionMergedMessages}",
                                    e.GetType().Name,
                                    e.ToMergedMessages());

                throw;
            }
        }
    }
}
