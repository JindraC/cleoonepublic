﻿// <copyright file="RequiredPropertiesContractResolver.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace CleoOne.Services.Api.Infrastructure.Other
{
    public class RequiredPropertiesContractResolver : CamelCasePropertyNamesContractResolver
    {
        protected override JsonObjectContract CreateObjectContract(Type objectType)
        {
            var contract = base.CreateObjectContract(objectType);

            foreach (var contractProperty in contract.Properties)
            {
                if (contractProperty.AttributeProvider.GetAttributes(typeof(RequiredAttribute), inherit: true).Any())
                {
                    contractProperty.Required = Required.Always;
                }
            }

            return contract;
        }
    }
}
