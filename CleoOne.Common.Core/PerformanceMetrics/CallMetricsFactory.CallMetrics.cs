﻿// <copyright file="CallMetricsFactory.CallMetrics.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.Common.Logging;
using CleoOne.Common.Core.DotNet.Extensions.Classes;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;

namespace CleoOne.Common.Core.PerformanceMetrics
{
    public sealed partial class CallMetricsFactory
    {
        private class CallMetrics : ICallMetrics
        {
            private readonly Type _contextType;
            private readonly CallMetricsFactory _callMetricsFactory;
            private readonly Dictionary<string, Aggregation> _callStats;
            private readonly ILogger _contextLogger;

            public string CategoryTag { get; }

            private class Measurement : IDisposable
            {
                private readonly CallMetrics _callMetrics;
                private readonly string _methodName;
                private readonly Stopwatch _stopwatch;

                public Measurement(CallMetrics callMetrics, string methodName)
                {
                    _callMetrics = callMetrics;
                    _methodName = methodName;
                    _stopwatch = Stopwatch.StartNew();
                }

                public void Dispose()
                {
                    _stopwatch.Stop();

                    // report the measured time back to call metrics
                    _callMetrics.AddReport(_methodName, DateTime.UtcNow, _stopwatch.Elapsed);
                }
            }

            internal CallMetrics(Type contextType, CallMetricsFactory callMetricsFactory, string categoryTag, bool shouldLogEveryCall)
            {
                CategoryTag = categoryTag;
                _contextType = contextType;
                _callMetricsFactory = callMetricsFactory;
                _callStats = new Dictionary<string, Aggregation>();

                if (shouldLogEveryCall)
                {
                    _contextLogger = LoggerHelper.GetLogger(contextType);
                    _contextLogger = _contextLogger.WithProperty("IsTracing", "1");
                }
            }

            public IDisposable Measure([CallerMemberName] string methodName = "")
            {
                _callMetricsFactory.StartReportingLoopIfNotYet();

                _contextLogger?.LogInformation(LogLabel.OneCallMetricsStart,
                                               "One call metrics: {Type:l}.{MethodName:l}() started.",
                                               _contextType.Name,
                                               methodName);

                return new Measurement(this, methodName);
            }

            private void AddReport(string methodName, DateTime end, TimeSpan elapsed)
            {
                lock (_callStats)
                {
                    if (!_callStats.TryGetValue(methodName, out var agg))
                    {
                        agg = new Aggregation(_contextType, methodName, CategoryTag);
                        _callStats.Add(methodName, agg);
                    }

                    if (elapsed > agg.MaxDuration)
                    {
                        agg.MaxDuration = elapsed;
                    }

                    if (elapsed < agg.MinDuration)
                    {
                        agg.MinDuration = elapsed;
                    }

                    agg.TotalDurationMs += elapsed.TotalMilliseconds;

                    var bucketIndex = end.Second / 10;
                    agg.MinuteDistributions[bucketIndex]++;

                    var newAverage = agg.TotalDurationMs / (agg.TotalCallsCount + 1);

                    agg.AverageDurationMs = newAverage;
                    agg.TotalCallsCount++;
                }

                _contextLogger?.LogInformation(LogLabel.OneCallMetricsFinish,
                                               "One call metrics: {Type:l}.{MethodName:l}() finished in: {DurationSeconds_num} s.",
                                               _contextType.Name,
                                               methodName,
                                               Math.Round(elapsed.TotalSeconds, 3));
            }

            public List<Aggregation> GetCallStatsAndClear()
            {
                lock (_callStats)
                {
                    var ret = _callStats.Values.ToList();
                    _callStats.Clear();

                    return ret;
                }
            }
        }
    }
}
