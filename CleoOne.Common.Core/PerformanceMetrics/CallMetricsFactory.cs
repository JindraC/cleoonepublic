﻿// <copyright file="CallMetricsFactory.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.Common.Helpers;
using CleoOne.Common.Core.Common.Logging;
using CleoOne.Common.Core.DotNet.Extensions.Classes;
using CleoOne.Common.Core.DotNet.Helpers;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CleoOne.Common.Core.PerformanceMetrics
{
    public sealed partial class CallMetricsFactory : ICallMetricsFactory
    {
        private readonly ILogger _logger;

        private int _initialized = 0;

        private readonly ConcurrentDictionary<Type, CallMetrics> _callMetricsByType = new ConcurrentDictionary<Type, CallMetrics>();

        private Dictionary<(Type type, string methodName), Aggregation> _hourlyAggregations;
        private Dictionary<(Type type, string methodName), Aggregation> _dailyAggregations;
        private DateTime _lastHourlyReport;
        private DateTime _lastDailyReport;

        private bool _shouldLogEveryCallDefault;

        private HashSet<Type> _logEveryCallForTypesExceptions;

        private static readonly CallMetricsFactory _instance = new CallMetricsFactory(shouldLogEveryCallDefault: false);

        public static void ConfigureInstance(bool shouldLogEveryCallByDefault,
                                             IEnumerable<Type> logEveryCallExceptionsFromDefault = null)
        {
            _instance._shouldLogEveryCallDefault = shouldLogEveryCallByDefault;

            if (logEveryCallExceptionsFromDefault != null)
            {
                _instance._logEveryCallForTypesExceptions = new HashSet<Type>(logEveryCallExceptionsFromDefault);
            }
        }

        public static CallMetricsFactory Instance => _instance;

        private const string GENERAL_CATEGORY = "General";

        public static IDisposable Measure<TContextType>([CallerMemberName] string measuredMethod = "")
        {
            return Instance.GetMetrics<TContextType>(GENERAL_CATEGORY).Measure(measuredMethod);
        }

        public static IDisposable Measure(object thisObject, [CallerMemberName] string measuredMethod = "")
        {
            return Instance.GetMetricsForThis(thisObject, GENERAL_CATEGORY).Measure(measuredMethod);
        }

        private CallMetricsFactory(bool shouldLogEveryCallDefault)
        {
            _logger = this.GetLoggerForThis();
            _hourlyAggregations = new Dictionary<(Type, string), Aggregation>();
            _dailyAggregations = new Dictionary<(Type, string), Aggregation>();
            _shouldLogEveryCallDefault = shouldLogEveryCallDefault;
        }

        public ICallMetrics GetMetrics<TContextType>(string category)
        {
            var contextType = typeof(TContextType);

            return GetMetricsForType(contextType, category);
        }

        public ICallMetrics GetMetricsForThis(object thisObject, string category)
        {
            return GetMetricsForType(thisObject.GetType(), category);
        }

        private ICallMetrics GetMetricsForType(Type contextType, string category)
        {
            var metrics = _callMetricsByType.GetOrAdd(contextType,
                                                      _ => new CallMetrics(contextType,
                                                                           this,
                                                                           category,
                                                                           category == GENERAL_CATEGORY
                                                                           || ShouldLogEveryCallForType(contextType)));

            return metrics;
        }

        ICallMetrics ICallMetricsFactory.GetMetricsForThis(object contextObject)
        {
            return GetMetricsForThisWithCategory(contextObject, "GeneralCategory");
        }

        public ICallMetrics GetMetricsForThisWithCategory(object contextObject, string category)
        {
            if (contextObject == null)
            {
                throw new ArgumentNullException(nameof(contextObject));
            }

            var contextType = contextObject.GetType();

            // This is the case when someone passes this.GetType() or typeof(SomeType) by mistake as contextObject,
            // as we will never be measuring methods on System.Type.
            if (contextObject is Type type)
            {
                contextType = type;
            }

            var metrics = _callMetricsByType.GetOrAdd(contextType,
                                                      _ => new CallMetrics(contextType,
                                                                           this,
                                                                           category,
                                                                           ShouldLogEveryCallForType(contextType)));

            return metrics;
        }

        private bool ShouldLogEveryCallForType(Type type)
        {
            if (_logEveryCallForTypesExceptions == null) // no exceptions -> return the default
            {
                return _shouldLogEveryCallDefault;
            }
            else
            {
                // there are exceptions from default

                var isException = _logEveryCallForTypesExceptions.Contains(type);

                return isException ? !_shouldLogEveryCallDefault : _shouldLogEveryCallDefault;
            }
        }

        private void StartReportingLoopIfNotYet()
        {
            if (_initialized == 1)
            {
                return;
            }

            if (Interlocked.Exchange(ref _initialized, 1) == 0)
            {
                Task.Run(RunMinuteReporting);
            }
        }

        private async Task RunMinuteReporting()
        {
            if (EnvironmentHelper.IsDevelopment())
            {
                _logger.LogWarning("RUN-MINUTE-REPORTING IS TURNED OFF FOR DEVELOPMENT ENVIRONMENT.");

                return;
            }

            while (true)
            {
                try
                {
                    var minuteAggregates = new List<Aggregation>();

                    foreach (var (_, callMetrics) in _callMetricsByType)
                    {
                        var minuteAggregations = callMetrics.GetCallStatsAndClear();

                        foreach (var aggregate in minuteAggregations)
                        {
                            // log minute report
                            WriteReportToLogger("Minute", aggregate, _logger);

                            // save to aggregations
                            AddToAggregations(aggregate);

                            // add to minute summary
                            minuteAggregates.Add(aggregate);
                        }
                    }

                    LogSummaryByCategory("MinuteSummary", minuteAggregates, _logger);

                    var utcNow = DateTime.UtcNow;

                    LogSummaryReportAndReset(ref _hourlyAggregations,
                                             ref _lastHourlyReport,
                                             utcNow,
                                             TimeSpan.FromHours(1),
                                             "Hourly",
                                             _logger);

                    LogSummaryReportAndReset(ref _dailyAggregations, ref _lastDailyReport, utcNow, TimeSpan.FromDays(1), "Daily", _logger);

                    await Threads.WaitTillXSecondsAfterAWholeMinute(0, CancellationToken.None);
                }
                catch (Exception e)
                {
                    _logger.LogCritical(e, "Call metrics factory reporting loop failed with exception.");
                }
            }
        }

        private static DateTime RoundDownForFrequency(DateTime time, TimeSpan frequency)
        {
            if (frequency == TimeSpan.FromDays(1))
            {
                return new DateTime(time.Year,
                                    time.Month,
                                    time.Day,
                                    0,
                                    0,
                                    0,
                                    time.Kind);
            }

            if (frequency == TimeSpan.FromHours(1))
            {
                return new DateTime(time.Year,
                                    time.Month,
                                    time.Day,
                                    time.Hour,
                                    0,
                                    0,
                                    time.Kind);
            }

            throw new NotImplementedException();
        }

        private static void LogSummaryReportAndReset(ref Dictionary<(Type type, string methodName), Aggregation> aggregations,
                                                     ref DateTime lastReportAt,
                                                     DateTime utcNow,
                                                     TimeSpan frequency,
                                                     string metricsFrequency,
                                                     ILogger logger)
        {
            var utcNowRounded = RoundDownForFrequency(utcNow, frequency);

            if (lastReportAt < utcNowRounded)
            {
                // log reports one by one + reset hourly counters
                foreach (var pair in aggregations)
                {
                    var hourlyCallMetricsAggregation = pair.Value;

                    WriteReportToLogger(metricsFrequency, hourlyCallMetricsAggregation, logger);
                }

                LogSummaryByCategory(metricsFrequency + "Summary", aggregations.Select(x => x.Value), logger);

                aggregations = new Dictionary<(Type type, string methodName), Aggregation>();

                lastReportAt = utcNowRounded;
            }
        }

        private static void LogSummaryByCategory(string frequency, IEnumerable<Aggregation> aggregations, ILogger logger)
        {
            var categoryToStringSummaryBuilder = new Dictionary<string, StringBuilder>();

            foreach (var aggregation in aggregations.OrderByDescending(x => x.TotalDurationMs))
            {
                if (!categoryToStringSummaryBuilder.TryGetValue(aggregation.MetricsCategory,
                                                                out var summaryBuilder))
                {
                    summaryBuilder = new StringBuilder("{MetricsFrequency} {MetricsCategory} CALL METRICS SUMMARY REPORT:\n\n");
                    categoryToStringSummaryBuilder.Add(aggregation.MetricsCategory, summaryBuilder);
                }

                AppendAggregationToSummary(summaryBuilder, aggregation);
            }

            foreach (var (categoryTag, summary) in categoryToStringSummaryBuilder)
            {
                logger.LogInformation(summary.ToString().Trim(), frequency, categoryTag);
            }
        }

        private static void AppendAggregationToSummary(StringBuilder sb, Aggregation agg)
        {
            sb.AppendLine($"{agg.Type}.{agg.MethodName}():");
            sb.AppendLine($"   TotalDuration: {agg.TotalDurationMs / 1000:0.000} s");
            sb.AppendLine($"   TotalCallsCount: {agg.TotalCallsCount}");
            sb.AppendLine($"   AverageDuration: {agg.AverageDurationMs / 1000:0.000} s");
            sb.AppendLine($"   MinDuration: {agg.MinDuration.TotalSeconds:0.000} s");
            sb.AppendLine($"   MaxDuration: {agg.MaxDuration.TotalSeconds:0.000} s");
            sb.AppendLine($"   First10Seconds: {agg.MinuteDistributions[0]}");
            sb.AppendLine($"   Second10Seconds: {agg.MinuteDistributions[1]}");
            sb.AppendLine($"   Third10Seconds: {agg.MinuteDistributions[2]}");
            sb.AppendLine($"   Fourth10Seconds: {agg.MinuteDistributions[3]}");
            sb.AppendLine($"   Fifth10Seconds: {agg.MinuteDistributions[4]}");
            sb.AppendLine($"   Sixth10Seconds: {agg.MinuteDistributions[5]}");
            sb.AppendLine();
        }

        private void AddToAggregations(Aggregation agg)
        {
            AddToAggregation(_hourlyAggregations, agg);
            AddToAggregation(_dailyAggregations, agg);
        }

        private static void AddToAggregation(Dictionary<(Type type, string methodName), Aggregation> aggregations, Aggregation newAgg)
        {
            if (!aggregations.TryGetValue((newAgg.Type, newAgg.MethodName), out var oldAgg))
            {
                aggregations.Add((newAgg.Type, newAgg.MethodName), newAgg);
            }
            else
            {
                if (newAgg.MaxDuration > oldAgg.MaxDuration)
                {
                    oldAgg.MaxDuration = newAgg.MaxDuration;
                }

                if (newAgg.MinDuration < oldAgg.MinDuration)
                {
                    oldAgg.MinDuration = newAgg.MinDuration;
                }

                for (int i = 0; i < newAgg.MinuteDistributions.Length; i++)
                {
                    oldAgg.MinuteDistributions[i] += newAgg.MinuteDistributions[i];
                }

                var newTotalCount = oldAgg.TotalCallsCount + newAgg.TotalCallsCount;

                double newAverage = 0;

                if (newTotalCount != 0)
                {
                    newAverage = (oldAgg.TotalDurationMs + newAgg.TotalDurationMs)
                                 / newTotalCount;
                }

                oldAgg.AverageDurationMs = newAverage;
                oldAgg.TotalCallsCount = newTotalCount;
                oldAgg.TotalDurationMs += newAgg.TotalDurationMs;
            }
        }

        private static void WriteReportToLogger(string metricsFrequency, Aggregation agg, ILogger logger)
        {
            logger.LogInformation(LogLabel.MethodCallMetrics,
                                  "{MetricsFrequency} {MetricsCategory} call metrics method report for: {Type:l}.{MethodName:l}():"
                                  + "\n  TotalDuration: {TotalDurationSec_num} s"
                                  + "\n  TotalCallsCount: {TotalCallsCount_num}"
                                  + "\n  AverageDuration: {AverageDurationSec_num} s"
                                  + "\n  MinDuration: {MinDurationSec_num} s"
                                  + "\n  MaxDuration: {MaxDurationSec_num} s"
                                  + "\n  First10SecondsCount: {First10SecondsCount_num}"
                                  + "\n  Second10SecondsCount: {Second10SecondsCount_num}"
                                  + "\n  Third10SecondsCount: {Third10SecondsCount_num}"
                                  + "\n  Fourth10SecondsCount: {Fourth10SecondsCount_num}"
                                  + "\n  Fifth10SecondsCount: {Fifth10SecondsCount_num}"
                                  + "\n  Sixth10SecondsCount: {Sixth10SecondsCount_num}",
                                  metricsFrequency,
                                  agg.MetricsCategory,
                                  agg.Type.Name,
                                  agg.MethodName,
                                  Math.Round(agg.TotalDurationMs / 1000, 3),
                                  agg.TotalCallsCount,
                                  Math.Round(agg.AverageDurationMs / 1000, 3),
                                  Math.Round(agg.MinDuration.TotalSeconds, 3),
                                  Math.Round(agg.MaxDuration.TotalSeconds, 3),
                                  agg.MinuteDistributions[0],
                                  agg.MinuteDistributions[1],
                                  agg.MinuteDistributions[2],
                                  agg.MinuteDistributions[3],
                                  agg.MinuteDistributions[4],
                                  agg.MinuteDistributions[5]);
        }
    }
}
