﻿// <copyright file="CallMetricsFactory.Aggregation.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;

namespace CleoOne.Common.Core.PerformanceMetrics
{
    public sealed partial class CallMetricsFactory
    {
        private class Aggregation
        {
            public Type Type { get; }

            public string MethodName { get; }

            public string MetricsCategory { get; }

            public long TotalCallsCount { get; set; }

            public double AverageDurationMs { get; set; }

            public TimeSpan MinDuration { get; set; }

            public TimeSpan MaxDuration { get; set; }

            public long[] MinuteDistributions { get; set; }

            public double TotalDurationMs { get; set; }

            public Aggregation(Type type, string methodName, string metricsCategory)
            {
                Type = type;
                MethodName = methodName;
                MetricsCategory = metricsCategory;

                AverageDurationMs = 0;
                TotalCallsCount = 0;
                MinDuration = TimeSpan.MaxValue;
                MaxDuration = TimeSpan.MinValue;
                MinuteDistributions = new long[6];
                TotalDurationMs = 0;
            }
        }
    }
}
