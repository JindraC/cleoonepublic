﻿// <copyright file="ICallMetrics.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Runtime.CompilerServices;

namespace CleoOne.Common.Core.PerformanceMetrics
{
    public interface ICallMetrics
    {
        IDisposable Measure([CallerMemberName] string methodName = "");
    }
}
