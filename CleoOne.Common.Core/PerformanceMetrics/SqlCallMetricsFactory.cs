﻿// <copyright file="SqlCallMetricsFactory.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace CleoOne.Common.Core.PerformanceMetrics
{
    public class SqlCallMetricsFactory : ICallMetricsFactory
    {
        private readonly CallMetricsFactory _callMetricsFactory;

        public SqlCallMetricsFactory(CallMetricsFactory callMetricsFactory)
        {
            _callMetricsFactory = callMetricsFactory;
        }

        public virtual ICallMetrics GetMetricsForThis(object contextObject)
        {
            var meter = _callMetricsFactory.GetMetricsForThisWithCategory(contextObject, "SqlMetrics");

            return meter;
        }
    }
}
