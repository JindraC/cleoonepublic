﻿// <copyright file="OrderException.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;

namespace CleoOne.Common.Core.Exceptions
{
    public class OrderException : Exception, IHasFrontendErrorMessage
    {
        public string FrontendErrorMessage { get; set; }

        //public OrderException()
        //{
        //}

        //public OrderException(string message)
        //    : base(message)
        //{
        //}

        //public OrderException(string message, Exception innerException)
        //    : base(message, innerException)
        //{
        //}

        public OrderException(string message, string frontendErrorMessage)
            : base(message)
        {
            FrontendErrorMessage = frontendErrorMessage;
        }

        public OrderException(string message, string frontendErrorMessage, Exception innerException)
            : base(message, innerException)
        {
            FrontendErrorMessage = frontendErrorMessage;
        }
    }
}
