// <copyright file="HttpMethod.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace CleoOne.Common.Core.Common.Web
{
    public static class HttpMethod
    {
        public const string GET = "GET";
        public const string POST = "POST";
        public const string DELETE = "DELETE";
        public const string PUT = "PUT";
        public const string OPTIONS = "OPTIONS";
    }
}
