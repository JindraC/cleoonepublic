﻿// <copyright file="StartUpHostBuilder.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.Common.Helpers;
using CleoOne.Common.Core.Common.Logging;
using CleoOne.Common.Core.DotNet.Helpers;
using Destructurama;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Events;
using Serilog.Extensions.Logging;
using System;
using System.Diagnostics;
using System.IO;

namespace CleoOne.Common.Core.Common.Web
{
    public static class StartUpHostBuilder
    {
        public static IConfigurationRoot CreateHostConfiguration(Type typeInAppsettingsAssembly)
        {
            var projectName = ConfigurationHelper.GetProjectNameForType(typeInAppsettingsAssembly);
            var currentEnv = EnvironmentHelper.GetEnvironmentAsString();

            var configuration = new ConfigurationBuilder().AddAppSettings(projectName, currentEnv)
                                                          .Build();

            return configuration;
        }

        public static void SetUpSerilogLogging(IConfigurationRoot configuration, string serviceName)
        {
            // Create logger configuration from application configuration
            var loggerConfiguration = new LoggerConfiguration()
                                     .ReadFrom.Configuration(configuration)
                                     .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                                     .MinimumLevel.Override("System.Net.Http.HttpClient", LogEventLevel.Warning)
                                     .Enrich.FromLogContext()
                                     .Enrich.WithProperty("ServiceName", serviceName)
                                     .Enrich.WithProperty("InstanceId", Guid.NewGuid())
                                     .Enrich.WithMachineName()
                                     .Enrich.With(new CorrelationEnricher())
                                     .Enrich.With(new LogLabelEnricher())
                                     .Destructure.UsingAttributes();

            var gitHash = configuration.GetValue<string>("GIT_HASH", null);

            if (gitHash != null) // We have git hash
            {
                loggerConfiguration = loggerConfiguration.Enrich.WithProperty("GitHash", gitHash);
            }

            if (configuration.GetValue<string>("CONTAINER", null) == "1") // this means running in docker
            {
                loggerConfiguration =
                    loggerConfiguration.WriteTo.Console(LogEventLevel.Error); // write only errors and fatals

                Serilog.Debugging.SelfLog.Enable(msg => Console.Error.WriteLine(msg));
            }
            else // this means local visual studio run
            {
                // string homePath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                // var dir = Path.Combine(homePath, "CleoLogs");
                // Directory.CreateDirectory(dir);

                // var file = File.CreateText(Path.Combine(dir, serviceName + "_serilog.log"));
                // var writer = TextWriter.Synchronized(file);
                if (Directory.Exists(@"C:\Program Files\Seq")) // write to seq if installed
                {
                    loggerConfiguration = loggerConfiguration.WriteTo.Seq("http://localhost:5341");
                }

                loggerConfiguration = loggerConfiguration
                                     .WriteTo.Console(
                                          outputTemplate:
                                          "[{Timestamp:HH:mm:ss:fff} {Level:u3}] {Message:lj}{NewLine}{Exception}"); // during local debug write everything
                //.WriteTo.Debug(); // for visual studio output window - removed because it really slows down debugging run (10 sec per 4k of messages) - need to put this into developers appsettings

                Serilog.Debugging.SelfLog.Enable(msg =>
                {
                    Console.Error.WriteLine(msg);
                    // writer.WriteLine(msg);
                    // writer.Flush();
                });
            }

            // TODO Add custom IFormatProvider for DateTimes, so we can render them in a better way
            // https://github.com/serilog/serilog/wiki/Formatting-Output#format-providers

            var logger = loggerConfiguration.CreateLogger();
            Log.Logger = logger;
            LoggerHelper.Factory = new SerilogLoggerFactory(logger);
        }

        public static IHost BuildStandardWebHost<TStartup>(string serviceName, string[] args)
            where TStartup : class
        {
            CultureHelper.SetInvariantCulture();

            Activity.DefaultIdFormat = ActivityIdFormat.W3C;

            var configuration = CreateHostConfiguration(typeof(TStartup));

            SetUpSerilogLogging(configuration, serviceName);

            var host = Host.CreateDefaultBuilder(args)
                           .UseSerilog()
                           .ConfigureWebHostDefaults(webBuilder =>
                            {
                                webBuilder.UseConfiguration(configuration);
                                webBuilder.UseStartup<TStartup>();

                                webBuilder.ConfigureKestrel(serverOptions =>
                                {
                                    serverOptions.AllowSynchronousIO = true;
                                });
                            })
                           .Build();

            Log.Logger.Information("Starting App {App}!", serviceName);

            return host;
        }
    }
}
