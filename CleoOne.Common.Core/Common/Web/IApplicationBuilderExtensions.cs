﻿// <copyright file="IApplicationBuilderExtensions.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.Common.Logging;
using Microsoft.AspNetCore.Builder;
using Serilog;

namespace CleoOne.Common.Core.Common.Web
{
    public static class IApplicationBuilderExtensions
    {
        public static void UseRequestLogging(this IApplicationBuilder app)
        {
            app.UseSerilogRequestLogging(opts =>
            {
                opts.GetLevel = LoggerHelper.ExcludeHealthChecks; // Use custom level function
            });
        }
    }
}
