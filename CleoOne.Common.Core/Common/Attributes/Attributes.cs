﻿// <copyright file="Attributes.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;

namespace CleoOne.Common.Core.Common.Attributes
{
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    #pragma warning disable SA1649 // File name should match first type name
    public sealed class EnumMessageAttribute : Attribute
        #pragma warning restore SA1649 // File name should match first type name
    {
        public string Message { get; set; }

        internal EnumMessageAttribute(string message)
        {
            Message = message;
        }
    }

    [AttributeUsage(AttributeTargets.Field, AllowMultiple = true)]
    public sealed class EnumCanParseFromAttribute : Attribute
    {
        public string Text { get; set; }

        internal EnumCanParseFromAttribute(string text)
        {
            Text = text;
        }
    }
}
