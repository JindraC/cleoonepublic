﻿// <copyright file="Enums.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.ComponentModel;

namespace CleoOne.Common.Core
{
    public static class Enums
    {
        /// <summary>
        /// Gets description of Enum or if there is none returns name of the enum as a string.
        /// </summary>
        public static string GetDescription(Enum value)
        {
            var fieldInfo = value.GetType().GetField(value.ToString());
            var attributes = (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);

            return attributes.Length > 0 ? attributes[0].Description : value.ToString();
        }
    }

    public enum EnvironmentSwitch
    {
        None = 0,
        Production = 1,
        Testing = 2,
        Development = 3,
    }

    public enum LogLabel
    {
        None = 0,
        MethodCallMetrics = 10,
        OneCallMetricsStart = 11,
        OneCallMetricsFinish = 12,
        FrontendError = 600,
    }
}
