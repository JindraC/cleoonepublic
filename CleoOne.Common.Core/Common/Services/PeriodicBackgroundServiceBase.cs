﻿// <copyright file="PeriodicBackgroundServiceBase.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.Common.Helpers;
using CleoOne.Common.Core.Common.Logging;
using CleoOne.Common.Core.DotNet.Extensions.Classes;
using CleoOne.Common.Core.DotNet.Helpers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace CleoOne.Common.Core.Common.Services
{
    public abstract class PeriodicBackgroundServiceBase : BackgroundService
    {
        protected readonly ILogger _logger;
        private readonly IConfiguration _configuration;

        protected bool _serviceInitialized;
        private int _executeCalledFlag = 0;

        public PeriodicBackgroundServiceBase(IConfiguration configuration)
        {
            _logger = this.GetLoggerForThis();
            _configuration = configuration;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            // This is here to force async execution, if there would be infinite loop before first await we would freeze whole application
            await Task.Yield();

            // this is here to detect if DI is not trying to start one service multiple times...
            if (Threads.IsInitialized(ref _executeCalledFlag))
            {
                _logger.LogCritical(
                    "{PeriodicService} ExecuteAsync called multiple times - this is bad. We need to reconsider whole usage of IHostedService iface.",
                    GetType());
            }

            if (!_configuration.IsServiceEnabledCustom(GetType().Name, _logger))
            {
                return;
            }

            var initTries = 0;
            var periodicServiceName = GetType().Name;

            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    if (!_serviceInitialized)
                    {
                        initTries++;

                        if (initTries == 4)
                        {
                            _logger.LogCritical("{PeriodicService} failed to initialize for {Tries} times. Quitting the service",
                                                periodicServiceName,
                                                initTries);

                            break;
                        }

                        await InitializeService();
                        _logger.LogInformation("{PeriodicService} initialized.", periodicServiceName);
                        _serviceInitialized = true;
                    }

                    _logger.LogInformation("{PeriodicService} starting one round.", periodicServiceName);
                    await DoWork(stoppingToken);
                    _logger.LogInformation("{PeriodicService} successfully finished one round.", periodicServiceName);

                    var hasFinished = await HasFinished();

                    if (hasFinished)
                    {
                        _logger.LogInformation("{PeriodicService} has successfully finished its lifetime. Quitting the run loop.",
                                               periodicServiceName);

                        break;
                    }

                    var nextTimeRun = await GetTimeOfNextRunAsync();

                    _logger.LogInformation("Next run of the service is {At}.", nextTimeRun);

                    await TaskHelper.DelayUntilRealTime(nextTimeRun, stoppingToken);
                }
                catch (TaskCanceledException)
                {
                    _logger.LogInformation("{PeriodicService} stopped using the stopping token.", periodicServiceName);
                }
                // IDEA add here catch for stopping exception - which can be thrown from the inside of the service
                catch (Exception ex)
                {
                    _logger.LogError(ex, "{PeriodicService} run threw and exception. Restarting.", periodicServiceName);

                    await TaskHelper.DelaySafely(10_000, stoppingToken);
                }
            }
        }

        protected virtual Task<bool> HasFinished()
        {
            return Task.FromResult(false);
        }

        protected abstract Task InitializeService();

        protected abstract Task DoWork(CancellationToken stoppingToken);

        protected abstract Task<DateTime> GetTimeOfNextRunAsync();
    }
}
