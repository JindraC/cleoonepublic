﻿// <copyright file="UnhandledExceptionsService.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.DotNet.Extensions.Classes;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;
using System.Runtime.ExceptionServices;
using System.Security;
using System.Threading;
using System.Threading.Tasks;

namespace CleoOne.Common.Core.Common.Services
{
    public class UnhandledExceptionsService : IHostedService
    {
        public UnhandledExceptionsService()
        {
            AppDomain.CurrentDomain.UnhandledException -= CurrentDomainOnUnhandledException;
            AppDomain.CurrentDomain.UnhandledException += CurrentDomainOnUnhandledException;

            TaskScheduler.UnobservedTaskException -= TaskSchedulerOnUnobservedTaskException;
            TaskScheduler.UnobservedTaskException += TaskSchedulerOnUnobservedTaskException;
        }

        [HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
        private static void TaskSchedulerOnUnobservedTaskException(object sender,
                                                                   UnobservedTaskExceptionEventArgs e)
        {
            var baseException = e.Exception?.GetBaseException();

            Log.Logger.Fatal(baseException,
                             "Unhandled exception caught by TaskScheduler.UnobservedTaskException: {ExceptionMessage}",
                             baseException?.Message);

            e.SetObserved();
        }

        [HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
        private static void CurrentDomainOnUnhandledException(object sender,
                                                              UnhandledExceptionEventArgs e)
        {
            var terminating = e.IsTerminating ? "TERMINATING" : "(not terminating)";

            if (e.ExceptionObject is Exception ex)
            {
                if (ex is StackOverflowException
                    || ex.InnerException is StackOverflowException
                    || ex is OutOfMemoryException
                    || ex.InnerException is OutOfMemoryException)
                {
                    Log.Logger.Fatal(
                        $"Unhandled {terminating} exception {ex.GetType().FullName} caught by CurrentDomain.UnhandledException. Stacktrace: {ex.InnerException?.StackTrace}\r\n{ex.StackTrace}");
                }
                else
                {
                    Log.Logger.Fatal(
                        $"Unhandled {terminating} exception {ex.GetType().Name}: {ex.ToMergedMessages()} caught by CurrentDomain.UnhandledException. Stacktrace: {ex.ToMergedStackTraces()}");
                }
            }
            else
            {
                Log.Logger.Fatal("Super critical "
                                 + terminating
                                 + " exception caught by CurrentDomain.UnhandledException - it was even not the 'Exception' type."
                                 + e.ExceptionObject);
            }

            Log.CloseAndFlush();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}
