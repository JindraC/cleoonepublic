﻿// <copyright file="WarmUpService.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.Application.Interfaces;
using CleoOne.Common.Core.Common.Helpers;
using CleoOne.Common.Core.Common.Logging;
using CleoOne.Common.Core.DependencyInjection;
using CleoOne.Common.Core.DotNet.Extensions;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace CleoOne.Common.Core
{
    public class WarmUpService : BackgroundService
    {
        private readonly ILogger _logger;
        private readonly IServiceProvider _provider;
        private readonly IServiceCollection _services;

        public WarmUpService(IServiceProvider provider, ServiceCollectionExposer collectionExposer)
        {
            _logger = this.GetLoggerForThis();
            _provider = provider;
            _services = collectionExposer.ServiceCollection;
        }

        private IList<IInitializable> GetInitializableServices()
        {
            var initializableTypes = _services.Where(s => s.ImplementationType?.IsAssignableTo(typeof(IInitializable)) == true)
                                              .Select(x => x.ImplementationType)
                                              .ToList();

            var entryAssemblyHashCode = GetEntryPointHashCode();

            var initializableServices = initializableTypes.Select(x => (IInitializable)_provider.GetService(x))
                                                          .Where(x => x != null)
                                                          .ToList()
                                                          .Shuffle(entryAssemblyHashCode);

            return initializableServices;
        }

        private int GetEntryPointHashCode()
        {
            var startup = _provider.GetService<ICleoStartup>()?.GetType().FullName;
            var startupOrEntryAssembly = startup ?? (Assembly.GetEntryAssembly()?.FullName ?? "").Split(",").FirstOrDefault() ?? "";
            var hashCode = startupOrEntryAssembly.Select(x => (int)x).Sum();

            return hashCode;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            if (stoppingToken.IsCancellationRequested)
            {
                return;
            }

            var initializableServices = GetInitializableServices();

            foreach (var initializableService in initializableServices)
            {
                if (stoppingToken.IsCancellationRequested)
                {
                    return;
                }

                await InitializeServiceAsync(initializableService, stoppingToken);

                // little sleep after each initialization:
                await TaskHelper.DelaySafely(TimeSpan.FromSeconds(1), stoppingToken);
            }
        }

        private async Task InitializeServiceAsync(IInitializable initializable,
                                                  CancellationToken cancellationToken)
        {
            if (cancellationToken.IsCancellationRequested)
            {
                return;
            }

            try
            {
                using var _ = _logger.MeasureAndLog("Initialization of {InitializableType} was successful.",
                                                    initializable.GetType());

                await initializable.InitializeAsync();
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex,
                                    "Cache updating failed for type {CacheType} with {Message}.",
                                    initializable.GetType(),
                                    ex.Message);
            }
        }
    }
}
