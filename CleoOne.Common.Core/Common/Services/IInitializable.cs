﻿// <copyright file="IInitializable.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Threading.Tasks;

namespace CleoOne.Common.Core.Application.Interfaces
{
    public interface IInitializable
    {
        Task InitializeAsync();
    }
}
