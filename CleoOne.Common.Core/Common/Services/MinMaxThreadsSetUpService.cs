﻿// <copyright file="MinMaxThreadsSetUpService.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.Common.Helpers;
using Microsoft.Extensions.Hosting;
using System.Threading;
using System.Threading.Tasks;

namespace CleoOne.Common.Core.Common.Services
{
    public class MinMaxThreadsSetUpService : IHostedService
    {
        public MinMaxThreadsSetUpService()
        {
            ThreadPool.SetMaxThreads(600, 600);
            //ThreadPool.SetMaxThreads(400, 200);

            if (EnvironmentHelper.IsDevelopment())
            {
                ThreadPool.SetMinThreads(200, 50);
            }
            else
            {
                ThreadPool.SetMinThreads(200, 200);
            }
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}
