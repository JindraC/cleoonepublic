﻿// <copyright file="UsedRamMemoryLoggerService.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.Common.Helpers;
using CleoOne.Common.Core.Common.Logging;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace CleoOne.Common.Core.Common.Services
{
    public class UsedRamMemoryLoggerService : IDisposable, IHostedService
    {
        private const int MAXIMUM_SLEEP_PERIODS = 60;
        private const int DEFAULT_CHECK_TIMEOUT_SECONDS = 2;
        private const int DEVELOPMENT_CHECK_TIMEOUT_SECONDS = 60;
        private readonly ILogger _logger;

        private readonly CancellationTokenSource _tokenSource;
        private readonly CancellationToken _cancellationToken;
        private Thread _backgroundThread;

        private long _highestLoggedMemory;

        public UsedRamMemoryLoggerService()
        {
            _logger = LoggerHelper.GetLoggerForThis(this);

            _tokenSource = new CancellationTokenSource();
            _cancellationToken = _tokenSource.Token;

            _backgroundThread = new Thread(_ => NeverEndingInformAboutRamMemory());
            _backgroundThread.IsBackground = true;
            _backgroundThread.Start();
        }

        private void NeverEndingInformAboutRamMemory()
        {
            if (EnvironmentHelper.IsDevelopment())
            {
                _logger.LogInformation("Memory service is turned off during development");

                return;
            }

            try
            {
                var checkTimeoutSeconds = EnvironmentHelper.IsDevelopment()
                                              ? DEVELOPMENT_CHECK_TIMEOUT_SECONDS
                                              : DEFAULT_CHECK_TIMEOUT_SECONDS;

                while (!_cancellationToken.IsCancellationRequested)
                {
                    try
                    {
                        for (int i = 0; i < MAXIMUM_SLEEP_PERIODS; i++)
                        {
                            _cancellationToken.WaitHandle.WaitOne(checkTimeoutSeconds * 1000);

                            var currentMemory = MemoryHelper.GcUsedMemoryBytes();

                            if (currentMemory > _highestLoggedMemory)
                            {
                                _highestLoggedMemory = Math.Max(currentMemory, _highestLoggedMemory);

                                break;
                            }
                        }

                        var shortAssemblyName = Assembly.GetEntryAssembly()?.FullName.Split(',').FirstOrDefault();

                        _logger.LogInformation("{ShortAssemblyName}: Used RAM: {UsedRamMb_num} MB",
                                               shortAssemblyName,
                                               Math.Round(MemoryHelper.GcUsedMemoryMegaBytes()));
                    }
                    catch (Exception ex)
                    {
                        if (!_cancellationToken.IsCancellationRequested)
                        {
                            _logger.LogError(ex, "Error while logging RAM memory.");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "NeverEndingInformAboutRamMemory crashed unexpectedly.");
            }
        }

        public void Dispose()
        {
            _tokenSource?.Cancel();
            _tokenSource?.Dispose();
            GC.SuppressFinalize(this);
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _tokenSource.Cancel();

            return Task.CompletedTask;
        }
    }
}
