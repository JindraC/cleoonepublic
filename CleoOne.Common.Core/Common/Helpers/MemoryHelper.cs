﻿// <copyright file="MemoryHelper.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Diagnostics;

namespace CleoOne.Common.Core.Common.Helpers
{
    public static class MemoryHelper
    {
        public static long UsedMemoryBytes()
        {
            using (var p = Process.GetCurrentProcess())
            {
                return p.PagedMemorySize64;
            }
        }

        public static string UsedMemoryMegaBytes(string stringFormat = "0 MB")
        {
            var usedInMB = UsedMemoryBytes() / 1000000D;

            return usedInMB.ToString(stringFormat);
        }

        public static long GcUsedMemoryBytes()
        {
            return GC.GetTotalMemory(false);
        }

        public static double GcUsedMemoryMegaBytes()
        {
            return (double)GC.GetTotalMemory(false) / (1024 * 1024);
        }
    }
}
