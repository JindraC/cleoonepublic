﻿// <copyright file="ConfigurationHelper.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.DotNet.Helpers;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using System.Reflection;

namespace CleoOne.Common.Core.Common.Helpers
{
    public static class ConfigurationHelper
    {
        public static IConfigurationRoot GetConfigurationForService<TServiceStartUp>()
        {
            return GetConfigurationWithAppSettingsOf(typeof(TServiceStartUp));
        }

        public static IConfigurationRoot GetConfigurationWithAppSettingsOf(Type typeInAppsettingsAssembly)
        {
            var projectName = GetProjectNameForType(typeInAppsettingsAssembly);

            var configuration = new ConfigurationBuilder()
                               .AddAppSettings(projectName)
                               .Build();

            return configuration;
        }

        public static string GetProjectNameForType(Type typeInAppsettingsAssembly)
        {
            return typeInAppsettingsAssembly.GetTypeInfo().Assembly.GetName().Name;
        }

        public static IConfigurationBuilder AddAppSettings(this IConfigurationBuilder configurationBuilder,
                                                           string projectName,
                                                           string currentEnvironment)
        {
            Assertion.ArgumentNotNull(projectName, nameof(projectName));

            return configurationBuilder.AddJsonFile($"appsettings.{projectName}.json", false)
                                       .AddJsonFile($"appsettings.{projectName}.{currentEnvironment}.json", optional: true)
                                       .AddEnvironmentVariables();
        }

        private static IConfigurationBuilder AddAppSettings(this IConfigurationBuilder configurationBuilder, string projectName)
        {
            var currentEnv = EnvironmentHelper.GetEnvironmentAsString();

            return configurationBuilder.AddAppSettings(projectName, currentEnv);
        }

        /// Ref: https://stackoverflow.com/a/52136848/3634867
        /// <summary>
        /// Gets the full path to the target project that we wish to test
        /// </summary>
        /// <param name="referencedAssembly">The target project's assembly.</param>
        /// <returns>The full path to the target project.</returns>
        public static string GetReferencedProjectPath(Assembly referencedAssembly)
        {
            // Get name of the target project which we want to test
            var projectName = referencedAssembly.GetName().Name
                              ?? throw new NullReferenceException($"Name of the referenced assembly was NULL");

            // Get currently executing test project path
            var applicationBasePath = AppContext.BaseDirectory;

            // Find the path to the target project
            var directoryInfo = new DirectoryInfo(applicationBasePath);

            do
            {
                var projectFileInfo = new FileInfo(Path.Combine(directoryInfo.FullName, projectName, $"{projectName}.csproj"));

                if (projectFileInfo.Exists)
                {
                    return Path.Combine(directoryInfo.FullName, projectName);
                }

                directoryInfo = directoryInfo.Parent;
            }
            while (directoryInfo != null);

            throw new Exception($"Project root could not be located using the application root {applicationBasePath}.");
        }
    }
}
