﻿// <copyright file="TaskHelper.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Runtime.ExceptionServices;
using System.Threading;
using System.Threading.Tasks;

namespace CleoOne.Common.Core.Common.Helpers
{
    public static class TaskHelper
    {
        public static async Task DelayUntilRealTime(DateTime delayUntil, CancellationToken token)
        {
            if (delayUntil.Kind != DateTimeKind.Utc)
            {
                throw new ArgumentException("Next time run should be UTC kind.");
            }

            var waitTime = delayUntil.Ticks - DateTime.UtcNow.Ticks;

            if (waitTime <= 0)
            {
                return;
            }

            await DelaySafely(TimeSpan.FromTicks(waitTime), token);

            // This is to ensure that we return from this method only really after the specified time as Task.Delay can finish earlier

            while (DateTime.UtcNow < delayUntil)
            {
                Thread.Yield();
            }
        }

        /// <summary>
        /// Task.Delay() method made safe: does not throw exception when cancel is requested.
        /// </summary>
        /// <param name="delay"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public static async Task DelaySafely(TimeSpan delay, CancellationToken cancellationToken)
        {
            // The method can still throw ArgumentOutOfRangeException

            try
            {
                await Task.Delay(delay, cancellationToken);
            }
            catch (TaskCanceledException)
            {
            }
            catch (ObjectDisposedException)
            {
            }
        }

        /// <summary>
        /// Task.Delay() method made safe: does not throw exception when cancel is requested.
        /// </summary>
        /// <param name="delayMs"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public static async Task DelaySafely(int delayMs, CancellationToken cancellationToken)
        {
            await DelaySafely(TimeSpan.FromMilliseconds(delayMs), cancellationToken);
        }

        /// <summary>
        /// Task.WaitAll() function. But if exception is thrown, it is the base exception, not the AggregateException
        /// </summary>
        /// <param name="tasks"></param>
        public static void WaitAllAndRethrow(params Task[] tasks)
        {
            try
            {
                Task.WaitAll(tasks);
            }
            catch (AggregateException ex)
            {
                var baseException = ex.GetBaseException();

                ExceptionDispatchInfo.Capture(baseException).Throw();
            }
        }
    }
}
