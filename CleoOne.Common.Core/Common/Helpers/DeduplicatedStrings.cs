﻿// <copyright file="DeduplicatedStrings.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Concurrent;

namespace CleoOne.Business.Crypto.Common.Components.Caches
{
    internal class DeduplicatedStrings
    {
        private static readonly Lazy<DeduplicatedStrings> lazyExchanges =
            new Lazy<DeduplicatedStrings>(() => new DeduplicatedStrings(), isThreadSafe: true);

        public static DeduplicatedStrings Exchanges => lazyExchanges.Value;

        private static readonly Lazy<DeduplicatedStrings> lazyCurrencies =
            new Lazy<DeduplicatedStrings>(() => new DeduplicatedStrings(), isThreadSafe: true);

        public static DeduplicatedStrings Currencies => lazyCurrencies.Value;

        private readonly ConcurrentDictionary<string, string> _stringsDictionary = new ConcurrentDictionary<string, string>();

        protected DeduplicatedStrings()
        {
        }

        public string Deduplicate(string currencyString)
        {
            if (currencyString == null)
            {
                return null;
            }

            return _stringsDictionary.GetOrAdd(currencyString, _ => currencyString);
        }
    }
}
