﻿// <copyright file="HtmlHelper.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace CleoOne.Common.Core.Common.Helpers
{
    public static class HtmlHelper
    {
        // This method prevents HTML injection attack
        public static string ReplaceNotAllowedCharacters(string nameOrDescription)
        {
            if (string.IsNullOrEmpty(nameOrDescription)
                || !nameOrDescription.Contains("<"))
            {
                return nameOrDescription;
            }

            // replacing html tag "<" by similar looking "<", which will prevent insertion attacks
            // See: https://www.compart.com/en/unicode/U+003C
            return nameOrDescription.Replace("<", "\uFE64");
        }
    }
}
