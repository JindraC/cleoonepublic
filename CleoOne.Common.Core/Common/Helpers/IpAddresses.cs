﻿// <copyright file="IpAddresses.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.Common.Logging;
using CleoOne.Common.Core.DotNet.Extensions.Classes;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;

namespace CleoOne.Common.Core.Common.Helpers
{
    public static class IpAddresses
    {
        private static readonly Lazy<string> _myExternalIpAddressLazy = new Lazy<string>(GetMyExternalIpAddressPrivate, isThreadSafe: true);

        private static readonly Lazy<string> _myInternalIpAddressLazy = new Lazy<string>(GetMyInternalIpAddressPrivate, isThreadSafe: true);

        private static readonly ILogger _logger = LoggerHelper.GetLogger(typeof(IpAddresses));

        public static bool PingHost(string nameOrAddress, int timeout = 5000)
        {
            bool pingable = false;

            using (Ping pinger = new Ping())
            {
                try
                {
                    PingReply reply = pinger.Send(nameOrAddress, timeout);
                    pingable = reply?.Status == IPStatus.Success;
                }
                catch (PingException)
                {
                    // Discard PingExceptions and return false;
                }
            }

            return pingable;
        }

        public static string GetMyExternalIpAddress() => _myExternalIpAddressLazy.Value;

        private static string GetMyExternalIpAddressPrivate()
        {
            var addresses = new[]
            {
                "http://bot.whatismyipaddress.com",
                "http://ipinfo.io/ip",
                "http://api.ipify.org",
                "http://icanhazip.com",
                "http://ident.me",
            };

            var exceptions = new List<(string address, Exception exception)>();

            foreach (var address in addresses)
            {
                try
                {
                    WebRequest request = WebRequest.Create(address);
                    request.Timeout = 3000;

                    using WebResponse response = request.GetResponse();

                    using var responseStream = response.GetResponseStream() ?? throw new Exception("Response stream was null");

                    // Warning disabled because of false positive CA2000 from analyzer.
                    #pragma warning disable CA2000 // Dispose objects before losing scope
                    using var streamReader = new StreamReader(responseStream, leaveOpen: true);
                    #pragma warning restore CA2000 // Dispose objects before losing scope

                    return streamReader.ReadToEndWithTimeout(TimeSpan.FromMilliseconds(request.Timeout),
                                                             () => new Exception("Timeout for: " + address),
                                                             _logger)
                                       .Trim();
                }
                catch (Exception ex)
                {
                    exceptions.Add((address, ex));
                }
            }

            throw new Exception($"Failed {exceptions.Count} times with obtaining the external IP", exceptions.Last().exception);
        }

        public static string GetMyInternalIpAddress() => _myInternalIpAddressLazy.Value;

        private static string GetMyInternalIpAddressPrivate()
        {
            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());

            string lastLocalIP = null;

            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily.ToString() == "InterNetwork")
                {
                    lastLocalIP = ip.ToString();
                }
            }

            if (string.IsNullOrEmpty(lastLocalIP))
            {
                throw new ApplicationException("Local IP address failed to obtain.");
            }

            return lastLocalIP;
        }
    }
}
