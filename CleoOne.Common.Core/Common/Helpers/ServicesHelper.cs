﻿// <copyright file="ServicesHelper.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.Common.Components.Times;
using CleoOne.Common.Core.DependencyInjection;
using CleoOne.Common.Core.PerformanceMetrics;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace CleoOne.Common.Core.Common.Helpers
{
    public static class ServicesHelper
    {
        /// <summary>
        /// Adds EVERYTHING what is LAZY from project CleoOne.Common.Core (services, caches, repositories ...)
        /// </summary>
        public static IServiceCollection AddCleoOneCommonCore(this IServiceCollection services,
                                                              IConfiguration configuration)
        {
            // we prevent running this method twice for same IServiceCollection:
            if (RegisteredServices.Has(services))
            {
                return services;
            }

            services.AddHttpClient();

            services.AddSingleton(configuration);

            // call metrics:
            services.AddSingleton(CallMetricsFactory.Instance);
            services.AddSingleton<SqlCallMetricsFactory>();
            services.AddSingleton<MongoCallMetricsFactory>();
            services.AddSingleton<ApiCallMetricsFactory>();

            // other:
            services.AddSingleton<ITime>(_ => RealTime.Instance)
                    .AddSingleton<ILogger>(s => s.GetRequiredService<ILogger<object>>())
                    .AddSingleton<ServiceCollectionExposer>(_ => new ServiceCollectionExposer(services));

            RegisteredServices.Add(services);

            return services;
        }
    }
}
