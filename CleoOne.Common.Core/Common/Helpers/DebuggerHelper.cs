﻿// <copyright file="DebuggerHelper.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Collections.Generic;

namespace CleoOne.Common.Core
{
    /// <summary>
    /// Could help with debugging in whole solution<br/>
    /// use Notes property like storage in cooperation with<br/>
    /// <strong>Action</strong> and <strong>Condition</strong><br/>
    /// feature of Visual Studio breakpoints
    /// </summary>
    public static class DebuggerHelper
    {
        /// <summary>
        /// Set <strong>Action</strong> breakpoint like this:<br/>
        /// {DebuggerHelper.Notes["AddPortfolioTest"] = 1;}
        /// <br/>
        /// <br/>Set <strong>Condition</strong> breakpoint like this:<br/>
        /// DebuggerHelper.Notes.TryGetValue("AddPortfolioTest",out var _)
        /// </summary>
        public static Dictionary<string, object> Notes { get; } = new Dictionary<string, object>();
    }
}
