﻿// <copyright file="RegisteredServices.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.DotNet.Collections;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace CleoOne.Common.Core.Common.Helpers
{
    public static class RegisteredServices
    {
        private const int COUNT_LIMIT = 100;

        private static readonly object _locker = new object();
        private static ServicesDictionary _registered = new ServicesDictionary();

        public static bool Has(IServiceCollection services,
                               [CallerMemberName] string methodName = null,
                               [CallerFilePath] string filePath = null)
        {
            lock (_locker)
            {
                var container = GetContainerFor(services);

                return container.Has(methodName, filePath);
            }
        }

        public static void Add(IServiceCollection services,
                               [CallerMemberName] string methodName = null,
                               [CallerFilePath] string filePath = null)
        {
            lock (_locker)
            {
                if (_registered.Count >= COUNT_LIMIT)
                {
                    _registered = new ServicesDictionary(_registered.Skip(_registered.Count / 2));
                }

                var container = GetContainerFor(services);

                container.Add(methodName, filePath);
            }
        }

        private static ServicesContainer GetContainerFor(IServiceCollection services)
        {
            lock (_locker)
            {
                if (_registered.TryGetValue(services, out var container))
                {
                    return container;
                }

                container = ServicesContainer.From(services);

                _registered.Add(container);

                return container;
            }
        }

        private class ServicesContainer
        {
            public object Services { get; private set; }

            private readonly HashSet<(string methodName, string filePath)> _registeredMethods =
                new HashSet<(string methodName, string filePath)>();

            public static ServicesContainer From(object services)
            {
                return new ServicesContainer()
                {
                    Services = services,
                };
            }

            public bool Has(string methodName, string filePath)
            {
                return _registeredMethods.Contains((methodName, filePath));
            }

            public void Add(string methodName, string filePath)
            {
                _registeredMethods.Add((methodName, filePath));
            }
        }

        private class ServicesDictionary : DictionaryList<object, ServicesContainer>
        {
            public ServicesDictionary()
                : base(ReferenceEqualityComparer.Instance)
            {
            }

            public ServicesDictionary(IEnumerable<ServicesContainer> items)
                : this()
            {
                AddRange(items);
            }

            protected override object GetKey(ServicesContainer item)
            {
                return item.Services;
            }
        }
    }
}
