﻿// <copyright file="EnvironmentHelper.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.Common.Logging;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;

namespace CleoOne.Common.Core.Common.Helpers
{
    public static class EnvironmentHelper
    {
        private static readonly ILogger _logger = LoggerHelper.GetLogger(typeof(EnvironmentHelper));

        private static readonly EnvironmentSwitch _environment = GetEnvironment();

        public const string Development = "Development";
        public const string Testing = "Testing";
        public const string Production = "Production";

        private const string ENVIRONMENT_LABEL = "ASPNETCORE_ENVIRONMENT";

        private static EnvironmentSwitch GetEnvironment()
        {
            var value = Environment.GetEnvironmentVariable(ENVIRONMENT_LABEL);

            switch (value)
            {
                case Development:
                    return EnvironmentSwitch.Development;
                case Testing:
                    return EnvironmentSwitch.Testing;
                case Production:
                    return EnvironmentSwitch.Production;
                case null:
                case "":
                    var myExternalIpAddress = IpAddresses.GetMyExternalIpAddress();

                    _logger.LogError($"Environment variable: {ENVIRONMENT_LABEL} is not set"
                                     + $" while using external ip address: {{ExternalIpAddress}}. "
                                     + $"Used environment: {{Environment}}",
                                     myExternalIpAddress,
                                     Development);

                    return EnvironmentSwitch.Development;

                default:
                    throw new ArgumentException($"Unknown environment value: '{value}'");
            }
        }

        public static string GetEnvironmentAsString()
        {
            return _environment.ToString();
        }

        public static bool IsDevelopment() => _environment == EnvironmentSwitch.Development;

        public static bool IsTesting() => _environment == EnvironmentSwitch.Testing;

        public static bool IsProduction() => _environment == EnvironmentSwitch.Production;

        public static bool IsDevelopment(this IWebHostEnvironment hostingEnvironment)
        {
            return hostingEnvironment.IsEnvironment(Development);
        }

        public static bool IsTesting(this IWebHostEnvironment hostingEnvironment)
        {
            return hostingEnvironment.IsEnvironment(Testing);
        }

        public static bool IsProduction(this IWebHostEnvironment hostingEnvironment)
        {
            return hostingEnvironment.IsEnvironment(Production);
        }

        public static void SetAsDevelopment()
        {
            Environment.SetEnvironmentVariable(ENVIRONMENT_LABEL, Development);
        }
    }
}
