﻿// <copyright file="ActivityExtensions.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Diagnostics;

namespace CleoOne.Common.Core.Common.Logging
{
    public static class ActivityExtensions
    {
        private static string _correlationIdKey = "CorrelationId";

        public static string GetCorrelationIdOrNull(this Activity activity)
        {
            return activity.GetBaggageItem(_correlationIdKey);
        }

        public static Activity AddCorrelationId(this Activity activity, string correlationId)
        {
            return activity.AddBaggage(_correlationIdKey, correlationId);
        }
    }
}
