﻿// <copyright file="LoggerHelper.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Serilog.Events;
using Serilog.Extensions.Logging;
using System;

namespace CleoOne.Common.Core.Common.Logging
{
    public static class LoggerHelper
    {
        // Internally this factory uses Serilog.Log.Logger property, if it is not null.
        // Because we set our configured logger to Log.Logger in startup classes - the factory is using correctly configured logger.
        // We set this factory here like this -> so in test run it would provide to classes logger saved in Log.Logger
        public static SerilogLoggerFactory Factory { get; internal set; } = new SerilogLoggerFactory();

        public static ILogger GetLoggerForThis(this object contextObject)
        {
            if (Factory == null)
            {
                throw new Exception("Logger was not initialized properly.");
            }

            if (contextObject == null)
            {
                throw new ArgumentNullException(nameof(contextObject));
            }

            var contextType = contextObject.GetType();

            // this is the case when someone passes this.GetType() or typeof(SomeType) in static context
            if (contextObject is Type type)
            {
                contextType = type;
            }

            return Factory.CreateLogger(contextType);
        }

        public static ILogger GetLogger(Type type)
        {
            if (Factory == null)
            {
                throw new Exception("Logger was not initialized properly.");
            }

            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            return Factory.CreateLogger(type);
        }

        private static bool IsHealthCheckEndpoint(HttpContext ctx)
        {
            var endpoint = ctx.GetEndpoint();

            if (endpoint != null) // same as !(endpoint is null)
            {
                return string.Equals(
                    endpoint.DisplayName,
                    "Health checks",
                    StringComparison.Ordinal);
            }

            // No endpoint, so not a health check endpoint
            return false;
        }

        public static LogEventLevel ExcludeHealthChecks(HttpContext ctx, double _, Exception ex)
        {
            if (ex != null)
            {
                return LogEventLevel.Error;
            }
            else
            {
                if (ctx.Response.StatusCode > 499)
                {
                    return LogEventLevel.Error;
                }
                else
                {
                    if (IsHealthCheckEndpoint(ctx))
                    {
                        return LogEventLevel.Verbose;
                    }
                    else
                    {
                        return LogEventLevel.Information;
                    }
                }
            }
        }
    }
}
