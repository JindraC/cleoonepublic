﻿// <copyright file="LoggingExtensions.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.DotNet.Extensions.Strings;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.Linq;

namespace CleoOne.Common.Core.Common.Logging
{
    public static class LoggingExtensions
    {
        private class TimedOperation : IDisposable
        {
            private readonly ILogger _logger;
            private readonly string _message;
            private readonly object[] _args;
            private readonly Stopwatch _stopwatch;

            public TimedOperation(ILogger logger, string message, params object[] args)
            {
                _logger = logger;
                _message = message;
                _args = args;
                _stopwatch = new Stopwatch();
                _stopwatch.Start();
            }

            public void Dispose()
            {
                _stopwatch.Stop();

                _logger.LogInformation(_message + " completed in {ElapsedSeconds_num} s.",
                                       _args.Append(_stopwatch.ElapsedMilliseconds.MillisecondsToSeconds()).ToArray());
            }
        }

        public static IDisposable MeasureAndLog(this ILogger logger, string message, params object[] args)
        {
            return new TimedOperation(logger, message, args);
        }

        public class AsLazyJsonWrapper<T>
        {
            private readonly T _wrapped;
            private readonly bool _withIndents;

            public AsLazyJsonWrapper(T thingBeingWrapped, bool withIndents)
            {
                _wrapped = thingBeingWrapped;
                _withIndents = withIndents;
            }

            public override string ToString()
            {
                return _wrapped?.ToJsonSafe(withIndents: _withIndents);
            }
        }

        public static AsLazyJsonWrapper<T> AsLazyJson<T>(this T o, bool withIndents = false)
        {
            return new AsLazyJsonWrapper<T>(o, withIndents);
        }

        private static bool _alreadyLogged = false;

        [Obsolete("Call this method only for debugging reasons, then remove it. It can affect performance")]
        public static void LogStackTraceDeeperThan(this ILogger logger, int deeperThan)
        {
            if (_alreadyLogged)
            {
                return;
            }

            var stackTrace = new StackTrace();

            if (stackTrace.FrameCount > deeperThan)
            {
                logger.LogCritical($"Too deep in the stack {stackTrace.FrameCount}. StackTrace: " + Environment.StackTrace);
                _alreadyLogged = true;
            }
        }

        public static double MillisecondsToSeconds(this long milliseconds) => Math.Round(milliseconds / 1000d, 3);

        public static double MillisecondsToSeconds(this double milliseconds) => Math.Round(milliseconds / 1000d, 3);

        public static double MillisecondsToSeconds(this int milliseconds) => Math.Round(milliseconds / 1000d, 3);

        public static double MillisecondsToSeconds(this decimal milliseconds) => Math.Round((double)milliseconds / 1000d, 3);
    }
}
