﻿// <copyright file="CorrelationEnricher.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using Serilog.Core;
using Serilog.Events;
using System.Diagnostics;

namespace CleoOne.Common.Core.Common.Logging
{
    internal class CorrelationEnricher : ILogEventEnricher
    {
        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            var correlationId = Activity.Current?.GetCorrelationIdOrNull();

            if (correlationId != null)
            {
                logEvent.AddPropertyIfAbsent(
                    propertyFactory.CreateProperty("CorrelationId", correlationId));
            }
        }
    }
}
