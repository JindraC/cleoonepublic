﻿// <copyright file="LogLabelEnricher.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using Serilog.Core;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CleoOne.Common.Core.Common.Logging
{
    public class LogLabelEnricher : ILogEventEnricher
    {
        private static readonly IReadOnlyDictionary<int, string> _labels =
            Enum.GetValues(typeof(LogLabel)).Cast<int>().ToDictionary(x => x, x => ((LogLabel)x).ToString());

        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            if (logEvent.Properties.TryGetValue("EventId", out var eventId)
                && eventId is StructureValue structureValue
                && structureValue.Properties.Count == 1
                && structureValue.Properties[0]?.Value is ScalarValue scalarValue
                && scalarValue.Value is int labelId
                && _labels.TryGetValue(labelId, out var labelName))
            {
                logEvent.AddPropertyIfAbsent(propertyFactory.CreateProperty("LogLabel", labelName));
            }
        }
    }
}
