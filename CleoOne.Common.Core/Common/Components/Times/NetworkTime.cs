﻿// <copyright file="NetworkTime.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.Common.Logging;
using CleoOne.Common.Core.DotNet.Extensions.Strings;
using CleoOne.Common.Core.DotNet.Helpers;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace CleoOne.Common.Core.Common.Components.Times
{
    public class NetworkTime : IDisposable
    {
        public NetworkTimeType TimeType { get; }

        private readonly ILogger _logger;

        private ManualResetEventSlim _wasInitiatedResetEvent = new ManualResetEventSlim();
        private volatile int _millisecondsShift;

        private bool _continueNeverendingLoop = true;
        private DateTime _lastRecordedPcTime;

        public bool IsInitialized => _wasInitiatedResetEvent == null;

        private Thread _backgroundThread;
        private Exception _initializationException;

        public NetworkTime(NetworkTimeType networkTimeType)
        {
            TimeType = networkTimeType;
            _logger = LoggerHelper.GetLoggerForThis(this);

            _backgroundThread = Threads.SafeRun(NeverendingTimeUpdate, _logger);
        }

        public void WaitForFirstSynchronization()
        {
            var timeoutMs = 5000;

            var initialized = _wasInitiatedResetEvent?.Wait(timeoutMs) ?? true;

            if (!initialized)
            {
                var errorMessage = $"{nameof(NetworkTime)} of type {TimeType} failed to initialize in {timeoutMs} ms";

                if (_initializationException is Exception exception)
                {
                    throw new Exception(errorMessage, exception);
                }
                else
                {
                    throw new Exception(errorMessage);
                }
            }
        }

        public DateTime GetSynchronizedTimeUtc()
        {
            WaitForFirstSynchronization();

            return DateTime.UtcNow.AddMilliseconds(_millisecondsShift);
        }

        ~NetworkTime()
        {
            _continueNeverendingLoop = false;
        }

        private void NeverendingTimeUpdate()
        {
            while (_continueNeverendingLoop)
            {
                UpdateNetworkTimeSafely();

                // check every 1 minute, if time did not go back
                RecordAndCheckTimeDidNotMoveBack(periodMilliseconds: 100, repeatCount: 60 * 10);
            }
        }

        private void RecordAndCheckTimeDidNotMoveBack(int periodMilliseconds, int repeatCount)
        {
            for (int i = 0; i < repeatCount; i++)
            {
                var utcNow = DateTime.UtcNow;

                var previousRecordedPcTime = _lastRecordedPcTime;

                _lastRecordedPcTime = utcNow;

                if (previousRecordedPcTime > utcNow)
                {
                    break;
                }

                Thread.Sleep(periodMilliseconds);
            }
        }

        private void UpdateNetworkTimeSafely()
        {
            try
            {
                UpdateNetworkTime();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Error while updating {nameof(NetworkTime)}");
            }
        }

        private const string BINANCE_TIME_ENDPOINT = "http://www.binance.com/api/v1/time";
        private const string BINANCE_US_TIME_ENDPOINT = "https://api.binance.us/api/v1/time";

        private void UpdateNetworkTime()
        {
            int numberOfMeasurements = 5;

            DateTime networkTime;

            switch (TimeType)
            {
                case NetworkTimeType.BinanceTime:
                    networkTime = this.GetBinanceTimeUtcSafely(BINANCE_TIME_ENDPOINT);

                    break;
                case NetworkTimeType.BinanceUsTime:
                    networkTime = this.GetBinanceTimeUtcSafely(BINANCE_US_TIME_ENDPOINT);

                    break;
                case NetworkTimeType.WindowsTimeServers:
                    networkTime = this.GetNetworkTimeUtc(numberOfMeasurements);

                    break;
                case NetworkTimeType.CleoTimeServer:
                    networkTime = this.GetSynchronizedTimeFromCleoTimeServer(numberOfMeasurements);

                    break;
                default: throw new NotImplementedException($"Not implemented for type: {TimeType}");
            }

            var candidateShift = (int)(networkTime - DateTime.UtcNow).TotalMilliseconds;

            _millisecondsShift = candidateShift;

            var manualResetEvent = _wasInitiatedResetEvent;
            _wasInitiatedResetEvent = null;
            manualResetEvent?.Set();
        }

        private DateTime GetBinanceTimeUtcSafely(string address)
        {
            var trialsCount = 0;

            while (true)
            {
                try
                {
                    var stopwatch = Stopwatch.StartNew();

                    string downloadedTimeString;

                    using (var client = new WebClient())
                    {
                        client.CachePolicy = new System.Net.Cache.RequestCachePolicy(System.Net.Cache.RequestCacheLevel.NoCacheNoStore);

                        downloadedTimeString = client.DownloadString(address);
                    }

                    if (stopwatch.ElapsedMilliseconds < 700)
                    {
                        var unixTimestamp = downloadedTimeString.DeserializeJson<BinanceTimeResp>().ServerTime;

                        var responseTime = ToBinanceDatetimeUtc(unixTimestamp);

                        var elapsedMs = (int)stopwatch.ElapsedMilliseconds;

                        var adjustedResponseTime = responseTime.AddMilliseconds(elapsedMs / 2.0);

                        return adjustedResponseTime;
                    }
                }
                catch (Exception ex)
                {
                    _initializationException = ex;

                    _logger.LogError(ex, $"Error while updating time from Binance");
                }

                Thread.Sleep(Math.Min(10000, 1000 * trialsCount++));
            }
        }

        private static DateTime ToBinanceDatetimeUtc(long milliseconds)
        {
            return new DateTime(1970,
                                1,
                                1,
                                0,
                                0,
                                0,
                                DateTimeKind.Utc).AddMilliseconds(milliseconds);
        }

        private class BinanceTimeResp
        {
            public long ServerTime { get; set; }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Usage",
                                                         "CA1801:Review unused parameters",
                                                         Justification = "Not yet implemented.")]
        private DateTime GetSynchronizedTimeFromCleoTimeServer(int numberOfMeasurements)
        {
            throw new NotImplementedException($"Cleo time server is not yet implemented.");

            //var client = new WebClient();
            //client.CachePolicy = new System.Net.Cache.RequestCachePolicy(System.Net.Cache.RequestCacheLevel.NoCacheNoStore);
            //var endpoint = $"http://{CleoTimeServerIpAddress}{CleoTimeServerPort}/api/datetime";
            //var elapsedWithDifference = new List<(int elapsedMs, double differenceMs)>();
            //for (int i = 0; i < numberOfMeasurements; i++)
            //{
            //    var startedAt = DateTime.UtcNow;
            //    var stopwatch = Stopwatch.StartNew();
            //    var endpointWithCachePrevention = $"{endpoint}?random={DateTime.UtcNow.Ticks}";
            //    var responseTime = DateTime.Parse(client.DownloadString(endpointWithCachePrevention).Trim('"'), CultureInfo.InvariantCulture).ToUniversalTime();
            //    var elapsedMs = (int)stopwatch.ElapsedMilliseconds;
            //    var adjustedResponseTime = responseTime.AddMilliseconds(elapsedMs / 2.0);
            //    var differenceMs = (adjustedResponseTime - startedAt).TotalMilliseconds;
            //    elapsedWithDifference.Add((elapsedMs, differenceMs));
            //}

            //var finalDifferenceMs = elapsedWithDifference.OrderBy(tuple => tuple.elapsedMs).Take(3).Average(tuple => tuple.differenceMs);
            //return DateTime.UtcNow.AddMilliseconds(finalDifferenceMs);
        }

        private DateTime GetNetworkTimeUtc(int numberOfMeasurements)
        {
            //default Windows time server
            const string NtpServer = "time.windows.com";

            var addresses = Dns.GetHostEntry(NtpServer).AddressList;
            var firstIpAddress = addresses.First().ToString();

            var ntpDatas = new List<(DateTime starttime, int elapsedMs, byte[] ntpData)>();

            //The UDP port number assigned to NTP is 123
            var ipEndPoint = new IPEndPoint(IPAddress.Parse(firstIpAddress), 123);
            //NTP uses UDP

            using (var socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp))
            {
                socket.Connect(ipEndPoint);

                //Stops code hang if NTP is blocked
                socket.ReceiveTimeout = 1000;

                for (int i = 0; i < numberOfMeasurements; i++)
                {
                    try
                    {
                        var ntpData = GenerateNtpDataBytes();
                        var startedAt = DateTime.UtcNow;
                        var stopwatch = Stopwatch.StartNew();

                        socket.Send(ntpData);
                        socket.Receive(ntpData);

                        ntpDatas.Add((starttime: startedAt, (int)stopwatch.ElapsedMilliseconds, ntpData));
                    }
                    catch (Exception ex)
                    {
                        throw new Exception($"Error while {i}th socket call to NTP Network Time server: {ipEndPoint}. "
                                            + $"All addresses: {string.Join(", ", addresses.Select(item => item.ToString()))}",
                                            ex);
                    }
                }

                socket.Close();
            }

            //Offset to get to the "Transmit Timestamp" field (time at which the reply 
            //departed the server for the client, in 64-bit timestamp format."
            const byte ServerReplyTime = 40;

            var resultTimes = new List<(double adjustedMilliseconds, int elapsed)>();

            foreach (var tuple in ntpDatas)
            {
                var ntpData = tuple.ntpData;

                //Get the seconds part
                ulong intPart = BitConverter.ToUInt32(ntpData, ServerReplyTime);

                //Get the seconds fraction
                ulong fractPart = BitConverter.ToUInt32(ntpData, ServerReplyTime + 4);

                //Convert From big-endian to little-endian
                intPart = SwapEndianness(intPart);
                fractPart = SwapEndianness(fractPart);

                var milliseconds = (intPart * 1000) + ((fractPart * 1000) / 0x100000000L);

                var adjustedMilliseconds =
                    milliseconds + (DateTime.UtcNow - tuple.starttime.AddMilliseconds(tuple.elapsedMs / 2)).TotalMilliseconds;

                resultTimes.Add((adjustedMilliseconds, tuple.elapsedMs));
            }

            var finalMilliseconds = resultTimes.OrderBy(tuple => tuple.elapsed).Take(3).Average(tuple => tuple.adjustedMilliseconds);

            //**UTC** time
            var networkDateTime = new DateTime(1900,
                                               1,
                                               1,
                                               0,
                                               0,
                                               0,
                                               DateTimeKind.Utc).AddMilliseconds(finalMilliseconds);

            return networkDateTime;
        }

        private static byte[] GenerateNtpDataBytes()
        {
            // NTP message size - 16 bytes of the digest (RFC 2030)
            var ntpData = new byte[48];

            //Setting the Leap Indicator, Version Number and Mode values
            ntpData[0] = 0x1B; //LI = 0 (no warning), VN = 3 (IPv4 only), Mode = 3 (Client Mode)

            return ntpData;
        }

        // stackoverflow.com/a/3294698/162671
        private static uint SwapEndianness(ulong x)
        {
            return (uint)(((x & 0x000000ff) << 24) + ((x & 0x0000ff00) << 8) + ((x & 0x00ff0000) >> 8) + ((x & 0xff000000) >> 24));
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            _continueNeverendingLoop = false;
            _wasInitiatedResetEvent?.Dispose();
        }
    }
}
