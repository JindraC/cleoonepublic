﻿// <copyright file="SynchronizedTime.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;

namespace CleoOne.Common.Core.Common.Components.Times
{
    internal class SynchronizedTime : ITime, IDisposable
    {
        private DateTime _biggestTime;
        private readonly object _biggestTimeLocker = new object();

        private readonly NetworkTime _networkTime;

        public SynchronizedTime(NetworkTime networkTime)
        {
            _networkTime = networkTime;
        }

        public DateTime UtcNow
        {
            get
            {
                if (!_networkTime.IsInitialized)
                {
                    return _biggestTime = DateTime.UtcNow;
                }

                DateTime timeCandidate = _networkTime.GetSynchronizedTimeUtc();

                // we ensure that all times do follow after each other (that we do not go back in time)
                lock (_biggestTimeLocker)
                {
                    if (timeCandidate > _biggestTime)
                    {
                        _biggestTime = timeCandidate;

                        return timeCandidate;
                    }
                    else
                    {
                        return _biggestTime;
                    }
                }
            }
        }

        public override string ToString()
        {
            return $"{nameof(SynchronizedTime)}: {UtcNow:yyyy-MM-dd HH:mm:ss.fff}";
        }

        public void Dispose()
        {
            _networkTime.Dispose();
        }
    }
}
