﻿// <copyright file="RealTime.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;

namespace CleoOne.Common.Core.Common.Components.Times
{
    public class RealTime : ITime
    {
        private RealTime()
        {
        }

        private static readonly Lazy<RealTime> lazyInstance = new Lazy<RealTime>(() => new RealTime(), isThreadSafe: true);

        public static RealTime Instance => lazyInstance.Value;

        public DateTime UtcNow => DateTime.UtcNow;

        public override string ToString()
        {
            return $"{nameof(RealTime)}: {UtcNow:yyyy-MM-dd HH:mm:ss.fff}";
        }
    }
}
