﻿// <copyright file="GuidHashSetList.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Generic;

namespace CleoOne.Common.Core.DotNet.Collections
{
    public class GuidHashSetList : DictionaryList<Guid, Guid>
    {
        public GuidHashSetList()
            : base(null)
        {
        }

        public GuidHashSetList(IEnumerable<Guid> items)
            : this()
        {
            AddRange(items);
        }

        protected override Guid GetKey(Guid item)
        {
            return item;
        }

        public override void Add(Guid item)
        {
            if (ContainsKey(item))
            {
                return;
            }

            base.Add(item);
        }
    }
}
