﻿// <copyright file="IReadOnlyOrderedHashSet.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Collections.Generic;

namespace CleoOne.Common.Core.DotNet.Collections
{
    public interface IReadOnlyOrderedHashSet<T> : IEnumerable<T>
    {
        int Count { get; }

        bool Contains(T item);

        bool TryGetValue(T equalTo, out T actualValue);
    }
}
