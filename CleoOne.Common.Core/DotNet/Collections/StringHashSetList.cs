﻿// <copyright file="StringHashSetList.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Collections.Generic;

namespace CleoOne.Common.Core.DotNet.Collections
{
    public class StringHashSetList : DictionaryList<string, string>
    {
        public StringHashSetList()
            : base(null)
        {
        }

        protected override string GetKey(string item)
        {
            return item;
        }

        public StringHashSetList(IEnumerable<string> items)
            : this()
        {
            AddRange(items);
        }

        public override void Add(string item)
        {
            if (ContainsKey(item))
            {
                return;
            }

            base.Add(item);
        }
    }
}
