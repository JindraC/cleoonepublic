﻿// <copyright file="OrderedHashSet.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections;
using System.Collections.Generic;

namespace CleoOne.Common.Core.DotNet.Collections
{
    // Copied from: https://gmamaladze.wordpress.com/2013/07/25/hashset-that-preserves-insertion-order-or-net-implementation-of-linkedhashset/
    public class OrderedHashSet<T> : ICollection<T>, IReadOnlyOrderedHashSet<T>
    {
        private readonly IDictionary<T, LinkedListNode<T>> _dictionary;
        private readonly LinkedList<T> _linkedList;

        public OrderedHashSet()
            : this(EqualityComparer<T>.Default)
        {
        }

        public OrderedHashSet(IEqualityComparer<T> comparer)
            : this(Array.Empty<T>(), comparer)
        {
        }

        public OrderedHashSet(IEnumerable<T> items, IEqualityComparer<T> comparer = null)
        {
            _dictionary = new Dictionary<T, LinkedListNode<T>>(comparer ?? EqualityComparer<T>.Default);
            _linkedList = new LinkedList<T>();

            foreach (var item in items)
            {
                Add(item);
            }
        }

        public int Count
        {
            get { return _dictionary.Count; }
        }

        public virtual bool IsReadOnly
        {
            get { return _dictionary.IsReadOnly; }
        }

        void ICollection<T>.Add(T item)
        {
            Add(item);
        }

        public void Clear()
        {
            _linkedList.Clear();
            _dictionary.Clear();
        }

        public bool Remove(T item)
        {
            LinkedListNode<T> node;
            bool found = _dictionary.TryGetValue(item, out node);

            if (!found)
            {
                return false;
            }

            _dictionary.Remove(item);
            _linkedList.Remove(node);

            return true;
        }

        public bool TryRemoveFirst(out T item)
        {
            if (_dictionary.Count == 0)
            {
                item = default;

                return false;
            }

            item = _linkedList.First.Value;

            _linkedList.RemoveFirst();
            _dictionary.Remove(item);

            return true;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _linkedList.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public bool Contains(T item)
        {
            return _dictionary.ContainsKey(item);
        }

        public bool TryGetValue(T equalTo, out T actualValue)
        {
            if (_dictionary.TryGetValue(equalTo, out var linkedNode))
            {
                actualValue = linkedNode.Value;

                return true;
            }
            else
            {
                actualValue = default;

                return false;
            }
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            _linkedList.CopyTo(array, arrayIndex);
        }

        public bool Add(T item)
        {
            if (_dictionary.ContainsKey(item))
            {
                return false;
            }

            LinkedListNode<T> node = _linkedList.AddLast(item);
            _dictionary.Add(item, node);

            return true;
        }
    }
}
