﻿// <copyright file="ConcurrentHashSet.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace CleoOne.Common.Core.DotNet.Collections
{
    public class ConcurrentHashSet<T> : IEnumerable<T>
    {
        private readonly ConcurrentDictionary<T, bool> _innerDictionary;

        public ConcurrentHashSet()
        {
            _innerDictionary = new ConcurrentDictionary<T, bool>();
        }

        public ConcurrentHashSet(IEqualityComparer<T> comparer)
        {
            _innerDictionary = new ConcurrentDictionary<T, bool>(comparer);
        }

        public ConcurrentHashSet(IEnumerable<T> collection)
        {
            _innerDictionary =
                new ConcurrentDictionary<T, bool>(collection.Distinct().Select(item => new KeyValuePair<T, bool>(item, false)));
        }

        public ConcurrentHashSet(IEnumerable<T> collection, IEqualityComparer<T> comparer)
        {
            if (comparer != null)
            {
                _innerDictionary =
                    new ConcurrentDictionary<T, bool>(collection.Distinct(comparer).Select(item => new KeyValuePair<T, bool>(item, false)),
                                                      comparer);
            }
            else
            {
                _innerDictionary =
                    new ConcurrentDictionary<T, bool>(collection.Distinct().Select(item => new KeyValuePair<T, bool>(item, false)));
            }
        }

        public bool Add(T item)
        {
            return _innerDictionary.TryAdd(item, false);
        }

        public void Clear()
        {
            _innerDictionary.Clear();
        }

        public bool Contains(T item)
        {
            return _innerDictionary.ContainsKey(item);
        }

        public bool Remove(T item)
        {
            return _innerDictionary.TryRemove(item, out _);
        }

        public int RemoveWhere(Predicate<T> match)
        {
            int count = 0;

            foreach (var key in _innerDictionary.Keys.Where(item => match(item)))
            {
                count += Remove(key) ? 1 : 0;
            }

            return count;
        }

        public int Count => _innerDictionary.Count;

        public IEnumerator<T> GetEnumerator()
        {
            return _innerDictionary.Keys.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _innerDictionary.Keys.GetEnumerator();
        }
    }
}
