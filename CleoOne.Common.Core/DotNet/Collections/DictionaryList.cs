﻿// <copyright file="DictionaryList.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CleoOne.Common.Core.DotNet.Collections
{
    /// <summary>
    /// Has fast .Add(), Contains(), and Dictionary methods. Has slow Insert(), Remove() and RemoveAt() functions.
    /// </summary>
    public abstract class
        DictionaryList<TKey, TValue> : IList<TValue> // Commented, because else IEnumerable does not work: , IReadOnlyDictionary<TKey, TValue>
    {
        private List<TValue> _list;
        private readonly Dictionary<TKey, TValue> _idToTheItem;
        private readonly Dictionary<TKey, int> _idToIndex;

        public DictionaryList(IEqualityComparer<TKey> comparer = null)
        {
            _list = new List<TValue>();
            _idToTheItem = new Dictionary<TKey, TValue>(comparer ?? EqualityComparer<TKey>.Default);
            _idToIndex = new Dictionary<TKey, int>(comparer ?? EqualityComparer<TKey>.Default);
        }

        protected abstract TKey GetKey(TValue item);

        public IReadOnlyDictionary<TKey, TValue> InnerDictionary => _idToTheItem;

        public IReadOnlyList<TValue> InnerList => _list;

        //IEnumerator<KeyValuePair<TKey, TValue>> IEnumerable<KeyValuePair<TKey, TValue>>.GetEnumerator()
        //{
        //    return idToTheItem.GetEnumerator();
        //}

        public IEnumerator<TValue> GetEnumerator()
        {
            return _list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public virtual void AddOrUpdate(TValue item)
        {
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            if (_idToTheItem.TryAdd(GetKey(item), item))
            {
                _list.Add(item);
                _idToIndex.Add(GetKey(item), _list.Count - 1);
            }
            else
            {
                var currentIndex = _idToIndex[GetKey(item)];

                _idToTheItem[GetKey(item)] = item;
                _list[currentIndex] = item;
            }
        }

        public virtual void Add(TValue item)
        {
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            // throws exception if already added
            _idToTheItem.Add(GetKey(item), item);
            _list.Add(item);
            _idToIndex.Add(GetKey(item), _list.Count - 1);
        }

        public void AddRange(IEnumerable<TValue> items)
        {
            foreach (var item in items)
            {
                Add(item);
            }
        }

        public void TryAddRange(IEnumerable<TValue> items)
        {
            foreach (var item in items)
            {
                TryAdd(item);
            }
        }

        public bool TryAdd(TValue item)
        {
            var key = GetKey(item);

            if (_idToTheItem.ContainsKey(key))
            {
                return false;
            }
            else
            {
                Add(item);

                return true;
            }
        }

        public virtual void Clear()
        {
            _list.Clear();
            _idToTheItem.Clear();
            _idToIndex.Clear();
        }

        public bool Contains(TValue item)
        {
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            return _idToTheItem.ContainsKey(GetKey(item));
        }

        public void CopyTo(TValue[] array, int arrayIndex)
        {
            _list.CopyTo(array, arrayIndex);
        }

        public virtual bool Remove(TValue item)
        {
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            if (!_idToIndex.TryGetValue(GetKey(item), out var index))
            {
                return false;
            }

            RemoveAt(index);

            return true;
        }

        public void RemoveAll(IEnumerable<TValue> items)
        {
            foreach (var item in items)
            {
                Remove(item);
            }
        }

        public void RemoveKey(TKey key)
        {
            if (TryGetValue(key, out var storedValue))
            {
                Remove(storedValue);
            }
        }

        public int Count => _list.Count;

        public bool IsReadOnly => false;

        public int IndexOf(TValue item)
        {
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            return _idToIndex[GetKey(item)];
        }

        public virtual void Insert(int index, TValue item)
        {
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            if (index > _list.Count)
            {
                throw new IndexOutOfRangeException($"Index {index} > {_list.Count} (items count in list)");
            }

            _idToTheItem.Add(GetKey(item), item);
            _list.Insert(index, item);

            var count = _list.Count;

            for (int i = index; i < count; i++)
            {
                _idToIndex[GetKey(_list[i])] = i;
            }
        }

        public void InsertMany(int index, IList<TValue> items)
        {
            if (index > _list.Count)
            {
                throw new IndexOutOfRangeException($"Index {index} > {_list.Count} (items count in list)");
            }

            _list = _list.Take(index).Concat(items).Concat(_list.Skip(index)).ToList();

            foreach (var item in items)
            {
                _idToTheItem.Add(GetKey(item), item);
            }

            var count = _list.Count;

            for (int i = index; i < count; i++)
            {
                _idToIndex[GetKey(_list[i])] = i;
            }
        }

        public virtual void RemoveAt(int index)
        {
            var item = _list[index];

            _idToTheItem.Remove(GetKey(item));
            _list.RemoveAt(index);
            _idToIndex.Remove(GetKey(item));

            var count = _list.Count;

            for (int i = index; i < count; i++)
            {
                _idToIndex[GetKey(_list[i])] = i;
            }
        }

        public virtual TValue this[int index]
        {
            get => _list[index];
            set
            {
                var previousItem = _list[index];
                var newItem = value;

                _idToTheItem.Remove(GetKey(previousItem));
                _idToIndex.Remove(GetKey(previousItem));

                _list[index] = newItem;

                _idToTheItem.Add(GetKey(newItem), newItem);
                _idToIndex.Add(GetKey(newItem), index);
            }
        }

        public bool ContainsKey(TKey key)
        {
            return _idToTheItem.ContainsKey(key);
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            return _idToTheItem.TryGetValue(key, out value);
        }

        public bool TryGetSame(TValue value, out TValue valueWithSameKey)
        {
            return TryGetValue(GetKey(value), out valueWithSameKey);
        }

        public TValue GetValueOrDefault(TKey key)
        {
            return TryGetValue(key, out var value) ? value : default;
        }

        public void UnionWith(IEnumerable<TValue> values)
        {
            TryAddRange(values);
        }

        public TValue this[TKey key] => _idToTheItem[key];

        public IEnumerable<TKey> Keys => _idToTheItem.Keys;

        public IEnumerable<TValue> Values => _idToTheItem.Values;
    }
}
