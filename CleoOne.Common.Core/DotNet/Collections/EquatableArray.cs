﻿// <copyright file="EquatableArray.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.DotNet.Extensions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CleoOne.Common.Core.DotNet.Collections
{
    public class EquatableArray<T> : IEquatable<EquatableArray<T>>, IReadOnlyList<T>
    {
        private readonly T[] _items;
        private int? _hashCode;

        public int Count => _items.Length;

        public int Length => _items.Length;

        public T this[int index] => _items[index];

        public IEqualityComparer<T> Comparer { get; }

        public EquatableArray(IEnumerable<T> items, IEqualityComparer<T> comparer = default)
        {
            if (comparer is null)
            {
                if (typeof(T).IsInterface)
                {
                    throw new ArgumentException($"Comparer expected. Cannot create an EquatableArray "
                                                + $"for interface '{typeof(T).Name}' without a comparer.");
                }

                var isInheritingIEquatable = typeof(T).GetInterfaces()
                                                      .Any(theInterface => theInterface.IsGenericType
                                                                           && theInterface.GetGenericTypeDefinition()
                                                                           == typeof(IEquatable<>));

                if (!isInheritingIEquatable)
                {
                    throw new ArgumentException($"Comparer expected. Cannot create an EquatableArray for a class "
                                                + $"not inheriting IEquatable<>: '{typeof(T).Name}' without a comparer.");
                }
            }

            Comparer = comparer ?? EqualityComparer<T>.Default;
            _items = items.ToArray();
        }

        public EquatableArray<T> AssertComparer(IEqualityComparer<T> comparer)
        {
            if (Comparer != comparer)
            {
                throw new ArgumentException("Same comparer expected");
            }

            return this;
        }

        public EquatableArray<T> AssertDefaultComparer()
        {
            return AssertComparer(EqualityComparer<T>.Default);
        }

        public EquatableArray<T> ConvertAll(Func<T, T> operation)
        {
            return this.Select(operation).ToEquatableArray(Comparer);
        }

        public bool Equals(EquatableArray<T> other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }

            if (ReferenceEquals(this, other))
            {
                return true;
            }

            if (!Comparer.Equals(other.Comparer))
            {
                throw new InvalidOperationException("This class is expected to be compared only against instance with the same comparer.");
            }

            if (_items.Length != other._items.Length)
            {
                return false;
            }

            if (GetHashCode() != other.GetHashCode())
            {
                return false;
            }

            for (int i = 0; i < _items.Length; i++)
            {
                if (!Comparer.Equals(_items[i], other._items[i]))
                {
                    return false;
                }
            }

            return true;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj.GetType() != GetType())
            {
                return false;
            }

            return Equals((EquatableArray<T>)obj);
        }

        public override int GetHashCode()
        {
            // ReSharper disable three times NonReadonlyMemberInGetHashCode
            if (_hashCode is null)
            {
                var hashCode = HashCode.Combine(0);

                for (int i = 0; i < _items.Length; i++)
                {
                    hashCode = HashCode.Combine(hashCode, Comparer.GetHashCode(_items[i]));
                }

                _hashCode = hashCode;
            }

            return _hashCode.Value;
        }

        public IEnumerator<T> GetEnumerator() => _items.AsEnumerable().GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public static bool operator ==(EquatableArray<T> left, EquatableArray<T> right) => Equals(left, right);

        public static bool operator !=(EquatableArray<T> left, EquatableArray<T> right) => !Equals(left, right);
    }
}
