﻿// <copyright file="StreamReaderExtensions.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.Common.Logging;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Threading.Tasks;

namespace CleoOne.Common.Core.DotNet.Extensions.Classes
{
    public static class StreamReaderExtensions
    {
        private static readonly ILogger _logger = LoggerHelper.GetLogger(typeof(StreamReaderExtensions));

        public static async Task<string> ReadToEndWithTimeoutAsync(this StreamReader streamReader,
                                                                   TimeSpan timeout,
                                                                   Func<Exception> timeoutException,
                                                                   ILogger logger = null)
        {
            var originalStackTrace = Environment.StackTrace;
            logger ??= _logger;

            var task = Task.Run(streamReader.ReadToEndAsync);

            return await task.ThrowOnTimeout(timeout, timeoutException, logger.WithProperty("OriginalStackTrace", originalStackTrace));
        }

        public static string ReadToEndWithTimeout(this StreamReader streamReader,
                                                  TimeSpan timeout,
                                                  Func<Exception> timeoutException,
                                                  ILogger logger = null)
        {
            var originalStackTrace = Environment.StackTrace;
            logger ??= _logger;

            var task = Task.Run(streamReader.ReadToEndAsync);

            if (task.FinishInTime(timeout, out var result, logger.WithProperty("OriginalStackTrace", originalStackTrace)))
            {
                return result;
            }

            throw timeoutException();

            //return ReadToEndWithTimeoutAsync(streamReader, timeOut, timeoutException).GetAwaiter().GetResult();
        }
    }
}
