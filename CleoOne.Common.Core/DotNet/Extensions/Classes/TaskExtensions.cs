﻿// <copyright file="TaskExtensions.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.Common.Logging;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace CleoOne.Common.Core.DotNet.Extensions.Classes
{
    public static class TaskExtensions
    {
        public static async Task<TResult> ThrowOnTimeout<TResult>(this Task<TResult> task,
                                                                  TimeSpan timeout,
                                                                  Func<Exception> timeoutException,
                                                                  ILogger logger = null,
                                                                  Func<Exception, bool> onExceptionAfterTimeoutHandler = null)
        {
            using var tokenSource = new CancellationTokenSource();

            var timeoutTask = Task.Delay(timeout, tokenSource.Token);

            var finishedTask = await Task.WhenAny(task, timeoutTask);

            if (finishedTask == task)
            {
                tokenSource.Cancel();

                return await task;
            }
            else
            {
                _ = task.ContinueWith(t => PreventUnobservedTaskException(t, logger, onExceptionAfterTimeoutHandler),
                                      TaskScheduler.Current);

                throw timeoutException();
            }
        }

        public static bool FinishInTime<TResult>(this Task<TResult> task,
                                                 TimeSpan timeout,
                                                 out TResult result,
                                                 ILogger logger = null,
                                                 Func<Exception, bool> onExceptionAfterTimeoutHandler = null)
        {
            using var resetEvent = new ManualResetEventSlim();

            // ReSharper disable once AccessToDisposedClosure
            var resetTask = task.ContinueWith(t =>
                                              {
                                                  if (t.IsFaulted)
                                                  {
                                                      _ = t.ContinueWith(
                                                          faultedTask =>
                                                              PreventUnobservedTaskException(
                                                                  faultedTask,
                                                                  logger,
                                                                  onExceptionAfterTimeoutHandler),
                                                          TaskScheduler.Current);
                                                  }

                                                  resetEvent.Set();
                                              },
                                              TaskScheduler.Current);

            if (resetEvent.Wait(timeout))
            {
                // potentially throwing an exception:
                result = task.GetAwaiter().GetResult();

                return true;
            }

            _ = resetTask.ContinueWith(t => PreventUnobservedTaskException(t, logger, onExceptionAfterTimeoutHandler),
                                       TaskScheduler.Current);

            result = default;

            return false;
        }

        public static bool FinishInTime(this Task task,
                                        TimeSpan timeout,
                                        ILogger logger = null,
                                        Func<Exception, bool> onExceptionAfterTimeoutHandler = null)
        {
            using var resetEvent = new ManualResetEventSlim();

            // ReSharper disable once AccessToDisposedClosure
            var resetTask = task.ContinueWith(t =>
                                              {
                                                  if (t.IsFaulted)
                                                  {
                                                      _ = t.ContinueWith(
                                                          faultedTask =>
                                                              PreventUnobservedTaskException(
                                                                  faultedTask,
                                                                  logger,
                                                                  onExceptionAfterTimeoutHandler),
                                                          TaskScheduler.Current);
                                                  }

                                                  resetEvent.Set();
                                              },
                                              TaskScheduler.Current);

            if (resetEvent.Wait(timeout))
            {
                // potentially throwing an exception:
                task.GetAwaiter().GetResult();

                return true;
            }

            _ = resetTask.ContinueWith(t => PreventUnobservedTaskException(t, logger, onExceptionAfterTimeoutHandler),
                                       TaskScheduler.Current);

            return false;
        }

        private static readonly ILogger _logger = LoggerHelper.GetLogger(typeof(TaskExtensions));

        private static async Task PreventUnobservedTaskException(Task task,
                                                                 ILogger logger,
                                                                 Func<Exception, bool> onExceptionAfterTimeoutHandler)
        {
            logger ??= _logger;

            try
            {
                try
                {
                    await task;
                }
                catch (Exception e)
                {
                    var throwAtTheEnd = true;

                    try
                    {
                        if (onExceptionAfterTimeoutHandler != null
                            && onExceptionAfterTimeoutHandler.Invoke(e))
                        {
                            // When the supplied handler handled the exception, we do not do nothing
                            throwAtTheEnd = false;
                        }
                    }
                    catch (Exception handlerException)
                    {
                        logger.LogError(handlerException,
                                        "Exception handler threw an exception: {HandlerExceptionMessage}",
                                        handlerException.Message);
                    }

                    if (throwAtTheEnd)
                    {
                        throw;
                    }
                }
            }
            catch (TaskCanceledException)
            {
            }
            catch (ObjectDisposedException)
            {
            }
            catch (Exception e) when (e.Message.Contains("Operation cancelled")
                                      || e.Message.Contains("Operation canceled"))
            {
            }
            catch (IOException e) when (e.Message.Contains("Received an unexpected EOF or 0 bytes from the transport stream"))
            {
                logger.LogWarning(e, "Exception after timeout: {Message}", e.Message);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Unexpected exception in {nameof(ThrowOnTimeout)}(): {{Message}}", e.Message);
            }
        }
    }
}
