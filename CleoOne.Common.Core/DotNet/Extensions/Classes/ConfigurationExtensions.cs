﻿// <copyright file="ConfigurationExtensions.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;

namespace CleoOne.Common.Core.DotNet.Extensions.Classes
{
    public static class ConfigurationExtensions
    {
        /// <summary>
        /// Checks configuration file if the service is enabled.
        /// If there is nothing specified in the configuration file sets default as true and logs Critical Error that configuration should be specified.
        /// If service is disabled returns false and logs Critical Error.
        /// </summary>
        public static bool IsServiceEnabledCustom(this IConfiguration configuration, string serviceName, ILogger logger)
        {
            switch (configuration.IsServiceEnabledNullable(serviceName))
            {
                case null:
                    logger.LogCritical($"Service {serviceName} is missing the configuration. Setting to default - enabled.");

                    return true;

                case true:
                    return true;

                case false:
                    logger.LogCritical(
                        $"{serviceName} is disabled in the configuration. Service will not be started!"); // logging as Critical so if this happens in production we will not miss it (because it should not happen in production)

                    return false;
            }
        }

        /// <summary>
        /// Checks configuration file if the service is enabled.
        /// If there is nothing specified in the configuration file returns null.
        /// </summary>
        public static bool? IsServiceEnabledNullable(this IConfiguration configuration, string serviceName)
        {
            return configuration.GetValue<bool?>($"EnableDataConnectorServices:{serviceName}");
        }

        /// <summary>
        /// Checks configuration file if the service is enabled.
        /// If there is nothing specified in the configuration file throws exception.
        /// </summary>
        public static bool IsServiceEnabled(this IConfiguration configuration, string serviceName)
        {
            return configuration.GetValue<bool?>($"EnableDataConnectorServices:{serviceName}")
                   ?? throw new Exception("Service doesn't have the configuration specified.");
        }
    }
}
