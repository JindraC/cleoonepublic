﻿// <copyright file="ExpressionExtensions.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace CleoOne.Common.Core.DotNet.Extensions.Classes
{
    public static class ExpressionExtensions
    {
        public static Expression<Func<T, bool>> And<T>(this Expression<Func<T, bool>> left, Expression<Func<T, bool>> right)
        {
            return Expression.Lambda<Func<T, bool>>(Expression.AndAlso(left.Body, right.WithParametersOf(left).Body), left.Parameters);
        }

        public static Expression<Func<T, bool>> Or<T>(this Expression<Func<T, bool>> left, Expression<Func<T, bool>> right)
        {
            return Expression.Lambda<Func<T, bool>>(Expression.OrElse(left.Body, right.WithParametersOf(left).Body), left.Parameters);
        }

        public static Expression<Func<T2, bool>> CallExpression<T, T2>(this Expression<Func<T, bool>> caller,
                                                                       Expression<Func<T2, T>> callee)
        {
            return Expression.Lambda<Func<T2, bool>>(
                Expression.Invoke(
                    caller,
                    callee),
                callee.Parameters);
        }

        public static Expression<Func<T, bool>> NotExpression<T>(this Expression<Func<T, bool>> predicate)
        {
            return Expression.Lambda<Func<T, bool>>(Expression.Not(predicate.Body), predicate.Parameters);
        }

        public static Expression<Func<T, bool>> ContainesExpression<T>(this HashSet<T> hashset)
        {
            var intParameter = Expression.Parameter(typeof(T));

            Expression<Func<T, bool>> containsExpression =
                Expression.Lambda<Func<T, bool>>(
                    Expression.Call(Expression.Constant(hashset, typeof(HashSet<T>)),
                                    typeof(HashSet<T>).GetMethod("Contains"),
                                    intParameter),
                    intParameter);

            return containsExpression;
        }

        private static Expression<Func<TResult>> WithParametersOf<T, TResult>(this Expression<Func<T, TResult>> left,
                                                                              Expression<Func<T, TResult>> right)
        {
            return new ReplaceParameterVisitor<Func<TResult>>(left.Parameters[0], right.Parameters[0]).Visit(left);
        }

        private class ReplaceParameterVisitor<TResult> : ExpressionVisitor
        {
            private readonly ParameterExpression _parameter;
            private readonly Expression _replacement;

            public ReplaceParameterVisitor(ParameterExpression parameter, Expression replacement)
            {
                _parameter = parameter;
                _replacement = replacement;
            }

            public Expression<TResult> Visit<T>(Expression<T> node)
            {
                var parameters = node.Parameters.Where(p => p != _parameter);

                return Expression.Lambda<TResult>(Visit(node.Body), parameters);
            }

            protected override Expression VisitParameter(ParameterExpression node)
            {
                return node == _parameter ? _replacement : base.VisitParameter(node);
            }
        }

        public static string GetMemberName<T, TMember>(this Expression<Func<T, TMember>> expression)
        {
            return expression.GetMemberExpression().Member.Name;
        }

        public static PropertyInfo GetMemberPropertyInfo<T, TMember>(this Expression<Func<T, TMember>> expression)
        {
            return expression.GetMemberExpression().Member as PropertyInfo;
        }

        public static MemberExpression GetMemberExpression<T, TMember>(this Expression<Func<T, TMember>> expression)
        {
            return GetMemberExpression(expression.Body);
        }

        public static MemberExpression GetMemberExpression<T>(this Expression<Action<T>> expression)
        {
            return GetMemberExpression(expression.Body);
        }

        public static MemberExpression GetMemberExpression<T1, T2>(this Expression<Action<T1, T2>> expression)
        {
            return GetMemberExpression(expression.Body);
        }

        /// <summary>
        /// Wraps an action expression with no arguments inside an expression that takes an 
        /// argument of the specified type (the argument is ignored, but the original expression is
        /// invoked)
        /// </summary>
        /// <typeparam name="TArgument">The type of argument to accept in the wrapping expression</typeparam>
        /// <param name="expression">The expression to wrap</param>
        /// <returns></returns>
        public static Expression<Action<TArgument>> WrapActionWithArgument<TArgument>(this Expression<Action> expression)
        {
            ParameterExpression parameter = Expression.Parameter(typeof(TArgument), "x");

            return Expression.Lambda<Action<TArgument>>(Expression.Invoke(expression), parameter);
        }

        private static MemberExpression GetMemberExpression(Expression body)
        {
            MemberExpression memberExpression = null;

            if (body.NodeType == ExpressionType.Convert)
            {
                var unaryExpression = (UnaryExpression)body;
                memberExpression = unaryExpression.Operand as MemberExpression;
            }
            else if (body.NodeType == ExpressionType.MemberAccess)
            {
                memberExpression = body as MemberExpression;
            }

            if (memberExpression == null)
            {
                throw new ArgumentException("Expression is not a member access");
            }

            return memberExpression;
        }
    }
}
