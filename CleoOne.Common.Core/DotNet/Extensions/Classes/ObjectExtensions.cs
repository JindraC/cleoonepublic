﻿// <copyright file="ObjectExtensions.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.DotNet.Extensions.Strings;
using System;
using System.Globalization;

namespace CleoOne.Common.Core.DotNet.Extensions.Classes
{
    public static class ObjectExtensions
    {
        public static int GetHashCodeForNullable(this object nullableObject)
        {
            return nullableObject?.GetHashCode() ?? 0;
        }

        public static decimal ToDecimalInvariant(object obj)
        {
            if (obj is string str)
            {
                return StringExtensions.ToDecimalInvariant(str);
            }
            else
            {
                return Convert.ToDecimal(obj, CultureInfo.InvariantCulture);
            }
        }
    }
}
