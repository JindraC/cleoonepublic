﻿// <copyright file="PropertyInfoExtensions.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace CleoOne.Common.Core.DotNet.Extensions.Classes
{
    public static class PropertyInfoExtensions
    {
        public static bool HasAttribute<TAttribute>(this PropertyInfo propertyInfo)
            where TAttribute : Attribute
        {
            return propertyInfo.GetCustomAttribute<TAttribute>(inherit: true) != null;
        }

        public static Expression<Func<TDocument, bool>> GetWhereEqualsExpression<TDocument>(
            this PropertyInfo propertyInfo,
            object constantValue)
        {
            var propertyType = propertyInfo.PropertyType;

            var parameterExpression = Expression.Parameter(typeof(TDocument));

            return Expression.Lambda<Func<TDocument, bool>>(
                Expression.Equal(
                    Expression.Property(
                        parameterExpression,
                        propertyInfo),
                    Expression.Constant(
                        constantValue,
                        propertyType)),
                parameterExpression);
        }

        public static Expression<Func<TDocument, object>> GetPropertySelectorExpression<TDocument>(this PropertyInfo propertyInfo)
        {
            var propertyType = propertyInfo.PropertyType;

            var parameterExpression = Expression.Parameter(typeof(TDocument));

            var idSelector = Expression.Lambda<Func<TDocument, object>>(
                Expression.Convert(
                    Expression.Property(
                        parameterExpression,
                        propertyInfo),
                    typeof(object)),
                parameterExpression);

            return idSelector;
        }

        public static bool TryGetBackingField(this PropertyInfo propertyInfo, out FieldInfo backingFieldInfo)
        {
            var actualType = propertyInfo.DeclaringType;
            var backingFieldName = $"<{propertyInfo.Name}>k__BackingField";

            while ((actualType != null)
                   && (actualType != typeof(object)))
            {
                var backingField = actualType.GetFields(BindingFlags.GetField | BindingFlags.NonPublic | BindingFlags.Instance)
                                             .FirstOrDefault(f => (f.Name == backingFieldName)
                                                                  && f.GetCustomAttributes(typeof(CompilerGeneratedAttribute),
                                                                                           true)
                                                                      .Any());

                if (backingField != null)
                {
                    backingFieldInfo = backingField;

                    return true;
                }

                actualType = actualType.BaseType;
            }

            backingFieldInfo = default;

            return false;
        }
    }
}
