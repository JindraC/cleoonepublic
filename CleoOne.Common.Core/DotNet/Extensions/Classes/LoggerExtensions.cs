﻿// <copyright file="LoggerExtensions.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.Common.Logging;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace CleoOne.Common.Core.DotNet.Extensions.Classes
{
    public static class LoggerExtensions
    {
        public static void LogInformation(this ILogger logger, LogLabel label, string message, params object[] args)
        {
            logger.LogInformation((int)label, message, args);
        }

        public static void LogWarning(this ILogger logger, LogLabel label, string message, params object[] args)
        {
            logger.LogWarning((int)label, message, args);
        }

        public static void LogError(this ILogger logger, LogLabel label, Exception exception, string message, params object[] args)
        {
            logger.LogError((int)label, exception, message, args);
        }

        public static IDisposable BeginScopeWith(this ILogger logger, string key, object value)
        {
            return logger.BeginScope(new Dictionary<string, object>(1) { { key, value }, });
        }

        public static IDisposable BeginScopeWith(this ILogger logger, params (string key, object value)[] keys)
        {
            return logger.BeginScope(keys.ToDictionary(x => x.key, x => x.value));
        }

        private static readonly ILogger _logger = LoggerHelper.GetLogger(typeof(LoggerExtensions));

        private const long BYTES_CONSUMED_LIMIT = 100_000 * 100; // cca 10MB
        private static long _bytes_consumed = 0;

        public static void LogDebugOncePerHour(this ILogger logger, string message, params object[] args)
        {
            LogOncePerHours(LogLevel.Debug, logger, message, args, 1);
        }

        public static void LogInformationOncePerHour(this ILogger logger, string message, params object[] args)
        {
            LogOncePerHours(LogLevel.Information, logger, message, args, 1);
        }

        public static void LogInformationOncePerHours(this ILogger logger, int intervalInHours, string message, params object[] args)
        {
            LogOncePerHours(LogLevel.Information, logger, message, args, intervalInHours);
        }

        public static void LogWarningOncePerHour(this ILogger logger, string message, params object[] args)
        {
            LogOncePerHours(LogLevel.Warning, logger, message, args, 1);
        }

        public static void LogWarningWithExceptionOncePerHour(this ILogger logger,
                                                              Exception exception,
                                                              string message,
                                                              params object[] args)
        {
            LogOncePerHours(LogLevel.Warning, logger, message, args, 1, exception);
        }

        public static void LogErrorOncePerHour(this ILogger logger, Exception exception, string message, params object[] args)
        {
            LogOncePerHours(LogLevel.Error, logger, message, args, 1, exception);
        }

        public static void LogErrorOncePerHour(this ILogger logger, string message, params object[] args)
        {
            LogOncePerHours(LogLevel.Error, logger, message, args, 1);
        }

        public static void LogErrorOncePerHours(this ILogger logger,
                                                Exception exception,
                                                string message,
                                                int intervalInHours,
                                                params object[] args)
        {
            LogOncePerHours(LogLevel.Error, logger, message, args, intervalInHours, exception);
        }

        public static void LogErrorOncePerDay(this ILogger logger, string message, params object[] args)
        {
            LogOncePerHours(LogLevel.Error, logger, message, args, 24);
        }

        public static void LogCriticalOncePerHour(this ILogger logger, string message, params object[] args)
        {
            LogOncePerHours(LogLevel.Critical, logger, message, args, 1);
        }

        public static void LogCriticalOncePerHour(this ILogger logger, Exception exception, string message, params object[] args)
        {
            LogOncePerHours(LogLevel.Critical, logger, message, args, 1, exception);
        }

        private static bool _loggedThreeQuarters = false;

        private static void LogOncePerHours(LogLevel logLevel,
                                            ILogger logger,
                                            string message,
                                            object[] args,
                                            int intervalInHours,
                                            Exception exception = null)
        {
            if (_bytes_consumed > BYTES_CONSUMED_LIMIT)
            {
                logReservationsByStackTrace.Clear();
                _bytes_consumed = 0;
                _loggedThreeQuarters = false;

                _logger.LogCritical($"Log reservation exhausted more than {BYTES_CONSUMED_LIMIT} bytes "
                                    + "- the full limit - resetting the log dictionary");
            }

            if (_bytes_consumed > BYTES_CONSUMED_LIMIT * 3 / 4
                && !_loggedThreeQuarters)
            {
                _loggedThreeQuarters = true;

                _logger.LogCritical(
                    $"Log reservation exhausted more than {BYTES_CONSUMED_LIMIT * 3 / 4} bytes (3/4 of the limit)");
            }

            // todo optimize the cache by using this
            // https://ayende.com/blog/3879/reducing-the-cost-of-getting-a-stack-trace
            // will need some modifications based on these files
            // https://github.com/dotnet/coreclr/blob/master/src/System.Private.CoreLib/src/System/Diagnostics/StackFrameHelper.cs
            // https://github.com/dotnet/coreclr/blob/master/src/System.Private.CoreLib/shared/System/Diagnostics/StackTrace.cs
            // https://github.com/dotnet/coreclr/blob/master/src/System.Private.CoreLib/src/System/Diagnostics/StackTrace.CoreCLR.cs

            // var stackTrace = Environment.StackTrace; //new StackTrace(false); -> if it is shared

            var key = message + exception?.GetType().Name + exception?.ToMergedStackTraces();

            var logReservation = logReservationsByStackTrace.GetOrAdd(key,
                                                                      _ =>
                                                                      {
                                                                          // char size is 2 bytes
                                                                          // we add only in case of adding new value
                                                                          Interlocked.Add(ref _bytes_consumed, key.Length * sizeof(char));

                                                                          return new LogReservation(logLevel, logger);
                                                                      });

            if (logReservation.TryGetLogPermission(out var callCount, out var lastLogTime, intervalInHours))
            {
                if (args == null)
                {
                    args = Array.Empty<object>();
                }

                if (callCount > 1)
                {
                    // enhance message with count for the last period
                    message += " | Logged {LogCallCount}-times from {From} till now";
                    args = args.Append(callCount).Append(lastLogTime).ToArray();
                }

                LogUniversal(logLevel, logger, message, args, exception);
            }
        }

        private static void LogUniversal(LogLevel logLevel, ILogger logger, string message, object[] args, Exception exception)
        {
            switch (logLevel)
            {
                case LogLevel.Trace:
                    logger.LogTrace(message, args);

                    break;

                case LogLevel.Debug:
                    logger.LogDebug(message, args);

                    break;

                case LogLevel.Information:
                    logger.LogInformation(message, args);

                    break;

                case LogLevel.Warning:
                    logger.LogWarning(message, args);

                    break;

                case LogLevel.Error:
                    if (exception == null)
                    {
                        logger.LogError(message, args);
                    }
                    else
                    {
                        logger.LogError(exception, message, args);
                    }

                    break;

                case LogLevel.Critical:
                    if (exception == null)
                    {
                        logger.LogCritical(message, args);
                    }
                    else
                    {
                        logger.LogCritical(exception, message, args);
                    }

                    break;

                default: throw new NotImplementedException($"Not implemented for level: {logLevel}");
            }
        }

        private static readonly ConcurrentDictionary<string, LogReservation> logReservationsByStackTrace =
            new ConcurrentDictionary<string, LogReservation>();

        private class LogReservation
        {
            private readonly object _locker = new object();
            private DateTime _waitTillUtc = DateTime.MinValue;
            private DateTime _lastLogTime = DateTime.MinValue;
            private int _callCount = 0;

            public LogReservation(LogLevel logLevel, ILogger logger)
            {
                LogLevel = logLevel;
                Logger = logger;
            }

            public LogLevel LogLevel { get; }

            public ILogger Logger { get; }

            public bool TryGetLogPermission(out int callCount, out DateTime lastLogTime, int intervalInHours)
            {
                Interlocked.Increment(ref _callCount);

                callCount = _callCount;
                lastLogTime = _lastLogTime;

                var utcNow = DateTime.UtcNow;

                if (_waitTillUtc > utcNow)
                {
                    return false;
                }

                lock (_locker)
                {
                    utcNow = DateTime.UtcNow;

                    if (_waitTillUtc > utcNow)
                    {
                        return false;
                    }

                    _waitTillUtc = RoundDownForH1(utcNow.AddHours(intervalInHours));
                    _callCount = 0;
                    _lastLogTime = utcNow;

                    return true;
                }
            }

            private static DateTime RoundDownForH1(DateTime time)
            {
                return new DateTime(time.Year,
                                    time.Month,
                                    time.Day,
                                    time.Hour,
                                    0,
                                    0,
                                    time.Kind);
            }
        }
    }
}
