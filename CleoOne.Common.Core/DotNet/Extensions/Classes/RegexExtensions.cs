﻿// <copyright file="RegexExtensions.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Text.RegularExpressions;

namespace CleoOne.Common.Core.DotNet.Extensions.Classes
{
    public static class RegexExtensions
    {
        public static string ReplaceGroup(this Regex regex, string input, string groupName, string replacement)
        {
            return regex.Replace(input,
                                 m =>
                                 {
                                     return ReplaceNamedGroup(groupName, replacement, m);
                                 });
        }

        public static string ReplaceGroup(this Regex regex, string input, string groupName, Func<string, string> replacementFunc)
        {
            return regex.Replace(input,
                                 m =>
                                 {
                                     return ReplaceNamedGroup(groupName, replacementFunc, m);
                                 });
        }

        private static string ReplaceNamedGroup(string groupName, string replacement, Match m)
        {
            string capture = m.Value;
            capture = capture.Remove(m.Groups[groupName].Index - m.Index, m.Groups[groupName].Length);
            capture = capture.Insert(m.Groups[groupName].Index - m.Index, replacement);

            return capture;
        }

        private static string ReplaceNamedGroup(string groupName, Func<string, string> replacementFunc, Match m)
        {
            string capture = m.Value;
            capture = capture.Remove(m.Groups[groupName].Index - m.Index, m.Groups[groupName].Length);
            capture = capture.Insert(m.Groups[groupName].Index - m.Index, replacementFunc(m.Groups[groupName].Value));

            return capture;
        }
    }
}
