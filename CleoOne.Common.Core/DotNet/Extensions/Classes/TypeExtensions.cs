﻿// <copyright file="TypeExtensions.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Concurrent;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace CleoOne.Common.Core.DotNet.Extensions.Classes
{
    public static class TypeExtensions
    {
        public static PropertyInfo[] GetEfCompatibleProperties(this Type type)
        {
            return type.GetProperties(BindingFlags.Instance | BindingFlags.Public)
                       .Where(property => property.CanRead
                                          && property.CanWrite
                                          && property.GetCustomAttribute<NotMappedAttribute>() == null
                                          && (property.PropertyType.IsValueType
                                              || property.PropertyType == typeof(string)
                                              || (property.PropertyType.IsGenericType
                                                  && property.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))))
                       .ToArray();
        }

        private static readonly ConcurrentDictionary<Type, Func<object>> _defaultValuesConstructors =
            new ConcurrentDictionary<Type, Func<object>>();

        public static object GetDefaultValueOfType(this Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            var defaultValueConstructor = _defaultValuesConstructors.GetOrAdd(
                type,
                theType =>
                {
                    Expression<Func<object>> getDefaultValueLambda =
                        Expression.Lambda<Func<object>>(
                            Expression.Convert(
                                Expression.Default(theType),
                                typeof(object)));

                    return getDefaultValueLambda.Compile();
                });

            return defaultValueConstructor();
        }

        public static bool IsAssignableTo(this Type objectType, Type variableType)
        {
            return variableType.IsAssignableFrom(objectType);
        }

        public static bool ImplementsInterface(this Type typeOfObject, Type regularInterfaceType)
        {
            var implements = typeOfObject.GetInterfaces()
                                         .Any(interfaceType => interfaceType == regularInterfaceType);

            return implements;
        }

        public static bool ImplementsGenericInterface(this Type typeOfObject, Type genericInterfaceType)
        {
            if (!genericInterfaceType.IsGenericType)
            {
                throw new Exception($"Type {genericInterfaceType.Name} is not generic");
            }

            var implements = typeOfObject.GetInterfaces()
                                         .Any(interfaceType =>
                                                  interfaceType.IsGenericType
                                                  && interfaceType.GetGenericTypeDefinition() == genericInterfaceType);

            return implements;
        }

        public static bool InheritsFromType(this Type typeOfObject, Type parentType)
        {
            if (parentType == typeof(object))
            {
                return true;
            }

            var iterationType = typeOfObject;

            while (iterationType != null
                   && iterationType != typeof(object))
            {
                if (iterationType == parentType)
                {
                    return true;
                }

                iterationType = iterationType.BaseType;
            }

            return false;
        }

        public static bool InheritsFromGenericType(this Type typeOfObject, Type genericParentType)
        {
            if (!genericParentType.IsGenericType)
            {
                throw new Exception($"Type {genericParentType.Name} is not generic");
            }

            var iterationType = typeOfObject;

            while (iterationType != null
                   && iterationType != typeof(object))
            {
                if (iterationType == genericParentType)
                {
                    return true;
                }

                if (iterationType.IsGenericType
                    && iterationType.GetGenericTypeDefinition() == genericParentType)
                {
                    return true;
                }

                iterationType = iterationType.BaseType;
            }

            return false;
        }

        public static bool IsNullable(this Type type, out Type nullableTypeParameter)
        {
            nullableTypeParameter = null;

            TypeInfo typeInfo = type.GetTypeInfo();
            var isNullable = typeInfo.IsGenericType && typeInfo.GetGenericTypeDefinition() == typeof(Nullable<>);

            if (isNullable)
            {
                nullableTypeParameter = typeInfo.GetGenericArguments().Single();
            }

            return isNullable;
        }

        public static bool HasProperty(this Type type, string propertyName)
        {
            return type.GetProperty(propertyName) != null;
        }
    }
}
