﻿// <copyright file="ExceptionExtensions.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Text;

namespace CleoOne.Common.Core.DotNet.Extensions.Classes
{
    public static class ExceptionExtensions
    {
        private const string END_OF_INNER_EXCEPTION = "   --- End of inner exception stack trace ---";
        private const string INNER_EXCEPTION = "InnerException: ";

        public static string ToFullExceptionString(this Exception exception)
        {
            return exception.ToExceptionInfoStringInternal();
        }

        public static string ToMergedMessages(this Exception exception)
        {
            if (exception == null)
            {
                return string.Empty;
            }

            var sb = new StringBuilder();

            sb.Append(exception.Message);

            AddSpecificMessagesIfAny(exception, sb);

            var iterationException = exception.InnerException;

            while (iterationException != null)
            {
                sb.Append("; " + INNER_EXCEPTION);
                sb.Append(iterationException.Message);

                AddSpecificMessagesIfAny(iterationException, sb);

                iterationException = iterationException.InnerException;
            }

            return sb.ToString();
        }

        public static string ToMergedStackTraces(this Exception exception)
        {
            var sb = new StringBuilder();

            if (exception.InnerException != null)
            {
                sb.AppendLine(exception.InnerException.ToMergedStackTraces());
                sb.AppendLine(END_OF_INNER_EXCEPTION);
            }
            else
            {
                AddSpecificStackTraceIfAny(exception, sb);
            }

            sb.AppendLine((exception.StackTrace ?? "").TrimEnd());

            return sb.ToString().Trim();
        }

        private static string ToExceptionInfoStringInternal(this Exception exception)
        {
            var sb = new StringBuilder();

            sb.AppendLine(exception.GetType() + ": " + exception.Message);

            AddSpecificMessagesIfAny(exception, sb);
            AddSpecificExceptionInfo(exception, sb);

            if (exception.InnerException != null)
            {
                sb.AppendLine("   " + INNER_EXCEPTION + exception.InnerException.ToExceptionInfoStringInternal());
                sb.AppendLine(END_OF_INNER_EXCEPTION);
            }
            else
            {
                AddSpecificStackTraceIfAny(exception, sb);
            }

            sb.AppendLine((exception.StackTrace ?? "").TrimEnd());

            return sb.ToString().Trim();
        }

        private static void AddSpecificExceptionInfo(Exception exception, StringBuilder sb)
        {
            if (exception is DbUpdateException dbUpdateException)
            {
                sb.Append("First three Entries: ");
                sb.AppendLine(string.Join(", ", dbUpdateException.Entries.Take(3).Select(entry => entry.ToString())));
            }
        }

        #pragma warning disable CA1801 // Review unused parameters
        private static void AddSpecificMessagesIfAny(Exception exception, StringBuilder sb)
        {
        }

        private static void AddSpecificStackTraceIfAny(Exception exception, StringBuilder sb)
            #pragma warning restore CA1801 // Review unused parameters
        {
        }
    }
}
