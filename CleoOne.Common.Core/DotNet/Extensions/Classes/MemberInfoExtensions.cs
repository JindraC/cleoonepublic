﻿// <copyright file="MemberInfoExtensions.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace CleoOne.Common.Core.DotNet.Extensions.Classes
{
    public static class MemberInfoExtensions
    {
        public static bool ArePropertyOrFieldSame(MemberInfo a, MemberInfo b)
        {
            // This is not perfect as it does not take inheritance into account. Probably will not work for inherited/overriden properties correctly.
            // Unit tests need to be written.
            return a.Name == b.Name
                   && a.DeclaringType == b.DeclaringType
                   && a.MemberType == b.MemberType
                   && a.GetPropertyOrFieldType() == b.GetPropertyOrFieldType();
        }

        public static Type GetPropertyOrFieldType(this MemberInfo memberInfo)
        {
            if (memberInfo is PropertyInfo propertyInfo)
            {
                return propertyInfo.PropertyType;
            }

            if (memberInfo is FieldInfo fieldInfo)
            {
                return fieldInfo.FieldType;
            }

            throw new Exception("Not property or field");
        }

        public static object GetPropertyOrFieldValue(this MemberInfo memberInfo, object o)
        {
            if (memberInfo is PropertyInfo propertyInfo)
            {
                return propertyInfo.GetValue(o);
            }

            if (memberInfo is FieldInfo fieldInfo)
            {
                return fieldInfo.GetValue(o);
            }

            throw new Exception("Not property or field");
        }

        internal static object GetPropertyOrFieldValueByExpression(this object o, string memberName)
        {
            var objectType = o.GetType();

            var propertyGettersForObject = GetOrCreateGetter(objectType, memberName);

            return propertyGettersForObject(o);
        }

        private static readonly ConcurrentDictionary<Type, Dictionary<string, Func<object, object>>> _cachedMaps =
            new ConcurrentDictionary<Type, Dictionary<string, Func<object, object>>>();

        private static Func<object, object> GetOrCreateGetter(Type objectType, string memberName)
        {
            var typeMap = _cachedMaps.GetOrAdd(objectType, GenerateMapForType);

            if (typeMap.TryGetValue(memberName, out var getter))
            {
                return getter;
            }
            else
            {
                throw new InvalidOperationException($"Member name '{memberName}' not found. Possible reasons: "
                                                    + $"1/ typo in the member name, \n"
                                                    + $"2/ member is explicit interface implementation\n"
                                                    + $"3/ or member is private property of parent class.");
            }
        }

        private static List<FieldInfo> GetFieldInfos(Type type)
        {
            var memberInfos = new List<FieldInfo>();
            var actualType = type;

            while ((actualType != null)
                   && (actualType != typeof(object)))
            {
                var memberInfosToAdd = actualType.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

                memberInfos.AddRange(memberInfosToAdd);

                actualType = actualType.BaseType;
            }

            return memberInfos;
        }

        private static IEnumerable<PropertyInfo> GetPropertyInfos(Type type)
        {
            return type.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                       .Where(info => info.GetIndexParameters().Length == 0);
        }

        private static Dictionary<string, Func<object, object>> GenerateMapForType(Type arg)
        {
            var fieldInfos = GetFieldInfos(arg);

            var propertyInfos = GetPropertyInfos(arg);

            var result = new Dictionary<string, Func<object, object>>();

            foreach (var fieldInfo in fieldInfos)
            {
                var parameterExpression = Expression.Parameter(typeof(object), "o"); // object parameter;

                var lambda = Expression.Lambda<Func<object, object>>(
                    body: Expression.Convert(Expression.Field(Expression.Convert(parameterExpression, fieldInfo.DeclaringType),
                                                              fieldInfo),
                                             typeof(object)), // return (object)((realTypeOfTheObject)parameter).Property;
                    parameters: parameterExpression);         // function(object parameter)

                var compiled = lambda.Compile();

                result.Add(fieldInfo.Name, compiled);
            }

            foreach (var propertyInfo in propertyInfos)
            {
                var parameterExpression = Expression.Parameter(typeof(object), "o"); // object parameter;

                var lambda = Expression.Lambda<Func<object, object>>(
                    body: Expression.Convert(Expression.Property(Expression.Convert(parameterExpression, propertyInfo.DeclaringType),
                                                                 propertyInfo),
                                             typeof(object)), // return (object)((realTypeOfTheObject)parameter).Property;
                    parameters: parameterExpression);         // function(object parameter)

                var compiled = lambda.Compile();

                result.Add(propertyInfo.Name, compiled);
            }

            return result;
        }
    }
}
