﻿// <copyright file="SortBySample.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;

namespace CleoOne.Common.Core.DotNet.Extensions.Collections
{
    public static class SortBySample
    {
        public static BySampleSorter<TKey> Create<TKey>(IEnumerable<TKey> fixedOrder, IEqualityComparer<TKey> comparer = null)
        {
            return new BySampleSorter<TKey>(fixedOrder, comparer);
        }

        public static BySampleSorter<TKey> Create<TKey>(IEqualityComparer<TKey> comparer, params TKey[] fixedOrder)
        {
            return new BySampleSorter<TKey>(fixedOrder, comparer);
        }

        public static IOrderedEnumerable<TSource> OrderBySample<TSource, TKey>(this IEnumerable<TSource> source,
                                                                               Func<TSource, TKey> keySelector,
                                                                               BySampleSorter<TKey> sample)
        {
            return sample.OrderBySample(source, keySelector);
        }

        public static IOrderedEnumerable<TSource> ThenBySample<TSource, TKey>(this IOrderedEnumerable<TSource> source,
                                                                              Func<TSource, TKey> keySelector,
                                                                              BySampleSorter<TKey> sample)
        {
            return sample.ThenBySample(source, keySelector);
        }
    }

    public class BySampleSorter<TKey>
    {
        private readonly Dictionary<TKey, int> _dict;

        public BySampleSorter(IEnumerable<TKey> fixedOrder, IEqualityComparer<TKey> comparer = null)
        {
            _dict = fixedOrder
                   .Select((key, index) => new KeyValuePair<TKey, int>(key, index))
                   .ToDictionary(kv => kv.Key, kv => kv.Value, comparer ?? EqualityComparer<TKey>.Default);
        }

        public BySampleSorter(IEqualityComparer<TKey> comparer, params TKey[] fixedOrder)
            : this(fixedOrder, comparer)
        {
        }

        public IOrderedEnumerable<TSource> OrderBySample<TSource>(IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            return source.OrderBy(item => GetOrderKey(keySelector(item)));
        }

        public IOrderedEnumerable<TSource> ThenBySample<TSource>(IOrderedEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            return source.CreateOrderedEnumerable(item => GetOrderKey(keySelector(item)), Comparer<int>.Default, false);
        }

        private int GetOrderKey(TKey key)
        {
            int index;

            return _dict.TryGetValue(key, out index) ? index : int.MaxValue;
        }
    }
}
