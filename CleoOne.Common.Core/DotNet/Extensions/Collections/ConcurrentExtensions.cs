﻿// <copyright file="ConcurrentExtensions.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Collections.Concurrent;
using System.Collections.Generic;

namespace CleoOne.Common.Core.DotNet.Extensions.Collections
{
    public static class ConcurrentExtensions
    {
        public static void AddRange<T>(this ConcurrentQueue<T> queue, IEnumerable<T> items)
        {
            foreach (var item in items)
            {
                queue.Enqueue(item);
            }
        }

        public static List<T> DequeueAllToList<T>(this ConcurrentQueue<T> queue)
        {
            var list = new List<T>(queue.Count);

            while (queue.TryDequeue(out var item))
            {
                list.Add(item);
            }

            return list;
        }

        // Not concurrent, but also queue
        public static List<T> DequeueAllToList<T>(this Queue<T> queue)
        {
            var list = new List<T>();

            while (queue.TryDequeue(out var item))
            {
                list.Add(item);
            }

            return list;
        }
    }
}
