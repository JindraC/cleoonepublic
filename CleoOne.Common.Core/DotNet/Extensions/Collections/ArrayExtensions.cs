﻿// <copyright file="ArrayExtensions.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;

namespace CleoOne.Common.Core.DotNet.Extensions.Collections
{
    public static class ArrayExtensions
    {
        /// <summary>
        /// Creates new array and copies elements from first and second into it.
        /// </summary>
        /// <returns>Returns new array containing all elements.</returns>
        public static T[] FastArrayConcat<T>(this T[] first, T[] second)
        {
            T[] newArray = new T[first.Length + second.Length];
            Array.Copy(first, newArray, first.Length);
            Array.Copy(second, 0, newArray, first.Length, second.Length);

            return newArray;
        }

        public static T[] SubArray<T>(this T[] original, int startIndex, int count)
        {
            var length = original.Length;

            if (startIndex + count - 1 >= length)
            {
                throw new Exception($"StartIndex + Count - 1 ({startIndex + count - 1}) was bigger or equal to array length ({length})");
            }

            var resultArray = new T[count];

            Array.Copy(original, startIndex, resultArray, 0, count);

            return resultArray;
        }

        public static T[] ToArrayOf<T>(this T value, int count)
        {
            var array = new T[count];

            for (int i = 0; i < count; i++)
            {
                array[i] = value;
            }

            return array;
        }

        public static T[] ToArrayIfNotArray<T>(this IEnumerable<T> items)
        {
            if (items is T[] array)
            {
                return array;
            }

            return items.ToArray();
        }

        public static TOutput[] ConvertAll<TInput, TOutput>(this TInput[] input, Converter<TInput, TOutput> converter)
        {
            return Array.ConvertAll(input, converter);
        }
    }
}
