﻿// <copyright file="LinqExtensions.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Collections.Generic;
using System.Linq;

namespace CleoOne.Common.Core.DotNet.Extensions
{
    public static class LinqExtensions
    {
        public static T Second<T>(this IEnumerable<T> items)
        {
            if (items is IReadOnlyList<T> indexableCollection
                && indexableCollection.Count > 1)
            {
                return indexableCollection[1];
            }

            return items.Skip(1).First();
        }

        public static T SecondOrDefault<T>(this IEnumerable<T> items)
        {
            if (items is IReadOnlyList<T> indexableCollection)
            {
                if (indexableCollection.Count > 1)
                {
                    return indexableCollection[1];
                }
                else
                {
                    return default;
                }
            }

            return items.Skip(1).FirstOrDefault();
        }

        public static T Third<T>(this IEnumerable<T> items)
        {
            if (items is IReadOnlyList<T> indexableCollection
                && indexableCollection.Count > 2)
            {
                return indexableCollection[2];
            }

            return items.Skip(2).First();
        }
    }
}
