﻿// <copyright file="QueryableExtensions.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using Microsoft.EntityFrameworkCore.Query;
using Microsoft.EntityFrameworkCore.Query.SqlExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CleoOne.Common.Core.DotNet.Extensions.Collections
{
    public static class QueryableExtensions
    {
        // took from - https://stackoverflow.com/questions/37527783/get-sql-code-from-an-entity-framework-core-iqueryablet

        /// <summary>
        /// This version is targeted for .NET Core 3.1; From NET 5+ there is native method '.ToQueryString()'.
        /// Also its not perfect and can throw exception if it fails to translate the query (like .Contains() fails to translate into 'IN')
        /// </summary>
        /// <exception cref="System.InvalidOperationException">Can throw exception for some "valid" queries.</exception>
        public static string ToSqlQueryString<TEntity>(this IQueryable<TEntity> query)
            where TEntity : class
        {
            using var enumerator = query.Provider.Execute<IEnumerable<TEntity>>(query.Expression).GetEnumerator();
            var relationalCommandCache = enumerator.Private("_relationalCommandCache");
            var selectExpression = relationalCommandCache.Private<SelectExpression>("_selectExpression");
            var factory = relationalCommandCache.Private<IQuerySqlGeneratorFactory>("_querySqlGeneratorFactory");

            var sqlGenerator = factory.Create();
            var command = sqlGenerator.GetCommand(selectExpression);

            string sql = command.CommandText;

            return sql;
        }

        private static object Private(this object obj, string privateField) =>
            obj?.GetType().GetField(privateField, BindingFlags.Instance | BindingFlags.NonPublic)?.GetValue(obj);

        private static T Private<T>(this object obj, string privateField) =>
            (T)obj?.GetType().GetField(privateField, BindingFlags.Instance | BindingFlags.NonPublic)?.GetValue(obj);
    }
}
