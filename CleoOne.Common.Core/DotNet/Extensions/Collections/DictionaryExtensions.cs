﻿// <copyright file="DictionaryExtensions.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace CleoOne.Common.Core.DotNet.Extensions.Collections
{
    public static class DictionaryExtensions
    {
        public static TValue GetValueOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dictionary,
                                                             TKey key,
                                                             TValue defaultValue = default)
        {
            if (dictionary.TryGetValue(key, out var value))
            {
                return value;
            }

            return defaultValue;
        }

        public static bool IsNullOrEmpty<TKey, TValue>(this IDictionary<TKey, TValue> dictionary)
        {
            if (dictionary == null)
            {
                return true;
            }

            return dictionary.Count == 0;
        }

        /// <summary>
        /// Adds value to the List if specified key already exists, otherwise creates new List and adds it with the new key.
        /// </summary>
        public static void AddToValue<TKey, TValue>(this IDictionary<TKey, IList<TValue>> dictionary, TKey key, TValue value)
        {
            if (dictionary.TryGetValue(key, out var list))
            {
                list.Add(value);
            }
            else
            {
                dictionary.Add(key, new List<TValue> { value, });
            }
        }

        /// <summary>
        /// Adds value to the List if specified key already exists, otherwise creates new List and adds it with the new key.
        /// </summary>
        public static void AddToValue<TKey, TValue>(this IDictionary<TKey, List<TValue>> dictionary, TKey key, TValue value)
        {
            if (dictionary.TryGetValue(key, out var list))
            {
                list.Add(value);
            }
            else
            {
                dictionary.Add(key, new List<TValue> { value, });
            }
        }

        public static ConcurrentDictionary<TKey, TValue> ToConcurrentDictionary<TKey, TValue>(this IDictionary<TKey, TValue> dictionary,
                                                                                              IEqualityComparer<TKey> comparer = null)
        {
            return new ConcurrentDictionary<TKey, TValue>(dictionary, comparer ?? EqualityComparer<TKey>.Default);
        }

        public static TValue GetOrAdd<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, TValue value)
        {
            if (dictionary.TryGetValue(key, out var existingValue))
            {
                return existingValue;
            }

            dictionary.Add(key, value);

            return value;
        }

        public static TValue GetOrAdd<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, Func<TKey, TValue> valueFactory)
        {
            if (dictionary.TryGetValue(key, out var existingValue))
            {
                return existingValue;
            }

            var value = valueFactory(key);

            dictionary.Add(key, value);

            return value;
        }
    }
}
