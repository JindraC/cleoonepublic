﻿// <copyright file="ListExtensions.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;

namespace CleoOne.Common.Core.DotNet.Extensions
{
    public static class ListExtensions
    {
        /// <summary>
        /// Moves a single element in list from one position to another (via index number).
        /// </summary>
        public static void MoveByIndex<T>(this IList<T> list, int oldIndex, int newIndex)
        {
            var item = list[oldIndex];

            list.RemoveAt(oldIndex);
            list.Insert(newIndex, item);
        }

        public static List<T> ToListIfNotList<T>(this IEnumerable<T> items)
        {
            if (items is List<T> list)
            {
                return list;
            }

            return items.ToList();
        }

        public static IList<T> ToIList<T>(this IEnumerable<T> items)
        {
            if (items is IList<T> iList)
            {
                return iList;
            }
            else
            {
                return items.ToList();
            }
        }

        public static List<T> ToNullIfEmpty<T>(this List<T> list)
        {
            return list.IsNullOrEmpty() ? null : list;
        }

        private static readonly Random _random = new Random(123);

        public static T RandomItem<T>(this List<T> list)
        {
            if (list.Count == 0)
            {
                throw new ArgumentException("Cannot return item from empty list.");
            }

            return list[_random.Next(0, list.Count - 1)];
        }

        public static decimal SumFast<TItem>(this IList<TItem> list, Func<TItem, decimal> selector)
        {
            var count = list.Count;

            switch (count)
            {
                case 0:  return 0;
                case 1:  return selector(list[0]);
                case 2:  return selector(list[0]) + selector(list[1]);
                case 3:  return selector(list[0]) + selector(list[1]) + selector(list[2]);
                default: break;
            }

            var sum = 0.0m;

            for (int i = 0; i < count; i++)
            {
                sum += selector(list[i]);
            }

            return sum;
        }

        public static IList<T> Shuffle<T>(this IList<T> list, int seed = 0)
        {
            var random = new Random(seed);
            int n = list.Count;

            while (n > 1)
            {
                n--;
                int k = random.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }

            return list;
        }

        public static IList<T> TakeLast<T>(this IList<T> items, int count)
        {
            return items.Skip(Math.Max(0, items.Count - count)).ToList();
        }
    }
}
