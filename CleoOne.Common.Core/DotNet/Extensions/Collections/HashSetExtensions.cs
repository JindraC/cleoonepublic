﻿// <copyright file="HashSetExtensions.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.DotNet.Collections;
using System.Collections.Generic;

namespace CleoOne.Common.Core.DotNet.Extensions
{
    public static class HashSetExtensions
    {
        public static void AddRange<T>(this HashSet<T> hashSet, IEnumerable<T> items)
        {
            hashSet.UnionWith(items);
        }

        public static void AddRange<T>(this ConcurrentHashSet<T> hashSet, IEnumerable<T> items)
        {
            foreach (var item in items)
            {
                hashSet.Add(item);
            }
        }
    }
}
