﻿// <copyright file="EnumerableExtensions.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.DotNet.Collections;
using CleoOne.Common.Core.DotNet.Extensions.Classes;
using CleoOne.Common.Core.DotNet.Extensions.Strings;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace CleoOne.Common.Core.DotNet.Extensions
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<T[]> ChunkBy<T>(this IEnumerable<T> items, int chunkSize)
        {
            var subResult = new List<T>(chunkSize);

            foreach (var item in items)
            {
                subResult.Add(item);

                if (subResult.Count >= chunkSize)
                {
                    yield return subResult.ToArray();

                    subResult.Clear();
                }
            }

            if (subResult.Count > 0)
            {
                yield return subResult.ToArray();
            }
        }

        public static IEnumerable<T[]> GroupToBulks<T>(this IEnumerable<T> items, int bulkSize)
        {
            if (bulkSize <= 0)
            {
                throw new ArgumentException("Bulk size must be positive.", nameof(bulkSize));
            }

            return
                items.Select((item, index) => (item, index))
                     .GroupBy(pair => pair.index / bulkSize, pair => pair.item)
                     .Select(grp => grp.ToArray());
        }

        public static IEnumerable<T[]> GroupToBulksByMaxCount<T>(this IEnumerable<T> items, int bulksMaxCount)
        {
            var itemsList = items.ToListIfNotList();

            var count = itemsList.Count;

            var bulkSize = (int)Math.Ceiling((double)count / bulksMaxCount);

            if (bulkSize <= 0)
            {
                yield break;
            }

            foreach (var bulk in itemsList.GroupToBulks(bulkSize))
            {
                yield return bulk;
            }
        }

        public static DataTable ConvertToDataTable<T>(this IEnumerable<T> items,
                                                      bool onlyEfCompatibleProperties = true)
        {
            var properties =
                onlyEfCompatibleProperties
                    ? typeof(T).GetEfCompatibleProperties()
                    : typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);

            var table = new DataTable();

            for (int i = 0; i < properties.Length; i++)
            {
                var propertyInfo = properties[i];
                var propertyType = propertyInfo.PropertyType;

                if (propertyType.IsGenericType
                    && propertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                {
                    table.Columns.Add(propertyInfo.Name, Nullable.GetUnderlyingType(propertyType));
                }
                else
                {
                    table.Columns.Add(propertyInfo.Name, propertyType);
                }
            }

            var values = new object[properties.Length];

            foreach (var item in items)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = properties[i].GetValue(item);
                }

                table.Rows.Add(values);
            }

            return table;
        }

        public static ConcurrentHashSet<T> ToConcurrentHashSet<T>(this IEnumerable<T> items, IEqualityComparer<T> comparer = null)
        {
            return new ConcurrentHashSet<T>(items, comparer ?? EqualityComparer<T>.Default);
        }

        public static ConcurrentDictionary<TKey, TItem> ToConcurrentDictionary<TItem, TKey>(this IEnumerable<TItem> items,
                                                                                            Func<TItem, TKey> keySelector,
                                                                                            IEqualityComparer<TKey> comparer = null)
        {
            return ToConcurrentDictionary(items, keySelector, item => item, comparer);
        }

        public static ConcurrentDictionary<TKey, TValue> ToConcurrentDictionary<TItem, TKey, TValue>(this IEnumerable<TItem> items,
                                                                                                     Func<TItem, TKey> keySelector,
                                                                                                     Func<TItem, TValue> valueSelector,
                                                                                                     IEqualityComparer<TKey> comparer =
                                                                                                         null)
        {
            var result = new ConcurrentDictionary<TKey, TValue>(comparer ?? EqualityComparer<TKey>.Default);

            foreach (var item in items)
            {
                var key = keySelector(item);
                var value = valueSelector(item);

                result.AddOrUpdate(key, value, (_, __) => throw new NotImplementedException());
            }

            return result;
        }

        public static SynchronizedCollection<TItem> ToSynchronizedCollection<TItem>(this IEnumerable<TItem> items)
        {
            return new SynchronizedCollection<TItem>(syncRoot: new object(), items);
        }

        public static IEnumerable<T> WhereNotNull<T>(this IEnumerable<T> items)
            where T : class
        {
            return items.Where(item => !(item is null));
        }

        public static IEnumerable<T> WhereNotNull<T>(this IEnumerable<T?> items)
            where T : struct
        {
            return items.Where(x => !(x is null)).Cast<T>();
        }

        public static T FastDistinctAndSingle<T>(this IEnumerable<T> items, IEqualityComparer<T> comparer = null)
        {
            // equivalent to 'return items.Distinct().Single();'

            var resultItem = default(T);
            var wasResultAssigned = false;

            comparer ??= EqualityComparer<T>.Default;

            foreach (var item in items)
            {
                if (!wasResultAssigned)
                {
                    resultItem = item;
                    wasResultAssigned = true;

                    continue;
                }

                if (!comparer.Equals(item, resultItem))
                {
                    throw new Exception(
                        $"Single() function failed, because there were at least 2 distinct items: '{resultItem?.ToString()}' != '{item?.ToString()}'");
                }
            }

            if (!wasResultAssigned)
            {
                throw new Exception($"Single() function failed because the enumerable was empty.");
            }

            return resultItem;
        }

        public static double Median<T>(this IEnumerable<T> items, Func<T, double> selector)
        {
            return items.Select(selector).Median();
        }

        public static T Median<T>(this IEnumerable<T> numbers)
        {
            var numbersList = numbers as IList<T> ?? numbers.ToList();
            int count = numbersList.Count;

            if (count == 0)
            {
                if (typeof(T) == typeof(double))
                {
                    return (T)(object)double.NaN;
                }
                else
                {
                    return default(T);
                }
            }

            int half = count / 2;

            return numbersList.OrderBy(a => a).ElementAt(half);
        }

        public static IEnumerable<TSource> DistinctBy<TSource, TResult>(this IEnumerable<TSource> source,
                                                                        Func<TSource, TResult> selector,
                                                                        IEqualityComparer<TResult> comparer = default)
        {
            var hashSet = comparer == null ? new HashSet<TResult>() : new HashSet<TResult>(comparer);

            foreach (var item in source)
            {
                var selectedValue = selector(item);

                if (hashSet.Add(selectedValue))
                {
                    yield return item;
                }
            }
        }

        public static bool SequencePredicateEqual<TSource>(this IEnumerable<TSource> first,
                                                           IEnumerable<TSource> second,
                                                           Func<TSource, TSource, bool> predicate)
        {
            return first.SequenceEqual(second, new PredicateEqualityComparer<TSource>(predicate));
        }

        private class PredicateEqualityComparer<T> : IEqualityComparer<T>
        {
            private readonly Func<T, T, bool> _predicate;

            public PredicateEqualityComparer(Func<T, T, bool> predicate)
            {
                _predicate = predicate;
            }

            public bool Equals(T a, T b)
            {
                return _predicate(a, b);
            }

            public int GetHashCode(T a)
            {
                throw new NotImplementedException("Predicate equality comparer is not intended to be used, when hashcode is needed.");
            }
        }

        public static (List<TSource> truthy, List<TSource> falsy) SplitOn<TSource>(this IEnumerable<TSource> first,
                                                                                   Func<TSource, bool> predicate)
        {
            var t = new List<TSource>();
            var f = new List<TSource>();

            foreach (var item in first)
            {
                if (predicate(item))
                {
                    t.Add(item);
                }
                else
                {
                    f.Add(item);
                }
            }

            return (t, f);
        }

        public static IEnumerable<(int index, T item)> WithIndex<T>(this IEnumerable<T> items) => items.Select((item, idx) => (idx, item));

        public static IEnumerable<TItem> With<TItem>(this IEnumerable<TItem> items, Action<TItem> operation)
        {
            if (items is IList<TItem> list)
            {
                foreach (var item in list)
                {
                    operation(item);
                }

                return list;
            }
            else
            {
                return items.Select(item =>
                {
                    operation(item);

                    return item;
                });
            }
        }

        public static async Task<List<TResult>> SelectToListAsync<TSource, TResult>(this IEnumerable<TSource> source,
                                                                                    Func<TSource, Task<TResult>> selectorAsync)
        {
            var results = new List<TResult>();

            foreach (var item in source)
            {
                results.Add(await selectorAsync(item));
            }

            return results;
        }

        public static async Task<IEnumerable<TResult>> SelectManyAsync<TSource, TResult>(this IEnumerable<TSource> items,
                                                                                         Func<TSource, Task<IEnumerable<TResult>>> function)
        {
            return (await Task.WhenAll(items.Select(function))).SelectMany(_ => _);
        }

        public static bool IsNullOrEmpty<T>(this IEnumerable<T> enumerable)
        {
            return enumerable == null
                   || (enumerable as string)?.Length == 0
                   || !enumerable.Any();
        }

        public static bool IsSingle<T>(this IEnumerable<T> enumerable)
        {
            // optimized so enumerable.Count(), doesnt have to enumerate trough the whole collection
            return enumerable.Take(2).Count() == 1;
        }

        public static bool IsNotSingle<T>(this IEnumerable<T> enumerable)
        {
            return !IsSingle(enumerable);
        }

        public static IEnumerable<T> JoinWithSeparator<T>(this IEnumerable<T> items, T separator)
        {
            using var enumerator = items.GetEnumerator();

            var shouldGiveSeparator = false;

            while (enumerator.MoveNext())
            {
                if (shouldGiveSeparator)
                {
                    yield return separator;
                }

                yield return enumerator.Current;

                // try fetch next
                var hasNext = enumerator.MoveNext();

                if (hasNext)
                {
                    yield return separator;
                    yield return enumerator.Current;
                    shouldGiveSeparator = true;
                }
                else
                {
                    break;
                }
            }
        }

        public static EquatableArray<T> ToEquatableArray<T>(this IEnumerable<T> items, IEqualityComparer<T> comparer = default)
        {
            return new EquatableArray<T>(items, comparer);
        }

        public static EquatableArray<T> ToEquatableArrayIfNotYet<T>(this IEnumerable<T> items,
                                                                    IEqualityComparer<T> comparer = default)
        {
            comparer ??= EqualityComparer<T>.Default;

            if (items is EquatableArray<T> array)
            {
                return array.AssertComparer(comparer);
            }
            else
            {
                return items.ToEquatableArray(comparer);
            }
        }
    }
}
