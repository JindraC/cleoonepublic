﻿// <copyright file="ShallowCopyExtensions.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Linq.Expressions;
using System.Reflection;

namespace CleoOne.Common.Core.DotNet.Extensions.Copying
{
    public static class ShallowCopyExtensions
    {
        private static Type ObjectType { get; } = typeof(object);

        private static Type StringType { get; } = typeof(string);

        private static MethodInfo MemberwiseCloneMethodInfo { get; } =
            ObjectType.GetMethod("MemberwiseClone", BindingFlags.NonPublic | BindingFlags.Instance);

        private static Func<object, object> MemberwiseCloneFunc { get; } = CreateMemberwiseCloneFunction();

        private static Func<object, object> CreateMemberwiseCloneFunction()
        {
            var inputParameter = Expression.Parameter(ObjectType);

            var outputVariable = Expression.Variable(ObjectType);

            var memberwiseCloneInputExpression =
                Expression.Assign(
                    outputVariable,
                    Expression.Call(
                        inputParameter,
                        MemberwiseCloneMethodInfo));

            var lambdaBody = Expression.Block(new[] { outputVariable, }, memberwiseCloneInputExpression, outputVariable);

            var lambdaExpression = Expression.Lambda<Func<object, object>>(lambdaBody, inputParameter);

            var compiledLambda = lambdaExpression.Compile();

            return compiledLambda;
        }

        public static T ShallowCopy<T>(this T item)
        {
            if (item == null)
            {
                return default;
            }

            var type = item.GetType();

            if (type.IsValueType
                || type == StringType)
            {
                return item;
            }

            var shallowCopy = (T)MemberwiseCloneFunc(item);

            return shallowCopy;
        }
    }
}
