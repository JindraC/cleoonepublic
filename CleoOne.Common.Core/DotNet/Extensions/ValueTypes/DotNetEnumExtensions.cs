﻿// <copyright file="DotNetEnumExtensions.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace CleoOne.Common.Core.DotNet.Extensions.ValueTypes
{
    /// <summary>
    /// Contains only extensions not bind to Cleo internal enum types.
    /// </summary>
    public static class DotNetEnumExtensions
    {
        private static Random _random = new Random();

        public static TAttribute GetAttribute<TAttribute>(this Enum value)
            where TAttribute : Attribute
        {
            var type = value.GetType();
            var name = Enum.GetName(type, value);

            return type.GetField(name)
                       .GetCustomAttributes(false)
                       .OfType<TAttribute>()
                       .SingleOrDefault();
        }

        /// <summary>
        /// Get Enum Attributes of specified type.
        /// </summary>
        /// <typeparam name="TAttribute">Type of Attributes to get.</typeparam>
        /// <typeparam name="TEnum">Type of Enum.</typeparam>
        /// <param name="value">Enum to get Attributes of.</param>
        public static IEnumerable<TAttribute> GetAttributes<TAttribute, TEnum>(this TEnum value)
            where TAttribute : Attribute
            where TEnum : struct
        {
            var type = value.GetType();
            var name = Enum.GetName(type, value);

            return type.GetField(name)
                       .GetCustomAttributes(false)
                       .OfType<TAttribute>();
        }

        public static string GetDescription<TEnum>(this TEnum value)
            where TEnum : Enum
        {
            return value.GetDescriptionOrNull()
                   ?? throw new Exception($"Description is missing for enum value: {value}");
        }

        public static string GetDescriptionOrNull<TEnum>(this TEnum value)
            where TEnum : Enum
        {
            return value.GetAttribute<DescriptionAttribute>()?.Description;
        }

        public static IEnumerable<TEnum> GetAllValues<TEnum>()
            where TEnum : Enum
        {
            return (TEnum[])Enum.GetValues(typeof(TEnum));
        }

        public static IEnumerable<TEnum> GetValuesWithoutDefault<TEnum>()
            where TEnum : Enum
        {
            return GetAllValues<TEnum>().Where(e => !e.Equals(default(TEnum)));
        }

        public static IEnumerable<TEnum> GetValuesWithoutNone<TEnum>(IEnumerable<TEnum> enumsToSkip = null)
            where TEnum : Enum
        {
            var enumsWithoutNone = GetAllValues<TEnum>().Where(e => e.ToString() != "None");

            return (enumsToSkip == null) ? enumsWithoutNone : enumsWithoutNone.Except(enumsToSkip);
        }

        public static TEnum GetRandomEnumValueWithoutNone<TEnum>(IEnumerable<TEnum> enumsToSkip = null)
            where TEnum : Enum
        {
            var toSkipWithoutNone = GetValuesWithoutNone(enumsToSkip).ToList();

            if (toSkipWithoutNone.Count == 0)
            {
                throw new ArgumentOutOfRangeException();
            }

            var position = _random.Next(0, toSkipWithoutNone.Count);

            return toSkipWithoutNone[position];
        }

        public static object GetRandomEnumWithoutCleoOneNone(Type type)
        {
            if (type.IsEnum)
            {
                var enums = Enum.GetValues(type)
                                .Cast<object>()
                                .Where(
                                     x => (!(type?.FullName ?? "").StartsWith("CleoOne.") && !(type?.FullName ?? "").StartsWith("DSuite."))
                                          || x.ToString() != "None")
                                .ToArray();

                var result = enums[_random.Next(0, enums.Length)];

                return result;
            }

            throw new Exception($"There is no enum for {nameof(type)}");
        }

        public static string GetDescriptionOrToString<TEnum>(this TEnum value)
            where TEnum : Enum
        {
            return value.GetAttribute<DescriptionAttribute>()?.Description ?? value.ToString();
        }

        public static TFlag MinusFlag<TFlag>(this TFlag value, TFlag minusValue)
            where TFlag : struct, Enum
        {
            // we use conversion to long (instead of int), because enum can
            // inherit also from long and this would work for both integer and long
            // (if we used integer and some enum was long, we would silently lose information here)
            var valueAsLong = Convert.ToInt64(value);
            var minusAsLong = Convert.ToInt64(minusValue);

            var result = valueAsLong & ~minusAsLong;

            return (TFlag)Enum.ToObject(typeof(TFlag), result);
        }
    }
}
