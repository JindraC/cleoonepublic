﻿// <copyright file="IntegerExtensions.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;

namespace CleoOne.Common.Core.DotNet.Extensions.ValueTypes
{
    public static class IntegerExtensions
    {
        public static int MaxOrDefault(this IEnumerable<int> numbers)
        {
            var max = int.MinValue;
            var any = false;

            foreach (var number in numbers)
            {
                any = true;

                if (number > max)
                {
                    max = number;
                }
            }

            return any ? max : default;
        }

        public static int MaxOrDefault<T>(this IEnumerable<T> items, Func<T, int> selector)
        {
            return items.Select(selector).MaxOrDefault();
        }
    }
}
