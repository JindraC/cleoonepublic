﻿// <copyright file="LongExtensions.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;

namespace CleoOne.Common.Core.DotNet.Extensions.ValueTypes
{
    public static class LongExtensions
    {
        private static readonly DateTime unixStartDateUtc = DateTimeExtensions.UnixStartDateUtc;

        public static DateTime ToUtcFromUnixSeconds(this long seconds)
        {
            return unixStartDateUtc.AddSeconds(seconds);
        }

        public static DateTime ToUtcFromUnixMilliseconds(this long milliseconds)
        {
            return unixStartDateUtc.AddMilliseconds(milliseconds);
        }

        public static int ToInt(this long number)
        {
            return Convert.ToInt32(number);
        }
    }
}
