﻿// <copyright file="DateTimeExtensions.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CleoOne.Common.Core.DotNet.Extensions.ValueTypes
{
    public static class DateTimeExtensions
    {
        public static DateTime RoundUpToDate(this DateTime time)
        {
            var date = time.Date;

            if (time == date)
            {
                return date;
            }
            else
            {
                return date.AddDays(1);
            }
        }

        public static DateTime RoundDownToHour(this DateTime time)
        {
            return new DateTime(time.Year,
                                time.Month,
                                time.Day,
                                time.Hour,
                                0,
                                0,
                                time.Kind);
        }

        public static DateTime RoundDownForH4(this DateTime time)
        {
            if (time == DateTime.MinValue)
            {
                return DateTime.MinValue;
            }

            if (time.Hour >= 2)
            {
                return new DateTime(time.Year,
                                    time.Month,
                                    time.Day,
                                    ((time.Hour - 2) / 4 * 4) + 2,
                                    0,
                                    0,
                                    time.Kind);
            }
            else
            {
                return time.Date.AddHours(-2);
            }
        }

        public static DateTime RoundDownForW1(this DateTime time)
        {
            if (time == DateTime.MinValue)
            {
                return DateTime.MinValue;
            }

            var date = time.Date;
            var subtractDaysCount = ((int)date.DayOfWeek + 6) % 7;
            var weekStart = date.AddDays(-subtractDaysCount);

            return weekStart;
        }

        public static DateTime RoundUpForW1(this DateTime time)
        {
            if (time.DayOfWeek == DayOfWeek.Monday
                && time.Date == time)
            {
                return time;
            }

            return time.RoundDownForW1().AddDays(7);
        }

        public static long ToUnixTimeSeconds(this DateTime time, ILogger logger = null)
        {
            if (time.Kind != DateTimeKind.Utc)
            {
                logger?.LogError("Converted time shall be UTC, investigate! Stacktrace: {ToUnixTimeErrorStackTrace}",
                                 Environment.StackTrace);
            }

            return new DateTimeOffset(time).ToUnixTimeSeconds();
        }

        private static DateTime RoundDownForS1(this DateTime time)
        {
            return new DateTime(time.Year,
                                time.Month,
                                time.Day,
                                time.Hour,
                                time.Minute,
                                time.Second,
                                time.Kind);
        }

        private static DateTime RoundDownForM1(this DateTime time)
        {
            return new DateTime(time.Year,
                                time.Month,
                                time.Day,
                                time.Hour,
                                time.Minute,
                                0,
                                time.Kind);
        }

        private static DateTime RoundDownForM5(this DateTime time)
        {
            return new DateTime(time.Year,
                                time.Month,
                                time.Day,
                                time.Hour,
                                (time.Minute / 5) * 5,
                                0,
                                time.Kind);
        }

        private static DateTime RoundDownForM15(this DateTime time)
        {
            return new DateTime(time.Year,
                                time.Month,
                                time.Day,
                                time.Hour,
                                (time.Minute / 15) * 15,
                                0,
                                time.Kind);
        }

        private static DateTime RoundDownForM30(this DateTime time)
        {
            return new DateTime(time.Year,
                                time.Month,
                                time.Day,
                                time.Hour,
                                (time.Minute / 30) * 30,
                                0,
                                time.Kind);
        }

        private static DateTime RoundDownForH1(this DateTime time)
        {
            return new DateTime(time.Year,
                                time.Month,
                                time.Day,
                                time.Hour,
                                0,
                                0,
                                time.Kind);
        }

        private static DateTime RoundDownForD1(this DateTime time)
        {
            return new DateTime(time.Year,
                                time.Month,
                                time.Day,
                                0,
                                0,
                                0,
                                time.Kind);
        }

        private static DateTime RoundDownForMonth1(this DateTime time)
        {
            return new DateTime(time.Year,
                                time.Month,
                                1,
                                0,
                                0,
                                0,
                                time.Kind);
        }

        private static DateTime RoundDownForQ1(this DateTime time)
        {
            return new DateTime(time.Year,
                                (((time.Month - 1) / 3) * 3) + 1,
                                1,
                                0,
                                0,
                                0,
                                time.Kind);
        }

        private static DateTime RoundDownForY1(this DateTime time)
        {
            return new DateTime(time.Year,
                                1,
                                1,
                                0,
                                0,
                                0,
                                time.Kind);
        }

        internal static readonly DateTime UnixStartDateUtc = new DateTime(1970,
                                                                          1,
                                                                          1,
                                                                          0,
                                                                          0,
                                                                          0,
                                                                          DateTimeKind.Utc);

        public static long ToUnixMilliseconds(this DateTime utcTime)
        {
            var result = (long)(utcTime - UnixStartDateUtc).TotalMilliseconds;

            return result;
        }

        public static long ToUnixSeconds(this DateTime utcTime)
        {
            var result = (long)(utcTime - UnixStartDateUtc).TotalSeconds;

            return result;
        }

        public static DateTime GetStartOfMonth(this DateTime date)
        {
            var result = new DateTime(date.Year,
                                      date.Month,
                                      1,
                                      0,
                                      0,
                                      0,
                                      0,
                                      date.Kind);

            return result;
        }

        public static IEnumerable<DateTime> EachDay(this DateTime from, DateTime thru)
        {
            for (var day = from.Date; day.Date <= thru.Date; day = day.AddDays(1))
            {
                yield return day;
            }
        }

        public static DateTime Max(params DateTime[] dates) => dates.Max();

        public static DateTime Min(params DateTime[] dates) => dates.Min();

        public static DateTime? Max(params DateTime?[] dates) => dates.Max();

        public static DateTime? Min(params DateTime?[] dates) => dates.Min();

        public static DateTime SetHours(this DateTime d, int hours)
        {
            return d.AddHours(-d.Hour).AddHours(hours);
        }
    }
}
