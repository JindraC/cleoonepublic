﻿// <copyright file="DecimalExtensions.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CleoOne.Common.Core.DotNet.Extensions.ValueTypes
{
    public static class DecimalExtensions
    {
        /// <summary>
        /// Division that allows division by 0 (which is not mathematically correct, but prevents exceptions).
        /// Returns 0 if denominator is 0, otherwise normal devision.
        /// </summary>
        /// <param name="numenator"> The term of a fraction, usually above the line, that indicates the number of equal parts that are to be added together.
        /// The numerator of the fraction 2/5 is 2.</param>
        /// <param name="denominator">That term of a fraction, usually written under the line, that indicates the number of equal parts into which the unit is divided.
        /// The denominator of the fraction 2/5 is 5.</param>
        /// <returns>0 if denominator is 0, otherwise normal devision.</returns>
        public static decimal SafeDivision(decimal numenator, decimal denominator, ILogger logger = null)
        {
            try
            {
                return denominator == 0 ? 0 : numenator / denominator;
            }
            catch (OverflowException) // this can happen when denominator is aproximatly zero => 0.00000000001 etc
            {
                logger?.LogWarning("SafeDivision throwed overflow exception for input {Numenator} * {Denominator}", numenator, denominator);

                return 0m;
            }
            catch (Exception e)
            {
                throw new Exception("NumericExtension.SafeDivision throwed unexpected exception.", e);
            }
        }

        public static decimal? AverageOrNull(this IEnumerable<decimal> numbers)
        {
            var count = 0;
            var sum = 0m;

            foreach (var number in numbers)
            {
                count++;
                sum += number;
            }

            if (count == 0)
            {
                return null;
            }

            return sum / count;
        }

        public static decimal AverageOrZero(this IEnumerable<decimal> numbers)
        {
            var count = 0;
            var sum = 0m;

            foreach (var number in numbers)
            {
                count++;
                sum += number;
            }

            if (count == 0)
            {
                return 0m;
            }

            return sum / count;
        }

        public static decimal AverageOrZero<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal> selector)
        {
            var count = 0;
            var sum = 0m;

            foreach (var item in source)
            {
                count++;
                sum += selector(item);
            }

            if (count == 0)
            {
                return 0m;
            }

            return sum / count;
        }

        public static decimal? ToNullIfZero(this decimal number)
        {
            return number == 0m ? (decimal?)null : number;
        }

        public static decimal RemoveTrailingZeros(this decimal d)
        {
            // https://stackoverflow.com/questions/4525854/remove-trailing-zeros

            return d / 1.00000_00000_00000_00000_00000_00000_000m;
        }

        public static decimal WeightedAverage<T>(this IEnumerable<T> records,
                                                 Func<T, decimal> value,
                                                 Func<T, decimal> weight,
                                                 bool throwOnError = true)
        {
            var recordsList = records.ToListIfNotList();

            if (!recordsList.Any())
            {
                if (throwOnError)
                {
                    throw new InvalidOperationException("Sequence contains no elements");
                }
                else
                {
                    return 0;
                }
            }

            decimal weightedValueSum = recordsList.Sum(x => value(x) * weight(x));
            decimal weightSum = recordsList.Sum(weight);

            if (weightSum != 0)
            {
                return weightedValueSum / weightSum;
            }
            else if (throwOnError)
            {
                throw new DivideByZeroException();
            }
            else
            {
                return 0;
            }
        }
    }
}
