﻿// <copyright file="StringExtensions.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace CleoOne.Common.Core.DotNet.Extensions.Strings
{
    public static class StringExtensions
    {
        public static string SubStringMaxLength(this string str, int maxLength)
        {
            if (str.Length <= maxLength)
            {
                return str;
            }

            return str.Substring(0, maxLength);
        }

        /// <summary>
        /// Converts string into Enum. If string doesn't match any type, returns default => zero value enum.
        /// </summary>
        /// <typeparam name="TEnum">Type of Enum to convert to.</typeparam>
        /// <param name="str">String to be converted.</param>
        /// <param name="ignoreCase">Ignore case of string - default is true.</param>
        public static TEnum ToEnum<TEnum>(this string str, bool ignoreCase = true)
            where TEnum : struct
        {
            return Enum.TryParse<TEnum>(str, ignoreCase, out var result) ? result : default;
        }

        public static bool IsNullOrEmpty(this string input)
        {
            return string.IsNullOrEmpty(input);
        }

        public static string[] SplitToNonEmptyLines(this string input)
        {
            return input.Split(new[] { "\r", "\n", }, StringSplitOptions.RemoveEmptyEntries);
        }

        public static string[] SplitToLines(this string source)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            string[] result = source.Split(new[] { "\r\n", "\r", "\n", }, StringSplitOptions.None);

            return result;
        }

        public static string ReplaceCharacters(this string s, char[] characters, string newVal)
        {
            string[] temp;
            temp = s.Split(characters, StringSplitOptions.None);

            return string.Join(newVal, temp);
        }

        public static string RemoveCharacters(this string s, params char[] characters)
        {
            string[] temp;
            temp = s.Split(characters, StringSplitOptions.RemoveEmptyEntries);

            return string.Join(string.Empty, temp);
        }

        public static string RemoveCharacters(this string s, char character)
        {
            string[] temp;
            temp = s.Split(character, StringSplitOptions.RemoveEmptyEntries);

            return string.Join(string.Empty, temp);
        }

        public static decimal ToDecimalOrZero(this string value)
        {
            // using NumberStyles.Float to allow also exponential format: 1e-8
            return decimal.TryParse(value ?? "", NumberStyles.Number | NumberStyles.Float, CultureInfo.InvariantCulture, out var result)
                       ? result
                       : 0;
        }

        public static decimal ToDecimalInvariant(this string value)
        {
            // using NumberStyles.Float to allow also exponential format: 1e-8
            if (decimal.TryParse(value, NumberStyles.Number | NumberStyles.Float, CultureInfo.InvariantCulture, out var result))
            {
                return result;
            }
            else
            {
                throw new FormatException($"Cannot parse into decimal: '{value}'");
            }
        }

        public static int ToIntInvariant(this string value)
        {
            return int.Parse(value, CultureInfo.InvariantCulture);
        }

        public static long ToLongInvariant(this string value)
        {
            return long.Parse(value, CultureInfo.InvariantCulture);
        }

        public static string ReplaceInvariant(this string str, string oldValue, string newValue)
        {
            return str.Replace(oldValue, newValue, StringComparison.InvariantCulture);
        }

        public static bool StartsWithInvariant(this string str, string value)
        {
            return str.StartsWith(value, StringComparison.InvariantCulture);
        }

        public static bool EndsWithInvariant(this string str, string value)
        {
            return str.EndsWith(value, StringComparison.InvariantCulture);
        }

        public static bool ContainsInvariant(this string str, string value)
        {
            return str.Contains(value, StringComparison.InvariantCulture);
        }

        private static readonly Regex WhiteSpacesRegex = new Regex(@"\s", RegexOptions.Singleline);

        public static string RemoveWhiteSpaces(this string input)
        {
            return WhiteSpacesRegex.Replace(input, "");
        }

        public static string ToTitleCase(this string input)
        {
            return CultureInfo.InvariantCulture.TextInfo.ToTitleCase(input);
        }

        public static string RemoveDiacritics(this string text)
        {
            if (text == null)
            {
                return null;
            }

            var normalizedString = text.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString)
            {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);

                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }

        public static string HideHalfWithStars(this string input)
        {
            var length = input.Length;
            var charsToShow = (int)Math.Floor(length / 2m);

            var partiallyHidden = input.SubStringMaxLength(charsToShow);

            var chars = Enumerable.Range(0, length - charsToShow)
                                  .Select(x => '*')
                                  .ToArray();

            partiallyHidden += new string(chars);

            return partiallyHidden;
        }
    }
}
