﻿// <copyright file="HtmlExtensions.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.DotNet.Extensions.Classes;
using HtmlAgilityPack;
using System.Text.RegularExpressions;
using System.Web;

namespace CleoOne.Common.Core.DotNet.Extensions.Strings
{
    public static class HtmlExtensions
    {
        private static readonly Regex commentsRegex =
            new Regex("(?<comment>\\<!--.*?--\\>)", RegexOptions.Singleline | RegexOptions.IgnoreCase);

        public static string RemoveCommentsFromHtml(this string html)
        {
            var modifiedHtml = commentsRegex.ReplaceGroup(html, "comment", "");

            return modifiedHtml;
        }

        private static readonly Regex scriptsRegex =
            new Regex("(?<script><script.*?</script>)", RegexOptions.Singleline | RegexOptions.IgnoreCase);

        public static string RemoveScriptsFromHtml(this string html)
        {
            var modifiedHtml = scriptsRegex.ReplaceGroup(html, "script", "");

            return modifiedHtml;
        }

        public static string RemoveScriptsAndComments(this string html)
        {
            return html.RemoveScriptsFromHtml().RemoveCommentsFromHtml();
        }

        public static string GetHtmlInnerText(this string html)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(html);
            var htmlNode = doc.DocumentNode;

            return htmlNode.InnerText;
        }

        public static string UrlEncode(this string s)
        {
            return HttpUtility.UrlEncode(s);
        }
    }
}
