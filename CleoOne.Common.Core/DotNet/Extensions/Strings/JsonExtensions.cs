﻿// <copyright file="JsonExtensions.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.DotNet.JsonText;
using Microsoft.Extensions.Logging;
using System;

namespace CleoOne.Common.Core.DotNet.Extensions.Strings
{
    public static class JsonExtensions
    {
        /// <summary>
        /// Parses object to json as a string. Throws exception if parsing fails.
        /// </summary>
        /// <param name="input">Object to be serialized.</param>
        /// <param name="withIndents">Include white chars?</param>
        /// <returns>Input object parsed as json string. Can throw exceptions.</returns>
        public static string ToJson(this object input, bool withIndents = false, bool serializeNotLogged = true)
        {
            return JsonDotNetSerializer.Serialize(input, withIndents, serializeNotLogged);
        }

        /// <summary>
        /// Parses object to json as a string. Skips over single properties if they shall fail to convert or returns empty string 
        /// if whole method fails / throws exception.
        /// </summary>
        /// <param name="input">Object to be serialized.</param>
        /// <param name="withIndents">Include white chars?</param>
        /// <param name="logger">Microsoft logger.</param>
        /// <returns>Input object parsed as json string. Empty string if parsing fails. Can leave certain properties empty (not parsed if parsing fails).</returns>
        public static string ToJsonSafe(this object input, bool withIndents = false, ILogger logger = null, bool serializeNotLogged = true)
        {
            try
            {
                return JsonDotNetSerializer.Serialize(input, withIndents, serializeNotLogged);
            }
            catch (Exception e)
            {
                logger?.LogError(e, "Error during serialization");

                return string.Empty;
            }
        }

        public static T DeserializeJson<T>(this string json)
        {
            return JsonDotNetSerializer.DeserializeJson<T>(json);
        }

        public static bool TryDeserializeJson<T>(this string json, out T deserialized, ILogger logger = null)
        {
            try
            {
                deserialized = JsonDotNetSerializer.DeserializeJson<T>(json);

                return true;
            }
            catch (Exception e)
            {
                deserialized = default;
                logger?.LogError(e, "Error during deserialization.");

                return false;
            }
        }

        public static string ToJsonCamelCase(this object input, bool withIndents = true, bool serializeNotLogged = true)
        {
            return JsonDotNetSerializer.SerializeCamelCase(input, withIndents, serializeNotLogged);
        }
    }
}
