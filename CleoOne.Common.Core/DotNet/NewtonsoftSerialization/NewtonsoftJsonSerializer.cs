﻿// <copyright file="NewtonsoftJsonSerializer.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.DotNet.Classes;
using CleoOne.Common.Core.DotNet.Extensions.Strings;
using CleoOne.Common.Core.DotNet.Extensions.ValueTypes;
using Destructurama.Attributed;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Globalization;
using System.Linq;

namespace CleoOne.Common.Core.DotNet.NewtonsoftSerialization
{
    public static class NewtonsoftJsonSerializer
    {
        private static readonly JsonSerializerSettings _defaultSettings =
            new JsonSerializerSettings().ConfigureDefaultCleoSettings(serializeNotLogged: false);

        private static readonly JsonSerializerSettings _serializeNotLoggedSettings =
            new JsonSerializerSettings().ConfigureDefaultCleoSettings(serializeNotLogged: true);

        public static JsonSerializerSettings ConfigureDefaultCleoSettings(this JsonSerializerSettings settings,
                                                                          bool serializeNotLogged,
                                                                          bool camelCase = false)
        {
            settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

            if (serializeNotLogged)
            {
                if (camelCase)
                {
                    // Serialize all props, camel casing
                    settings.ContractResolver = new CamelCaseExceptDictionary();
                }
                else
                {
                    // Serialize all props, don't change property casing
                    // We can use the default serializer
                }
            }
            else
            {
                if (camelCase)
                {
                    // Don't serialize [NotLogged] properties, camel casing
                    settings.ContractResolver = new NotLoggedCamelCasedResolverContractResolver();
                }
                else
                {
                    // Don't serialize [NotLogged] properties, don't change casing
                    settings.ContractResolver = new NotLoggedResolverContractResolver();
                }
            }

            settings.Converters.Add(DecimalJsonConverter.Instance);
            settings.Converters.Add(StringEnumConverter.Instance);

            return settings;
        }

        /// <summary>
        /// Parses object to json as a string. Throws exception if parsing fails.
        /// </summary>
        /// <param name="input">Object to be serialized.</param>
        /// <param name="withIndents">Include white chars?</param>
        /// <returns>Input object parsed as json string. Can throw exceptions.</returns>
        public static string Serialize(object input, bool withIndents = false, bool serializeNotLogged = false)
        {
            JsonSerializerSettings settings = serializeNotLogged ? _serializeNotLoggedSettings : _defaultSettings;

            return JsonConvert.SerializeObject(input,
                                               withIndents ? Formatting.Indented : Formatting.None,
                                               settings);
        }

        /// <summary>
        /// Parses object to json as a string. Skips over single properties if they shall fail to convert or returns empty string 
        /// if whole method fails / throws exception.
        /// </summary>
        /// <param name="input">Object to be serialized.</param>
        /// <param name="withIndents">Include white chars?</param>
        /// <param name="logger">Microsoft logger.</param>
        /// <returns>Input object parsed as json string. Empty string if parsing fails. Can leave certain properties empty (not parsed if parsing fails).</returns>
        public static string ToJsonSafe(object input, bool withIndents = false, ILogger logger = null, bool serializeNotLogged = false)
        {
            try
            {
                var settings = new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                    Error = (o, args) =>
                    {
                        logger?.LogError(args.ErrorContext.Error,
                                         "JsonExtensions.ToJson caught an error for {PropertyType}, skipping this property and continue in parsing.",
                                         args.ErrorContext.Error?.GetType().Name);

                        args.ErrorContext.Handled = true;
                    },
                };

                if (!serializeNotLogged)
                {
                    settings.ContractResolver = new NotLoggedResolverContractResolver();
                }

                return JsonConvert.SerializeObject(input,
                                                   withIndents ? Formatting.Indented : Formatting.None,
                                                   settings);
            }
            catch (Exception e)
            {
                logger?.LogError(e, "JsonExtensions.ToJson caught an unexpected error.");

                return "";
            }
        }

        /// <summary>
        /// Deserializes the json string passed into object of the generic type used to call the method.       
        /// </summary>
        /// <typeparam name="T">Type the object will be deserialized into.</typeparam>
        /// <param name="json">Input json string to be deserialized into object.</param>
        /// <returns>Deserialized object.</returns>
        public static T DeserializeJson<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json, _serializeNotLoggedSettings);
        }

        /// <summary>
        /// Deserializes the json string passed into object of the generic type used to called the method. 
        /// Skips over single properties if they shall fail to deserialize or returns default object (null) if whole method fails / throws exception.
        /// </summary>
        /// <typeparam name="T">Type the object will be deserialized into.</typeparam>
        /// <param name="json">Input json string to be deserialized into object.</param>
        /// <param name="logger">Microsoft logger.</param>
        /// <returns>Deserialized object.</returns>
        public static T DeserializeJsonSafe<T>(string json, ILogger logger = null)
        {
            try
            {
                var settings = new JsonSerializerSettings
                {
                    Error = (o, args) =>
                    {
                        logger?.LogError(args.ErrorContext.Error,
                                         "JsonExtensions.DeserializeJson caught an error for {PropertyPath}, trying to continue in deserializing."
                                         + "\n\n"
                                         + "Full stacktrace:\n"
                                         + "{FullStacktrace}"
                                         + "\n\n"
                                         + "First 1000 chars of the json:\n"
                                         + "{BeginningOfTheProblematicJson}",
                                         args.ErrorContext.Path,
                                         Environment.StackTrace,
                                         json?.SubStringMaxLength(1000));

                        args.ErrorContext.Handled = true;
                    },
                }.ConfigureDefaultCleoSettings(serializeNotLogged: true);

                return JsonConvert.DeserializeObject<T>(json, settings);
            }
            catch (Exception e)
            {
                logger?.LogError(e,
                                 "JsonExtensions.DeserializeJson caught an unexpected error."
                                 + "\n\n"
                                 + "Full stacktrace:\n"
                                 + "{FullStacktrace}"
                                 + "\n\n"
                                 + "First 1000 chars of the json:\n"
                                 + "{BeginningOfTheProblematicJson}",
                                 Environment.StackTrace,
                                 json?.SubStringMaxLength(1000));

                return default;
            }
        }

        public static string SerializeCamelCase(object input,
                                                bool withIndents = true,
                                                bool serializeNotLogged = false)
        {
            var settings = new JsonSerializerSettings()
               .ConfigureDefaultCleoSettings(serializeNotLogged: serializeNotLogged, camelCase: true);

            return JsonConvert.SerializeObject(input, withIndents ? Formatting.Indented : Formatting.None, settings);
        }

        private class NotLoggedResolverContractResolver : DefaultContractResolver
        {
            protected override JsonObjectContract CreateObjectContract(Type objectType)
            {
                var contract = base.CreateObjectContract(objectType);

                foreach (var contractProperty in contract.Properties)
                {
                    if (contractProperty.AttributeProvider.GetAttributes(typeof(NotLoggedAttribute), inherit: true).Any())
                    {
                        contractProperty.Ignored = true;
                    }
                }

                return contract;
            }
        }

        private class CamelCaseExceptDictionary : CamelCasePropertyNamesContractResolver
        {
            protected override JsonDictionaryContract CreateDictionaryContract(Type objectType)
            {
                JsonDictionaryContract contract = base.CreateDictionaryContract(objectType);

                // Here we can set how the dictionary keys will be serialized:
                // Current behaviour ENUMS: Usd -> usd, USD -> usd
                // Current behaviour strings: Usd -> Usd, USD -> USD
                // contract.DictionaryKeyResolver = propertyName => propertyName;

                if (contract.DictionaryKeyType == typeof(string))
                {
                    contract.DictionaryKeyResolver = propertyName => propertyName;
                }

                return contract;
            }
        }

        private class NotLoggedCamelCasedResolverContractResolver : CamelCasePropertyNamesContractResolver
        {
            protected override JsonObjectContract CreateObjectContract(Type objectType)
            {
                var contract = base.CreateObjectContract(objectType);

                foreach (var contractProperty in contract.Properties)
                {
                    if (contractProperty.AttributeProvider.GetAttributes(typeof(NotLoggedAttribute), inherit: true).Any())
                    {
                        contractProperty.Ignored = true;
                    }
                }

                return contract;
            }

            protected override JsonDictionaryContract CreateDictionaryContract(Type objectType)
            {
                JsonDictionaryContract contract = base.CreateDictionaryContract(objectType);

                // Here we can set how the dictionary keys will be serialized:
                // TODO do we want to make difference between enum and strings?
                // Current behaviour: Usd -> usd, USD -> usd
                // contract.DictionaryKeyResolver = propertyName => propertyName;

                return contract;
            }
        }

        private class DecimalJsonConverter : JsonConverter
        {
            private static readonly Lazy<DecimalJsonConverter> _lazyInstance =
                new Lazy<DecimalJsonConverter>(() => new DecimalJsonConverter(), isThreadSafe: true);

            public static DecimalJsonConverter Instance => _lazyInstance.Value;

            public override bool CanConvert(Type objectType)
            {
                if (objectType == typeof(decimal))
                {
                    return true;
                }

                var underlyingType = Nullable.GetUnderlyingType(objectType);

                if (underlyingType == typeof(decimal))
                {
                    // it is decimal?
                    return true;
                }

                return false;
            }

            public override object ReadJson(JsonReader reader,
                                            Type objectType,
                                            object existingValue,
                                            JsonSerializer serializer)
            {
                var value = reader.Value;

                if (value == null)
                {
                    return null;
                }

                if (value is decimal decimalValue)
                {
                    return decimalValue;
                }

                var stringValue = value.ToString();

                if (decimal.TryParse(stringValue, NumberStyles.Number | NumberStyles.Float, CultureInfo.InvariantCulture, out decimalValue))
                {
                    return decimalValue;
                }

                throw new Exception($"Cannot convert value '{stringValue}' to decimal.");
            }

            public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
            {
                if (value == null)
                {
                    writer.WriteNull();

                    return;
                }

                var withoutTrailingZeros = ((decimal)value).RemoveTrailingZeros();

                var decimalString = withoutTrailingZeros
                   .ToString("0.###############################");
                //.ToString("G29");

                writer.WriteRawValue(decimalString);
            }
        }
    }
}
