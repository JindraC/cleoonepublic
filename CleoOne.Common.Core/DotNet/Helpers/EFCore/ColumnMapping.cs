﻿// <copyright file="ColumnMapping.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using Microsoft.EntityFrameworkCore.Metadata;
using System.Reflection;

namespace CleoOne.Common.Core.DotNet.Helpers.EFCore
{
    internal class ColumnMapping
    {
        public ColumnMapping()
        {
        }

        public string SqlColumnName { get; set; }

        public PropertyInfo EntityClrPropertyInfo { get; set; }

        public bool IsIdentityColumn { get; set; }

        public bool IsKey { get; set; }

        public IProperty EfColumnInfo { get; set; }
    }
}
