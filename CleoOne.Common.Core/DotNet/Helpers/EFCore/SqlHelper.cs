﻿// <copyright file="SqlHelper.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.Common.Logging;
using CleoOne.Common.Core.DotNet.Extensions;
using CleoOne.Common.Core.DotNet.Extensions.Classes;
using EFCore.BulkExtensions;
using FastMember;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.SqlServer.Storage.Internal;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CleoOne.Common.Core.DotNet.Helpers.EFCore
{
    public static class SqlHelper
    {
        private static readonly ILogger _logger = LoggerHelper.GetLogger(typeof(SqlHelper));

        public static int DefaultMaxSqlRetryCount => 3;

        public static TimeSpan DefaultSqlRetryDelay => TimeSpan.FromSeconds(10);

        private const string PRE_LOGIN_HANDSHAKE_ERROR_MESSAGE =
            "A connection was successfully established with the server, but then an error occurred during the pre-login handshake.";

        internal static ICollection<int> GetAdditionalErrorsConsideredTransient()
        {
            return null;
        }

        public static SqlBulkCopyOptions GetSqlBulkCopyOptions()
        {
            return SqlBulkCopyOptions.CheckConstraints | SqlBulkCopyOptions.FireTriggers;
        }

        internal static bool ShouldFailFastOnException(Exception ex)
        {
            // later we can add here some specific errors thrown from SqlBulkCopy
            // also we can add here reference equals for check if exception was already thrown
            // https://stackoverflow.com/a/16005643/3623593
            if (ex is SqlException sqlException)
            {
                if (sqlException.Number == -2      // Timeout
                    || sqlException.Number == 258  // Timeout with inner Win32 exception
                    || sqlException.Number == 11   // General network error
                    || sqlException.Number == 1205 // Deadlock
                    || sqlException.Message.Contains(PRE_LOGIN_HANDSHAKE_ERROR_MESSAGE))
                {
                    return false;
                }
            }

            #pragma warning disable EF1001 // Internal EF Core API usage.
            return !SqlServerTransientExceptionDetector.ShouldRetryOn(ex);
            #pragma warning restore EF1001 // Internal EF Core API usage.
        }

        internal static Task SqlWaitAndRetryAsync(Func<Task> operation, ILogger logger)
        {
            return SqlWaitAndRetryAsync(operation, trialsCount: DefaultMaxSqlRetryCount, logger: logger);
        }

        private static Task SqlWaitAndRetryAsync(Func<Task> operation, int trialsCount, ILogger logger)
        {
            return Retrials.RetryAndWaitAsync(operation,
                                              trialsCount: trialsCount,
                                              waitMilliseconds: (int)DefaultSqlRetryDelay.TotalMilliseconds,
                                              shouldFailFast: ShouldFailFastOnException,
                                              logger: logger);
        }

        internal static Task<TResult> SqlWaitAndRetryAsync<TResult>(Func<Task<TResult>> operation, ILogger logger)
        {
            return Retrials.RetryAndWaitAsync(operation,
                                              trialsCount: DefaultMaxSqlRetryCount,
                                              waitMilliseconds: (int)DefaultSqlRetryDelay.TotalMilliseconds,
                                              shouldFailFast: ShouldFailFastOnException,
                                              logger: logger);
        }

        private static void SqlWaitAndRetry(Action operation, int trialsCount)
        {
            Retrials.MoreTimes(operation,
                               trialsCount: trialsCount,
                               waitMilliseconds: (int)DefaultSqlRetryDelay.TotalMilliseconds,
                               shouldFailFast: ShouldFailFastOnException);
        }

        public static async Task BulkInsertByDataTableAsync<TDbItem>(IEnumerable<TDbItem> entities,
                                                                     string connectionString,
                                                                     string tableName,
                                                                     ILogger logger)
        {
            await SqlWaitAndRetryAsync(async () =>
                                       {
                                           using var dt = entities.ConvertToDataTable();

                                           if (dt.Rows.Count == 0)
                                           {
                                               return;
                                           }

                                           await using var connection = new SqlConnection(connectionString);

                                           await connection.OpenAsync();

                                           await using var transaction = connection.BeginTransaction();

                                           using var bulkCopy = new SqlBulkCopy(connection, GetSqlBulkCopyOptions(), transaction);

                                           bulkCopy.BulkCopyTimeout = 0;
                                           bulkCopy.BatchSize = 1000000;
                                           bulkCopy.DestinationTableName = tableName;

                                           try
                                           {
                                               bulkCopy.WriteToServer(dt);
                                               transaction.Commit();
                                           }
                                           catch (Exception ex)
                                           {
                                               logger?.LogWarning(ex, "Error inserting to database");

                                               logger?.LogInformation("Commit Exception Type: {0}", ex.GetType());
                                               logger?.LogInformation(" Message: {0}", ex.Message);

                                               try
                                               {
                                                   transaction.Rollback();
                                               }
                                               catch (Exception ex2)
                                               {
                                                   logger?.LogInformation("Rollback Exception Type: {0}", ex2.GetType());
                                                   logger?.LogInformation("Message: {0}", ex2.Message);
                                               }
                                           }
                                       },
                                       logger);
        }

        public static async Task<List<TDbItem>> LoadAllEntitiesAsync<TDbItem>(string connectionString, string tableName)
            where TDbItem : new()
        {
            var properties = typeof(TDbItem).GetEfCompatibleProperties();

            var propertyNames = properties.Select(property => property.Name).ToList();

            var propertyTypes = properties.Select(property => property.PropertyType.IsGenericType
                                                              && property.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>)
                                                                  ? Nullable.GetUnderlyingType(property.PropertyType)
                                                                  : property.PropertyType)
                                          .ToList();

            var entities = await SqlWaitAndRetryAsync(async () =>
                                                      {
                                                          var result = new List<TDbItem>();

                                                          await using var connection = new SqlConnection(connectionString);

                                                          await connection.OpenAsync();

                                                          await using var command = connection.CreateCommand();

                                                          var columns = string.Join(", ", propertyNames);

                                                          string query = $@"SELECT {columns} FROM {tableName}";

                                                          #pragma warning disable CA2100 // Review SQL queries for security vulnerabilities
                                                          command.CommandText = query;
                                                          #pragma warning restore CA2100 // Review SQL queries for security vulnerabilities

                                                          await using var reader = await command.ExecuteReaderAsync();

                                                          while (await reader.ReadAsync())
                                                          {
                                                              var row = new TDbItem();

                                                              var accessor = ObjectAccessor.Create(row);

                                                              for (int column = 0; column < properties.Length; column++)
                                                              {
                                                                  var property = properties[column];
                                                                  var value = reader.GetValue(column);

                                                                  var propertyType = propertyTypes[column];

                                                                  var convertedValue = value != null
                                                                                       && value.GetType() != typeof(DBNull)
                                                                                           ? Convert.ChangeType(
                                                                                               value,
                                                                                               propertyType,
                                                                                               CultureInfo.InvariantCulture)
                                                                                           : null;

                                                                  accessor[property.Name] = convertedValue;
                                                              }

                                                              result.Add(row);
                                                          }

                                                          return result;
                                                      },
                                                      logger: null);

            return entities;
        }

        /// <summary>
        /// see https://github.com/borisdj/EFCore.BulkExtensions look for BatchExtensions
        /// 
        /// Updates selected columns by where condition. UpdateExpression example: x => new User() { IsActive = true }
        ///
        /// Batch Extensions are made on IQueryable DbSet and can be used as in the following code segment.
        /// They are done as pure sql and no check is done whether some are prior loaded in memory and are being Tracked. (updateColumns is optional parameter in which PropertyNames added explicitly when we need update to it's default value)
        /// 
        /// see https://github.com/borisdj/EFCore.BulkExtensions look for BatchExtensions
        /// 
        /// UpdateExpression example: x => new User() { IsActive = true }
        /// You can even use x => new User() { Age = x.Age.AddDays(5) }
        ///
        /// For setting default value and other see: see https://github.com/borisdj/EFCore.BulkExtensions look for BatchExtensions
        /// <param name="filterExpression">Which entities to update</param>
        public static async Task<int> BatchUpdateAsync<TEntity>(this IQueryable<TEntity> query,
                                                                Expression<Func<TEntity, TEntity>> updateExpression)
            where TEntity : class
        {
            // no need for retrials as the Batch method would use the execution policy set on DbContext, also no need for transaction settings as it uses transaction from dbcontext
            return await IQueryableBatchExtensions.BatchUpdateAsync(query, updateExpression);
        }

        public static int BatchUpdate<TEntity>(this IQueryable<TEntity> query, Expression<Func<TEntity, TEntity>> updateExpression)
            where TEntity : class
        {
            // no need for retrials as the Batch method would use the execution policy set on DbContext, also no need for transaction settings as it uses transaction from dbcontext
            return IQueryableBatchExtensions.BatchUpdate(query, updateExpression);
        }

        public static async Task<int> BatchDeleteAsync<TEntity>(this IQueryable<TEntity> query)
            where TEntity : class
        {
            // no need for retrials as the Batch method would use the execution policy set on DbContext, also no need for transaction settings as it uses transaction from dbcontext
            return await IQueryableBatchExtensions.BatchDeleteAsync(query);
        }

        public static int BatchDelete<TEntity>(this IQueryable<TEntity> query)
            where TEntity : class
        {
            // no need for retrials as the Batch method would use the execution policy set on DbContext, also no need for transaction settings as it uses transaction from dbcontext
            return IQueryableBatchExtensions.BatchDelete(query);
        }

        public static void BulkInsert<TEntity>(this DbSet<TEntity> dbSet, IList<TEntity> items, bool setOutputIdentity = false)
            where TEntity : class
        {
            RunBulkOperation(dbSet, BulkOperationEnum.Insert, items, setOutputIdentity: setOutputIdentity);
        }

        public static async Task BulkInsertAsync<TEntity>(this DbSet<TEntity> dbSet, IList<TEntity> items, bool setOutputIdentity = false)
            where TEntity : class
        {
            await RunBulkOperationAsync(dbSet, BulkOperationEnum.Insert, items, setOutputIdentity: setOutputIdentity);
        }

        public static async Task BulkInsertKeepSourceIdentityAsync<TEntity>(this DbSet<TEntity> dbSet, IList<TEntity> items)
            where TEntity : class
        {
            await RunBulkOperationAsync(dbSet, BulkOperationEnum.InsertKeepSourceIdentity, items);
        }

        public static void BulkInsertKeepSourceIdentity<TEntity>(this DbSet<TEntity> dbSet, IList<TEntity> items)
            where TEntity : class
        {
            RunBulkOperation(dbSet, BulkOperationEnum.InsertKeepSourceIdentity, items);
        }

        public static void BulkUpdateByPrimaryKeys<TEntity>(this DbSet<TEntity> dbSet,
                                                            IList<TEntity> items,
                                                            IList<string> columnsToInclude = null,
                                                            IList<string> columnsToExclude = null)
            where TEntity : class
        {
            RunBulkOperation(dbSet,
                             BulkOperationEnum.Update,
                             items,
                             columnsToInclude: columnsToInclude,
                             columnsToExclude: columnsToExclude);
        }

        public static async Task BulkUpdateByPrimaryKeysAsync<TEntity>(this DbSet<TEntity> dbSet,
                                                                       IList<TEntity> items,
                                                                       IList<string> columnsToInclude = null,
                                                                       IList<string> columnsToExclude = null)
            where TEntity : class
        {
            await RunBulkOperationAsync(dbSet,
                                        BulkOperationEnum.Update,
                                        items,
                                        columnsToInclude: columnsToInclude,
                                        columnsToExclude: columnsToExclude);
        }

        public static void BulkUpdateBy<TEntity>(this DbSet<TEntity> dbSet,
                                                 IList<TEntity> items,
                                                 IList<string> updateByColumns,
                                                 IList<string> columnsToInclude = null,
                                                 IList<string> columnsToExclude = null)
            where TEntity : class
        {
            RunBulkOperation(dbSet,
                             BulkOperationEnum.Update,
                             items,
                             updateByColumns: updateByColumns,
                             columnsToInclude: columnsToInclude,
                             columnsToExclude: columnsToExclude);
        }

        public static async Task BulkUpdateByAsync<TEntity>(this DbSet<TEntity> dbSet,
                                                            IList<TEntity> items,
                                                            IList<string> updateByColumns,
                                                            IList<string> columnsToInclude = null,
                                                            IList<string> columnsToExclude = null)
            where TEntity : class
        {
            await RunBulkOperationAsync(dbSet,
                                        BulkOperationEnum.Update,
                                        items,
                                        updateByColumns: updateByColumns,
                                        columnsToInclude: columnsToInclude,
                                        columnsToExclude: columnsToExclude);
        }

        public static void BulkInsertOrUpdateByPrimaryKeys<TEntity>(this DbSet<TEntity> dbSet, IList<TEntity> items)
            where TEntity : class
        {
            RunBulkOperation(dbSet, BulkOperationEnum.InsertOrUpdate, items);
        }

        public static async Task BulkInsertOrUpdateByPrimaryKeysAsync<TEntity>(this DbSet<TEntity> dbSet, IList<TEntity> items)
            where TEntity : class
        {
            await RunBulkOperationAsync(dbSet, BulkOperationEnum.InsertOrUpdate, items);
        }

        public static void BulkInsertOrUpdateBy<TEntity>(this DbSet<TEntity> dbSet, IList<TEntity> items, IList<string> updateByColumns)
            where TEntity : class
        {
            RunBulkOperation(dbSet, BulkOperationEnum.InsertOrUpdate, items, updateByColumns: updateByColumns);
        }

        public static async Task BulkInsertOrUpdateByAsync<TEntity>(this DbSet<TEntity> dbSet,
                                                                    IList<TEntity> items,
                                                                    IList<string> updateByColumns)
            where TEntity : class
        {
            await RunBulkOperationAsync(dbSet, BulkOperationEnum.InsertOrUpdate, items, updateByColumns: updateByColumns);
        }

        private enum BulkOperationEnum
        {
            None,
            Insert,
            Update,
            InsertOrUpdate,
            InsertKeepSourceIdentity,
        }

        // disallow output identity for update - or then decide with frakon how to implement it
        // disallow include/exclude columns for insertOrUpdate

        // Insert - output identity / IDs = 0
        // InsertOrUpdateByPrimaryKeys - insertIDs = 0
        // InsertOrUpdateByColumns - updateBy cols, insertIds - 0
        // UpdateByPrimaryKeys - include/exclude cols
        // UpdateByOtherColumns - updateBy, include/exclude cols , update also PKs

        private class WrapperBulkConfig
        {
            public BulkOperationEnum BulkOperationEnum { get; set; }

            public bool SetOuputIdentity { get; set; }

            public bool UseInnerTransaction { get; set; }

            public DbContext DbContext { get; set; }

            public IDbContextTransaction Transaction { get; set; }

            public bool ShouldRollbackIdentity { get; set; }

            public TableTypeInformation TableInfo { get; set; }

            public bool PreserveInsertOrder { get; set; }

            public List<string> ColumnsToInclude { get; set; }

            public List<string> ColumnsToExclude { get; set; }

            public List<string> UpdateByColumns { get; set; }

            public bool IsUpdateBy
            {
                get
                {
                    return UpdateByColumns?.Count > 0;
                }
            }
        }

        private static async Task RunBulkOperationAsync<TEntity>(DbSet<TEntity> dbSet,
                                                                 BulkOperationEnum operationEnum,
                                                                 IList<TEntity> items,
                                                                 bool setOutputIdentity = false,
                                                                 IList<string> updateByColumns = null,
                                                                 IList<string> columnsToExclude = null,
                                                                 IList<string> columnsToInclude = null)
            where TEntity : class
        {
            var config = CreateWrappedBulkOperationConfig(dbSet,
                                                          operationEnum,
                                                          items,
                                                          setOutputIdentity,
                                                          updateByColumns,
                                                          columnsToExclude,
                                                          columnsToInclude);

            var bulkConfig = CreateDefaultBulkConfig(config);

            // we will not be retrying in case of outer transaction, this needs to be done at upper level
            // see https://dzfweb.gitbooks.io/microsoft-microservices-book/implement-resilient-applications/implement-resilient-entity-framework-core-sql-connections.html
            // see https://docs.microsoft.com/cs-cz/ef/core/miscellaneous/connection-resiliency#execution-strategies-and-transactions

            async Task operation()
            {
                if (config.UseInnerTransaction)
                {
                    config.Transaction = await config.DbContext.Database.BeginTransactionAsync();
                }

                CheckAndPrepareIdentityValues(config, items);

                try
                {
                    switch (operationEnum)
                    {
                        case BulkOperationEnum.None:
                            throw new NotImplementedException();
                        case BulkOperationEnum.Insert:
                            await config.DbContext.BulkInsertAsync(items, bulkConfig);

                            break;
                        case BulkOperationEnum.Update:
                            await config.DbContext.BulkUpdateAsync(items, bulkConfig);

                            break;
                        case BulkOperationEnum.InsertOrUpdate:
                            await config.DbContext.BulkInsertOrUpdateAsync(items, bulkConfig);

                            break;
                        case BulkOperationEnum.InsertKeepSourceIdentity:
                            await config.DbContext.BulkInsertAsync(items, bulkConfig);

                            break;
                        default:
                            throw new ArgumentOutOfRangeException(nameof(operationEnum), operationEnum, null);
                    }

                    await CommitAsync(config);
                }
                catch (Exception e)
                {
                    await RollbackAsync(config, items, e);

                    throw;
                }
                finally
                {
                    Finally(config);
                }
            }

            if (config.UseInnerTransaction)
            {
                var executionStrategy = config.DbContext.Database.CreateExecutionStrategy();
                await executionStrategy.ExecuteAsync(operation);
            }
            else
            {
                // outer transaction - we do not do retrials, as they must be handled by creator of the transaction needs to do that

                await operation();
            }
        }

        private static void RunBulkOperation<TEntity>(DbSet<TEntity> dbSet,
                                                      BulkOperationEnum operationEnum,
                                                      IList<TEntity> items,
                                                      bool setOutputIdentity = false,
                                                      IList<string> updateByColumns = null,
                                                      IList<string> columnsToExclude = null,
                                                      IList<string> columnsToInclude = null)
            where TEntity : class
        {
            var config = CreateWrappedBulkOperationConfig(dbSet,
                                                          operationEnum,
                                                          items,
                                                          setOutputIdentity,
                                                          updateByColumns,
                                                          columnsToExclude,
                                                          columnsToInclude);

            var bulkConfig = CreateDefaultBulkConfig(config);

            // we will not be retrying in case of outer transaction, this needs to be done at outer level (creator of the transaction needs to do that)
            // see https://dzfweb.gitbooks.io/microsoft-microservices-book/implement-resilient-applications/implement-resilient-entity-framework-core-sql-connections.html
            // see https://docs.microsoft.com/cs-cz/ef/core/miscellaneous/connection-resiliency#execution-strategies-and-transactions

            void operation()
            {
                if (config.UseInnerTransaction)
                {
                    config.Transaction = config.DbContext.Database.BeginTransaction();
                }

                CheckAndPrepareIdentityValues(config, items);

                try
                {
                    switch (operationEnum)
                    {
                        case BulkOperationEnum.None:
                            throw new NotImplementedException();
                        case BulkOperationEnum.Insert:
                            config.DbContext.BulkInsert(items, bulkConfig);

                            break;
                        case BulkOperationEnum.Update:
                            config.DbContext.BulkUpdate(items, bulkConfig);

                            break;
                        case BulkOperationEnum.InsertOrUpdate:
                            config.DbContext.BulkInsertOrUpdate(items, bulkConfig);

                            break;
                        case BulkOperationEnum.InsertKeepSourceIdentity:
                            config.DbContext.BulkInsert(items, bulkConfig);

                            break;
                        default:
                            throw new ArgumentOutOfRangeException(nameof(operationEnum), operationEnum, null);
                    }

                    Commit(config);
                }
                catch (Exception e)
                {
                    Rollback(config, items, e);

                    throw;
                }
                finally
                {
                    Finally(config);
                }
            }

            if (config.UseInnerTransaction)
            {
                var executionStrategy = config.DbContext.Database.CreateExecutionStrategy();
                executionStrategy.Execute(operation);
            }
            else
            {
                // outer transaction - we do not do retrials, as they must be handled by creator of the transaction needs to do that
                operation();
            }
        }

        private static void CheckAndPrepareIdentityValues<TEntity>(WrapperBulkConfig config, IList<TEntity> items)
        {
            if (config.SetOuputIdentity
                && config.BulkOperationEnum != BulkOperationEnum.Insert)
            {
                // this can be implemented, but it can be be quite complicated if the rollback of ids should work - see the RollBack and 
                throw new NotImplementedException("SetOutputIdentity not implemented for InsertOrUpdate. Only for Insert.");
            }

            PrepareForIdentityChanges<TEntity>(config, out var identityPropertyName, out var typeAccessor);
            var identityColumnPresent = identityPropertyName != null;

            if (!identityColumnPresent
                && config.SetOuputIdentity)
            {
                throw new InvalidOperationException("Cannot set output identity on table without identity column.");
            }

            if (config.TableInfo.ColumnMappings.Count(x => x.IsKey) != 1)
            {
                throw new NotImplementedException(
                    "Currently we do not support composite PrimaryKeys (or tables without PKs) - it can be implemented if needed. Be careful with auto numbering");
            }

            var primaryKey = config.TableInfo.ColumnMappings.Single(x => x.IsKey);

            if (config.SetOuputIdentity
                && identityColumnPresent
                && primaryKey.EntityClrPropertyInfo.Name != identityPropertyName)
            {
                throw new NotImplementedException(
                    "We do not support different Id form PK when setting output identity - because setting output identity is done trough sorting by PK."
                    + " We would need to make sure, that the list is already ordered. But it can be implemented.");
            }

            var distinctByColumnsSelector = new Func<TEntity, DynamicMultiKey>((TEntity item) =>
            {
                var keys = config.UpdateByColumns.Select(x => typeAccessor[item, x]).ToList();
                var key = new DynamicMultiKey(keys);

                return key;
            });

            var itemsIds = items.Select(x => (int)typeAccessor[x, identityPropertyName]);

            if (primaryKey.EntityClrPropertyInfo.Name == identityPropertyName)
            {
                // Case: Id == PK
                if (config.UseInnerTransaction
                    && config.SetOuputIdentity
                    && config.BulkOperationEnum == BulkOperationEnum.Insert)
                {
                    // This is the only case (inner transaction, set-output-identity and insert) when we will modify (and possibly do the rollback)
                    SetAndCheckValuesForInsertWithSetOutputIdentity();
                }
                else
                {
                    // User initiated (outer) transaction || SetOutputIdentity == false
                    // in any case, we will not be doing roll-backing identity changes, as we are not setting anything
                    config.ShouldRollbackIdentity = false;
                    CheckInvariantsFor_UserInitiatedTransactionOrNotSettingOutputIdentity();
                }
            }
            else
            {
                // Case: No Identity column || Id != PK
                config.ShouldRollbackIdentity = false;
                CheckInvariantsFor_IdMissing_Or_IdDifferentFromPK();
            }

            void SetAndCheckValuesForInsertWithSetOutputIdentity()
            {
                // set the values to be order able, so the lib can set the output identity correctly
                // if we would implement the InsertOrUpdate option for SetOutputIdentity - this loop needs to be changed
                int idOrder = int.MinValue;

                foreach (var entity in items)
                {
                    var identityValue = (int)typeAccessor[entity, identityPropertyName];

                    if (identityValue != default)
                    {
                        throw new InvalidOperationException(
                            $"You are trying insert item with identity column ({identityPropertyName}) value set to non-zero value ({identityValue})."
                            + $"\nMaybe you are trying to bulk insert items already tracked by EntityFramework."
                            + $"\nPlease insert only items with identity column ({identityPropertyName}) set to zero.");
                    }

                    typeAccessor[entity, identityPropertyName] = idOrder++;
                }

                config.ShouldRollbackIdentity = true;
            }

            void CheckInvariantsFor_UserInitiatedTransactionOrNotSettingOutputIdentity()
            {
                switch (config.BulkOperationEnum)
                {
                    case BulkOperationEnum.None: throw new NotImplementedException();
                    case BulkOperationEnum.Insert:
                        if (config.SetOuputIdentity)
                        {
                            // for Insert-SetIdentity: all ids negative && distinct
                            if (items.Count != itemsIds.Where(x => x < 0).Distinct().Count())
                            {
                                throw new InvalidOperationException(
                                    "For user initiated transaction for Insert-SetIdentity: All Ids expected negative && distinct. "
                                    + "User is responsible for retries and rollback of the ids.");
                            }
                        }
                        else
                        {
                            // for Insert-DoNotSetId: ids all zeros
                            if (items.Count != itemsIds.Count(x => x == 0))
                            {
                                throw new InvalidOperationException("For Insert-DoNotSetId: Ids should be all zeros");
                            }
                        }

                        break;
                    case BulkOperationEnum.Update:
                        if (config.IsUpdateBy)
                        {
                            // UpdateByCols: distinct update by columns
                            if (items.Count != items.DistinctBy(x => distinctByColumnsSelector(x)).Count())
                            {
                                throw new InvalidOperationException("For UpdateByColumns: distinct by update by columns expected.");
                            }
                        }
                        else
                        {
                            // UpdateByPK: distinct positive ids
                            if (items.Count != itemsIds.Where(x => x > 0).Distinct().Count())
                            {
                                throw new InvalidOperationException("For UpdateByPK: distinct positive ids expected");
                            }
                        }

                        break;
                    case BulkOperationEnum.InsertOrUpdate:
                        if (config.IsUpdateBy)
                        {
                            // InsertOrUpdateByCols: ids zeros and non zeros are distinct positive, update by columns are distinct
                            if (itemsIds.Count(x => x > 0) != itemsIds.Where(x => x > 0).Distinct().Count())
                            {
                                throw new InvalidOperationException("For UpdateByColumns: Distinct positive Ids expected");
                            }

                            if (items.Count != items.DistinctBy(x => distinctByColumnsSelector(x)).Count())
                            {
                                throw new InvalidOperationException("For UpdateByColumns: UpdateBy columns are expected to be distinct.");
                            }
                        }
                        else
                        {
                            // InsertOrUpdateByPK: ids zeros and non zeros distinct positive
                            if (itemsIds.Count(x => x != 0) != itemsIds.Where(x => x > 0).Distinct().Count())
                            {
                                throw new InvalidOperationException("For UpdateByPK: Distinct positive ids expected");
                            }
                        }

                        break;
                    case BulkOperationEnum.InsertKeepSourceIdentity:
                        if (config.SetOuputIdentity)
                        {
                            throw new InvalidOperationException(
                                "This is unsupported operation, since we are forcibly setting the identity by ourselves.");
                        }
                        else
                        {
                            // For identity insert we need the ids to be distinct and positive
                            if (items.Count != itemsIds.Distinct().Count(x => x > 0))
                            {
                                throw new InvalidOperationException("For Insert-KeepSourceIdentity: Ids should be distinct and positive.");
                            }
                        }

                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(BulkOperationEnum) + " is out ouf range");
                }
            }

            void CheckInvariantsFor_IdMissing_Or_IdDifferentFromPK()
            {
                switch (config.BulkOperationEnum)
                {
                    case BulkOperationEnum.None: throw new ArgumentOutOfRangeException(nameof(config.BulkOperationEnum));
                    case BulkOperationEnum.Insert:
                        // Insert: PK must be set distinct (not need to check - will be checked by db), Ids are zero OR does not exist
                        if (identityColumnPresent && itemsIds.Any(x => x != 0))
                        {
                            throw new InvalidOperationException(
                                "Ids must be default - this check is here to prevent mistake, when inserting again already tracked items by EF.");
                        }

                        break;
                    case BulkOperationEnum.Update:
                        // UpdateBy: UpdateByCols distinct, Ids are not checked
                        if (config.IsUpdateBy)
                        {
                            if (items.Count != items.DistinctBy(x => distinctByColumnsSelector(x)).Count())
                            {
                                throw new InvalidOperationException("For UpdateByColumns: distinct by update by columns expected.");
                            }
                        }
                        else
                        {
                            // UpdateByPk: PK must be set, distinct (not checked, as it will checked by db), Ids are not checked (as the values can come from previous select)
                        }

                        break;
                    case BulkOperationEnum.InsertOrUpdate:
                        if (config.IsUpdateBy)
                        {
                            // InsertOrUpdateBy: UpdateByCols distinct, Ids are zero
                            if (items.Count != items.DistinctBy(x => distinctByColumnsSelector(x)).Count())
                            {
                                throw new InvalidOperationException("For UpdateByColumns: distinct by update by columns expected.");
                            }
                        }
                        else
                        {
                            // InsertOrUpdateByPK: PKs should be distinct (we do not check, as this will be anyway checked by db) Ids are zeros
                        }

                        // Ids are zeros:  When we have identity (but Id != PK) then the Ids must be zeros - this is check against unwanted insert of already tracked items.
                        if (identityColumnPresent && itemsIds.Any(x => x != 0))
                        {
                            throw new InvalidOperationException(
                                "Ids must be default - this check is here to prevent mistake, when inserting again already tracked items by EF.");
                        }

                        break;
                    case BulkOperationEnum.InsertKeepSourceIdentity:
                        throw new InvalidOperationException("Cannot set source identity column, since there is none.");
                    default: throw new ArgumentOutOfRangeException();
                }
            }
        }

        private static WrapperBulkConfig CreateWrappedBulkOperationConfig<TEntity>(DbSet<TEntity> dbSet,
                                                                                   BulkOperationEnum operationEnum,
                                                                                   IList<TEntity> items,
                                                                                   bool setOutputIdentity,
                                                                                   IList<string> updateByColumns,
                                                                                   IList<string> columnsToExclude,
                                                                                   IList<string> columnsToInclude)
            where TEntity : class
        {
            var dbContext = GetDbContext(dbSet);

            var transaction = dbContext.Database.CurrentTransaction;

            var useInnerTransaction = transaction == null;

            var tableInfo = EFCoreContextDiscovery.GetTableInfo(dbContext, typeof(TEntity));

            // check if objects are distinct
            if (items.Count != items.Distinct(ReferenceEqualityComparer.Instance).Count())
            {
                throw new InvalidOperationException("Doing bulk operation with list with duplicate objects.");
            }

            // the preserve insert order matters only for set output identity true
            // for more see readme in github for EFCore.BulkExtensions library
            var preserveInsertOrder = setOutputIdentity;

            var config = new WrapperBulkConfig()
            {
                DbContext = dbContext,
                BulkOperationEnum = operationEnum,
                SetOuputIdentity = setOutputIdentity,
                UseInnerTransaction = useInnerTransaction,
                Transaction = transaction,
                PreserveInsertOrder = preserveInsertOrder,
                ShouldRollbackIdentity = false, // this property will be set later (only if we made successful changes to the objects)
                TableInfo = tableInfo,
            };

            if (updateByColumns != null)
            {
                if (updateByColumns.Count == 0)
                {
                    throw new InvalidOperationException("UpdateBy Columns specified, but is empty.");
                }

                // table can have at max one identity column
                var identityColumn = tableInfo.ColumnMappings.Where(x => x.IsIdentityColumn)
                                              .Select(x => x.EntityClrPropertyInfo.Name)
                                              .SingleOrDefault();

                var primaryKeys = tableInfo.ColumnMappings.Where(x => x.IsKey).Select(x => x.EntityClrPropertyInfo.Name).ToList();

                if (updateByColumns.Contains(identityColumn))
                {
                    throw new NotImplementedException(
                        "When updating by columns, you specified identity column - do you have Id column different from Primary Key columns? If so - this needs to be implemented.");
                }

                if (updateByColumns.Any(x => primaryKeys.Contains(x)))
                {
                    throw new InvalidOperationException(
                        "When updating by columns, do not specify primary columns - just use the BulkOperation.UpdateByPrimaryKeys");
                }

                config.UpdateByColumns = updateByColumns.ToList();

                // Exclusion of identity column is not need from EFCore.BulkExtension version >=2.4.6 (Identity column is excluded by default - since it cannot be updated anyway)

                // We do not want to ever update primary keys (or maybe in some weird situation - like fix of badly inserted child data - but this should be done manually)
                if (primaryKeys.Count > 1
                    || primaryKeys[0] != identityColumn) // identity column is automatically excluded
                {
                    if (primaryKeys.All(pk => columnsToExclude?.Contains(pk) == true))
                    {
                        // all primary keys are excluded - we can safely continue
                    }
                    else
                    {
                        // primary keys would be updated - which is something we probably do not want do ever, only as a fix of badly inserted child data - but this should be done manually)
                        throw new InvalidOperationException(
                            "Primary key would be updated. This is probably unwanted scenario. You should exclude them using the columnsToExclude property.");
                    }
                }
            }

            config.ColumnsToInclude = columnsToInclude?.ToList();
            config.ColumnsToExclude = columnsToExclude?.Distinct().ToList();

            return config;
        }

        private static BulkConfig CreateDefaultBulkConfig(WrapperBulkConfig config)
        {
            if (config.SetOuputIdentity
                && !config.PreserveInsertOrder)
            {
                throw new InvalidOperationException(
                    "SetOutputIdentity without PreserveInsertOrder means that the items in insert list would be overwritten with new objects");
            }

            var sqlBulkCopyOptions = GetSqlBulkCopyOptions();

            if (config.BulkOperationEnum == BulkOperationEnum.InsertKeepSourceIdentity)
            {
                sqlBulkCopyOptions = sqlBulkCopyOptions | SqlBulkCopyOptions.KeepIdentity;
            }

            var bulkConfig = new BulkConfig
            {
                SqlBulkCopyOptions = sqlBulkCopyOptions,
                BulkCopyTimeout = config.DbContext.Database.GetCommandTimeout(),
                CalculateStats = false,
                UseTempDB = true,
                EnableStreaming = false,
                BatchSize = int.MaxValue, // this can be changed if we experience problems with OOM or Timeouts
                SetOutputIdentity = config.SetOuputIdentity,
                PreserveInsertOrder = config.PreserveInsertOrder,
                TrackingEntities = false,
                PropertiesToInclude = config.ColumnsToInclude?.ToList(),
                PropertiesToExclude = config.ColumnsToExclude?.ToList(),
                UpdateByProperties = config.UpdateByColumns?.ToList(),
            };

            return bulkConfig;
        }

        private static void Commit(WrapperBulkConfig config)
        {
            if (config.UseInnerTransaction)
            {
                config.Transaction.Commit();
            }
        }

        private static async Task CommitAsync(WrapperBulkConfig config)
        {
            if (config.UseInnerTransaction)
            {
                await config.Transaction.CommitAsync();
            }
        }

        private static void Finally(WrapperBulkConfig config)
        {
            if (config.UseInnerTransaction)
            {
                config.Transaction?.Dispose();
            }
        }

        private static void Rollback<TEntity>(WrapperBulkConfig config, IList<TEntity> items, Exception originalException)
            where TEntity : class
        {
            if (config.ShouldRollbackIdentity)
            {
                PrepareForIdentityChanges<TEntity>(config, out var identityPropertyName, out var typeAccessor);

                int defaultInt = default;

                foreach (var entity in items)
                {
                    typeAccessor[entity, identityPropertyName] = defaultInt;
                }
            }

            if (config.UseInnerTransaction)
            {
                try
                {
                    config.Transaction.Rollback();
                }
                catch (Exception rollbackException)
                {
                    LogRollbackException(originalException, rollbackException);

                    throw originalException;
                }
            }
        }

        private static async Task RollbackAsync<TEntity>(WrapperBulkConfig config, IList<TEntity> items, Exception originalException)
            where TEntity : class
        {
            if (config.ShouldRollbackIdentity)
            {
                PrepareForIdentityChanges<TEntity>(config, out var identityPropertyName, out var typeAccessor);

                int defaultInt = default;

                foreach (var entity in items)
                {
                    typeAccessor[entity, identityPropertyName] = defaultInt;
                }
            }

            if (config.UseInnerTransaction)
            {
                try
                {
                    await config.Transaction.RollbackAsync();
                }
                catch (Exception rollbackException)
                {
                    LogRollbackException(originalException, rollbackException);

                    throw originalException;
                }
            }
        }

        private static void LogRollbackException(Exception originalException, Exception rollbackException)
        {
            _logger.LogWarning(rollbackException,
                               "Exception during rollback. Original exception was {OriginalException}",
                               originalException.ToFullExceptionString());
        }

        private static void PrepareForIdentityChanges<TEntity>(WrapperBulkConfig config,
                                                               out string identityPropertyName,
                                                               out TypeAccessor typeAccessor)
        {
            var identityColumn = config.TableInfo.ColumnMappings.SingleOrDefault(x => x.IsIdentityColumn);

            if (identityColumn == null)
            {
                identityPropertyName = null;
                typeAccessor = null;

                return;
            }

            typeAccessor = TypeAccessor.Create(typeof(TEntity));
            identityPropertyName = identityColumn.EntityClrPropertyInfo.Name;
            var identityPropertyType = identityColumn.EntityClrPropertyInfo.PropertyType;

            if (identityPropertyType != typeof(int))
            {
                throw new NotImplementedException("Other identity columns than int(Int32) are not supported yet.");
            }
        }

        private static DbContext GetDbContext<TEntity>(DbSet<TEntity> dbSet)
            where TEntity : class
        {
            var infrastructure = (IInfrastructure<IServiceProvider>)dbSet;
            var serviceProvider = infrastructure.Instance;
            var currentDbContext = (ICurrentDbContext)serviceProvider.GetService(typeof(ICurrentDbContext));

            return currentDbContext.Context;
        }
    }
}
