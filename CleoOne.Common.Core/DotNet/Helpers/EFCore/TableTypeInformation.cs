﻿// <copyright file="TableTypeInformation.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;

namespace CleoOne.Common.Core.DotNet.Helpers.EFCore
{
    internal class TableTypeInformation
    {
        public string SchemaName { get; internal set; }

        public string TableName { get; internal set; }

        public Type TableType { get; internal set; }

        public ColumnMapping[] ColumnMappings { get; internal set; }
    }
}
