﻿// <copyright file="EFCoreContextDiscovery.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using System;
using System.Collections.Generic;

namespace CleoOne.Common.Core.DotNet.Helpers.EFCore
{
    internal class EFCoreContextDiscovery
    {
        private static readonly object cacheLocker = new object();

        private static Dictionary<ValueTuple<Type, Type>, TableTypeInformation> TableInformationCache =
            new Dictionary<(Type, Type), TableTypeInformation>();

        public static TableTypeInformation GetTableInfo(DbContext dbContext, Type tableType)
        {
            var contextRealType = dbContext.GetType();

            lock (cacheLocker)
            {
                if (TableInformationCache.TryGetValue((contextRealType, tableType), out var cachedTableTypeInfo))
                {
                    return cachedTableTypeInfo;
                }
            }

            var entityType = dbContext.Model.FindEntityType(tableType);
            var tableName = entityType.GetTableName();

            var colMaps = new List<ColumnMapping>();

            foreach (var propertyType in entityType.GetProperties())
            {
                var columnName = propertyType.GetColumnName();
                var propInfo = propertyType.PropertyInfo;
                var isKey = propertyType.IsKey();
                var sqlColumnType = propertyType.GetColumnType();

                // we do not care about columns without mapped C# property
                if (propInfo == null)
                {
                    continue;
                }

                var columnMapping = new ColumnMapping()
                {
                    SqlColumnName = columnName,
                    EntityClrPropertyInfo = propInfo,
                    IsIdentityColumn = propertyType.GetValueGenerationStrategy() == SqlServerValueGenerationStrategy.IdentityColumn,
                    IsKey = isKey,
                    EfColumnInfo = propertyType,
                };

                colMaps.Add(columnMapping);
            }

            var schema = entityType.GetSchema() ?? "dbo";
            // When schema is null then it is default schema (for us it is 'dbo'):
            // https://github.com/aspnet/EntityFrameworkCore/issues/7725

            var tableInfo = new TableTypeInformation()
            {
                TableName = tableName,
                SchemaName = schema,
                TableType = tableType,
                ColumnMappings = colMaps.ToArray(),
            };

            // cache the result
            lock (cacheLocker)
            {
                TableInformationCache[(contextRealType, tableType)] = tableInfo;
            }

            return tableInfo;
        }
    }
}
