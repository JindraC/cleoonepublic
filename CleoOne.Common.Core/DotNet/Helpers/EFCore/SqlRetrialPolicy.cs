﻿// <copyright file="SqlRetrialPolicy.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CleoOne.Common.Core.DotNet.Helpers.EFCore
{
    public class SqlRetrialPolicy
    {
        private readonly ILogger _logger;

        private SqlRetrialPolicy(ILogger logger)
        {
            _logger = logger;
        }

        public static SqlRetrialPolicy GetSqlRetrialPolicy(ILogger logger)
        {
            return new SqlRetrialPolicy(logger);
        }

        public async Task SqlWaitAndRetryAsync(Func<Task> operation)
        {
            await SqlHelper.SqlWaitAndRetryAsync(operation, _logger);
        }

        public async Task<TResult> SqlWaitAndRetryAsync<TResult>(Func<Task<TResult>> operation)
        {
            return await SqlHelper.SqlWaitAndRetryAsync(operation, _logger);
        }
    }
}
