﻿// <copyright file="ClassTreeVisitor.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CleoOne.Common.Core.DotNet.Helpers
{
    public static class ClassTreeVisitor
    {
        private static readonly ConcurrentDictionary<Type, PropertyInfo[]> propertyInfoDictionary =
            new ConcurrentDictionary<Type, PropertyInfo[]>();

        private static PropertyInfo[] GetClassPropertyInfos(Type type)
        {
            var propertyInfos = propertyInfoDictionary.GetOrAdd(
                type,
                (theType) =>
                {
                    return theType.GetProperties(BindingFlags.Instance | BindingFlags.Public)
                                  .Where(propertyInfo => !propertyInfo.PropertyType.IsValueType
                                                         && propertyInfo.PropertyType != typeof(object)
                                                         && propertyInfo.PropertyType != typeof(string)
                                                         && propertyInfo.PropertyType != typeof(Type)
                                                         && propertyInfo.CanRead
                                                         && propertyInfo.GetMethod.GetParameters().Length == 0)
                                  .ToArray();
                });

            return propertyInfos;
        }

        public static void PerformOperationOnAllClassesInTree(object theObject,
                                                              Action<object> operationOnObject)
        {
            PerformOperationObjectTreeBase(theObject,
                                           operationOnObject,
                                           new HashSet<object>(ReferenceEqualityComparer.Instance));
        }

        private static void PerformOperationObjectTreeBase(object theObject,
                                                           Action<object> operationOnObject,
                                                           HashSet<object> alreadyProcessedHashSet)
        {
            if (theObject == null)
            {
                return;
            }

            var type = theObject.GetType();

            if (type.IsValueType
                || type == typeof(string)
                || type == typeof(object)
                || type == typeof(Type))
            {
                return;
            }

            if (alreadyProcessedHashSet.Contains(theObject))
            {
                return;
            }

            alreadyProcessedHashSet.Add(theObject);

            operationOnObject(theObject);

            if (theObject is IEnumerable)
            {
                foreach (var valueObj in ((IEnumerable)theObject).Cast<object>())
                {
                    PerformOperationObjectTreeBase(valueObj, operationOnObject, alreadyProcessedHashSet);
                }
            }

            var propertyInfos = GetClassPropertyInfos(type);

            foreach (var propertyInfo in propertyInfos)
            {
                var value = propertyInfo.GetValue(theObject);

                if (value != null)
                {
                    if (value is IEnumerable)
                    {
                        foreach (var valueObj in ((IEnumerable)value).Cast<object>())
                        {
                            PerformOperationObjectTreeBase(valueObj, operationOnObject, alreadyProcessedHashSet);
                        }
                    }

                    PerformOperationObjectTreeBase(value, operationOnObject, alreadyProcessedHashSet);
                }
            }
        }
    }
}
