﻿// <copyright file="EmbeddedResources.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.IO;
using System.Linq;
using System.Reflection;

namespace CleoOne.Common.Core.DotNet.Helpers
{
    public static class EmbeddedResources
    {
        public static string GetFromCurrentAssembly(string fileName)
        {
            var assembly = Assembly.GetCallingAssembly();

            var resourceName = assembly.GetManifestResourceNames().Single(name => name.EndsWith(fileName));

            using (var stream = assembly.GetManifestResourceStream(resourceName))
            {
                using (var reader = new StreamReader(stream))
                {
                    var result = reader.ReadToEnd();

                    return result;
                }
            }
        }
    }
}
