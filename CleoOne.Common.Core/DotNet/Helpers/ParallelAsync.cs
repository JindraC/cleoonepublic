﻿// <copyright file="ParallelAsync.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace CleoOne.Common.Core.DotNet.Helpers
{
    public static class ParallelAsync
    {
        /// <summary>
        /// Will return list of completed tasks (either successfully completed or faulted).
        /// </summary>
        public static async Task<List<Task<TResult>>> RunInParallel<TInput, TResult>(IEnumerable<TInput> items,
                                                                                     Func<TInput, Task<TResult>> asyncAction,
                                                                                     int maximumConcurrentTasks)
        {
            var wrapperTasks = new List<Task<TResult>>();

            using (var throttler = new SemaphoreSlim(initialCount: maximumConcurrentTasks))
            {
                foreach (var item in items)
                {
                    await throttler.WaitAsync();

                    var wrapperTask = Task.Run(async () =>
                    {
                        try
                        {
                            var tResult = await asyncAction(item);

                            return tResult;
                        }
                        finally
                        {
                            throttler.Release();
                        }
                    });

                    wrapperTasks.Add(wrapperTask);
                }

                try
                {
                    await Task.WhenAll(wrapperTasks);
                }
                catch { }

                return wrapperTasks;
            }
        }

        /// <summary>
        /// Will return list of completed tasks (either successfully completed or faulted).
        /// </summary>
        public static async Task<List<Task>> RunInParallel<TInput>(IEnumerable<TInput> items,
                                                                   Func<TInput, Task> asyncAction,
                                                                   int maximumConcurrentTasks)
        {
            var wrapperTasks = new List<Task>();

            using (var throttler = new SemaphoreSlim(initialCount: maximumConcurrentTasks))
            {
                foreach (var item in items)
                {
                    await throttler.WaitAsync();

                    var wrapperTask = Task.Run(async () =>
                    {
                        try
                        {
                            await asyncAction(item);
                        }
                        finally
                        {
                            throttler.Release();
                        }
                    });

                    wrapperTasks.Add(wrapperTask);
                }

                try
                {
                    await Task.WhenAll(wrapperTasks);
                }
                catch { }

                return wrapperTasks;
            }
        }
    }
}
