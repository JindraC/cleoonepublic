﻿// <copyright file="ReflectionHelper.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CleoOne.Common.Core.DotNet.Helpers
{
    public static class ReflectionHelper
    {
        public static T GetPropertyValue<T>(object source,
                                            string propertyName)
        {
            Assertion.ArgumentNotNull(source, nameof(source));
            Assertion.ArgumentNotNull(propertyName, nameof(propertyName));

            var type = source.GetType();
            var property = source.GetType().GetProperty(propertyName);

            Assertion.AssertNotNull(property, $"Property {propertyName} did not exist on type {type.Name}");

            return (T)property.GetValue(source);
        }

        public static object GetPropertyValue(object source,
                                              string propertyName) => GetPropertyValue<object>(source, propertyName);

        public static IEnumerable<T> GetValuesFromPropertiesOfAssignableToType<T>(object source)
        {
            Assertion.ArgumentNotNull(source, nameof(source));

            var properties = source.GetType()
                                   .GetProperties(BindingFlags.Instance | BindingFlags.Public)
                                   .Where(property =>
                                              property.PropertyType.IsAssignableTo(typeof(T))
                                              && property.CanRead);

            var result = properties.Select(p => GetPropertyValue<T>(source, p.Name));

            return result;
        }
    }
}
