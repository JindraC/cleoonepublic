﻿// <copyright file="Retrials.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CleoOne.Common.Core.DotNet.Helpers
{
    public static class Retrials
    {
        public const int DefaultTrialsCount = 3;
        public const int DefaultWaitMilliseconds = 10_000;
        private static Random rnd = new Random(123);

        public static void MoreTimes(Action action,
                                     int trialsCount = DefaultTrialsCount,
                                     int waitMilliseconds = DefaultWaitMilliseconds,
                                     ILogger logger = null,
                                     Func<Exception, bool> shouldFailFast = null,
                                     Func<int, TimeSpan> waitTimeProvider = null)
        {
            GetMoreTimes(() =>
                         {
                             action();

                             return true;
                         },
                         trialsCount,
                         waitMilliseconds,
                         logger,
                         shouldFailFast,
                         waitTimeProvider);
        }

        public static TResult GetMoreTimes<TResult>(Func<TResult> function,
                                                    int trialsCount = DefaultTrialsCount,
                                                    int waitMilliseconds = DefaultWaitMilliseconds,
                                                    ILogger logger = null,
                                                    Func<Exception, bool> shouldFailFast = null,
                                                    Func<int, TimeSpan> waitTimeProvider = null)
        {
            for (int i = 0; i < trialsCount; i++)
            {
                try
                {
                    return function();
                }
                catch (Exception e)
                {
                    if (i == trialsCount - 1
                        || shouldFailFast?.Invoke(e) == true)
                    {
                        throw;
                    }

                    logger?.LogWarning(e, "Retried {Retrial} call attempt threw an exception. Will be retrying again.", i + 1);

                    double wait = waitMilliseconds;

                    if (waitTimeProvider != null)
                    {
                        wait = waitTimeProvider(i).TotalMilliseconds;
                    }

                    System.Threading.Thread.Sleep((int)wait);
                }
            }

            throw new NotImplementedException($"Code should never end up here");
        }

        public static async Task<TResult> RetryAndWaitAsync<TResult>(Func<Task<TResult>> func,
                                                                     int trialsCount = DefaultTrialsCount,
                                                                     int waitMilliseconds = DefaultWaitMilliseconds,
                                                                     ILogger logger = null,
                                                                     Func<Exception, bool> shouldFailFast = null,
                                                                     Func<int, TimeSpan> waitTimeProvider = null)
        {
            for (int i = 0; i < trialsCount; i++)
            {
                try
                {
                    return await func();
                }
                catch (Exception e)
                {
                    if (i == trialsCount - 1
                        || shouldFailFast?.Invoke(e) == true)
                    {
                        throw;
                    }

                    logger?.LogWarning(e, "Retried {Retrial} call attempt threw an exception. Will be retrying again.", i + 1);

                    double wait = waitMilliseconds;

                    if (waitTimeProvider != null)
                    {
                        wait = waitTimeProvider(i).TotalMilliseconds;
                    }

                    await Task.Delay((int)wait);
                }
            }

            throw new NotImplementedException($"Code should never end up here");
        }

        public static Task RetryAndWaitAsync(Func<Task> func,
                                             int trialsCount = DefaultTrialsCount,
                                             int waitMilliseconds = DefaultWaitMilliseconds,
                                             ILogger logger = null,
                                             Func<Exception, bool> shouldFailFast = null,
                                             Func<int, TimeSpan> waitTimeProvider = null)
        {
            return RetryAndWaitAsync(async () =>
                                     {
                                         await func();

                                         return true;
                                     },
                                     trialsCount,
                                     waitMilliseconds,
                                     logger,
                                     shouldFailFast,
                                     waitTimeProvider);
        }

        internal static double ComputeRandomizedWait(int i, int waitMilliseconds)
        {
            var randomized = waitMilliseconds * (rnd.Next(50, 150) / 100d); // randomized 50-150% of initial value
            var wait = randomized;

            if (i != 0)
            {
                wait = randomized * (2 * (i + 1)); // retrial - so we add the exponential back-off
            }

            return wait;
        }
    }
}
