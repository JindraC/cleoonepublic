﻿// <copyright file="StackTraceDumper.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.Common.Logging;
using Microsoft.Diagnostics.Runtime;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CleoOne.Common.Core.DotNet.Helpers
{
    public static class StackTraceDumper
    {
        private static readonly ILogger _logger = LoggerHelper.GetLogger(typeof(StackTraceDumper));

        public static string GetStackTracesReport(int? processId = null,
                                                  HashSet<int> forManagedThreadIds = null)
        {
            try
            {
                var sb = new StringBuilder();

                var stackTraces = GetAllCurrentStackTracesWithThreadIds(processId, forManagedThreadIds);

                if (forManagedThreadIds == null)
                {
                    sb.AppendLine($"ALL CURRENT STACKTRACES BY COUNTS");
                    sb.AppendLine($"---------------------------------");
                }
                else
                {
                    sb.AppendLine($"CURRENT STACKTRACES OF THE SPECIFIED THREADS");
                    sb.AppendLine($"--------------------------------------------");
                }

                foreach (var keyValuePair in stackTraces)
                {
                    var stackTrace = keyValuePair.Key;
                    var count = keyValuePair.Value.Length;

                    sb.AppendLine($"Count: {count}:");
                    sb.AppendLine(stackTrace);
                }

                return sb.ToString();
            }
            catch (Exception e)
            {
                _logger.LogError($"Error while getting stacktraces: {e.Message}");

                return "<error_while_obtaining_stacktraces_report>";
            }
        }

        public static Dictionary<string, int[]> GetAllCurrentStackTracesWithThreadIds(int? processId = null,
                                                                                      HashSet<int> forManagedThreadIds = null)
        {
            return Retrials.GetMoreTimes(
                () =>
                {
                    var stackTraces = new List<(string stackTrace, int managedThreadId)>();

                    var pid = processId ?? Environment.ProcessId;

                    using (var dataTarget = DataTarget.CreateSnapshotAndAttach(pid))
                    {
                        foreach (var dataTargetClrVersion in dataTarget.ClrVersions)
                        {
                            var runtime = dataTargetClrVersion.CreateRuntime();

                            foreach (var clrThread in runtime.Threads)
                            {
                                if (forManagedThreadIds?.Contains(clrThread.ManagedThreadId) ?? true)
                                {
                                    stackTraces.Add((ClrStackTraceToString(clrThread.EnumerateStackTrace()), clrThread.ManagedThreadId));
                                }
                            }
                        }
                    }

                    var result = stackTraces.Where(pair => !string.IsNullOrWhiteSpace(pair.stackTrace))
                                            .GroupBy(pair => pair.stackTrace)
                                            .OrderByDescending(grp => grp.Count())
                                            .ToDictionary(grp => grp.Key, grp => grp.Select(pair => pair.managedThreadId).ToArray());

                    return result;
                },
                trialsCount: 3,
                waitMilliseconds: 100,
                _logger);
        }

        private static string ClrStackTraceToString(IEnumerable<ClrStackFrame> stackFrames)
        {
            var sb = new StringBuilder();

            foreach (var stackFrame in stackFrames)
            {
                sb.Append("      at ");
                sb.Append(stackFrame.Method?.Name ?? "<method_name_was_null>");
                sb.Append("()");
                sb.AppendLine();
            }

            var result = sb.ToString().TrimEnd();

            return result;
        }
    }
}
