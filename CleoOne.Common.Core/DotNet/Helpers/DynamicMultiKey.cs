﻿// <copyright file="DynamicMultiKey.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Collections.Generic;

namespace CleoOne.Common.Core.DotNet.Helpers
{
    public class DynamicMultiKey
    {
        private List<object> _keys;

        public DynamicMultiKey(List<object> keys)
        {
            _keys = keys;
        }

        public override bool Equals(object obj)
        {
            if (this == obj)
            {
                return true;
            }

            if (obj is DynamicMultiKey other)
            {
                if (_keys.Count != other._keys.Count)
                {
                    return false;
                }

                for (int i = 0; i < _keys.Count; i++)
                {
                    var a = _keys[i];
                    var b = other._keys[i];

                    if (a == null
                        && b == null)
                    {
                        // for both null key parts we consider them equal
                        continue;
                    }
                    else if (a == null
                             || b == null)
                    {
                        // one is nul one is not
                        return false;
                    }

                    //both not null

                    if (a.GetType() != b.GetType())
                    {
                        // different types
                        return false;
                    }

                    if (!a.Equals(b))
                    {
                        return false;
                    }
                }

                return true;
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            var hashCode = 17;

            foreach (var key in _keys)
            {
                if (key != null)
                {
                    hashCode = (hashCode * 17) + key.GetHashCode();
                }
            }

            return hashCode;
        }
    }
}
