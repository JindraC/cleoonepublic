﻿// <copyright file="Assertion.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.Exceptions;
using JetBrains.Annotations;
using System;
using System.Runtime.CompilerServices;

namespace CleoOne.Common.Core.DotNet.Helpers
{
    // inspired by - https://blog.jetbrains.com/dotnet/2017/09/07/null-checking-improvements-resharper-2017-2/
    public static class Assertion
    {
        [AssertionMethod]
        public static T AssertNotNull<T>([AssertionCondition(AssertionConditionType.IS_NOT_NULL)]
                                         T value,
                                         string message = null,
                                         [CallerMemberName] string memberName = null,
                                         [CallerFilePath] string filePath = null,
                                         [CallerLineNumber] int lineNumber = 0)
            where T : class
        {
            if (value != null)
            {
                return value;
            }

            if (string.IsNullOrEmpty(message))
            {
                throw new AssertionException($"Value of type '{typeof(T).Name}' is null when not null "
                                             + $"was expected at {memberName}, {filePath}:{lineNumber}");
            }
            else
            {
                throw new AssertionException($"Not null not satisfied: '{message}' at {memberName}, {filePath}:{lineNumber}");
            }
        }

        [AssertionMethod]
        public static T AssertNotNull<T>([AssertionCondition(AssertionConditionType.IS_NOT_NULL)] [CanBeNull]
                                         T? value,
                                         string message = null,
                                         [CallerMemberName] string memberName = null,
                                         [CallerFilePath] string filePath = null,
                                         [CallerLineNumber] int lineNumber = 0)
            where T : struct
        {
            if (value != null)
            {
                return value.Value;
            }

            if (string.IsNullOrEmpty(message))
            {
                throw new AssertionException($"Value of type '{typeof(T).Name}' is null when not null "
                                             + $"was expected at {memberName}, {filePath}:{lineNumber}");
            }
            else
            {
                throw new AssertionException($"Not null not satisfied: '{message}' at {memberName}, {filePath}:{lineNumber}");
            }
        }

        [AssertionMethod]
        public static T ArgumentNotNull<T>([AssertionCondition(AssertionConditionType.IS_NOT_NULL)]
                                           T value,
                                           [NotNull] string parameterName,
                                           [CallerLineNumber] int lineNumber = 0)
            where T : class
        {
            if (value != null)
            {
                return value;
            }

            throw new ArgumentNullException(parameterName,
                                            $"ArgumentNotNull Assertion failed for parameter '{parameterName}' "
                                            + $"of type '{typeof(T).Name}' (ln. {lineNumber})");
        }

        [AssertionMethod]
        public static T ArgumentNotNull<T>([AssertionCondition(AssertionConditionType.IS_NOT_NULL)] [CanBeNull]
                                           T? value,
                                           [NotNull] string parameterName,
                                           [CallerLineNumber] int lineNumber = 0)
            where T : struct
        {
            if (value != null)
            {
                return value.Value;
            }

            throw new ArgumentNullException(parameterName,
                                            $"ArgumentNotNull Assertion failed for parameter '{parameterName}' "
                                            + $"of type '{typeof(T).Name}' (ln. {lineNumber})");
        }

        [AssertionMethod]
        public static T ArgumentNotDefault<T>(T value,
                                              [NotNull] string parameterName,
                                              [CallerLineNumber] int lineNumber = 0)
            where T : struct
        {
            if (!value.Equals(default(T)))
            {
                return value;
            }

            throw new ArgumentException(parameterName,
                                        $"Argument not default assertion failed for parameter '{parameterName}' "
                                        + $"of type '{typeof(T).Name}' (ln. {lineNumber})");
        }

        [AssertionMethod]
        public static void AssertTrue([AssertionCondition(AssertionConditionType.IS_TRUE)]
                                      bool value,
                                      string message = null,
                                      [CallerMemberName] string memberName = null,
                                      [CallerFilePath] string filePath = null,
                                      [CallerLineNumber] int lineNumber = 0)
        {
            if (!value)
            {
                if (string.IsNullOrEmpty(message))
                {
                    throw new AssertionException($"True expected at {memberName}, {filePath}:{lineNumber}");
                }
                else
                {
                    throw new AssertionException($"AssertTrue: '{message}' at {memberName}, {filePath}:{lineNumber}");
                }
            }
        }

        [AssertionMethod]
        public static void AssertFalse([AssertionCondition(AssertionConditionType.IS_FALSE)]
                                       bool value,
                                       string message = null,
                                       [CallerMemberName] string memberName = null,
                                       [CallerFilePath] string filePath = null,
                                       [CallerLineNumber] int lineNumber = 0)
        {
            if (value)
            {
                if (string.IsNullOrEmpty(message))
                {
                    throw new AssertionException($"False expected at {memberName}, {filePath}:{lineNumber}");
                }
                else
                {
                    throw new AssertionException($"AssertTrue: '{message}' at {memberName}, {filePath}:{lineNumber}");
                }
            }
        }
    }
}
