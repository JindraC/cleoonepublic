﻿// <copyright file="ExpressionHelper.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.DotNet.Extensions.Classes;
using System;
using System.Linq.Expressions;

namespace CleoOne.Common.Core.DotNet.Helpers
{
    public static class ExpressionHelper
    {
        public static Expression<Func<T, bool>> GetTrueConditionExpression<T>()
        {
            return x => true;
        }

        public static Expression<Func<T, bool>> ToExpression<T>(Expression<Func<T, bool>> func)
        {
            return func;
        }

        public static Expression<Func<T, bool>> AndExpression<T>(Expression<Func<T, bool>> left, Expression<Func<T, bool>> right)
        {
            return left.And(right);
        }
    }
}
