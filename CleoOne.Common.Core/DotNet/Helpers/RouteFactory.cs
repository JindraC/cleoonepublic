﻿// <copyright file="RouteFactory.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.DotNet.Extensions;
using System.Collections.Generic;

namespace CleoOne.Common.Core.DotNet.Helpers
{
    public class RouteFactory
    {
        /// <summary>
        /// <b>api</b> in '<b>api</b><i>/v1/live-trader/get-price/Binance/BTCUSDT</i>'
        /// </summary>
        private string ApiRoot { get; set; }

        /// <summary>
        /// <b>v1</b> in '<i>api/</i><b>v1</b><i>/live-trader/get-price/Binance/BTCUSDT</i>'
        /// </summary>
        private string ApiVersion { get; set; }

        /// <summary>
        /// <b>live-trader</b> in '<i>api/v1/</i><b>live-trader</b><i>/get-price/Binance/BTCUSDT</i>'
        /// </summary>
        private string Route { get; set; }

        /// <summary>
        /// <b>get-price</b> in '<i>api/v1/live-trader/</i><b>get-price</b><i>/Binance/BTCUSDT</i>'
        /// </summary>
        private string Endpoint { get; set; }

        /// <summary>
        /// This is to create the route signature for example '<i>api/v1/live-trader/get-price/</i><b>{exchange}/{assetSymbol}</b>'
        /// </summary>
        private Queue<string> Parameters { get; set; }

        /// <summary>
        /// This is to set the route for request with filed data for example '<i>api/v1/live-trader/get-price/</i><b>Binance/BTCUSDT</b>'
        /// </summary>
        private Queue<string> ParameterValues { get; set; }

        /// <summary>
        /// Api root and version are set to default of <b>'api/v1'</b>
        /// </summary>
        public RouteFactory()
        {
            ApiRoot = "api";
            ApiVersion = "v1";
            Parameters = new Queue<string>();
            ParameterValues = new Queue<string>();
        }

        /// <summary>
        /// Creates default route with <b>api/v1</b> as root and version
        /// </summary>
        public static RouteFactory New()
        {
            return new RouteFactory();
        }

        /// <summary>
        /// <b>api</b> in '<b>api</b><i>/v1/live-trader/get-price/Binance/BTCUSDT</i>'
        /// </summary>
        internal RouteFactory SetApiRoot(string apiRoot)
        {
            ApiRoot = apiRoot;

            return this;
        }

        /// <summary>
        /// <b>v1</b> in '<i>api/</i><b>v1</b><i>/live-trader/get-price/Binance/BTCUSDT</i>'
        /// </summary>
        internal RouteFactory SetApiVersion(string apiVersion)
        {
            ApiVersion = apiVersion;

            return this;
        }

        /// <summary>
        /// <b>live-trader</b> in '<i>api/v1/</i><b>live-trader</b><i>/get-price/Binance/BTCUSDT</i>'
        /// </summary>
        internal RouteFactory SetRoute(string route)
        {
            Route = route;

            return this;
        }

        /// <summary>
        /// <b>get-price</b> in '<i>api/v1/live-trader/</i><b>get-price</b><i>/Binance/BTCUSDT</i>'
        /// </summary>
        internal RouteFactory SetEndpoint(string endpoint)
        {
            Endpoint = endpoint;

            return this;
        }

        /// <summary>
        /// This is to create the route signature for example '<i>api/v1/live-trader/get-price/</i><b>{exchange}/{assetSymbol}</b>'
        /// </summary>
        internal RouteFactory SetParameter(string nameOfParameter)
        {
            Parameters.Enqueue(nameOfParameter);

            return this;
        }

        /// <summary>
        /// This is to set the route for request with filed data for example '<i>api/v1/live-trader/get-price/</i><b>Binance/BTCUSDT</b>'
        /// </summary>
        internal RouteFactory AddParameterValue(string parameterValue)
        {
            ParameterValues.Enqueue(parameterValue);

            return this;
        }

        public override string ToString()
        {
            var root = ApiRoot.IsNullOrEmpty() ? string.Empty : $"{ApiRoot}";
            var version = ApiVersion.IsNullOrEmpty() ? string.Empty : $"/{ApiVersion}";
            var route = Route.IsNullOrEmpty() ? string.Empty : $"/{Route}";
            var endpoint = Endpoint.IsNullOrEmpty() ? string.Empty : $"/{Endpoint}";
            var parameters = Parameters.IsNullOrEmpty() ? string.Empty : "/{" + string.Join("}/{", Parameters) + "}";
            var parameterValues = ParameterValues.IsNullOrEmpty() ? string.Empty : "/" + string.Join("/", ParameterValues);

            // yes, bunch of things can be set here weirdly or wrong, and it can mess up lot of stuff in string
            // but considering that this is just helper developer extension and not production logic
            // its okay to leave it to unspecified behaviour when some developer will be using it in other then intended way
            return root + version + route + endpoint + parameters + parameterValues;
        }

        public static implicit operator string(RouteFactory route)
        {
            return route.ToString();
        }
    }
}
