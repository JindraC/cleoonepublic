﻿// <copyright file="Threads.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.Common.Helpers;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace CleoOne.Common.Core.DotNet.Helpers
{
    public static class Threads
    {
        public static bool IsInitialized(ref int isInitializedFlag)
        {
            if (isInitializedFlag == 1)
            {
                return true;
            }

            var wasInitialized = Interlocked.Exchange(ref isInitializedFlag, 1);

            return wasInitialized == 1;
        }

        public static Thread SafeRun(Action action, ILogger logger = null, bool backgroundThread = true)
        {
            var thread = new Thread(_ => RunInternal(action, logger));
            thread.IsBackground = backgroundThread;

            thread.Start();

            return thread;
        }

        private static void RunInternal(Action action, ILogger logger)
        {
            try
            {
                action();
            }
            catch (Exception e)
            {
                logger?.LogCritical(e, "Error in background thread execution.");
            }
        }

        public static async Task WaitTillXSecondsAfterAWholeMinute(int secondsAfterMinute, CancellationToken cancellationToken = default)
        {
            if (secondsAfterMinute >= 60)
            {
                throw new Exception($"Maximum number of seconds is 59. The input seconds was: {secondsAfterMinute}");
            }

            var utcNow = DateTime.UtcNow;

            var dueTime = utcNow.RoundDownForM1().AddSeconds(secondsAfterMinute);

            dueTime = dueTime <= utcNow ? dueTime.AddMinutes(1) : dueTime;

            await TaskHelper.DelayUntilRealTime(dueTime, cancellationToken);
        }

        private static DateTime RoundDownForM1(this DateTime time)
        {
            return new DateTime(time.Year,
                                time.Month,
                                time.Day,
                                time.Hour,
                                time.Minute,
                                0,
                                time.Kind);
        }
    }
}
