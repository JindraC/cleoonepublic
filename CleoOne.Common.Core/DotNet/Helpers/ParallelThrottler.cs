﻿// <copyright file="ParallelThrottler.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace CleoOne.Common.Core.DotNet.Helpers
{
    public sealed class ParallelThrottler : IAsyncDisposable
    {
        private readonly SemaphoreSlim _throttler;
        private readonly List<Task> _tasks;
        private bool _finishCalled;

        public ParallelThrottler(int maximumConcurrentTasks)
        {
            _throttler = new SemaphoreSlim(maximumConcurrentTasks);
            _tasks = new List<Task>();
        }

        public async Task Throttle(Func<Task> asyncMethod)
        {
            if (_finishCalled)
            {
                throw new InvalidOperationException("Trying to throttle on already disposed instance.");
            }

            await _throttler.WaitAsync();

            var t = Task.Run(async () =>
            {
                try
                {
                    var alreadyStartedTask = asyncMethod();
                    await alreadyStartedTask;
                }
                finally
                {
                    _throttler.Release();
                }
            });

            _tasks.Add(t);
        }

        public async Task<List<Task>> WhenAllSafely()
        {
            if (_finishCalled)
            {
                return _tasks;
            }

            _finishCalled = true;

            try
            {
                await Task.WhenAll(_tasks);
            }
            catch { }

            return _tasks;
        }

        public async ValueTask DisposeAsync()
        {
            if (_finishCalled)
            {
                return;
            }

            _finishCalled = true;

            try
            {
                await Task.WhenAll(_tasks);
            }
            catch { }

            _throttler?.Dispose();
        }
    }

    public sealed class ParallelThrottler<TResult> : IAsyncDisposable
    {
        private readonly SemaphoreSlim _throttler;
        private readonly List<Task<TResult>> _tasks;
        private bool _finishCalled;

        public ParallelThrottler(int maximumConcurrentTasks)
        {
            _throttler = new SemaphoreSlim(maximumConcurrentTasks);
            _tasks = new List<Task<TResult>>();
        }

        public async Task Throttle(Func<Task<TResult>> asyncMethod)
        {
            if (_finishCalled)
            {
                throw new InvalidOperationException("Trying to throttle on already disposed instance.");
            }

            await _throttler.WaitAsync();

            var t = Task.Run(async () =>
            {
                try
                {
                    var alreadyStartedTask = asyncMethod();

                    return await alreadyStartedTask;
                }
                finally
                {
                    _throttler.Release();
                }
            });

            _tasks.Add(t);
        }

        public async Task<List<Task<TResult>>> WhenAllSafely()
        {
            _finishCalled = true;

            try
            {
                await Task.WhenAll(_tasks);
            }
            catch { }

            return _tasks;
        }

        public async ValueTask DisposeAsync()
        {
            if (_finishCalled)
            {
                return;
            }

            _finishCalled = true;

            try
            {
                await Task.WhenAll(_tasks);
            }
            catch { }

            _throttler?.Dispose();
        }
    }
}
