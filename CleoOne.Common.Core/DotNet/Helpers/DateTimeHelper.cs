﻿// <copyright file="DateTimeHelper.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Linq;

namespace CleoOne.Common.Core.DotNet.Helpers
{
    public static class DateTimeHelper
    {
        public static DateTime Max(params DateTime[] dates) => dates.Max();

        public static DateTime Min(params DateTime[] dates) => dates.Min();
    }
}
