﻿// <copyright file="TryCatches.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using Microsoft.Extensions.Logging;
using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace CleoOne.Common.Core.DotNet.Helpers
{
    public static class TryCatches
    {
        public static TResult GetSafelyOrDefault<TResult>(Func<TResult> function,
                                                          ILogger logger = null,
                                                          [CallerMemberName] string callerMethodName = "",
                                                          [CallerFilePath] string callerFilePath = "",
                                                          [CallerLineNumber] int callerLineNumber = 0)
        {
            return GetSafelyOrDefaultPrivate(function, default(TResult), logger, callerMethodName, callerFilePath, callerLineNumber);
        }

        public static TResult GetSafelyOrDefault<TResult>(Func<TResult> function,
                                                          TResult defaultValue,
                                                          ILogger logger = null,
                                                          [CallerMemberName] string callerMethodName = "",
                                                          [CallerFilePath] string callerFilePath = "",
                                                          [CallerLineNumber] int callerLineNumber = 0)
        {
            return GetSafelyOrDefaultPrivate(function, defaultValue, logger, callerMethodName, callerFilePath, callerLineNumber);
        }

        public static TResult GetSafelyOrDefaultPrivate<TResult>(Func<TResult> function,
                                                                 TResult defaultValue,
                                                                 ILogger logger,
                                                                 string callerMethodName,
                                                                 string callerFilePath,
                                                                 int callerLineNumber)
        {
            try
            {
                return function();
            }
            catch (Exception e)
            {
                logger?.LogError(e,
                                 $"Error: {e.Message}\r\n\r\n   in method {callerMethodName}() at {callerFilePath}:line {callerLineNumber}");

                return defaultValue;
            }
        }

        public static void TryCatch(Action action,
                                    ILogger logger = null,
                                    [CallerMemberName] string callerMethodName = "",
                                    [CallerFilePath] string callerFilePath = "",
                                    [CallerLineNumber] int callerLineNumber = 0)
        {
            try
            {
                action();
            }
            catch (Exception e)
            {
                logger?.LogError(e,
                                 $"Error: {e.Message}\r\n\r\n   in method {callerMethodName}() at {callerFilePath}:line {callerLineNumber}");
            }
        }

        public static async Task TryCatchAsync(Func<Task> action,
                                               ILogger logger = null,
                                               [CallerMemberName] string callerMethodName = "",
                                               [CallerFilePath] string callerFilePath = "",
                                               [CallerLineNumber] int callerLineNumber = 0)
        {
            try
            {
                await action();
            }
            catch (Exception e)
            {
                logger?.LogError(e,
                                 $"Error: {e.Message}\r\n\r\n   in method {callerMethodName}() at {callerFilePath}:line {callerLineNumber}");
            }
        }
    }
}
