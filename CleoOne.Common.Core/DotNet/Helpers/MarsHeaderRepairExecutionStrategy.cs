﻿// <copyright file="MarsHeaderRepairExecutionStrategy.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.DotNet.Helpers.EFCore;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Data.Common;

namespace CleoOne.Common.Core.DotNet.Helpers
{
    // This class was incorporated because of issue: https://cleofinance.atlassian.net/browse/CD-1446
    // And was copied from: https://github.com/dotnet/SqlClient/issues/85#issuecomment-469432914
    public class MarsHeaderRepairExecutionStrategy : SqlServerRetryingExecutionStrategy
    {
        public MarsHeaderRepairExecutionStrategy(ExecutionStrategyDependencies dependencies)
            : base(dependencies,
                   SqlHelper.DefaultMaxSqlRetryCount,
                   SqlHelper.DefaultSqlRetryDelay,
                   SqlHelper.GetAdditionalErrorsConsideredTransient())
        {
        }

        protected override bool ShouldRetryOn(Exception exception)
        {
            if (!SqlHelper.ShouldFailFastOnException(exception)
                || base.ShouldRetryOn(exception))
            {
                return true;
            }

            if (exception is SqlException sqlException)
            {
                if (SqlRecoveryAttemptor.ShouldTryRecover(exception, ExceptionsEncountered.Count)
                    && Dependencies?.CurrentContext != null)
                {
                    var context = Dependencies.CurrentContext.Context;

                    if (context != null)
                    {
                        if (context.Database.GetDbConnection() is SqlConnection sqlConn)
                        {
                            SqlRecoveryAttemptor.TryRecover(sqlConn, ExceptionsEncountered.Count);

                            return true;
                        }
                    }
                }
            }

            return false;
        }

        private class SqlRecoveryAttemptor
        {
            public const int RetryCount = 4;

            public static bool ShouldTryRecover(Exception ex, int retryAttemptsCount)
            {
                return retryAttemptsCount < RetryCount && IsMarsError(ex);
            }

            public static bool IsMarsError(Exception ex)
            {
                return ex.Message.Contains("MARS TDS header");
            }

            public static void TryRecover(SqlConnection sqlConn, int retryAttemptsCount)
            {
                if (retryAttemptsCount > 2)
                {
                    System.Threading.Thread.Sleep(1500);
                }

                if (sqlConn.State != System.Data.ConnectionState.Closed)
                {
                    TryClose(sqlConn);
                    TryClearPool(sqlConn);
                    TryOpen(sqlConn);
                }
                else
                {
                    TryOpen(sqlConn);
                    TryClose(sqlConn);
                    TryClearPool(sqlConn);
                    TryOpen(sqlConn);
                    TryClose(sqlConn);
                }
            }

            private static void TryClearPool(SqlConnection conn)
            {
                try
                {
                    SqlConnection.ClearPool(conn);
                }
                catch (Exception)
                {
                }
            }

            private static void TryClose(DbConnection conn)
            {
                try
                {
                    conn.Close();
                }
                catch (Exception)
                {
                }
            }

            private static void TryOpen(DbConnection conn)
            {
                try
                {
                    conn.Open();
                }
                catch (Exception)
                {
                }
            }
        }
    }
}
