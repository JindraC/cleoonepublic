﻿// <copyright file="StringEnumConverter.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;

namespace CleoOne.Common.Core.DotNet.Classes
{
    public class StringEnumConverter : JsonConverter
    {
        private static readonly Lazy<StringEnumConverter> _lazyInstance =
            new Lazy<StringEnumConverter>(() => new StringEnumConverter(), isThreadSafe: true);

        public static StringEnumConverter Instance => _lazyInstance.Value;

        private readonly ConcurrentDictionary<Type, Type> _typesToEnumTypesDictionary = new ConcurrentDictionary<Type, Type>();

        public override bool CanRead => true;

        public override bool CanWrite => true;

        public override void WriteJson(JsonWriter writer,
                                       object value,
                                       JsonSerializer serializer)
        {
            if (value == null)
            {
                writer.WriteRawValue("null");
            }
            else
            {
                writer.WriteRawValue("\"" + value + "\"");
            }
        }

        public override object ReadJson(JsonReader reader,
                                        Type objectType,
                                        object existingValue,
                                        JsonSerializer serializer)
        {
            var enumType = GetEnumType(objectType);

            if (enumType == null)
            {
                throw new Exception($"Type {objectType} is not an enum type.");
            }

            var valueAsString = reader.Value?.ToString();

            if (valueAsString == null)
            {
                return null;
            }

            if (Enum.TryParse(enumType, valueAsString, out var parsedEnumValue))
            {
                return parsedEnumValue;
            }

            if (int.TryParse(valueAsString, out var parsedIntegerValue))
            {
                return Enum.ToObject(enumType, parsedIntegerValue);
            }

            if (Enum.TryParse(enumType, valueAsString, ignoreCase: true, out parsedEnumValue))
            {
                return parsedEnumValue;
            }

            throw new Exception($"Cannot parse value '{valueAsString}' into enum type: '{enumType.Name}'");
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType.IsEnum || GetEnumType(objectType) != null;
        }

        private Type GetEnumType(Type fromType)
        {
            Type RetrieveEnumType(Type type)
            {
                if (type.IsEnum)
                {
                    return type;
                }
                else if (type.IsGenericType
                         && type.GetGenericTypeDefinition() == typeof(Nullable<>)
                         && Nullable.GetUnderlyingType(type).IsEnum)
                {
                    return Nullable.GetUnderlyingType(type);
                }
                else
                {
                    return null;
                }
            }

            return _typesToEnumTypesDictionary.GetOrAdd(fromType, RetrieveEnumType);
        }
    }
}
