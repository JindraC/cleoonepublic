﻿// <copyright file="EmptyConfiguration.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;

namespace CleoOne.Common.Core.DotNet.Classes
{
    public class EmptyConfiguration : IConfiguration
    {
        private static readonly Lazy<EmptyConfiguration> _lazyInstance =
            new Lazy<EmptyConfiguration>(() => new EmptyConfiguration(), isThreadSafe: true);

        public static EmptyConfiguration Instance => _lazyInstance.Value;

        public IConfigurationSection GetSection(string key)
        {
            return null;
        }

        public IEnumerable<IConfigurationSection> GetChildren()
        {
            return null;
        }

        public IChangeToken GetReloadToken()
        {
            return null;
        }

        public string this[string key] { get => null; set => throw new NotImplementedException(); }
    }
}
