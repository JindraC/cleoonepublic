﻿// <copyright file="DeepObjectComparer.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.DotNet.Collections;
using CleoOne.Common.Core.DotNet.Extensions.Classes;
using JetBrains.Annotations;
using Microsoft.Extensions.Logging;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Linq.Expressions;
using System.Numerics;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace CleoOne.Common.Core.DotNet.Comparers
{
    public class DeepObjectComparer
    {
        public static void AssertAllMembersAreSame<T>(T x, T y, Action<DeepObjectComparerConfig> comparerConfiguration = null)
        {
            var comparer = new DeepObjectComparer(false);
            var configuration = new DeepObjectComparerConfig(comparer);

            comparerConfiguration?.Invoke(configuration);

            comparer.ThrowExceptionOnFalse = false;

            var result = comparer.CompareAllMembers(x, y);

            if (!result.AreSame)
            {
                throw new DeepObjectCompareException(string.Join('\n',
                                                                 result.Diffs
                                                                       .Where(diff =>
                                                                        {
                                                                            var memberInfo = diff.Path.Last().Member;

                                                                            // We do not want to report backing fields, when comparing also properties.
                                                                            if (memberInfo is FieldInfo fieldInfo
                                                                                && fieldInfo.Name.StartsWith("<"))
                                                                            {
                                                                                return false;
                                                                            }

                                                                            return true;
                                                                        })
                                                                       .Select(diff => diff.Message)),
                                                     result);
            }
        }

        public bool ThrowExceptionOnFalse { get; private set; }

        private List<DeepObjectComparerReport.Diff> _diffs;
        private bool _reportMode;

        private DeepObjectComparerReport GetCompareReport(object x, object y, ComparisonOption option)
        {
            _diffs = new List<DeepObjectComparerReport.Diff>();
            _reportMode = true;

            try
            {
                var areSame = AreObjectsSame(x, y, option, pathForDebug: null);

                var result = new DeepObjectComparerReport(areSame, _diffs);

                return result;
            }
            finally
            {
                // clean after run
                _reportMode = false;
                _diffs = null;
            }
        }

        public DeepObjectComparerReport CompareProperties(object x, object y)
        {
            return GetCompareReport(x, y, CreateComparisonOptionForProperties());
        }

        public DeepObjectComparerReport CompareFields(object x, object y)
        {
            return GetCompareReport(x, y, CreateComparisonOptionForFields());
        }

        public DeepObjectComparerReport CompareAllMembers(object x, object y)
        {
            var propertiesResult = CompareProperties(x, y);
            var fieldsResult = CompareFields(x, y);

            return new DeepObjectComparerReport(propertiesResult.AreSame && fieldsResult.AreSame,
                                                propertiesResult.Diffs.Concat(fieldsResult.Diffs).ToList());
        }

        private readonly List<OmittedType> _omittedTypes = new List<OmittedType>();
        private readonly List<OmittedMember> _omittedMembers = new List<OmittedMember>();

        private static readonly HashSet<Type> _directEqualTypes = new HashSet<Type>
        {
            typeof(bool),
            typeof(byte),
            typeof(char),
            typeof(decimal),
            typeof(double),
            typeof(float),
            typeof(int),
            typeof(long),
            typeof(sbyte),
            typeof(short),
            typeof(uint),
            typeof(ulong),
            typeof(ushort),
            typeof(Guid),
            typeof(BigInteger),
            typeof(DateTime),
            typeof(DateTimeOffset),
            typeof(TimeSpan),
            typeof(string),
        };

        private static bool IsTypeToValueSemanticsEquality(Type t)
        {
            return t.IsPrimitive || t.IsEnum || _directEqualTypes.Contains(t);
        }

        public class DeepObjectComparerConfig
        {
            private readonly DeepObjectComparer _comparer;

            internal DeepObjectComparerConfig(DeepObjectComparer comparer)
            {
                _comparer = comparer;
            }

            public void AddOmittedTypes(IEnumerable<Type> omittedTypes,
                                        MemberComparisonFlags memberComparisonFlags,
                                        bool compareAssignableTo)
            {
                foreach (var type in omittedTypes)
                {
                    AddOmittedType(type, memberComparisonFlags, compareAssignableTo);
                }
            }

            public void AddOmittedType(Type type, MemberComparisonFlags memberComparisonFlags, bool compareAssignableTo) =>
                _comparer.AddOmittedType(type, memberComparisonFlags, compareAssignableTo);

            public void AddOmittedProperty<TSource>(Expression<Func<TSource, object>> selector,
                                                    MemberOmittingFlags memberOmittingFlags = MemberOmittingFlags.OnSameType) =>
                _comparer.AddOmittedProperty(selector, memberOmittingFlags);

            public void AddOmittedBackingField(Type type, string propertyName, MemberOmittingFlags memberOmittingFlags) =>
                _comparer.AddOmittedBackingField(type, propertyName, memberOmittingFlags);

            public void AddOmittedField<TSource>(string fieldName,
                                                 MemberOmittingFlags memberOmittingFlags = MemberOmittingFlags.OnSameType) =>
                _comparer.AddOmittedField<TSource>(fieldName, memberOmittingFlags);
        }

        public DeepObjectComparer(bool throwOnFalse = false, Action<DeepObjectComparerConfig> config = null)
        {
            ThrowExceptionOnFalse = throwOnFalse;

            AddOmittedTypes(omittedTypes: new[] { typeof(Exception), typeof(ILogger), },
                            memberComparisonFlags: MemberComparisonFlags.ByMemberTypeAndObjectType,
                            compareAssignableTo: true);

            var configuration = new DeepObjectComparerConfig(this);
            config?.Invoke(configuration);
        }

        #region OmittingFromComparison

        private void AddOmittedTypes(IEnumerable<Type> omittedTypes,
                                     MemberComparisonFlags memberComparisonFlags,
                                     bool compareAssignableTo)
        {
            foreach (var type in omittedTypes)
            {
                AddOmittedType(type, memberComparisonFlags, compareAssignableTo);
            }
        }

        private void AddOmittedType(Type type, MemberComparisonFlags memberComparisonFlags, bool compareAssignableTo)
        {
            _omittedTypes.Add(new OmittedType
            {
                CompareAssignableTo = compareAssignableTo,
                Type = type,
                MemberComparison = memberComparisonFlags,
            });
        }

        private void AddOmittedProperty<TSource>(Expression<Func<TSource, object>> selector,
                                                 MemberOmittingFlags memberOmittingFlags = MemberOmittingFlags.OnSameType)
        {
            var propertyName = selector.GetMemberName();
            var type = typeof(TSource);

            ThrowOnUnsupportedAddingOmittedProperty(type, memberOmittingFlags);

            _omittedMembers.Add(new OmittedMember
            {
                ComparisonMode = ComparisonModeEnum.Properties,
                MemberName = propertyName,
                MemberOmittingFlags = memberOmittingFlags,
                Type = type,
            });

            AddOmittedBackingField(type, propertyName, memberOmittingFlags);
        }

        private void AddOmittedBackingField(Type type, string propertyName, MemberOmittingFlags memberOmittingFlags)
        {
            var actualType = type;
            var backingFieldName = $"<{propertyName}>k__BackingField";

            while ((actualType != null)
                   && (actualType != typeof(object)))
            {
                var backingField = actualType.GetFields(BindingFlags.GetField | BindingFlags.NonPublic | BindingFlags.Instance)
                                             .FirstOrDefault(f => (f.Name == backingFieldName)
                                                                  && f.GetCustomAttributes(typeof(CompilerGeneratedAttribute),
                                                                                           true)
                                                                      .Any());

                if (backingField != null)
                {
                    _omittedMembers.Add(new OmittedMember
                    {
                        ComparisonMode = ComparisonModeEnum.Fields,
                        MemberName = backingField.Name,
                        MemberOmittingFlags = memberOmittingFlags,
                        Type = actualType,
                    });

                    break;
                }

                actualType = actualType.BaseType;
            }
        }

        private void AddOmittedField<TSource>(string fieldName,
                                              MemberOmittingFlags memberOmittingFlags = MemberOmittingFlags.OnSameType)
        {
            var type = typeof(TSource);

            _omittedMembers.Add(new OmittedMember
            {
                ComparisonMode = ComparisonModeEnum.Fields,
                MemberName = fieldName,
                MemberOmittingFlags = memberOmittingFlags,
                Type = type,
            });
        }

        private static void ThrowOnUnsupportedAddingOmittedProperty(Type type, MemberOmittingFlags memberOmittingFlags)
        {
            if (type.IsInterface
                && (memberOmittingFlags != MemberOmittingFlags.OnAnyTypeInFamily)
                && ((memberOmittingFlags == MemberOmittingFlags.None)
                    || memberOmittingFlags.HasFlag(MemberOmittingFlags.OnSameType)))
            {
                throw new Exception($"Not allowed Member omitting for combination of Interface and {memberOmittingFlags} .");
            }
        }

        #endregion

        internal bool HaveSameAllMembers(object x, object y)
        {
            var result = AreObjectsSame(x, y, CreateComparisonOptionForProperties())
                         && AreObjectsSame(x, y, CreateComparisonOptionForFields());

            return result;
        }

        public bool HaveSameAllMembers<T>(T x, T y)
        {
            var result = AreObjectsSame(x, y, CreateComparisonOptionForProperties())
                         && AreObjectsSame(x, y, CreateComparisonOptionForFields());

            return result;
        }

        public bool HaveSamePublicProperties<T>(T x, T y)
        {
            var result = AreObjectsSame(x, y, CreateComparisonOptionForProperties());

            return result;
        }

        internal bool HaveSamePublicProperties(object x, object y)
        {
            var result = AreObjectsSame(x, y, CreateComparisonOptionForProperties());

            return result;
        }

        internal bool HaveSameFields(object x, object y)
        {
            var result = AreObjectsSame(x, y, CreateComparisonOptionForFields());

            return result;
        }

        public bool HaveSameFields<T>(T x, T y)
        {
            var result = AreObjectsSame(x, y, CreateComparisonOptionForFields());

            return result;
        }

        private bool AreObjectsSame(object x,
                                    object y,
                                    ComparisonOption comparisonOption,
                                    ImmutableQueue<DeepObjectComparerReport.DiffPathLink> pathForDebug = null)
        {
            pathForDebug ??=
                ImmutableQueue.Create(DeepObjectComparerReport.DiffPathLink.From(x?.GetType().Name ?? y?.GetType().Name ?? "NULL", x, y));

            if (TryShallowCompare(x, y, pathForDebug, out var shallowCompareResult))
            {
                return shallowCompareResult;
            }

            if (comparisonOption.WasCompared(x, y))
            {
                return true;
            }

            comparisonOption.AddCompared(x, y);

            if (IsEnumerableType(x.GetType())
                && !AreEnumerablesTheSame((IEnumerable)x, (IEnumerable)y, comparisonOption, pathForDebug))
            {
                // difference will be recorded inside AreEnumerablesSame
                // we do not care about difference of other members in report, if contents are different
                return false;
            }

            var members = comparisonOption.GetMembers(x).ToList();
            var allMembersAreSame = true;

            foreach (var memberInfo in members)
            {
                // by reflection
                var xMemberValue = memberInfo.GetPropertyOrFieldValue(x);
                var yMemberValue = memberInfo.GetPropertyOrFieldValue(y);

                // by expresion trees
                // var xMemberValue = x.GetMemberValueByExpression(memberInfo.Name);
                // var yMemberValue = y.GetMemberValueByExpression(memberInfo.Name);

                var membersSame = AreObjectsSame(xMemberValue,
                                                 yMemberValue,
                                                 comparisonOption,
                                                 pathForDebug.Enqueue(
                                                     DeepObjectComparerReport.DiffPathLink.From(memberInfo, xMemberValue, yMemberValue)));

                if (!membersSame)
                {
                    allMembersAreSame = false;

                    if (_reportMode)
                    {
                        // keep comparing the rest of members to record other differences
                    }
                    else
                    {
                        // we can short circuit
                        return false;
                    }
                }
            }

            return allMembersAreSame;
        }

        private bool TryShallowCompare(object x,
                                       object y,
                                       ImmutableQueue<DeepObjectComparerReport.DiffPathLink> pathForDebug,
                                       out bool shallowCompareResult)
        {
            bool? areSame = null;

            if (x is null
                && y is null)
            {
                areSame = true;
            }
            else if (x is null
                     || y is null)
            {
                areSame = FalseOrThrow(pathForDebug, x, y);
            }
            else if (ReferenceEquals(y, x))
            {
                areSame = true;
            }
            else
            {
                var xType = x.GetType();
                var yType = y.GetType();

                if (xType != yType)
                {
                    areSame = FalseOrThrow(pathForDebug, x, y);
                }
                else if (xType == typeof(object))
                {
                    areSame = true;
                }
                else if (IsTypeToValueSemanticsEquality(xType))
                {
                    areSame = x.Equals(y) || FalseOrThrow(pathForDebug, x, y);
                }
                else if (IsOmittedType(xType, _omittedTypes, MemberComparisonFlags.ByObjectType))
                {
                    areSame = true;
                }

                ThrowOnDelegateType(xType);
            }

            if (areSame.HasValue)
            {
                shallowCompareResult = areSame.Value;

                return true;
            }
            else
            {
                shallowCompareResult = default;

                return false;
            }
        }

        private bool FalseOrThrow(ImmutableQueue<DeepObjectComparerReport.DiffPathLink> path, object x, object y, bool recordDiff = true)
        {
            if (ThrowExceptionOnFalse)
            {
                var stringPath = DeepObjectComparerReport.DiffPathLink.GetStringPathRepresentation(path);

                throw new Exception($"Diff: '{stringPath}' (Type: {(x ?? y)?.GetType().Name}). '{x ?? "NULL"}' <--> '{y ?? "NULL"}'");
            }
            else
            {
                if (recordDiff)
                {
                    RecordDiff(path, x, y);
                }

                return false;
            }
        }

        private void RecordDiff(ImmutableQueue<DeepObjectComparerReport.DiffPathLink> path, object x, object y)
        {
            if (_reportMode)
            {
                var stringPath = DeepObjectComparerReport.DiffPathLink.GetStringPathRepresentation(path);

                _diffs.Add(new DeepObjectComparerReport.Diff()
                {
                    Message = $"Diff: '{stringPath}' (Type: {(x ?? y)?.GetType().Name}). '{x ?? "NULL"}' <--> '{y ?? "NULL"}'",
                    X = x,
                    Y = y,
                    Path = path,
                });
            }
        }

        private ComparisonOption CreateComparisonOptionForFields()
        {
            return new ComparisonOption { GetMembers = GetFields_WithoutOmitted, ComparisonMode = ComparisonModeEnum.Fields, };
        }

        private ComparisonOption CreateComparisonOptionForProperties()
        {
            return new ComparisonOption
                { GetMembers = GetPublicProperties_WithoutOmitted, ComparisonMode = ComparisonModeEnum.Properties, };
        }

        private readonly Dictionary<Type, MemberInfo[]> _fieldsByType = new Dictionary<Type, MemberInfo[]>();

        [NotNull]
        private MemberInfo[] GetFields_WithoutOmitted(object x)
        {
            if (x == null)
            {
                return Array.Empty<MemberInfo>();
            }

            var type = x.GetType();
            var iterationType = type;

            if (!_fieldsByType.TryGetValue(type, out var result))
            {
                var memberInfos = new List<MemberInfo>();

                while ((iterationType != null)
                       && (iterationType != typeof(object)))
                {
                    var isActualTypeLazy = IsLazyType(iterationType);

                    var isOmittedByDefault = IsListType(iterationType)
                                             || IsCircularBuffer(iterationType);

                    if (!isActualTypeLazy
                        && !isOmittedByDefault)
                    {
                        var memberInfosToAdd = iterationType
                                              .GetFields(BindingFlags.Instance
                                                         | BindingFlags.Public
                                                         | BindingFlags.NonPublic)
                                              .Where(f => !IsOmittedType(f.FieldType, _omittedTypes, MemberComparisonFlags.ByMemberType))
                                              .Where(field => !IsOmittedMember(field, _omittedMembers, ComparisonModeEnum.Fields))
                                              .Where(field =>
                                               {
                                                   if (IsHashSetOrDictionaryType(iterationType))
                                                   {
                                                       // for HashSet and Dictionary we omit private fields except _comparer
                                                       return field.IsPrivate && (field.Name == "_comparer");
                                                   }

                                                   return true;
                                               });

                        memberInfos.AddRange(memberInfosToAdd);
                    }

                    if (isActualTypeLazy)
                    {
                        var valueProperty = iterationType.GetProperty("Value");

                        if (!IsOmittedMember(valueProperty, _omittedMembers, ComparisonModeEnum.Properties))
                        {
                            memberInfos.Add(valueProperty);
                        }
                    }

                    iterationType = iterationType.BaseType;
                    ThrowOnUnsupportedBaseType(iterationType);
                }

                result = memberInfos.ToArray();
                _fieldsByType.Add(type, result);
            }

            return result;
        }

        private readonly Dictionary<Type, PropertyInfo[]> _propertiesByType = new Dictionary<Type, PropertyInfo[]>();

        [NotNull]
        private MemberInfo[] GetPublicProperties_WithoutOmitted(object x)
        {
            if (x == null)
            {
                return Array.Empty<MemberInfo>();
            }

            var type = x.GetType();

            if (!_propertiesByType.TryGetValue(type, out var propertyInfos))
            {
                var isOmittedByDefault = IsListType(type)
                                         || IsCircularBuffer(type)
                                         || IsLazyType(type);

                if (isOmittedByDefault)
                {
                    return Array.Empty<MemberInfo>();
                }

                var propertyInfosCreated = type.GetProperties(BindingFlags.Instance | BindingFlags.Public)
                                               .Where(
                                                    f => !IsOmittedType(f.PropertyType, _omittedTypes, MemberComparisonFlags.ByMemberType))
                                               .Where(info => !IsOmittedMember(info, _omittedMembers, ComparisonModeEnum.Properties))
                                               .Where(p => p.CanRead)

                                                // skipping indexers (to avoid errors when not passing parameter for indexer call)
                                               .Where(info => info.GetIndexParameters().Length == 0)
                                               .ToArray();

                // TODO there are missing explicit interface implementations!

                ThrowOnUnsupportedBaseType(type.BaseType);

                _propertiesByType.Add(type, propertyInfosCreated);

                propertyInfos = propertyInfosCreated;
            }

            return propertyInfos;
        }

        private bool AreEnumerablesTheSame(IEnumerable x,
                                           IEnumerable y,
                                           ComparisonOption comparisonOption,
                                           ImmutableQueue<DeepObjectComparerReport.DiffPathLink> pathForDebug)
        {
            if (TryShallowCompare(x, y, pathForDebug, out var areMembersSame))
            {
                return areMembersSame;
            }

            var xEnumerator = x.GetEnumerator();
            var yEnumerator = y.GetEnumerator();
            var i = 0;

            var difference = false;

            while (xEnumerator.MoveNext())
            {
                if (!yEnumerator.MoveNext())
                {
                    return FalseOrThrow(pathForDebug.Enqueue(DeepObjectComparerReport.DiffPathLink.ForCount(i, i + 1)), i + 1, i);
                }

                var xEnumeratorCurrent = xEnumerator.Current;
                var yEnumeratorCurrent = yEnumerator.Current;

                var enumeratorPath =
                    pathForDebug.Enqueue(DeepObjectComparerReport.DiffPathLink.CreateIndexer(i, xEnumeratorCurrent, yEnumeratorCurrent));

                if (!AreObjectsSame(xEnumeratorCurrent, yEnumeratorCurrent, comparisonOption, enumeratorPath))
                {
                    if (_reportMode)
                    {
                        difference = true;

                        // this is here for possible throw
                        _ = FalseOrThrow(enumeratorPath, xEnumeratorCurrent, yEnumeratorCurrent, recordDiff: false);
                    }
                    else
                    {
                        return FalseOrThrow(enumeratorPath, xEnumeratorCurrent, yEnumeratorCurrent);
                    }
                }

                i++;
            }

            if (difference)
            {
                return false;
            }

            return !yEnumerator.MoveNext()
                   || FalseOrThrow(pathForDebug.Enqueue(DeepObjectComparerReport.DiffPathLink.ForCount(i, i + 1)), i, i + 1);
        }

        private static bool IsOmittedMember(MemberInfo memberInfo,
                                            IEnumerable<OmittedMember> omittedMembers,
                                            ComparisonModeEnum comparisonMode)
        {
            var type = memberInfo.ReflectedType;

            if (type == null)
            {
                return false;
            }

            var memberName = memberInfo.Name;

            foreach (var omittedMember in omittedMembers.Where(om => om.ComparisonMode == comparisonMode && om.MemberName == memberName))
            {
                var omittedType = omittedMember.Type;
                var omittingFlags = omittedMember.MemberOmittingFlags;

                var isMatchingMember = (omittingFlags.HasFlag(MemberOmittingFlags.OnSameType)
                                        && (type == omittedType))
                                       || (omittingFlags.HasFlag(MemberOmittingFlags.OnParents)
                                           && type.IsAssignableFrom(omittedType)
                                           && (type != omittedType))
                                       || (omittingFlags.HasFlag(MemberOmittingFlags.OnSons)
                                           && type.IsAssignableTo(omittedType)
                                           && (type != omittedType));

                if (isMatchingMember)
                {
                    return true;
                }
            }

            return false;
        }

        private static bool IsOmittedType(Type type, IEnumerable<OmittedType> omittedTypes, MemberComparisonFlags memberComparisonFlags)
        {
            foreach (var omittedType in omittedTypes)
            {
                var isMatching = omittedType.CompareAssignableTo ? type.IsAssignableTo(omittedType.Type) : type == omittedType.Type;

                if (isMatching && omittedType.MemberComparison.HasFlag(memberComparisonFlags))
                {
                    return true;
                }
            }

            return false;
        }

        private static bool IsHashSetOrDictionaryType(Type type)
        {
            if (type.IsGenericType)
            {
                var genericDefinition = type.GetGenericTypeDefinition();

                return (typeof(ConcurrentDictionary<,>) == genericDefinition)
                       || (typeof(HashSet<>) == genericDefinition)
                       || (typeof(Dictionary<,>) == genericDefinition);
            }
            else
            {
                return false;
            }
        }

        private static bool IsCircularBuffer(Type type)
        {
            return type.IsGenericType && (type.GetGenericTypeDefinition() == typeof(CircularBuffer<>));
        }

        private static bool IsListType(Type type)
        {
            return type.IsGenericType && (type.GetGenericTypeDefinition() == typeof(List<>));
        }

        private static bool IsLazyType(Type type)
        {
            return type.IsGenericType && (type.GetGenericTypeDefinition() == typeof(Lazy<>));
        }

        private static bool IsEnumerableType(Type type)
        {
            return type.IsAssignableTo(typeof(IEnumerable));
        }

        private static void ThrowOnUnsupportedBaseType(Type type)
        {
            if ((type == null)
                || !type.IsGenericType)
            {
                return;
            }

            var genericDefinition = type.GetGenericTypeDefinition();

            var result = (typeof(ConcurrentDictionary<,>) == genericDefinition)
                         || (typeof(HashSet<>) == genericDefinition)
                         || (typeof(Dictionary<,>) == genericDefinition)
                         || (typeof(List<>) == genericDefinition);

            if (result)
            {
                throw new NotImplementedException($"Type {type.Name} is not supported as base type.");
            }
        }

        private static void ThrowOnDelegateType(Type type)
        {
            var isDelegate = type.IsAssignableTo(typeof(Delegate));

            if (isDelegate)
            {
                throw new NotImplementedException("Delegates are not supported type.");
            }
        }

        private class ComparisonOption
        {
            public Func<object, IEnumerable<MemberInfo>> GetMembers { get; set; }

            public ComparisonModeEnum ComparisonMode { get; set; }

            private ConcurrentDictionary<object, HashSet<object>> ComparedObjects { get; set; } =
                new ConcurrentDictionary<object, HashSet<object>>(ReferenceEqualityComparer.Instance);

            public void AddCompared(object x, object y)
            {
                ComparedObjects.AddOrUpdate(x,
                                            (key) =>
                                            {
                                                var h = new HashSet<object>(ReferenceEqualityComparer.Instance) { y, };

                                                return h;
                                            },
                                            (key, oldHashSet) =>
                                            {
                                                oldHashSet.Add(y);

                                                return oldHashSet;
                                            });
            }

            public bool WasCompared(object x, object y)
            {
                ComparedObjects.TryGetValue(x, out var alreadyComparedTo);

                return (alreadyComparedTo != null) && alreadyComparedTo.TryGetValue(y, out _);
            }
        }

        private class OmittedMember
        {
            public Type Type { get; set; }

            public string MemberName { get; set; }

            public MemberOmittingFlags MemberOmittingFlags { get; set; }

            public ComparisonModeEnum ComparisonMode { get; set; }
        }

        private class OmittedType
        {
            public bool CompareAssignableTo { get; set; }

            public MemberComparisonFlags MemberComparison { get; set; }

            public Type Type { get; set; }
        }

        private enum ComparisonModeEnum
        {
            None = 0,
            Fields = 1,
            Properties = 2,
        }

        [Flags]
        public enum MemberComparisonFlags
        {
            None = 0,
            ByMemberType = 1,
            ByObjectType = 2,
            ByMemberTypeAndObjectType = ByMemberType | ByObjectType,
        }

        [Flags]
        public enum MemberOmittingFlags
        {
            None = 0,
            OnSameType = 1,
            OnParents = 2,
            OnSons = 4,
            OnSameTypeOrOnParents = OnSameType | OnParents,
            OnSameTypeOrOnSons = OnSameType | OnSons,
            OnAnyTypeInFamily = OnSameType | OnParents | OnSons,
        }
    }
}
