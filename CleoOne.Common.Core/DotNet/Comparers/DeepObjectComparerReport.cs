﻿// <copyright file="DeepObjectComparerReport.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Generic;

namespace CleoOne.Common.Core.DotNet.Comparers
{
    public partial class DeepObjectComparerReport
    {
        public DeepObjectComparerReport(bool areSame, List<Diff> differences)
        {
            if (areSame && (differences.Count > 0))
            {
                throw new ArgumentException("Object considered same, but with differences listed.");
            }

            if (!areSame
                && (differences.Count == 0))
            {
                throw new ArgumentException("Objects considered different, but no differences listed.");
            }

            AreSame = areSame;
            Diffs = differences;
        }

        public bool AreSame { get; }

        public IReadOnlyList<Diff> Diffs { get; }

        public class Diff
        {
            public string Message { get; set; }

            public object X { get; set; }

            public object Y { get; set; }

            public IEnumerable<DiffPathLink> Path { get; set; }

            public override string ToString() => Message;
        }
    }
}
