﻿// <copyright file="DeepObjectCompareException.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;

namespace CleoOne.Common.Core.DotNet.Comparers
{
    public class DeepObjectCompareException : Exception
    {
        public DeepObjectComparerReport Report { get; }

        public DeepObjectCompareException(string message, DeepObjectComparerReport report)
            : base(message)
        {
            Report = report;
        }
    }
}
