﻿// <copyright file="DiffPathLink.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.DotNet.Extensions;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace CleoOne.Common.Core.DotNet.Comparers
{
    public partial class DeepObjectComparerReport
    {
        public class DiffPathLink
        {
            public static DiffPathLink From(MemberInfo memberInfo, object x, object y)
            {
                return new DiffPathLink()
                {
                    MemberName = memberInfo.Name,
                    Member = memberInfo,
                    Values = (x, y),
                };
            }

            public static DiffPathLink From(string memberName, object x, object y)
            {
                return new DiffPathLink()
                {
                    MemberName = memberName,
                    Values = (x, y),
                };
            }

            public static DiffPathLink ForCount(int x, int y)
            {
                return new DiffPathLink()
                {
                    MemberName = "Count",
                    IsCount = true,
                    Values = (x, y),
                };
            }

            public static DiffPathLink CreateIndexer(int indexValue, object x, object y)
            {
                return new DiffPathLink()
                {
                    Values = (x, y),
                    IndexValue = indexValue,
                };
            }

            public bool IsIndexer => IndexValue != null;

            public (object X, object Y) Values { get; set; }

            public MemberInfo Member { get; set; }

            public int? IndexValue { get; set; }

            public string MemberName { get; set; }

            public bool IsCount { get; set; }

            public override string ToString()
            {
                if (IsIndexer)
                {
                    return $"[{IndexValue}]";
                }
                else
                {
                    return MemberName;
                }
            }

            public static string GetStringPathRepresentation(IEnumerable<DiffPathLink> path)
            {
                var sb = new StringBuilder();

                foreach ((int i, var link) in path.WithIndex())
                {
                    if (link.IsIndexer)
                    {
                        sb.Append('[');
                        sb.Append(link.IndexValue.ToString());
                        sb.Append(']');
                    }
                    else
                    {
                        if (i != 0)
                        {
                            sb.Append('.');
                        }

                        sb.Append(link.MemberName);
                    }
                }

                return sb.ToString();
            }
        }
    }
}
