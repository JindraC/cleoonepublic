﻿// <copyright file="ReferenceEqualityComparer.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace CleoOne.Common.Core.DotNet.Comparers
{
    //public static class ReferenceEqualityComparer
    //{
    //    public static IEqualityComparer<object> Instance { get; } = ReferenceEqualityComparer<object>.Instance;

    //    public static ReferenceEqualityComparer<T> For<T>()
    //        where T : class => ReferenceEqualityComparer<T>.Instance;
    //}

    public class ReferenceEqualityComparer<T> : EqualityComparer<T>
    {
        private ReferenceEqualityComparer() { }

        public static ReferenceEqualityComparer<T> Instance { get; } = new ReferenceEqualityComparer<T>();

        public override bool Equals(T x, T y) => ReferenceEquals(x, y);

        public override int GetHashCode(T obj) => RuntimeHelpers.GetHashCode(obj);
    }
}
