﻿// <copyright file="TwoTimeoutsCache.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.Common.Components.Times;
using CleoOne.Common.Core.Common.Logging;
using CleoOne.Common.Core.DotNet.Helpers;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using System.Threading.Tasks;

namespace CleoOne.Common.Core.DotNet.Caches
{
    /// <summary>
    /// Does not refresh by timer: refreshes synchronously right before the call, or,
    /// if object cached and time between minLifetimeMs and maxLifeTimeMs, in background.
    /// </summary>
    // Warning CA1001 was disabled, because there is no need to dispose ManualResetEventSlim
    [SuppressMessage("Design", "CA1001:Types that own disposable fields should be disposable", Justification = "Approved")]
    public class TwoTimeoutsCache<T>
    {
        public T Value => GetValue();

        public int TrialsCount { get; set; } = 3;

        protected readonly ITime _time;
        private readonly ILogger _logger;

        protected readonly TimeSpan _refreshAfter;
        private readonly TimeSpan _validDuration;
        private readonly Func<T> _downloaderFunc;
        private T _valueCache;
        private readonly object _loadingLocker = new object();
        private Task _refreshTask;
        private bool _refreshIsScheduledOrRunning = false;
        private DateTime _lastSuccessfulRefreshAt;
        private readonly object _refreshTaskLocker = new object();
        private int _initialized;

        public DateTime LastSuccessfulRefreshAt => _lastSuccessfulRefreshAt;

        protected DateTime? LastThrewExceptionAt => _lastThrewExceptionAt;

        private DateTime? _lastThrewExceptionAt;
        private Exception _lastException;

        private readonly ManualResetEventSlim _firstSuccessResetEvent;
        private bool _firstFinished;

        protected event Action InitialCallStarted;

        public TwoTimeoutsCache(TimeSpan refreshAfter,
                                TimeSpan validDuration,
                                ITime time,
                                Func<T> downloaderFunc)
        {
            if (validDuration < refreshAfter)
            {
                throw new Exception($"{nameof(validDuration)} was smaller than {refreshAfter}");
            }

            _time = time;
            _logger = this.GetLoggerForThis();
            _refreshAfter = refreshAfter;
            _validDuration = validDuration;
            _downloaderFunc = downloaderFunc;
            _firstSuccessResetEvent = new ManualResetEventSlim();
        }

        public bool IsSuccessfullyInitialized => _lastSuccessfulRefreshAt > DateTime.MinValue;

        public bool FirstCallFinished => _firstFinished;

        public bool TryInitializeIfNotYet(int timeoutMilliseconds, bool waitJustOnce = true)
        {
            if (_lastSuccessfulRefreshAt > DateTime.MinValue)
            {
                return true;
            }

            if (waitJustOnce
                && _firstFinished)
            {
                return _lastSuccessfulRefreshAt > DateTime.MinValue;
            }

            var isNotRunning = Monitor.TryEnter(_loadingLocker);

            if (isNotRunning)
            {
                Monitor.Exit(_loadingLocker);

                StartRefreshIfNeeded();
            }

            if (timeoutMilliseconds > 0)
            {
                _firstSuccessResetEvent.Wait(timeoutMilliseconds);
            }

            return _lastSuccessfulRefreshAt > DateTime.MinValue;
        }

        public bool TryGetValueImmediately(out T value)
        {
            var success = TryGetValidValue(out value, out _);

            StartRefreshIfNeeded();

            return success;
        }

        public void Refresh()
        {
            lock (_loadingLocker)
            {
                ThrowIfExceptionLately();

                Load();
            }
        }

        private T GetValue()
        {
            var value = GetValidOrLoad();

            StartRefreshIfNeeded();

            return value;
        }

        private void InitializeIfNotYet()
        {
            if (Interlocked.Exchange(ref _initialized, 1) == 1)
            {
                return;
            }

            InitialCallStarted?.Invoke();
        }

        private bool TryGetValidValue(out T value, out DateTime loadedAt)
        {
            var lastSuccessAt = _lastSuccessfulRefreshAt;

            if (lastSuccessAt.Add(_validDuration) >= _time.UtcNow)
            {
                value = _valueCache;
                loadedAt = lastSuccessAt;

                return true;
            }

            value = default;
            loadedAt = default;

            return false;
        }

        private T GetValidOrLoad()
        {
            if (TryGetValidValue(out var value, out _))
            {
                return value;
            }

            return GetRecentOrLoad();
        }

        private T GetRecentOrLoad()
        {
            lock (_loadingLocker)
            {
                if (TryGetValidValue(out var value, out var downloadedAt)
                    && downloadedAt.Add(_refreshAfter) >= _time.UtcNow)
                {
                    return value;
                }

                ThrowIfExceptionLately();

                return Load();
            }
        }

        private void ThrowIfExceptionLately()
        {
            // We prevent waiting calls to repeat the same trials immediately again
            if (_lastException is { } exception
                && _lastThrewExceptionAt is { } lastThrownAt
                && lastThrownAt.AddSeconds(5) > _time.UtcNow)
            {
                throw new Exception($"In less then 5 seconds ago the previous call threw an exception.", exception);
            }
        }

        private T Load()
        {
            lock (_loadingLocker)
            {
                InitializeIfNotYet();

                try
                {
                    var value = Retrials.GetMoreTimes(_downloaderFunc, TrialsCount);

                    _lastThrewExceptionAt = null;
                    _lastException = null;

                    _valueCache = value;
                    _lastSuccessfulRefreshAt = _time.UtcNow;
                    _firstSuccessResetEvent.Set();

                    return value;
                }
                catch (Exception ex)
                {
                    _lastException = ex;
                    _lastThrewExceptionAt = _time.UtcNow;

                    throw;
                }
                finally
                {
                    _firstFinished = true;
                }
            }
        }

        protected void StartRefreshIfNeeded()
        {
            if (LastSuccessIsRecent()
                || _refreshIsScheduledOrRunning)
            {
                return;
            }

            lock (_refreshTaskLocker)
            {
                if (LastSuccessIsRecent()
                    || _refreshIsScheduledOrRunning)
                {
                    return;
                }

                _refreshIsScheduledOrRunning = true;

                _refreshTask = Task.Run(
                    () =>
                    {
                        try
                        {
                            GetRecentOrLoad();
                        }
                        catch (Exception e)
                        {
                            _logger.LogError(e, "Download for self refreshing cache of {CachedType} failed.", typeof(T).Name);
                        }
                        finally
                        {
                            lock (_refreshTaskLocker)
                            {
                                _refreshIsScheduledOrRunning = false;
                            }
                        }
                    });
            }
        }

        private bool LastSuccessIsRecent()
        {
            return _lastSuccessfulRefreshAt.Add(_refreshAfter) >= _time.UtcNow;
        }
    }
}
