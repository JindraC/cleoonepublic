﻿// <copyright file="StringsInMemoryCache.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Collections.Concurrent;

namespace CleoOne.Common.Core.DotNet.Caches
{
    /// <summary>
    /// Same strings like '2019Q1TTM' can be different objects in memory. This cache can unify them to the same instances.
    /// </summary>
    public class StringsInMemoryCache
    {
        private readonly ConcurrentDictionary<string, string> _dictionary = new ConcurrentDictionary<string, string>();

        public string GetCachedString(string theString)
        {
            if (theString == null)
            {
                return null;
            }

            return _dictionary.GetOrAdd(theString, _ => theString);
        }
    }
}
