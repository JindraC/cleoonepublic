﻿// <copyright file="CacheWithTimeoutAsync.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using Nito.AsyncEx;
using System;
using System.Threading.Tasks;

namespace CleoOne.Common.Core.DotNet.Caches
{
    public class CacheWithTimeoutAsync<TCached>
    {
        private readonly int _timeoutSeconds;
        private readonly Func<Task<TCached>> _constructorOrDownloader;
        private readonly AsyncLock _locker;

        private DateTime _lastUpdated;
        private TCached _cached;

        public CacheWithTimeoutAsync(int timeoutSeconds, Func<Task<TCached>> constructorOrDownloader)
        {
            _timeoutSeconds = timeoutSeconds;
            _constructorOrDownloader = constructorOrDownloader;
            _locker = new AsyncLock();
        }

        public async Task<TCached> GetOrCreate()
        {
            var utcNow = DateTime.UtcNow;

            if ((utcNow - _lastUpdated).TotalSeconds < _timeoutSeconds)
            {
                return _cached;
            }

            using var locked = await _locker.LockAsync();

            if ((utcNow - _lastUpdated).TotalSeconds < _timeoutSeconds)
            {
                return _cached;
            }

            _cached = await _constructorOrDownloader();
            _lastUpdated = utcNow;

            return _cached;
        }
    }
}
