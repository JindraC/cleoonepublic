﻿// <copyright file="ForgettingDictionaryCache.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Concurrent;

namespace CleoOne.Common.Core.DotNet.Caches
{
    public class ForgettingDictionaryCache<TKey, TValue>
    {
        public int TimeoutSeconds { get; }

        private ConcurrentDictionary<TKey, (TValue value, DateTime validAtLeastTill)> _dictionary =
            new ConcurrentDictionary<TKey, (TValue value, DateTime validAtLeastTill)>();

        private DateTime _doNotRunForgettingTill;
        private static object forgettingLocker = new object();

        public ForgettingDictionaryCache(int timeoutSeconds)
        {
            TimeoutSeconds = timeoutSeconds;
        }

        public TValue GetOrConstruct(TKey key, Func<TKey, TValue> constructor)
        {
            ForgetOldIfNeeded();

            return _dictionary.GetOrAdd(key,
                                        _ =>
                                        {
                                            var item = constructor(key);
                                            var validTill = DateTime.UtcNow.AddSeconds(TimeoutSeconds);

                                            return (item, validTill);
                                        })
                              .value;
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            ForgetOldIfNeeded();

            if (_dictionary.TryGetValue(key, out var pair))
            {
                value = pair.value;

                return true;
            }
            else
            {
                value = default;

                return false;
            }
        }

        private void ForgetOldIfNeeded()
        {
            if (_doNotRunForgettingTill > DateTime.UtcNow)
            {
                return;
            }

            lock (forgettingLocker)
            {
                if (_doNotRunForgettingTill > DateTime.UtcNow)
                {
                    return;
                }

                _doNotRunForgettingTill = DateTime.UtcNow.AddSeconds(TimeoutSeconds);
            }

            var utcNow = DateTime.UtcNow;

            foreach (var pair in _dictionary)
            {
                var key = pair.Key;
                var validAtLeastTill = pair.Value.validAtLeastTill;

                if (validAtLeastTill < utcNow)
                {
                    _dictionary.TryRemove(key, out _);
                }
            }
        }
    }
}
