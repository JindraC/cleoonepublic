﻿// <copyright file="CacheWithTimeout.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;

namespace CleoOne.Common.Core.DotNet.Caches
{
    [Obsolete("Use SelfRefreshingCache or TwoTimeoutsCache instead")]
    public class CacheWithTimeout<TCached>
    {
        private readonly int _timeoutSeconds;
        private readonly Func<TCached> _constructorOrDownloader;
        private readonly object _locker;

        private DateTime _lastUpdated;
        private TCached _cached;

        public CacheWithTimeout(int timeoutSeconds, Func<TCached> constructorOrDownloader)
        {
            _timeoutSeconds = timeoutSeconds;
            _constructorOrDownloader = constructorOrDownloader;
            _locker = new object();
        }

        public TCached GetOrCreate()
        {
            var utcNow = DateTime.UtcNow;

            if ((utcNow - _lastUpdated).TotalSeconds < _timeoutSeconds)
            {
                return _cached;
            }

            lock (_locker)
            {
                if ((utcNow - _lastUpdated).TotalSeconds < _timeoutSeconds)
                {
                    return _cached;
                }

                _cached = _constructorOrDownloader();
                _lastUpdated = utcNow;

                return _cached;
            }
        }
    }
}
