﻿// <copyright file="BaseHolder.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.Application.Interfaces;
using CleoOne.Common.Core.Common.Logging;
using CleoOne.Common.Core.DotNet.Collections;
using CleoOne.Common.Core.DotNet.Extensions;
using CleoOne.Common.Core.DotNet.Helpers;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CleoOne.Common.Core.DotNet.Caches.Holders
{
    public abstract class BaseHolder<TId, TDbItem> : IDisposable, IInitializable
    {
        protected readonly ILogger _logger;
        private readonly object _loadingLocker = new object();
        protected BaseHolderItems _holderItems;

        private readonly bool _hasDictionaryById;
        private readonly bool _hasDictionaryByCode;
        private readonly bool _hasDictionaryByCode2;
        private readonly bool _hasDictionaryByCode3;

        private readonly int? _refreshPeriodSeconds;
        private SelfRefreshingCache<bool> _selfRefreshing;
        private int _selfRefreshInitiated;
        private DateTime _lastReloadedAtUtc;

        protected const int DEFAULT_REFRESH_PERIOD_SECONDS = 5 * 60;

        public DateTime LastReloadedAtUtc => _lastReloadedAtUtc;

        protected BaseHolder(TimeSpan refreshPeriod,
                             bool hasDictionaryById = true,
                             bool hasDictionaryByCode = false,
                             bool hasDictionaryByCode2 = false,
                             bool hasDictionaryByCode3 = false)
            : this(refreshPeriod.Seconds,
                   hasDictionaryById,
                   hasDictionaryByCode,
                   hasDictionaryByCode2,
                   hasDictionaryByCode3)
        {
        }

        protected BaseHolder(int? refreshPeriodSeconds,
                             bool hasDictionaryById = true,
                             bool hasDictionaryByCode = false,
                             bool hasDictionaryByCode2 = false,
                             bool hasDictionaryByCode3 = false)
        {
            if (!(hasDictionaryById || hasDictionaryByCode || hasDictionaryByCode2 || hasDictionaryByCode3))
            {
                throw new Exception(
                    "Holder have to define at least one type of data storage option in optional constructor parameters (Generic Id or string Code).");
            }

            _hasDictionaryById = hasDictionaryById;
            _hasDictionaryByCode = hasDictionaryByCode;
            _hasDictionaryByCode2 = hasDictionaryByCode2;
            _hasDictionaryByCode3 = hasDictionaryByCode3;

            _refreshPeriodSeconds = refreshPeriodSeconds;
            _logger = LoggerHelper.GetLoggerForThis(this);
        }

        private void InitiateTimerIfNotYet()
        {
            if (Interlocked.Exchange(ref _selfRefreshInitiated, 1) == 1)
            {
                return;
            }

            if (_refreshPeriodSeconds.HasValue
                && _refreshPeriodSeconds.Value > 0)
            {
                var period = TimeSpan.FromMilliseconds(_refreshPeriodSeconds.Value * 1000);

                _selfRefreshing = new SelfRefreshingCache<bool>(period,
                                                                () =>
                                                                {
                                                                    ReloadItems();

                                                                    return true;
                                                                });
            }
        }

        protected abstract IList<TDbItem> LoadAllItems();

        protected abstract TId GetIdOf(TDbItem dbItem);

        protected virtual string GetCodeOf(TDbItem dbItem)
        {
            throw new NotImplementedException(
                "Holder setting the property HasDictionaryByCode to True, have to override method GetCodeOf");
        }

        protected virtual string GetCode2Of(TDbItem dbItem)
        {
            throw new NotImplementedException(
                "Holder setting the property HasDictionaryByCode2 to True, have to override method GetCodeOf2");
        }

        protected virtual string GetCode3Of(TDbItem dbItem)
        {
            throw new NotImplementedException(
                "Holder setting the property HasDictionaryByCode3 to True, have to override method GetCodeOf3");
        }

        protected virtual void WorkAfterReload()
        {
            // nothing, but can be overriden
        }

        public IReadOnlyList<TDbItem> GetOrLoadAllItems(bool forceReload = false)
        {
            return GetOrLoadAllItemsPrivate(forceReload)._loadedItems;
        }

        public IReadOnlyDictionary<TId, TDbItem> GetOrLoadDictionaryById()
        {
            return GetOrLoadAllItemsPrivate(forceReload: false)._dictionaryById;
        }

        public IReadOnlyDictionary<string, TDbItem> GetOrLoadDictionaryByCode()
        {
            return GetOrLoadAllItemsPrivate(forceReload: false)._dictionaryByCode;
        }

        public IReadOnlyDictionary<string, TDbItem> GetOrLoadDictionaryByCode2()
        {
            return GetOrLoadAllItemsPrivate(forceReload: false)._dictionaryByCode2;
        }

        public IReadOnlyDictionary<string, TDbItem> GetOrLoadDictionaryByCode3()
        {
            return GetOrLoadAllItemsPrivate(forceReload: false)._dictionaryByCode3;
        }

        public IReadOnlyList<TDbItem> GetAllByReload()
        {
            return GetOrLoadAllItems(forceReload: true);
        }

        public void ReloadItems()
        {
            GetAllByReload();
        }

        public Task InitializeAsync()
        {
            GetOrLoadAllItems(forceReload: false);

            return Task.CompletedTask;
        }

        public void ReloadItemsInBackground()
        {
            Task.Run(() => TryCatches.TryCatch(
                         () =>
                         {
                             lock (_loadingLocker)
                             {
                                 ReloadCoreWithoutLock();
                             }
                         },
                         _logger));
        }

        public void ReloadItemsInBackgroundNextCallWaiting()
        {
            _holderItems = null;

            ReloadItemsInBackground();
        }

        public TDbItem GetItemById(TId id, bool reloadOnceIfNotPresent = true)
        {
            if (TryGetItemById(id, reloadOnceIfNotPresent, out var item))
            {
                return item;
            }

            throw new Exception($"{GetType().Name}: no item with id={id} not found.");
        }

        private readonly ConcurrentHashSet<TId> _idsThatCausedReload = new ConcurrentHashSet<TId>();

        public bool TryGetItemById(TId id, bool reloadOnceIfNotPresent, out TDbItem item)
        {
            var dictionaryById = GetOrLoadDictionaryById();

            if (dictionaryById.TryGetValue(id, out item))
            {
                return true;
            }

            if (reloadOnceIfNotPresent
                && !_idsThatCausedReload.Contains(id))
            {
                ReloadItemsWithLockAndTimeCheck();

                _idsThatCausedReload.Add(id);

                return TryGetItemById(id, reloadOnceIfNotPresent: false, out item);
            }

            return false;
        }

        public TDbItem GetItemByCode(string code, bool reloadIfNotPresent)
        {
            if (TryGetItemByCode(code, reloadIfNotPresent, out var item))
            {
                return item;
            }

            throw new Exception($"{GetType().Name}: no item found for code: '{code}'");
        }

        public bool TryGetItemByCode(string code,
                                     bool reloadIfNotPresent,
                                     out TDbItem item)
        {
            var dictionaryByCode = GetOrLoadDictionaryByCode();

            if (dictionaryByCode.TryGetValue(code, out item))
            {
                return true;
            }

            if (reloadIfNotPresent)
            {
                ReloadItemsWithLockAndTimeCheck();

                return TryGetItemByCode(code, reloadIfNotPresent: false, out item);
            }

            return false;
        }

        public TDbItem GetItemByCode2(string code2, bool reloadIfNotPresent)
        {
            if (TryGetItemByCode2(code2, reloadIfNotPresent, out var item))
            {
                return item;
            }

            throw new Exception($"{GetType().Name}: no item found for code: '{code2}'");
        }

        public bool TryGetItemByCode2(string code2, bool reloadIfNotPresent, out TDbItem item)
        {
            var dictionaryByCode2 = GetOrLoadDictionaryByCode2();

            if (dictionaryByCode2.TryGetValue(code2, out item))
            {
                return true;
            }

            if (reloadIfNotPresent)
            {
                ReloadItemsWithLockAndTimeCheck();

                return TryGetItemByCode(code2, reloadIfNotPresent: false, out item);
            }

            return false;
        }

        public TDbItem GetItemByCode3(string code3, bool reloadIfNotPresent)
        {
            if (TryGetItemByCode3(code3, reloadIfNotPresent, out var item))
            {
                return item;
            }

            throw new Exception($"{GetType().Name}: no item found for code: '{code3}'");
        }

        public bool TryGetItemByCode3(string code3, bool reloadIfNotPresent, out TDbItem item)
        {
            var dictionaryByCode3 = GetOrLoadDictionaryByCode3();

            if (dictionaryByCode3.TryGetValue(code3, out item))
            {
                return true;
            }

            if (reloadIfNotPresent)
            {
                ReloadItemsWithLockAndTimeCheck();

                return TryGetItemByCode(code3, reloadIfNotPresent: false, out item);
            }

            return false;
        }

        public void AddOrUpdateItem(TDbItem item)
        {
            StoreRecentlySavedItems(new[] { item, }, skipIfAllExist: false);
        }

        public void StoreRecentlySavedItems(IList<TDbItem> newItems, bool skipIfAllExist = true)
        {
            // We ensure that holder is initialized:
            GetOrLoadAllItemsPrivate(forceReload: false);

            lock (_loadingLocker)
            {
                var hasById = _hasDictionaryById;
                var hasByCode = _hasDictionaryByCode;
                var hasByCode2 = _hasDictionaryByCode2;
                var hasByCode3 = _hasDictionaryByCode3;

                if (skipIfAllExist)
                {
                    bool IsStoredAlready(TDbItem item)
                    {
                        return hasById ? _holderItems._dictionaryById.ContainsKey(GetIdOf(item)) :
                               hasByCode ? _holderItems._dictionaryByCode.ContainsKey(GetCodeOf(item)) :
                               hasByCode2 ? _holderItems._dictionaryByCode2.ContainsKey(GetCode2Of(item)) :
                               hasByCode3 ? _holderItems._dictionaryByCode3.ContainsKey(GetCode3Of(item)) :
                               throw new Exception($"{GetType().Name}: For storing new items at least "
                                                   + $"one dictionary of 'ById' or 'ByCode' must be enabled.");
                    }

                    var allAlreadyPresent = newItems.All(IsStoredAlready);

                    if (allAlreadyPresent)
                    {
                        return;
                    }
                }

                var newItemsHolder = new BaseHolderItems();

                newItemsHolder._dictionaryById =
                    hasById ? _holderItems._dictionaryById.ToDictionary(pair => pair.Key, pair => pair.Value) : null;

                newItemsHolder._dictionaryByCode =
                    hasByCode ? _holderItems._dictionaryByCode.ToDictionary(pair => pair.Key, pair => pair.Value) : null;

                newItemsHolder._dictionaryByCode2 =
                    hasByCode2 ? _holderItems._dictionaryByCode2.ToDictionary(pair => pair.Key, pair => pair.Value) : null;

                newItemsHolder._dictionaryByCode3 =
                    hasByCode3 ? _holderItems._dictionaryByCode3.ToDictionary(pair => pair.Key, pair => pair.Value) : null;

                foreach (var newItem in newItems)
                {
                    if (hasById)
                    {
                        var newItemId = GetIdOf(newItem);

                        newItemsHolder._dictionaryById[newItemId] = newItem;
                    }

                    if (hasByCode)
                    {
                        var newItemCode = GetCodeOf(newItem);

                        newItemsHolder._dictionaryByCode[newItemCode] = newItem;
                    }

                    if (hasByCode2)
                    {
                        var newItemCode = GetCode2Of(newItem);

                        newItemsHolder._dictionaryByCode2[newItemCode] = newItem;
                    }

                    if (hasByCode3)
                    {
                        var newItemCode = GetCode3Of(newItem);

                        newItemsHolder._dictionaryByCode3[newItemCode] = newItem;
                    }
                }

                newItemsHolder._loadedItems =
                    hasById ? newItemsHolder._dictionaryById.Values.ToList() :
                    hasByCode ? newItemsHolder._dictionaryByCode.Values.ToList() :
                    hasByCode2 ? newItemsHolder._dictionaryByCode2.Values.ToList() :
                    hasByCode3 ? newItemsHolder._dictionaryByCode3.Values.ToList() :
                    throw new Exception(
                        $"{GetType().Name}: For storing new items at least one dictionary of 'ById' or 'ByCode' must be enabled.");

                ModifyHolderItemsBeforeItIsUsed(newItemsHolder);

                _holderItems = newItemsHolder;
            }
        }

        protected virtual void ModifyHolderItemsBeforeItIsUsed(BaseHolderItems holderItems) { }

        private DateTime _lastReloadStartedAt;
        private readonly object _reloadLatelyLocker = new object();

        protected void ReloadItemsWithLockAndTimeCheck()
        {
            //if (_lastReloadedAtUtc.AddSeconds(10) > DateTime.UtcNow)
            //{
            //    return;
            //}

            var reloadWaitsFrom = DateTime.UtcNow;

            lock (_reloadLatelyLocker)
            {
                if (_lastReloadStartedAt >= reloadWaitsFrom)
                {
                    return;
                }

                // timestamp is assigned directly in GetOrLoadAllItemsPrivate() method (therefore commented out here):
                //_lastReloadStartedAt = DateTime.UtcNow;

                ReloadItems();
            }
        }

        private IList<TDbItem> LoadAllItemsMoreTrials()
        {
            InitiateTimerIfNotYet();

            return Retrials.GetMoreTimes(LoadAllItems, trialsCount: 2, waitMilliseconds: 1000);
        }

        private BaseHolderItems GetOrLoadAllItemsPrivate(bool forceReload)
        {
            InitiateTimerIfNotYet();

            var result = _holderItems;

            if (result == null || forceReload)
            {
                lock (_loadingLocker)
                {
                    result = _holderItems;

                    if (result == null || forceReload)
                    {
                        result = ReloadCoreWithoutLock();
                    }
                }
            }

            return result;
        }

        private BaseHolderItems ReloadCoreWithoutLock()
        {
            var startedAt = DateTime.UtcNow;

            var items = LoadAllItemsMoreTrials();

            var newHolderItems = new BaseHolderItems()
            {
                _loadedItems = items.ToListIfNotList(),
                _dictionaryById = _hasDictionaryById ? items.ToDictionary(GetIdOf) : null,
                _dictionaryByCode = _hasDictionaryByCode ? items.ToDictionary(GetCodeOf) : null,
                _dictionaryByCode2 = _hasDictionaryByCode2 ? items.ToDictionary(GetCode2Of) : null,
                _dictionaryByCode3 = _hasDictionaryByCode3 ? items.ToDictionary(GetCode3Of) : null,
            };

            ModifyHolderItemsBeforeItIsUsed(newHolderItems);

            var result = _holderItems = newHolderItems;

            _lastReloadedAtUtc = DateTime.UtcNow;
            _lastReloadStartedAt = startedAt;

            WorkAfterReload();

            return result;
        }

        protected class BaseHolderItems
        {
            internal List<TDbItem> _loadedItems;
            internal Dictionary<TId, TDbItem> _dictionaryById;
            internal Dictionary<string, TDbItem> _dictionaryByCode;
            internal Dictionary<string, TDbItem> _dictionaryByCode2;
            internal Dictionary<string, TDbItem> _dictionaryByCode3;

            internal readonly Dictionary<string, object> _cacheForDescendants = new Dictionary<string, object>();
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            _selfRefreshing?.Dispose();
        }
    }
}
