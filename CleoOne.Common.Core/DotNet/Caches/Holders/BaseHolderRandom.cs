﻿// <copyright file="BaseHolderRandom.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;

namespace CleoOne.Common.Core.DotNet.Caches.Holders
{
    internal static class BaseHolderRandom
    {
        public static Random Random { get; } = new Random(1);
    }
}
