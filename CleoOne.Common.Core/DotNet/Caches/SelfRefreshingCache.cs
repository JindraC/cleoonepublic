﻿// <copyright file="SelfRefreshingCache.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.Common.Components.Times;
using CleoOne.Common.Core.DotNet.Helpers;
using System;
using System.Threading.Tasks;

namespace CleoOne.Common.Core.DotNet.Caches
{
    /// <summary>
    /// This is basically TwoTimeoutsCache enhanced for self refreshing loop, that is started LAZILY.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class SelfRefreshingCache<T> : TwoTimeoutsCache<T>, IDisposable
    {
        public SelfRefreshingCache(TimeSpan refreshAfter,
                                   TimeSpan validDuration,
                                   ITime time,
                                   Func<T> downloaderFunc)
            : base(refreshAfter,
                   validDuration,
                   time,
                   downloaderFunc)
        {
            InitialCallStarted += StartAutoRefresh;
        }

        public SelfRefreshingCache(TimeSpan refreshAfter,
                                   Func<T> downloaderFunc)
            : this(refreshAfter,
                   refreshAfter * 10,
                   RealTime.Instance,
                   downloaderFunc)
        {
        }

        private bool _continueAutoRefresh;

        private void StartAutoRefresh()
        {
            _continueAutoRefresh = true;

            var weakReference = new WeakReference<SelfRefreshingCache<T>>(this);

            Task.Run(() => DisposableAutoRefreshLoop(weakReference));
        }

        // This method uses WeakReference, because the SelfRefreshingCache can be Garbage collected even when .Dispose() was not called
        private static async Task DisposableAutoRefreshLoop<TT>(WeakReference<SelfRefreshingCache<TT>> weakReference)
        {
            while (true)
            {
                if (!weakReference.TryGetTarget(out var cache))
                {
                    return;
                }

                if (!cache._continueAutoRefresh)
                {
                    return;
                }

                var time = cache._time;

                var startedAt = time.UtcNow;

                cache.StartRefreshIfNeeded();

                var lastFinishedAt = cache.LastFinishedAttemptAt();
                var lastActionAt = DateTimeHelper.Max(startedAt, lastFinishedAt);

                var waitTill = lastActionAt.Add(cache._refreshAfter);

                var waitMilliseconds = (int)Math.Ceiling((waitTill - time.UtcNow).TotalMilliseconds);

                // Not needed line, but it is here to emphasize, that no hard reference to cache shall exist after this line (for finalizer being called):
                cache = null;

                if (waitMilliseconds > 0)
                {
                    await Task.Delay(waitMilliseconds);

                    while (waitTill >= time.UtcNow)
                    {
                        // empty loop waiting for the right time
                    }
                }
            }
        }

        private DateTime LastFinishedAttemptAt()
        {
            var lastSuccessAt = LastSuccessfulRefreshAt;
            var lastFailAt = LastThrewExceptionAt ?? DateTime.MinValue;

            var lastFinishedAt = DateTimeHelper.Max(lastSuccessAt, lastFailAt);

            return lastFinishedAt;
        }

        ~SelfRefreshingCache()
        {
            Dispose();
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            _continueAutoRefresh = false;
        }
    }
}
