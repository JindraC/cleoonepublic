﻿// <copyright file="DecimalConverter.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.DotNet.Extensions.ValueTypes;
using System;
using System.Buffers;
using System.Buffers.Text;
using System.Globalization;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace CleoOne.Common.Core.DotNet.JsonText.Converters
{
    public class DecimalConverter : JsonConverter<decimal>
    {
        public override decimal Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            if (reader.TokenType == JsonTokenType.String)
            {
                ReadOnlySpan<byte> span = reader.HasValueSequence ? reader.ValueSequence.ToArray() : reader.ValueSpan;

                if (Utf8Parser.TryParse(span, out decimal number, out var bytesConsumed)
                    && span.Length == bytesConsumed)
                {
                    return number;
                }

                if (decimal.TryParse(reader.GetString(),
                                     NumberStyles.Number | NumberStyles.Float,
                                     CultureInfo.InvariantCulture,
                                     out var decimalValue))
                {
                    return decimalValue;
                }
            }

            return reader.GetDecimal();
        }

        public override void Write(Utf8JsonWriter writer, decimal value, JsonSerializerOptions options)
        {
            // this will write the value using the "G" format specifier
            // https://docs.microsoft.com/en-us/dotnet/api/system.text.json.utf8jsonwriter.writenumbervalue?view=netcore-3.1#System_Text_Json_Utf8JsonWriter_WriteNumberValue_System_Decimal_
            // https://docs.microsoft.com/en-us/dotnet/standard/base-types/standard-numeric-format-strings#the-general-g-format-specifier
            writer.WriteNumberValue(value.RemoveTrailingZeros());
        }
    }
}
