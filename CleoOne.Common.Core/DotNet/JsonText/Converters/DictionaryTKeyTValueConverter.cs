﻿// <copyright file="DictionaryTKeyTValueConverter.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace CleoOne.Common.Core.DotNet.JsonText.Converters
{
    public class DictionaryTKeyTValueConverter : JsonConverterFactory
    {
        public override bool CanConvert(Type typeToConvert)
        {
            if (!typeToConvert.IsGenericType)
            {
                return false;
            }

            if (typeToConvert.GetGenericTypeDefinition() != typeof(Dictionary<,>))
            {
                return false;
            }

            var keyType = typeToConvert.GetGenericArguments()[0];

            return keyType.IsEnum || keyType == typeof(int);
        }

        public override JsonConverter CreateConverter(Type typeToConvert, JsonSerializerOptions options)
        {
            Type keyType = typeToConvert.GetGenericArguments()[0];
            Type valueType = typeToConvert.GetGenericArguments()[1];

            JsonConverter converter;

            if (keyType.IsEnum)
            {
                converter = (JsonConverter)Activator.CreateInstance(
                    typeof(DictionaryEnumConverterInner<,>).MakeGenericType(new Type[] { keyType, valueType, }),
                    BindingFlags.Instance | BindingFlags.Public,
                    binder: null,
                    args: new object[] { options, },
                    culture: null);
            }
            else if (keyType == typeof(int))
            {
                converter = (JsonConverter)Activator.CreateInstance(
                    typeof(DictionaryIntConverterInner<>).MakeGenericType(new Type[] { valueType, }),
                    BindingFlags.Instance | BindingFlags.Public,
                    binder: null,
                    args: new object[] { options, },
                    culture: null);
            }
            else
            {
                throw new NotImplementedException("Trying to convert unsupported type");
            }

            return converter;
        }

        private class DictionaryIntConverterInner<TValue> : JsonConverter<Dictionary<int, TValue>>
        {
            private readonly JsonConverter<TValue> _valueConverter;
            private readonly Type _valueType;

            public DictionaryIntConverterInner(JsonSerializerOptions options)
            {
                // For performance, use the existing converter if available.
                _valueConverter = (JsonConverter<TValue>)options.GetConverter(typeof(TValue));

                _valueType = typeof(TValue);
            }

            public override Dictionary<int, TValue> Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
            {
                if (reader.TokenType != JsonTokenType.StartObject)
                {
                    throw new JsonException();
                }

                Dictionary<int, TValue> dictionary = new Dictionary<int, TValue>();

                while (reader.Read())
                {
                    if (reader.TokenType == JsonTokenType.EndObject)
                    {
                        return dictionary;
                    }

                    // Get the key.
                    if (reader.TokenType != JsonTokenType.PropertyName)
                    {
                        throw new JsonException();
                    }

                    string propertyName = reader.GetString();

                    if (!int.TryParse(propertyName, out var key))
                    {
                        throw new JsonException($"Unable to convert \"{propertyName}\" to int.");
                    }

                    // Get the value.
                    TValue v;

                    if (_valueConverter != null)
                    {
                        reader.Read();
                        v = _valueConverter.Read(ref reader, _valueType, options);
                    }
                    else
                    {
                        v = JsonSerializer.Deserialize<TValue>(ref reader, options);
                    }

                    // Add to dictionary.
                    dictionary.Add(key, v);
                }

                throw new JsonException();
            }

            public override void Write(Utf8JsonWriter writer,
                                       Dictionary<int, TValue> dictionary,
                                       JsonSerializerOptions options)
            {
                writer.WriteStartObject();

                foreach (KeyValuePair<int, TValue> kvp in dictionary)
                {
                    writer.WritePropertyName(kvp.Key.ToString(CultureInfo.InvariantCulture));

                    if (_valueConverter != null)
                    {
                        _valueConverter.Write(writer, kvp.Value, options);
                    }
                    else
                    {
                        JsonSerializer.Serialize(writer, kvp.Value, options);
                    }
                }

                writer.WriteEndObject();
            }
        }

        private class DictionaryEnumConverterInner<TKey, TValue> : JsonConverter<Dictionary<TKey, TValue>>
            where TKey : struct, Enum
        {
            private readonly JsonConverter<TValue> _valueConverter;
            private readonly Type _keyType;
            private readonly Type _valueType;
            private readonly Dictionary<TKey, string> _camelCasedEnumNames;
            private readonly Dictionary<TKey, string> _enumNames;

            public DictionaryEnumConverterInner(JsonSerializerOptions options)
            {
                // For performance, use the existing converter if available.
                _valueConverter = (JsonConverter<TValue>)options.GetConverter(typeof(TValue));

                // Cache the key and value types.
                _keyType = typeof(TKey);
                _valueType = typeof(TValue);

                var enumValues = Enum.GetValues(_keyType).Cast<TKey>().ToList();

                _camelCasedEnumNames = enumValues.ToDictionary(x => x,
                                                               x =>
                                                               {
                                                                   var enumName = x.ToString();

                                                                   if (enumName.Where(char.IsLetter).All(char.IsUpper))
                                                                   {
                                                                       // All-upperCase is camel cased as all-lowerCase
                                                                       return enumName.ToLowerInvariant();
                                                                   }

                                                                   return char.ToLowerInvariant(enumName[0]) + enumName.Substring(1);
                                                               });

                _enumNames = enumValues.ToDictionary(x => x, x => x.ToString());
            }

            public override Dictionary<TKey, TValue> Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
            {
                if (reader.TokenType != JsonTokenType.StartObject)
                {
                    throw new JsonException();
                }

                Dictionary<TKey, TValue> dictionary = new Dictionary<TKey, TValue>();

                while (reader.Read())
                {
                    if (reader.TokenType == JsonTokenType.EndObject)
                    {
                        return dictionary;
                    }

                    // Get the key.
                    if (reader.TokenType != JsonTokenType.PropertyName)
                    {
                        throw new JsonException();
                    }

                    string propertyName = reader.GetString();

                    // For performance, parse with ignoreCase:false first.
                    if (!Enum.TryParse(propertyName, ignoreCase: false, out TKey key)
                        && !Enum.TryParse(propertyName, ignoreCase: true, out key))
                    {
                        throw new JsonException($"Unable to convert \"{propertyName}\" to Enum \"{_keyType}\".");
                    }

                    // Get the value.
                    TValue v;

                    if (_valueConverter != null)
                    {
                        reader.Read();
                        v = _valueConverter.Read(ref reader, _valueType, options);
                    }
                    else
                    {
                        v = JsonSerializer.Deserialize<TValue>(ref reader, options);
                    }

                    // Add to dictionary.
                    dictionary.Add(key, v);
                }

                throw new JsonException();
            }

            public override void Write(Utf8JsonWriter writer,
                                       Dictionary<TKey, TValue> dictionary,
                                       JsonSerializerOptions options)
            {
                writer.WriteStartObject();

                foreach (KeyValuePair<TKey, TValue> kvp in dictionary)
                {
                    var propertyName = options.PropertyNamingPolicy == JsonNamingPolicy.CamelCase
                                           ? _camelCasedEnumNames[kvp.Key]
                                           : _enumNames[kvp.Key];

                    writer.WritePropertyName(propertyName);

                    if (_valueConverter != null)
                    {
                        _valueConverter.Write(writer, kvp.Value, options);
                    }
                    else
                    {
                        JsonSerializer.Serialize(writer, kvp.Value, options);
                    }
                }

                writer.WriteEndObject();
            }
        }
    }
}
