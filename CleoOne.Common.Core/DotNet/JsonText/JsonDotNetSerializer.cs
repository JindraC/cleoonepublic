﻿// <copyright file="JsonDotNetSerializer.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using CleoOne.Common.Core.DotNet.JsonText.Converters;
using System;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace CleoOne.Common.Core.DotNet.JsonText
{
    public static class JsonDotNetSerializer
    {
        private static readonly JsonSerializerOptions _indentedSerializationOptions =
            new JsonSerializerOptions()
            {
                WriteIndented = true,
            }.AddDefaultCleoOptions();

        private static readonly JsonSerializerOptions _notIndentedSerializationOptions =
            new JsonSerializerOptions()
            {
                WriteIndented = false,
            }.AddDefaultCleoOptions();

        public static JsonSerializerOptions AddCleoCustomConverters(this JsonSerializerOptions jsonOptions)
        {
            jsonOptions.Converters.Add(new JsonStringEnumConverter());

            jsonOptions.Converters.Add(new DictionaryTKeyTValueConverter());

            jsonOptions.Converters.Add(new DecimalConverter());

            return jsonOptions;
        }

        public static JsonSerializerOptions AddDefaultCleoOptions(this JsonSerializerOptions jsonOptions)
        {
            jsonOptions.AddCleoCustomConverters();

            // We do not need escaping since we are not pasting the json into html.
            jsonOptions.Encoder = JavaScriptEncoder.UnsafeRelaxedJsonEscaping;

            // This is from 3-rd party library to enable construction of objects without parameter-less public constructors.
            // https://github.com/dahomey-technologies/Dahomey.Json
            // jsonOptions.SetupExtensions();
            // We do not need it since we have tests for missing constructors, and is is enough to just add private ctor to the type which is being serialized.

            // Be careful when adding this library since it will override some converters, for example the DictionaryTKeyTValueConverter

            return jsonOptions;
        }

        public static string Serialize(object obj, bool withIndents = false, bool serializeNotLogged = true)
        {
            if (!serializeNotLogged)
            {
                throw new NotImplementedException("For Text.Json NotLogged not yet implemented.");
            }

            var options = withIndents ? _indentedSerializationOptions : _notIndentedSerializationOptions;

            return JsonSerializer.Serialize(obj, options);
        }

        public static string SerializeCamelCase(object obj, bool withIndents = true, bool serializeNotLogged = true)
        {
            if (!serializeNotLogged)
            {
                throw new NotImplementedException("For Text.Json NotLogged not yet implemented.");
            }

            var options = new JsonSerializerOptions()
                {
                    WriteIndented = withIndents,
                    PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                }
               .AddDefaultCleoOptions();

            return JsonSerializer.Serialize(obj, options);
        }

        private static readonly JsonSerializerOptions _defaultDeserializerOptions = new JsonSerializerOptions()
        {
            PropertyNameCaseInsensitive = true,
        }.AddDefaultCleoOptions();

        public static T DeserializeJson<T>(string json)
        {
            return JsonSerializer.Deserialize<T>(json, _defaultDeserializerOptions);
        }
    }
}
