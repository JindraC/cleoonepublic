﻿// <copyright file="IgnoreNullJsonSerializationExtensions.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using System.Text.Json;

namespace CleoOne.Common.Core.DotNet.JsonText.CustomSerialization
{
    public static class IgnoreNullJsonSerializationExtensions
    {
        public static void AllowIgnoreNullJsonSerialization(this IServiceCollection services)
        {
            services.PostConfigure<MvcOptions>(opts =>
            {
                var options = new JsonSerializerOptions()
                {
                    IgnoreNullValues = true,
                }.AddDefaultCleoOptions();

                opts.OutputFormatters.Insert(0, new IgnoreNullJsonSerialization(options));
            });
        }
    }
}
