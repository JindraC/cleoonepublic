﻿// <copyright file="IgnoreNullJsonSerialization.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace CleoOne.Common.Core.DotNet.JsonText.CustomSerialization
{
    public class IgnoreNullJsonSerialization : TextOutputFormatter
    {
        private readonly SystemTextJsonOutputFormatter _formatter;

        public IgnoreNullJsonSerialization(JsonSerializerOptions jsonSerializerOptions)
        {
            _formatter = new SystemTextJsonOutputFormatter(jsonSerializerOptions);

            SupportedEncodings.Add(Encoding.UTF8);
            SupportedEncodings.Add(Encoding.Unicode);
            SupportedMediaTypes.Add("application/json");
            SupportedMediaTypes.Add("text/json");
        }

        public override async Task WriteResponseBodyAsync(OutputFormatterWriteContext context, Encoding selectedEncoding)
        {
            var mvcOpt = context.HttpContext.RequestServices.GetRequiredService<IOptions<MvcOptions>>().Value;
            var formatters = mvcOpt.OutputFormatters;
            TextOutputFormatter formatter;

            Endpoint endpoint = context.HttpContext.GetEndpoint();

            if (endpoint.Metadata.GetMetadata<IgnoreNullJsonSerializationAttribute>() != null)
            {
                formatter = _formatter;
            }
            else
            {
                formatter = formatters.OfType<SystemTextJsonOutputFormatter>().First();
            }

            await formatter.WriteResponseBodyAsync(context, selectedEncoding);
        }
    }
}
