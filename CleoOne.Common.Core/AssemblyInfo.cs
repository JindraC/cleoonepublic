﻿// <copyright file="AssemblyInfo.cs" company="CLEO Finance Ltd.">
// Copyright (c) CLEO Finance Ltd.. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("CleoOne.Common.Core.Tests")]
